### Preparation ###

* install Node JS
* install bower via npm : npm install bower -g
* install grunt via npm : npm install grunt -g
* install http-server via npm : npm install http-server -g



### How To ###

* go to project dorectory
* run command: npm install
* run command: bower install
* run command: grunt clean build
* go to folder dist in project dorectory
* run command: http-server -p 8080