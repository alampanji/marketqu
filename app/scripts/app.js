'use strict';
/**
 * @ngdoc overview
 * @name marketplaceApp
 * @description
 * # marketplaceApp
 *
 * Main module of the application.
 */
var app = angular
    .module('marketplaceApp', [
        'oc.lazyLoad',
        'ui.router',
        'ui.bootstrap',
        'ui.bootstrap.modal',
        'ui.bootstrap.tabs',
        'angular-loading-bar',
        'LocalStorageModule',
        'ngSanitize',
        'ngStorage',
        'angular-flexslider'
    ])
    .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {

        $ocLazyLoadProvider.config({
            debug: false,
            events: true,
        });

        $urlRouterProvider.otherwise('/user/home');

        $stateProvider

            .state('install', {
            url: '/install',
            controller: 'CoreCtrl',
            templateUrl: 'views/install/main.html',
            resolve: {
                loadMyDirectives: function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                            name: 'marketplaceApp',
                            files: [
                                'styles/customize.css',
                                'scripts/directives/user/header-user.js'
                            ]
                        }),
                        $ocLazyLoad.load({
                            name: 'toggle-switch',
                            files: ["bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                                "bower_components/angular-toggle-switch/angular-toggle-switch.css"
                            ]
                        }),
                        $ocLazyLoad.load({
                            name: 'ngAnimate',
                            files: ['bower_components/angular-animate/angular-animate.js']
                        }),
                        $ocLazyLoad.load({
                            name: 'ngCookies',
                            files: ['bower_components/angular-cookies/angular-cookies.js']
                        }),
                        $ocLazyLoad.load({
                            name: 'ngResource',
                            files: ['bower_components/angular-resource/angular-resource.js']
                        }),
                        $ocLazyLoad.load({
                            name: 'ngSanitize',
                            files: ['bower_components/angular-sanitize/angular-sanitize.js']
                        })
                }
            }
        })

        .state('install.install', {
                templateUrl: 'views/install/Install.html',
                url: '/database',
                controller: 'InstallCtrl'
            })
            .state('install.settingAccount', {
                templateUrl: 'views/install/SettingAccount.html',
                url: '/setting-account',
                controller: 'SettingAccountCtrl'
            })

        .state('user', {
            url: '/user',
            controller: 'CoreCtrl',
            templateUrl: 'views/user/main.html',
            resolve: {
                loadMyDirectives: function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                            name: 'marketplaceApp',
                            files: [
                                'styles/customize.css',
                                'scripts/directives/user/header-user.js'
                            ]
                        }),
                        $ocLazyLoad.load({
                            name: 'toggle-switch',
                            files: ["bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                                "bower_components/angular-toggle-switch/angular-toggle-switch.css"
                            ]
                        }),
                        $ocLazyLoad.load({
                            name: 'ngAnimate',
                            files: ['bower_components/angular-animate/angular-animate.js']
                        }),
                        $ocLazyLoad.load({
                            name: 'ngCookies',
                            files: ['bower_components/angular-cookies/angular-cookies.js']
                        }),
                        $ocLazyLoad.load({
                            name: 'ngResource',
                            files: ['bower_components/angular-resource/angular-resource.js']
                        }),
                        $ocLazyLoad.load({
                            name: 'ngSanitize',
                            files: ['bower_components/angular-sanitize/angular-sanitize.js']
                        })
                }
            }
        })

        // .state('user.install', {
        //     templateUrl: 'views/install/Install.html',
        //     url: '/install',
        //     controller: 'InstallCtrl'
        // })
        // .state('user.settingAccount', {
        //     templateUrl: 'views/install/SettingAccount.html',
        //     url: '/setting-account',
        //     controller: 'SettingAccountCtrl'
        // })
        .state('user.login', {
                templateUrl: 'views/auth/Login.html',
                url: '/login',
                controller: 'LoginCtrl'
            })
            .state('user.home', {
                templateUrl: 'views/user/Home.html',
                url: '/home',
                controller: 'HomeCtrl'
            })
            // .state('user.product', {
            //     templateUrl: 'views/user/Product.html',
            //     url: '/product',
            //     controller: 'UserProductCtrl'
            // })

            .state('product', {
                templateUrl: 'views/user/Product.html',
                url: '/product',
                controller: 'UserProductCtrl',
                resolve: {
                    loadMyDirectives: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'marketplaceApp',
                            files: [
                                'styles/customize.css',
                                'styles/style.less'
                            ]
                        })
                    }
                }
            })
            .state('productDetail', {
                templateUrl: 'views/user/ProductDetail.html',
                url: '/product-detail',
                controller: 'UserProductDetailCtrl'
            })
            .state('checkout', {
                templateUrl: 'views/user/CheckOut.html',
                url: '/checkout',
                controller: 'CheckOutCtrl',
                resolve: {
                    loadMyDirectives: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'marketplaceApp',
                            files: [
                                'styles/customize.css'
                            ]
                        })
                    }
                }
            })
            .state('paymentMethod', {
                templateUrl: 'views/user/PaymentMethod.html',
                url: '/payment-method',
                controller: 'PaymentMethodCtrl',
                resolve: {
                    loadMyDirectives: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'marketplaceApp',
                            files: [
                                'styles/customize.css'
                            ]
                        })
                    }
                }
            })
            .state('forgot', {
                url: '/forgot-password',
                controller: 'ForgotPassCtrl',
                templateUrl: 'views/auth/ForgotPassword.html',
                resolve: {
                    loadMyDirectives: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'marketplaceApp',
                            files: [
                                'styles/customize.css'
                            ]
                        })
                    }
                }
            })
            // .state('search', {
            //     templateUrl: 'views/news/search-billboard.html',
            //     url: '/search',
            //     controller: 'searchBillboardCtrl'
            // })
            // .state('activateAccount', {
            //     templateUrl: 'views/auth/ActivateAccount.html',
            //     url: '/activatePage',
            //     controller: 'ActivateAccountCtrl'
            // })

        .state('admin', {
                url: '/admin',
                controller: 'CoreCtrl',
                templateUrl: 'views/dashboard/main.html',
                resolve: {
                    loadMyDirectives: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                                name: 'marketplaceApp',
                                files: [

                                    // 'styles/customize.css',
                                    // 'styles/style.less',
                                    'scripts/directives/header/header-notification/header-notification.js',
                                    'scripts/directives/sidebar/sidebar.js',
                                    '../assets/css/font-awesome.css',
                                    '../assets/css/custom-styles.css',
                                    '../assets/js/bootstrap-notify.js',
                                    '../assets/css/custom.css',
                                    '../styles/main.css'
                                ]
                            }),
                            $ocLazyLoad.load({
                                name: 'toggle-switch',
                                files: ["bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                                    "bower_components/angular-toggle-switch/angular-toggle-switch.css"
                                ]
                            }),
                            $ocLazyLoad.load({
                                name: 'ngAnimate',
                                files: ['bower_components/angular-animate/angular-animate.js']
                            }),
                            $ocLazyLoad.load({
                                name: 'ngCookies',
                                files: ['bower_components/angular-cookies/angular-cookies.js']
                            }),
                            $ocLazyLoad.load({
                                name: 'ngResource',
                                files: ['bower_components/angular-resource/angular-resource.js']
                            }),
                            $ocLazyLoad.load({
                                name: 'ngSanitize',
                                files: ['bower_components/angular-sanitize/angular-sanitize.js']
                            })
                    }
                }
            })
            .state('admin.home', {
                templateUrl: 'views/dashboard/home.html',
                url: '/home',
                controller: 'MainCtrl'
            })
            .state('admin.profile', {
                templateUrl: 'views/dashboard/Profile.html',
                url: '/profile',
                controller: 'ProfileCtrl'
            })
            .state('admin.produsen', {
                templateUrl: 'views/dashboard/Produsen.html',
                url: '/produsen',
                controller: 'ProdusenCtrl'
            })
            .state('admin.createProdusen', {
                templateUrl: 'views/dashboard/CreateProdusen.html',
                url: '/create-produsen',
                controller: 'CreateProdusenCtrl'
            })
            .state('admin.updateProdusen', {
                templateUrl: 'views/dashboard/UpdateProdusen.html',
                url: '/update-produsen',
                controller: 'UpdateProdusenCtrl'
            })
            .state('admin.reseller', {
                templateUrl: 'views/dashboard/Reseller.html',
                url: '/reseller',
                controller: 'ResellerCtrl'
            })
            .state('admin.createReseller', {
                templateUrl: 'views/dashboard/CreateReseller.html',
                url: '/create-reseller',
                controller: 'CreateResellerCtrl'
            })
            .state('admin.updateReseller', {
                templateUrl: 'views/dashboard/UpdateReseller.html',
                url: '/update-reseller',
                controller: 'UpdateResellerCtrl'
            })
            .state('admin.group', {
                templateUrl: 'views/dashboard/Group.html',
                url: '/group',
                controller: 'GroupCtrl'
            })
            .state('admin.createPrivilege', {
                templateUrl: 'views/dashboard/CreatePrivilege.html',
                url: '/create-privilege',
                controller: 'CreatePrivilegeCtrl'
            })
            .state('admin.product', {
                templateUrl: 'views/dashboard/Product.html',
                url: '/product',
                controller: 'AdminProductCtrl'
            })
            .state('admin.createProduct', {
                templateUrl: 'views/dashboard/CreateProduct.html',
                url: '/create-product',
                controller: 'CreateProductCtrl'
            })
            .state('admin.updateProduct', {
                templateUrl: 'views/dashboard/UpdateProduct.html',
                url: '/update-product',
                controller: 'UpdateProductCtrl'
            })
            .state('admin.complainAdvice', {
                templateUrl: 'views/dashboard/ComplainAdvice.html',
                url: '/complain-advice',
                controller: 'ComplainAdviceCtrl'
            })
            .state('admin.detailComplainAdvice', {
                templateUrl: 'views/dashboard/DetailComplainAdvice.html',
                url: '/detail-complain-advice',
                controller: 'DetailComplainAdviceCtrl'
            })
            .state('admin.bank', {
            templateUrl: 'views/dashboard/Bank.html',
            url: '/bank',
            controller: 'BankCtrl'
            })
            .state('admin.category', {
                templateUrl: 'views/dashboard/Category.html',
                url: '/category',
                controller: 'AdminCategoryCtrl'
            })
            .state('admin.createCategory', {
                templateUrl: 'views/dashboard/CreateCategory.html',
                url: '/create-category',
                controller: 'CreateCategoryCtrl'
            })
            .state('admin.updateCategory', {
                templateUrl: 'views/dashboard/UpdateCategory.html',
                url: '/update-category',
                controller: 'UpdateCategoryCtrl'
            })
            .state('admin.transaction', {
                templateUrl: 'views/dashboard/Transaction.html',
                url: '/transaction',
                controller: 'TransactionCtrl'
            })
            .state('admin.transactionDetail', {
                templateUrl: 'views/dashboard/TransactionDetail.html',
                url: '/transaction-detail',
                controller: 'TransactionDetailCtrl'
            })
            .state('admin.marketing', {
                templateUrl: 'views/dashboard/Marketing.html',
                url: '/marketing',
                controller: 'MarketingCtrl'
            })
            .state('admin.createMarketing', {
                templateUrl: 'views/dashboard/CreateMarketing.html',
                url: '/create-marketing',
                controller: 'CreateMarketingCtrl'
            })
            .state('admin.updateMarketing', {
                templateUrl: 'views/dashboard/UpdateMarketing.html',
                url: '/update-marketing',
                controller: 'UpdateMarketingCtrl'
            })
            .state('admin.detailGroupMarketing', {
                templateUrl: 'views/dashboard/DetailGroupMarketing.html',
                url: '/detail-group-marketing',
                controller: 'DetailGroupMarketingCtrl'
            })
            .state('admin.createUserGroupMarketing', {
                templateUrl: 'views/dashboard/CreateUserGroupMarketing.html',
                url: '/create-user-group-marketing',
                controller: 'CreateUserGroupMarketingCtrl'
            })
            .state('admin.assignItem', {
                templateUrl: 'views/dashboard/AssignItem.html',
                url: '/assign-item',
                controller: 'AssignItemCtrl'
            })
            .state('admin.transactionFeedback', {
                templateUrl: 'views/dashboard/FeedbackReseller.html',
                url: '/transaction-feedback',
                controller: 'TransactionFeedbackCtrl'
            })
            .state('admin.feedbackProdusen', {
                templateUrl: 'views/dashboard/FeedbackProdusen.html',
                url: '/produsen-feedback',
                controller: 'FeedbackProdusenCtrl'
            })
            .state('admin.feedbackProdusenDetail', {
                templateUrl: 'views/dashboard/FeedbackProdusenDetail.html',
                url: '/detail-feedback-produsen',
                controller: 'DetailFeedbackDetailCtrl'
            })
            .state('admin.viewFeedbackReseller', {
                templateUrl: 'views/dashboard/ViewFeedbackReseller.html',
                url: '/view-feedback-reseller',
                controller: 'ViewFeedbackResellerCtrl'
            })
            .state('admin.reportSales', {
                templateUrl: 'views/dashboard/ReportSales.html',
                url: '/sales-report',
                controller: 'ReportSalesCtrl'
            })
            // .state('admin.favorite', {
            //     templateUrl: 'views/dashboard/Favorite.html',
            //     url: '/favorite',
            //     controller: 'FavoriteCtrl'
            // })

    }]);

app.config(['localStorageServiceProvider', function(localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('ls');
}]);

app.run(['$rootScope', '$location', 'RouteFilter', function($rootScope, $location, RouteFilter) {
    $rootScope.$on('$locationChangeStart', function() {
        RouteFilter.run($location.path());
    })
}]);

app.run(['RouteFilter', 'authService', 'navigationMenuService', function(RouteFilter, authService, navigationMenuService) {
    var menus = navigationMenuService.loadAllPrivateMenus();
    angular.forEach(menus, function(menu) {
        angular.forEach(menu.detail, function(submenu) {
            RouteFilter.register(submenu.name, [submenu.route], function() {
                return authService.isAuthenticated();
            }, '/');
        });
    });
    
    var otherMenus = navigationMenuService.loadOtherPrivateMenu();
    angular.forEach(otherMenus, function(menu) {
        RouteFilter.register(menu.name, [menu.route], function() {
            return authService.isAuthenticated();
        }, '/');
    });
    
}]);
app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('myHttpInterceptor');
}]);