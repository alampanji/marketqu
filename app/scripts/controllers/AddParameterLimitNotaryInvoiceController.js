'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('nostraApp')
    .controller('addParameterLimitNotaryInvoiceCtrl', [
        '$scope', 'BiayaResumeService', '$location', '$modal', '$modalInstance', 'modalParam', 'GroupService', 'ParameterLimitNotaryInvoiceService',
        addParameterLimitNotaryInvoiceCtrl
    ]);

function addParameterLimitNotaryInvoiceCtrl($scope, BiayaResumeService, $location, $modal, $modalInstance, modalParam,GroupService,ParameterLimitNotaryInvoiceService) {

    var self = $scope;
    self.close = close;

    var modelpassing = modalParam;
    self.userId = modelpassing.id;
    self.version = modelpassing.version;
    self.name = modelpassing.name;
    self.teacherVersion = modelpassing.version;
    self.input = {};
    function close() {
        $modalInstance.dismiss('close');
    };

    self.isDisabled = false;

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    };

    self.addNotary = function () {
        console.log(notaris);
        $modalInstance.close(notaris);
    };
    self.currentPage = 1;
    self.limit = 100;
    var listGroupPrivilege = function () {
        GroupService.getListGroup(self.currentPage-1, self.limit).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.privileges = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                    for(var i=0; i<self.privileges.length; i++){
                        self.privileges[i].value = false;
                    }
                    // checkExistValue();
                }
            });
    };

    self.setPrevilage = function (item) {
        // console.log(item)
        self.input.groupId = item.id;
    }
    self.doDeleteUser = function () {
        console.log(self.input)
        self.isDisabled = true;
        // self.input.effectiveDate = Date.parse(self.input.effectiveDate);
        ParameterLimitNotaryInvoiceService.createParameterLimitNotaryInvoice(self.input).then(
            function (response) {
                $modalInstance.close('close');
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Success',
                                    message: 'Sukses Add New Parameter Setting'
                                };
                            }

                        }

                    });
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    }

listGroupPrivilege();
};