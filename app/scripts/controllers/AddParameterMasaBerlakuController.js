'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('nostraApp')
    .controller('addParameterMasaBerlakuCtrl', [
        '$scope', 'MasaBerlakuService', '$location', '$modal', '$modalInstance', 'modalParam',
        addParameterMasaBerlakuCtrl
    ]);

function addParameterMasaBerlakuCtrl($scope, MasaBerlakuService, $location, $modal, $modalInstance, modalParam) {

    var self = $scope;
    self.close = close;

    var modelpassing = modalParam;
    self.userId = modelpassing.id;
    self.version = modelpassing.version;
    self.name = modelpassing.name;
    self.teacherVersion = modelpassing.version;

    function close() {
        $modalInstance.dismiss('close');
    };

    self.isDisabled = false;

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    };

    self.addNotary = function () {
        console.log(notaris);
        $modalInstance.close(notaris);
    };
    self.doDeleteUser = function () {
        self.isDisabled = true;
        self.input.effectiveDate = Date.parse(self.input.effectiveDate);
        MasaBerlakuService.createMasaBerlaku(self.input).then(
            function (response) {
                $modalInstance.close('close');
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Success',
                                    message: 'Data Baru Telah Dibuat!'
                                };
                            }

                        }

                    });
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    }


};