'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('approvalInvoiceCtrl', [
        '$scope', '$modal', '$window',
        '$location',
        'ResumeService',
        'InvoiceService',
        'ParameterNotaryService',
        approvalInvoiceCtrl
    ]);

function approvalInvoiceCtrl($scope, $modal, $window, $location, ResumeService, InvoiceService, ParameterNotaryService) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.arr = [];

    self.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    };

    self.search = [
        {"keyName" : "appId", "value":"APP ID"},
        {"keyName" : "npwp", "value":"NPWP"},
        {"keyName" : "companyName", "value":"Nama Badan Usaha"}
    ];
    self.otherDocument = false;

    self.retrieveAppId = function () {
        ResumeService.getRetrieveResumeApp(self.contentData.appId).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataUser = response.result;
                    self.contentData.companyName = self.dataUser.companyName;
                    self.contentData.npwp = self.dataUser.npwp;
                }
            });
    };

    var getListOfApprovalInvoice = function (invoice, notary,receiveDate) {
        InvoiceService.getlistOfApproveInvoice(self.currentPage - 1,self.limit,invoice,notary,receiveDate).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.listApproveInvoice = response.result;
                }
            });
    };

    self.goToDetail = function(id){
        $location.path('/dashboard/approval-invoice-detail').search({id: id});
    }
    self.doAddOtherFile = function (bool) {
        if(bool == undefined){
            return self.otherDocument = true;
        }
        else{
            return self.otherDocument = false;
        }

    };


    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah user?',
                        path: 'dashboard/security-admin'
                    };
                }
            }
        });

    };

    var checking = function () {
        self.nipInvalid = false;
        self.userNameInvalid = false;
        self.confirmationPasswordInvalid = false;
        self.fullNameInvalid = false;
        self.divisionInvalid = false;
        self.positionInvalid = false;
        self.emailInvalid = false;
        self.branchInvalid = false;
        self.centralBranchInvalid = false;
        self.groupInvalid = false;

        var bool = false;

        if (self.contentData.username == null) {
            self.userNameInvalid = true;
            bool = true;
        }
        if(self.contentData.nip == null){
            self.nipInvalid = true;
            bool = true;
        }
        if(self.contentData.fullName == null){
            self.fullNameInvalid = true;
            bool = true;
        }
        if(self.contentData.division == null){
            self.divisionInvalid = true;
            bool = true;
        }
        if(self.contentData.position == null){
            self.positionInvalid = true;
            bool = true;
        }
        if(self.contentData.centralBranch == null){
            self.centralBranchInvalid = true;
            bool = true;
        }
        if (self.contentData.group == null){
            self.groupInvalid = true;
            bool = true;
        }
        return bool;
    };

    self.kirimIndex = function (index) {
        console.log(index);
    }

    self.addFile = function (ele,index) {
        var files = ele.files[0];
        self.listDocument[index].file = files;
        console.log(self.listDocument);
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    function getListNotary() {
        backToTop();
        ParameterNotaryService.getAllNotaryInvoiceName().then(
            function (response) {
                if (response.message === 'OK') {
                    var notaries = response.result;
                    var constructNotary = [{"id":"", "version":"","name":"Silahkan Pilih"}];
                    angular.forEach(notaries, function(notary){
                        constructNotary.push(notary);
                    });
                    self.notaries = constructNotary;
                    self.itemNotary = self.notaries[0];

                    if(self.notaries[0]){
                        self.getListInvoice(0);
                    }
                }
            }
        );
    };



    self.getListInvoice = function(notary) {
        InvoiceService.getlistOfInvoiceApprovalByNotaryId(notary.id).then(
            function (response) {
                if (response.message === 'OK') {
                    self.invoices = response.result;
                    self.itemInvoice = self.invoices[0];
                }
            }
        );
    };



    $scope.fileInputs = [1,2,3];
    self.uploadFile = function () {

        var file = self.myFile;
        var file2 = self.myFile2;

        var fd = new FormData();
        fd.append('file1', file);
        fd.append('file2', file2);
        fd.append('contentData', angular.toJson(self.contentData));


        ResumeService.submitResume(fd).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    var result = response.result;
                    if(result.match==true){
                        $location.path('/dashboard/matching-result').search({id: result.resumeId});
                    }
                    else{
                        var request = {};
                        for(var i=0; i<self.listDocument.length; i++){
                            var fd = new FormData();
                            request.resumeId = result.resumeId;
                            request.documentCode = self.listDocument[i].code;
                            request.documentDesc = self.listDocument[i].name;
                            var extn = self.listDocument[i].file.name.split(".").pop();
                            extn = extn.toUpperCase();
                            console.log(self.listDocument[i]);
                            console.log(extn);
                        }
                    }
                    // $modal.open({
                    //     templateUrl: '/views/modal/Modal.html',
                    //     controller: 'ModalCtrl',
                    //     size: 'sm',
                    //     resolve: {
                    //         modalParam: function () {
                    //             return {
                    //                 title: 'Informasi',
                    //                 message: 'User berhasil disubmit',
                    //                 path: ''
                    //             };
                    //         }
                    //     }
                    // });
                }else{
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );

        // var hasil = checking();
        //
        //
        // if (hasil == true) {
        //     invalidModal("Ada yang tidak valid, mohon periksa kembali");
        // }else {
        //
        //     for(var i=0; i<self.contentData.branch.length; i++){
        //         var listId = {
        //             'code' : self.contentData.branch[i].code,
        //             'name'  : self.contentData.branch[i].name
        //         };
        //         request.branchList.push(listId);
        //     }
        //
        //     for(var i=0; i<self.contentData.centralBranch.length; i++){
        //         var listId = {
        //             'id' : self.contentData.centralBranch[i].id
        //         }
        //         request.branchCentralList.push(listId);
        //     }
        //
        //     for(var i=0; i<self.contentData.group.length; i++){
        //         var listId = {
        //             'id' : self.contentData.group[i].id
        //         }
        //         request.groupList.push(listId);
        //     }
        //
        //     request.nip = self.contentData.nip;
        //     request.fullName = self.contentData.fullName;
        //     request.username = self.contentData.username;
        //     request.email = self.contentData.email;
        //     request.division = self.contentData.division;
        //     request.position = self.contentData.position;
        //     request.firstName = "First Name";
        //     request.lastName = "Last Name";
        //     request.role = "BCA";
        //
        //     console.log(request);
        //
        //     UserService.createUserInternal(request).then(
        //         function (response) {
        //             if (response.message !== 'ERROR') {
        //                 $modal.open({
        //                     templateUrl: 'views/modal/Modal.html',
        //                     controller: 'ModalCtrl',
        //                     size: 'sm',
        //                     resolve: {
        //                         modalParam: function () {
        //                             return {
        //                                 title: 'Informasi',
        //                                 message: 'User berhasil ditambahkan',
        //                                 path: 'dashboard/security-admin'
        //                             };
        //                         }
        //                     }
        //                 });
        //             }else{
        //                 self.isDisabled = false;
        //                 $modal.open({
        //                     templateUrl: 'views/modal/Modal.html',
        //                     controller: 'ModalCtrl',
        //                     size: 'sm',
        //                     resolve: {
        //                         modalParam: function () {
        //                             return {
        //                                 title: 'Gagal',
        //                                 message: response.result,
        //                                 path: ''
        //                             };
        //                         }
        //                     }
        //                 });
        //             }
        //
        //         }
        //     );
        // }
    };

    self.doSearch = function (itemNotary,itemInvoice, receiveDate) {
        self.currentPage = 1;
        getListOfApprovalInvoice(itemNotary.id,itemInvoice.id,receiveDate);
    };


    self.doSearch = function (invoice, notary, date) {
        self.currentPage = 1;
        if(date!=undefined && date!=null){
            var receiveDate = Date.parse(date);
        }
        // console.log(invoice.id);
        // console.log(notary.id);
        // console.log(receiveDate);

        getListOfApprovalInvoice(invoice.id,notary.id,receiveDate);

    };

    getListNotary();

};
