'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('approvalInvoiceDetailCtrl', [
        '$scope', '$modal', '$window',
        '$location',
        'InvoiceService',
        approvalInvoiceDetailCtrl
    ]);

function approvalInvoiceDetailCtrl($scope, $modal, $window, $location, InvoiceService) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    var paramValue = $location.search().id;

    var getDetailResume = function () {
        backToTop();
        InvoiceService.getDetailApproveInvoice(paramValue).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;
                }
            });
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    self.method = [
        {"keyName" : "input", "value":"Input"},
        {"keyName" : "upload", "value":"Upload"},
    ];

    var constructSelect =  function () {
        var indexFound = self.method.map(function (e) {
            return e.keyName;
        }).indexOf(self.method);
        self.itemSearch = self.method[0];
    };

    constructSelect();


    self.approve = function () {
        var request = {
            id: self.dataResult.id,
            version : self.dataResult.version
        };

        InvoiceService.approvalInvoiceProcess(request).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Sukses',
                                    message: 'Invoice Sukses di Approve',
                                    path: ''
                                };
                            }
                        }
                    });

                    $location.path('/dashboard/approval-invoice');
                }
                else {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            });
    };

    self.reject = function () {
        var request = {
            id: self.dataResult.id,
            version : self.dataResult.version
        };
        InvoiceService.rejectInvoiceProcess(request).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Sukses',
                                    message: 'Invoice Sukses di Reject',
                                    path: ''
                                };
                            }
                        }
                    });

                    $location.path('/dashboard/approval-invoice');

                }
                else {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            });
    };

    getDetailResume();

};
