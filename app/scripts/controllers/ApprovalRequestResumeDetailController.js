'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('approvalRequestResumeDetailCtrl', [
        '$scope','CONF','$location','$modal','ResumeService','$sessionStorage','$window',
        approvalRequestResumeDetailCtrl
    ]);

function approvalRequestResumeDetailCtrl($scope,CONF,$location,$modal,ResumeService, $sessionStorage,$window) {
    var self = $scope;

    self.close = close;

    var paramId = $location.search().id;
    function close() {
        $modalInstance.dismiss('close');
    }
    self.goToDetail = function(id){
        $location.path('/dashboard/approval-request-resume-detail').search({id:id});

        // $location.path('/dashboard/matching-result').search({id: id});
    }
    self.createUser = function(){
        $location.path('/dashboard/create-user');
    };

    self.doUpdateUser = function(){
        $location.path('/dashboard/update-user');
    };

    var nip = '';
    var name = '';

    self.search = [
        { "type":"nip", "label": "NIP"},
        { "type":"name", "label": "Nama"},
    ];
    
    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.status = [
        {keyName: 'true', name: 'Aktif'},
        {keyName: 'false', name: 'Tidak Aktif'}
    ];

    self.pageChanged = function () {
        getListUser();
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var getListUser = function (paramId) {
        backToTop();
        // if(type!=undefined && value!=undefined){
        //     if(type=='NIP'){
        //         name='';
        //         nip=value;
        //     }
        //     else if(type=='Nama'){
        //         name = value;
        //         nip='';
        //     }
        // }
        ResumeService.getListRequestResumeDetail(paramId).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.users = response.result;
                    var access_token = $sessionStorage.accessToken;
                    
                        for (var j=0; j< self.users.documentList.length;j++){

                        var urlConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_DOCUMENT +'?access_token='+access_token+'&resumeId='+self.users.id+'&docType='+self.users.documentList[j].documentCode;
                       
                        self.users.documentList[j].url = urlConstruct
                        }

                        var urlAhuConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_RESUME +'?access_token='+access_token+'&resumeId='+self.users.id;
                        self.users.resumeAhuUrl = urlAhuConstruct;


                        var urlnpwpConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_NPWP +'?access_token='+access_token+'&resumeId='+self.users.id;
                        self.users.npwpUrl = urlnpwpConstruct;
                }
            });
    };
    self.reject = function () {
        var request = {
            id: self.users.id,
            version : self.users.version,
            description : self.users.description
        };
        ResumeService.rejectRequestResume(request).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Sukses',
                                    message: 'Resume Sukses di Reject',
                                    path: ''
                                };
                            }
                        }
                    });

        $location.path('/dashboard/approval-request-resume');
                    // self.bigTotalItems = response.elements;
                    // self.numPages = response.pages;
                }
                else {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            });
    };
    self.doRetrieve = function(input){
        $location.path('/dashboard/matching-result').search({id: input});
    }
    self.approve = function () {
        var request = {
            id: self.users.id,
            version : self.users.version ,
            description : self.users.description
        };
        ResumeService.approveRequestResume(request).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Sukses',
                                    message: 'Resume Sukses di Approve',
                                    path: ''
                                };
                            }
                        }
                    });

                    $location.path('/dashboard/approval-request-resume');
                    // self.bigTotalItems = response.elements;
                    // self.numPages = response.pages;
                }
                else {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            });
    };

    self.doUpdateUser = function (id) {
        $location.path('/dashboard/update-user').search({id: id});
    };

    self.doDeleteUser = function (id,name) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteUser.html',
            controller: 'deleteUserInternalCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id,
                        name:name,
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.users.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListUser();
            }
        });
    };

    self.doSearch = function (type,value) {
        self.currentPage = 1;
        getListUser(type,value);
    };

    getListUser(paramId);
};