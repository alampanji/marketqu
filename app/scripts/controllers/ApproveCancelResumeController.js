'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('approveCancelResumeCtrl', [
        '$scope', '$modal', '$window',
        'ResumeService',
        approveCancelResumeCtrl
    ]);

function approveCancelResumeCtrl($scope, $modal, $window, ResumeService) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    self.currentPage = 1;
    self.numPages = 1;

    var request = {
        "resumeCancelVOList" : []
    };

    self.search = [
        {"keyName" : "appId", "value":"APP ID"},
        {"keyName" : "npwp", "value":"NPWP"},
        {"keyName" : "companyName", "value":"Nama Perusahaan"}
    ];

    self.doCancelRequest = function(id,version){
        var modalHapus = $modal.open({
            templateUrl: 'views/modal/ModalRejectResume.html',
            controller: 'ModalRejectApproveCtrl',
            size: 'md',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id,
                        version : version
                    };
                }
            }
        });
        modalHapus.result.then(function () {
            getListOfApproveCancelResume();
        })
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var
        getListOfApproveCancelResume = function (type,value) {

        backToTop();

        var appId = '';
        var npwp = '';
        var companyName='';
        if(type && value != undefined){
            if(type=='appId'){
                appId = value;
                npwp='';
                companyName='';
            } else if(type=='npwp'){
                npwp = value;
                appId = '';
                companyName = '';
            }
            else if(type=='companyName'){
                companyName = value;
                appId = '';
                npwp = '';
            }

        }

        ResumeService.getCancelResumeList(self.currentPage,self.numPages,appId,npwp,companyName).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.listRequestResumes = response.result;
                    var indexFound = self.search.map(function (e) {
                        return e.keyName;
                    }).indexOf(self.search);
                    self.itemSearch = self.search[0];
                }
            });
    };


    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah user?',
                        path: 'dashboard/security-admin'
                    };
                }
            }
        });

    };

    self.checkedAll = function() {
        if(self.privilege){
            for(var i=0; i<self.listRequestResumes.length; i++){
                self.listRequestResumes[i].value = false;
                self.privilege = false;
            }

        }
        else{
            for(var i=0; i<self.listRequestResumes.length; i++) {
                self.listRequestResumes[i].value = true;
                self.privilege = true;
            }

        }
    };

    self.checkedChild = function (value){
        var check = true;
        if(!value){
            self.privilege = false;
        }
        else{
            for(var i=0; i<self.listRequestResumes.length; i++) {
                if(!self.listRequestResumes[i].value){
                    check = false;
                }

            }
            self.privilege=check;
        }

        console.log(self.listRequestResumes);

    };

    self.doApprove = function () {
        for(var i=0; i<self.listRequestResumes.length; i++){
            if(self.listRequestResumes[i].value==true){
                var listId = {
                    'id' : self.listRequestResumes[i].id,
                    'version': self.listRequestResumes[i].version
                };
                request.resumeCancelVOList.push(listId);
            }
        }
        ResumeService.cancelApproveResume(request).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Informasi',
                                    message: 'Berhasil cancel approve resume',
                                    path: 'dashboard/approve-cancel-resume'
                                };
                            }
                        }
                    });

                }else{
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    };

    var checking = function () {
        self.nipInvalid = false;
        self.userNameInvalid = false;
        self.confirmationPasswordInvalid = false;
        self.fullNameInvalid = false;
        self.divisionInvalid = false;
        self.positionInvalid = false;
        self.emailInvalid = false;
        self.branchInvalid = false;
        self.centralBranchInvalid = false;
        self.groupInvalid = false;

        var bool = false;

        if (self.contentData.username == null) {
            self.userNameInvalid = true;
            bool = true;
        }
        if(self.contentData.nip == null){
            self.nipInvalid = true;
            bool = true;
        }
        if(self.contentData.fullName == null){
            self.fullNameInvalid = true;
            bool = true;
        }
        if(self.contentData.division == null){
            self.divisionInvalid = true;
            bool = true;
        }
        if(self.contentData.position == null){
            self.positionInvalid = true;
            bool = true;
        }
        if(self.contentData.centralBranch == null){
            self.centralBranchInvalid = true;
            bool = true;
        }
        if (self.contentData.group == null){
            self.groupInvalid = true;
            bool = true;
        }
        return bool;
    };

    self.createUser = function () {
        var hasil = checking();


        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }else {

            for(var i=0; i<self.contentData.branch.length; i++){
                var listId = {
                    'code' : self.contentData.branch[i].code,
                    'name'  : self.contentData.branch[i].name
                };
                request.branchList.push(listId);
            }

            for(var i=0; i<self.contentData.centralBranch.length; i++){
                var listId = {
                    'id' : self.contentData.centralBranch[i].id
                }
                request.branchCentralList.push(listId);
            }

            for(var i=0; i<self.contentData.group.length; i++){
                var listId = {
                    'id' : self.contentData.group[i].id
                };
                request.groupList.push(listId);
            }

            request.nip = self.contentData.nip;
            request.fullName = self.contentData.fullName;
            request.username = self.contentData.username;
            request.email = self.contentData.email;
            request.division = self.contentData.division;
            request.position = self.contentData.position;
            request.firstName = "First Name";
            request.lastName = "Last Name";
            request.role = "BCA";

            console.log(request);

            UserService.createUserInternal(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'User berhasil ditambahkan',
                                        path: 'dashboard/security-admin'
                                    };
                                }
                            }
                        });
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        }
    };

    self.doSearch = function (type,value) {
        self.currentPage = 1;
        getListOfApproveCancelResume(type.keyName,value);
    };

    getListOfApproveCancelResume();

};
