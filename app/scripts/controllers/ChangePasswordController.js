'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('ChangePasswordCtrl' , [
        '$scope',
        'profilesService',
        'CONF',
        'messageService',
        '$modal',
        ChangePasswordCtrl
    ] );

        function ChangePasswordCtrl($scope, profilesService, CONF,messageService,$modal) {
        var self=$scope;

        // getProfile();
        self.Profile={};
        self.UpdateProfiles ={};
        self.disable = true;
        self.disableAdmin = true;
        self.changePasswordMethod ={};
        
        self.updateProfile = function(){
            self.disable = false;
        }
        self.updateProfileAdmin = function(){
            self.disable = false;
        }
        self.cancel = function(){
            self.disable = true;
            getProfile();
        }
        self.changePassword = function(){
            self.changePasswordMethod.oldPassword = self.oldpass;
            self.changePasswordMethod.newPassword = self.newpass;
            self.changePasswordMethod.confirmationPassword = self.confirmpass;
            if(self.changePasswordMethod.newPassword != self.changePasswordMethod.confirmationPassword){
                loadModal("Password baru yang di input tidak sama dengan confirm password");
            }
            else{
                profilesService.changePassword(self.changePasswordMethod).then(
                    function(response){
                        if (response.message !=='OK') {

                            self.isDisabled = false;
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Gagal',
                                            message: response.result,
                                            path: ''
                                        };
                                    }
                                }
                            });


                        }else{
                            // messageService.toasterMessage(CONF.TOASTER_TOP_CENTER,CONF.TOASTER_SUCCESS,"Password Success Updated");
                            self.disable = true;
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Informasi',
                                            message: 'Berhasil Mengubah Password',
                                            path: '/dashboard'
                                        };
                                    }
                                }
                            });
                        }
                    })
            }

        };
        self.submitProfileAdmin = function(){
            self.UpdateProfiles.email = self.ProfileAdmin.email;
            self.UpdateProfiles.phoneNumber = self.ProfileAdmin.phoneNumber;
            self.UpdateProfiles.active = true;
            profilesService.changeProfileAdmin(self.UpdateProfiles).then(
                function(response){
                    if (response.message !=='OK') {
                        messageService.toasterMessage(CONF.TOASTER_TOP_CENTER,CONF.TOASTER_ERROR,"Failed update Profile, Please Try Again");
                    }else{
                        messageService.toasterMessage(CONF.TOASTER_TOP_CENTER,CONF.TOASTER_SUCCESS,"Profile Success Updated");
                    self.disable = true;
                    }
                })
        };

        var loadModal = function (message) {
            self.isDisabled = false;
            $modal.open({
                templateUrl: 'views/modal/Modal.html',
                controller: 'ModalCtrl',
                backdrop: 'static',
                size: 'sm',
                resolve: {
                    modalParam: function () {
                        return {
                            title: 'Peringatan',
                            message: message,
                            path: ''
                        };
                    }
                }
            });

        };



    };
