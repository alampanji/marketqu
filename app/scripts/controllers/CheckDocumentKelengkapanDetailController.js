'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('checkKelengkapanDocumentDetailCtrl', [
        '$scope','CONF','$location','$modal','ResumeService','$sessionStorage','$window','DokumenKelengkapanService',
        checkKelengkapanDocumentDetailCtrl
    ]);

function checkKelengkapanDocumentDetailCtrl($scope,CONF,$location,$modal,ResumeService, $sessionStorage,$window,DokumenKelengkapanService) {
    var self = $scope;

    self.close = close;
    // self.tester = [];
    var paramId = $location.search().id;
    function close() {
        $modalInstance.dismiss('close');
    }
    self.goToDetail = function(id){
        $location.path('/dashboard/approval-request-resume-detail').search({id:id});

        // $location.path('/dashboard/matching-result').search({id: id});
    }
    self.createUser = function(){
        $location.path('/dashboard/create-user');
    };

    self.tab = 1;

    self.setTab = function (tabId) {
        self.tab = tabId;
    };

    self.isSet = function (tabId) {
        return self.tab === tabId;
    };
    self.doUpdateUser = function(){
        $location.path('/dashboard/update-user');
    };

    var nip = '';
    var name = '';

    self.search = [
        { "type":"nip", "label": "NIP"},
        { "type":"name", "label": "Nama"},
    ];


    // self.changed = function (parents, parent, id) {
    //     angular.forEach(self.privileges.privilegeList[parents].subCategoryPrivilegeList[parent].privilegeList, function (listOfPrivileges) {
    //         if (listOfPrivileges.id != id) {
    //             listOfPrivileges.value = false;
    //         }
    //     });
    // };
    var getListOfDocumentSetting = function () {
        DokumenKelengkapanService.getListOfDocumentSetting().then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.listDocument = response.result;
                }
            });
    };


    self.doAddOtherFile = function (bool) {
        getListOfDocumentSetting();
        if(bool == undefined){
            return self.otherDocument = true;
        }
        else{
            return self.otherDocument = false;
        }

    };
    self.lovBranch = function(){

        if(self.users.documentList.length==self.listOfDocType.length){
            invalidModal("Tidak Ada Dokumen Yang Bisa Ditambahkan");
        }
        else{
            var modalInstance = $modal.open({
                templateUrl: 'views/modal/ModalAddDocument.html',
                controller: 'modalLovAddDocumentCtrl',
                size: 'md',
                resolve: {
                    modalParam: function () {
                        return {
                            values: self.users.documentList
                        };
                    }
                }
            });
            modalInstance.result.then(function (listBranch) {

                self.users.documentList = listBranch;
                console.log(self.users.documentList);
            });
        }

        
    };

    self.deleteDocument = function (index) {
        self.users.documentList.splice(index,1);

    };

        self.method = [
        {"keyName" : "input", "value":"Input"},
        {"keyName" : "upload", "value":"Upload"},
    ];

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.status = [
        {keyName: 'true', name: 'Aktif'},
        {keyName: 'false', name: 'Tidak Aktif'}
    ];

    self.pageChanged = function () {
        getListUser();
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var getListUser = function (paramId) {
        backToTop();
        DokumenKelengkapanService.getListCheckKelengkapanDokumenDetail(paramId).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.users = response.result;
                    var access_token = $sessionStorage.accessToken;
                    var urlConstructResume=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_RESUME +'?access_token='+access_token+'&resumeId='+self.users.id;
                       self.users.urlResume = urlConstructResume;
                        for (var j=0; j< self.users.documentList.length;j++){

                        var urlConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_DOCUMENT +'?access_token='+access_token+'&resumeId='+self.users.id+'&docType='+self.users.documentList[j].code;
                       
                        self.users.documentList[j].url = urlConstruct;
                        if(self.users.documentList[j].verified == null){
                            self.users.documentList[j].privileges = [
                                                                        {
                                                                        id: "1",
                                                                        version: 0,
                                                                        name: "yes",
                                                                        description: "Yes",
                                                                        value: false
                                                                        },
                                                                        {
                                                                        id: "0",
                                                                        version: 0,
                                                                        name: "no",
                                                                        description: "No",
                                                                        value: false
                                                                        }
                                                                    ];

                        }
                        }
                    DokumenKelengkapanService.getListOfDocumentSetting().then(
                        function (response) {
                            if (response.message !== 'ERROR') {
                                self.listOfDocType = response.result;
                            }
                        });
                }
            });
       
    };
    self.changeValue = function (id,index) {
        angular.forEach(self.users.documentList[index].privileges, function (notary) {
                if(notary.id != id){
                    notary.value = false;
                    if(id == 1){
                    self.users.documentList[index].verified = true;
                    }
                    else{
                        self.users.documentList[index].verified = false;
                    }
                }
        });

    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.reject = function () {
        var request = {
            id: self.users.id,
            version : self.users.version,
            description : self.users.description
        };
        ResumeService.rejectRequestResume(request).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: '/views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Sukses',
                                    message: 'Resume Sukses di Reject',
                                    path: ''
                                };
                            }
                        }
                    });

        $location.path('/dashboard/approval-request-resume');
                }
                else {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            });
    };
    self.doRetrieve = function(input){
        $location.path('/dashboard/matching-result').search({id: input});
    };
    self.approve = function () {
        var request = {
            id: self.users.id,
            version : self.users.version ,
            description : self.users.description
        };
        ResumeService.rejectRequestResume(request).then(
function (response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Sukses',
                                    message: 'Resume Sukses di Approve',
                                    path: ''
                                };
                            }
                        }
                    });

        $location.path('/dashboard/approval-request-resume');
                    // self.bigTotalItems = response.elements;
                    // self.numPages = response.pages;
                }
                else {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            });
    };

    self.doUpdateUser = function (id) {
        $location.path('/dashboard/update-user').search({id: id});
    };

    self.doDeleteUser = function (id,name) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteUser.html',
            controller: 'deleteUserInternalCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id,
                        name:name,
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.users.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListUser();
            }
        });
    };

    self.doSearch = function (type,value) {
        self.currentPage = 1;
        getListUser(type,value);
    };
    self.process = function(){
        angular.forEach(self.users.documentList, function (data) {
            // console.log(data)
            var request = {"resumeId" : self.users.id,
            "documentCode":data.code,
            "verified" : data.verified,
            "description" : data.description,
            "pendingDescription" : data.pendingDescription}
            // console.log(request)
            DokumenKelengkapanService.processFileKelengkapanDokumen(request).then(
                function(response){
                        // console.log(response)
                    if(response.message !== 'ERROR'){
                    }
            })
        });
        processResume();
    };
    self.pending = function(){
        angular.forEach(self.users.documentList, function (data) {
            // console.log(data)
            var request = {"resumeId" : self.users.id,
            "documentCode":data.code,
            "verified" : data.verified,
            "description" : data.description,
            "pendingDescription" : data.pendingDescription}
            // console.log(request)
            DokumenKelengkapanService.pendingFileKelengkapanDokumen(request).then(
                function(response){
                        console.log(response)
                    if(response.message !== 'ERROR'){
                    }
            })
        });
        pendingResume();
    }
    function processResume (){
        var request = {"id":self.users.id,"version":self.users.version};
        DokumenKelengkapanService.processResumeKelengkapanDokumen(request).then(
                function(response){
                    if(response.message !== 'ERROR'){

        $location.path('/dashboard/check-kelengkapan-document')
                    }

            })
    }
    function pendingResume (){
        var request = {"id":self.users.id,"version":self.users.version};
        DokumenKelengkapanService.pendingResumeKelengkapanDokumen(request).then(
                function(response){
                    if(response.message !== 'ERROR'){
                        $location.path('/dashboard/check-kelengkapan-document')
                    }
                }
        );
    }
    getListUser(paramId);
};