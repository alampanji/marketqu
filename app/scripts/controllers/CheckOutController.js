'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('CheckOutCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'localStorageService', 'messageService', '$timeout', 'LocationService', 'CartService',
        CheckOutCtrl
    ]);

function CheckOutCtrl($scope, CONF, $location, $modal, $window, localStorageService, messageService, $timeout, LocationService, CartService) {
    var self = $scope;

    self.close = close;
    self.addressDefault = true;

    self.checkOutItems = [];

    function close() {
        $modalInstance.dismiss('close');
    }

    function getItems(){
        if(localStorageService.get(CONF.CART) != undefined){
            self.checkOutItems = angular.copy(localStorageService.get(CONF.CART));

            // getWeight();
            // self.totalWeight = getWeight();
        }
    }


    function getUserData () {
      self.userData = localStorageService.cookie.get(CONF.dataUser);
    };

    self.deleteCart = function(index) {
        messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_ERROR,'Berhasil menghapus item(s) ' + self.checkOutItems[index].name + ' dari keranjang belanja');
        self.checkOutItems.splice(index, 1);

        localStorageService.set(CONF.CART, self.checkOutItems);

        if(self.checkOutItems.length == 0){
            localStorageService.cookie.remove(CONF.CART);
        }

    };

    self.totalPrice = function () {
        var total = 0;
        for(var i=0; i<self.checkOutItems.length; i++){
            total += self.checkOutItems[i].price_low *  self.checkOutItems[i].quantity;
        }
        return total;
    };
        // self.checkOutItems.splice(index,1)

    self.showModalLogin = function() {
        $modal.open({
            templateUrl: '/views/auth/Login.html',
            controller: 'LoginCtrl',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        message: ""
                    };
                }
            }
        });
    };



    self.goToDashboard = function () {
        $location.path('/admin/home');
    };

    self.goToProduct= function () {
        $location.path('/product');
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.goToPaymentMethod = function () {
        var value = checking();

        if(value){
            invalidModal("Data is not valid, please check your input text");
        }
        else{
            if(self.checkOutItems.length > 0){

                if(self.notes == undefined){
                   self.notes = '';
                }

                if(self.lastName == undefined){
                    self.lastName = '';
                }

                localStorageService.set(CONF.CART, self.checkOutItems);
                localStorageService.cookie.set(CONF.ORDER_CODE, generateOrderCode());

                var request = {
                    first_name : self.firstName,
                    last_name : self.lastName,
                    address : self.address,
                    phone : self.phone,
                    city : self.itemCity.city_id,
                    postal_code:self.postCode,
                    notes: self.notes,
                    shipping_date:  Date.parse(self.shippingDate)
                };

                // console.log(request);

                localStorageService.cookie.set(CONF.SHIPPING_DATA, request);
                getOngkir();
            }
            else{

                invalidModal("Your shopping cart is empty");

            }
        }
    };
    
    function getOngkir() {
        var request = {};
        var dataorigin = [];
        request.destination = self.itemCity.city_id;
        angular.forEach(localStorageService.get(CONF.CART), function (data) {
           dataorigin.push(data.city.city_id);
        });
        request.origin = dataorigin.join(',');
        request.weight =  self.getWeight();
        request.courier = self.courier;

        console.log(request);

        LocationService.countCostOngkir(JSON.stringify(request)).then(
            function(response) {
                if (response.status !== 'ERROR') {
                    CartService.setCost(response.ongkir);
                    CartService.setCourier(response.courier);
                    localStorageService.cookie.set(CONF.DELIVERY_COST, response.ongkir);
                    localStorageService.cookie.set(CONF.DELIVERY_COURIER, response.courier);
                    localStorageService.cookie.set(CONF.DELIVERY_DETAIL, response.detail);

                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Success',
                                    message: 'Succeed checkout your cart',
                                    path: ''
                                };
                            }
                        }
                    });
                    $location.path('/payment-method');
                    // $window.location.href = 'http://localhost:9000/#/payment-method';
                }
                else{
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Failed',
                                    message: response.message,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            });
    }

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    };

    self.getWeight = function() {
        var weight = 0;
        angular.forEach(self.checkOutItems, function (data) {
           weight += data.weight * data.quantity;;
        });

        return weight;
    };

    var checking = function () {
        self.firstNameInvalid = false;
        self.phoneInvalid = false;
        self.addressInvalid = false;
        self.postCodeInvalid = false;
        self.courierInvalid = false;
        self.shippingDateInvalid = false;

        var bool = false;

        if (self.firstName == null || self.firstName == undefined) {
            self.firstNameInvalid = true;
            bool = true;
        }
        if(self.phone == null || self.phone == undefined){
            self.phoneInvalid = true;
            bool = true;
        }
        if(self.address == null || self.address == undefined){
            self.addressInvalid = true;
            bool = true;
        }
        if(self.postCode == null || self.postCode == undefined){
            self.postCodeInvalid = true;
            bool = true;
        }

        if(self.shippingDate == null || self.shippingDate == undefined){
            self.shippingDateInvalid = true;
            bool = true;
        }

        if(self.courier == null || self.courier == undefined){
            self.courierInvalid = true;
            bool = true;
        }

        return bool;
    };



    self.banner = 'img/banner.jpeg';

    self.chartIsActive = false;
    self.chartNotNull = false;

    self.showCart = function () {
        self.chartIsActive = true;
        if(self.cartProduct.length > 0){
            self.chartNotNull = true;
        }
    };

    self.hideCart = function () {
        self.chartIsActive = false;
    };


    function getPrice(quantity, price) {
        return quantity * price;
    }

    // constructCart();

    var productToCart = [];
    self.cartProduct = [];

    self.produsenLabel = function (index) {
        self.products[index].status = true;
    };


    self.produsenLabelHide = function (index) {
        self.products[index].status = false;
    };

    function generateOrderCode() {

        if(localStorageService.cookie.get(CONF.ORDER_CODE) == undefined) {

            var length = 8;
            var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            var result = '';
            for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
            return result;

        }
    }

    self.addToCart = function (product) {

        if(productToCart.length < 1){
            product.quantity = 1;
            productToCart.push(product);
            // localStorageService.cookie.set(CONF.CART, productToCart);
            messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_SUCCESS,'Berhasil menambahkan ' + product.name + ' ke keranjang belanja');
            constructCart();
        }

        else{
            var next = true;
            for(var i=0; i<productToCart.length; i++){
                if(productToCart[i].id == product.id){
                    productToCart[i].quantity = productToCart[i].quantity+1;
                    next = false;
                }
            }
            if(next){
                product.quantity = 1;
                productToCart.push(product);
            }

            localStorageService.set(CONF.CART, productToCart);
            messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_SUCCESS,'Berhasil menambahkan ' + product.name + ' ke keranjang belanja');
            constructCart();
        }
    };

    self.goToHome = function () {
        $location.path('user/home');
    };

    function getProvince (){
        var type = 'province';
        var id = '';
        LocationService.getDeliveryFeeCity(type, id).then(
            function(response) {
                if (response.status !== 'ERROR') {
                    self.province = response.data;
                    self.itemProvince = self.province[0];
                    self.setCity(self.itemProvince);
                }
            });
    };

    self.setCity = function(provinceId){
        var type = 'city';
        var id = provinceId.province_id;
        LocationService.getDeliveryFeeCity(type, id).then(
            function(response) {
                if (response.status !== 'ERROR') {
                    self.city = response.data;
                    self.itemCity = self.city[0];
                }
            });
    };

    self.selectCity = function (city) {
        self.itemCity = city
    };

    getProvince();

    getItems();

    getUserData();
};