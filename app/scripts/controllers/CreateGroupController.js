'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('createGroupCtrl', [
        '$scope', '$modal',
        'GroupService',
        'AccessLevelService','$location', '$window',
        createGroupCtrl
    ]);

function createGroupCtrl($scope, $modal, GroupService, AccessLevelService,$location, $window) {


   var self = $scope;

    var request = {
        "privilegeList" : []
    };
    
    self.canceled = function () {
        $location.path('/dashboard/search-maintenance-group');
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    var getListPrivilege = function () {
        backToTop();
        GroupService.getListPrivilege().then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.privilege = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                }
            });
    };

    var getListAccessLevel = function () {
        AccessLevelService.getListAccessLevel().then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.accessLevel = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                }
            });
    };

    var checking = function () {
        self.nameInvalid = false;
        self.privilegeInvalid = false;

        var bool = false;

        if (self.groupName == null) {
            self.nameInvalid = true;
            bool = true;
        }

        return bool;
    };

    var checkIdPrivilege = function () {
        angular.forEach(self.privilege, function (masterPrivilege) {
            angular.forEach(masterPrivilege.subCategoryPrivilegeList, function (subCategoryPrivilege) {
                angular.forEach(subCategoryPrivilege.privilegeList, function (listOfPrivileges) {
                    console.log(listOfPrivileges.value + " " + listOfPrivileges.id);
                    if (listOfPrivileges.value && listOfPrivileges.id != "0") {
                        var listId = {"id":listOfPrivileges.id};
                        request.privilegeList.push(listId);
                    }
                });
            });
        });
    };

    self.changed = function (parents, parent, id) {
        angular.forEach(self.privilege[parents].subCategoryPrivilegeList[parent].privilegeList, function (listOfPrivileges) {
            if (listOfPrivileges.id != id) {
                listOfPrivileges.value = false;
            }
        });
    };

    self.createGroup = function () {
        checkIdPrivilege();
        var hasil = checking();
        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }else {

            request.accessLevel = self.accessLevelSelect;
            request.name = self.groupName;
            console.log(request);

            GroupService.createGroup(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Group berhasil ditambahkan',
                                        path: 'dashboard/search-maintenance-group'
                                    };
                                }
                            }
                        });
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        }
    };

    getListAccessLevel();
    getListPrivilege();

}