'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('createParameterBranchCentralCtrl', [
        '$scope', '$modal', 'BranchCentralService', '$window',
        createParameterBranchCentralCtrl
    ]);

function createParameterBranchCentralCtrl($scope, $modal, BranchCentralService, $window) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    var request = {
        "branchList" : []
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    self.lovBranch = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovBranch.html',
            controller: 'modalLovBranchCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData.branch
                    };
                }
            }
        });
        modalInstance.result.then(function (listBranch) {
            self.contentData.branch = listBranch;

        });
    };

    self.deleteBranch = function(index){
        self.contentData.branch.splice(index,1);
    };


    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    var checking = function () {
        self.nameInvalid = false;
        self.branchInvalid = false;

        var bool = false;

        if (self.contentData.name == null) {
            self.nameInvalid = true;
            bool = true;
        }
        if(self.contentData.branch == null){
            self.branchInvalid = true;
            bool = true;
        }
        return bool;
    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah cabang sentral?',
                        path: 'dashboard/setting-branch-central'
                    };
                }
            }
        });

    };

    self.createBranchCentral = function () {
        var hasil = checking();
        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }else {
            for(var i=0; i<self.contentData.branch.length; i++){
                var listId = {
                    'code' : self.contentData.branch[i].code,
                    'name'  : self.contentData.branch[i].name
                };
                request.branchList.push(listId);
            }
            request.name = self.contentData.name;

            BranchCentralService.createBranchCentral(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Cabang Sentral berhasil ditambahkan',
                                        path: 'dashboard/setting-branch-central'
                                    };
                                }
                            }
                        });
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        }
    };

    backToTop();

};
