'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('createParameterNotaryCtrl', [
        '$scope', '$modal', 'ParameterNotaryService', '$window',
        createParameterNotaryCtrl
    ]);

function createParameterNotaryCtrl($scope, $modal, ParameterNotaryService, $window) {

    var self = $scope;
    self.contentData = {};
    self.notaryFeeResumeList = [];
    self.boolean = false;
    var request = {
        notaryFeeResumeList : []
    };

    self.notaryFeeResume = {};

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    self.retrieveNotary = function () {
        UserService.getRetrieveUser(self.contentData.username).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataUser = response.result;
                }
            });
    };

    self.notaryType = [
        {value: 'PERORANGAN', name: 'Perorangan'},
        {value: 'BADAN_USAHA', name: 'Badan Usaha'}

    ];

    self.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    };

    self.addResume = function(fee,start){
        if(fee!=undefined && start != undefined){
            var tempStart = angular.copy(start);
            var tempFee = angular.copy(fee);
            var insert={"effectiveDate":Date.parse(tempStart),"feeResume":tempFee};
            self.notaryFeeResumeList.push(insert);
            self.tglEfektif = null;
            self.feeResume = null;
        }
    };

    self.deleteFeeResume = function (index) {
        self.notaryFeeResumeList.splice(index,1);
    };

    self.lovBranch = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovBranch.html',
            controller: 'modalLovBranchCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.branch
                    };
                }
            }
        });
        modalInstance.result.then(function (listBranch) {
            self.contentData.branch = listBranch;
        });
    };

    self.contentData.npwpStatus = true;

    self.selectNpwp = function () {
        if(contentData.npwpStatus == true){
            self.showNpwp = true;
        }
    };

    self.lovListNotary = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovListNotary.html',
            controller: 'modalLovNotaryCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData
                    };
                }
            }
        });
        modalInstance.result.then(function (data) {
            self.contentData = data;
        });
    };



    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah setting notary?',
                        path: 'dashboard/setting-notary'
                    };
                }
            }
        });

    };

    var checking = function () {
        self.nameInvalid = false;
        self.notaryIdInvalid = false;
        self.addressInvalid = false;
        self.cityInvalid = false;
        self.phoneNumberInvalid = false;
        self.faxInvalid = false;
        self.contactPersonInvalid = false;
        self.accountNumberInvalid = false;
        self.accountNameInvalid = false;
        self.bankInvalid = false;
        self.bankBranchIdInvalid = false;
        self.branchBankInvalid = false;
        self.balanceInvalid = false;
        self.cliringCodeInvalid = false;
        self.notaryTypeInvalid = false;
        self.feeResumeInvalid = false;
        self.notaryFeeResumeInvalid = false;
        self.ppnInvalid = false;

        var bool = false;

        if (self.contentData.name == null) {
            self.nameInvalid = true;
            bool = true;
        }

        if(self.contentData.kliringCode==null){
            self.cliringCodeInvalid = true;
            bool = true;
        }

        if(self.contentData.ppn == null){
            self.ppnInvalid = true;
            bool = true;
        }

        if(self.contentData.skb == null){
            self.skbInvalid = true;
            bool = true;
        }

        // if(self.notaryId == null){
        //     self.notaryIdInvalid = true;
        //     bool = true;
        // }
        // if(self.address == null){
        //     self.addressInvalid = true;
        //     bool = true;
        // }
        // if(self.city == null){
        //     self.cityInvalid = true;
        //     bool = true;
        // }
        // if(self.phoneNumber == null){
        //     self.phoneNumberInvalid = true;
        //     bool = true;
        // }
        // if(self.fax == null){
        //     self.faxInvalid = true;
        //     bool = true;
        // }
        // if(self.contactPerson == null){
        //     self.contactPersonInvalid = true;
        //     bool = true;
        // }
        // if(self.accountNumber == null){
        //     self.accountNumberInvalid = true;
        //     bool = true;
        // }
        // if(self.accountName == null){
        //     self.accountNameInvalid = true;
        //     bool = true;
        // }
        // if(self.bank == null){
        //     self.bankInvalid = true;
        //     bool = true;
        // }
        // if(self.bankBranchId == null){
        //     self.bankBranchIdInvalid = true;
        //     bool = true;
        // }
        // if(self.branchBank == null){
        //     self.branchBankInvalid = true;
        //     bool = true;
        // }
        // if(self.balance == null){
        //     self.balanceInvalid = true;
        //     bool = true;
        // }
        // if(self.balance == null){
        //     self.balanceInvalid = true;
        //     bool = true;
        // }
        // if(self.cliringCode == null){
        //     self.cliringCodeInvalid = true;
        //     bool = true;
        // }
        if(self.notaryType.value == undefined){
            self.notaryTypeInvalid = true;
            bool = true;
        }
        return bool;
    };

    self.createSettingNotary = function () {

        var hasil = checking();

        //console.log(request);
        // for(var i=0; i<self.contentData.branch.length; i++){
        //     var listId = {
        //         'code' : self.contentData.branch[i].code,
        //         'name'  : self.contentData.branch[i].name
        //     }
        //     request.branchList.push(listId);
        // }
        //
        // for(var i=0; i<self.contentData.centralBranch.length; i++){
        //     var listId = {
        //         'id' : self.contentData.centralBranch[i].id
        //     }
        //     request.branchCentralList.push(listId);
        // }
        //
        // for(var i=0; i<self.contentData.group.length; i++){
        //     var listId = {
        //         'id' : self.contentData.group[i].id
        //     }
        //     request.groupList.push(listId);
        // }
        //
        //
        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }else {
                request.notaryFeeResumeList = self.notaryFeeResumeList;
                console.log(request.notaryFeeResumeList);

            request.name = self.contentData.name;
            request.notaryId = self.contentData.notaryId;
            request.address = self.contentData.address;
            request.city = self.contentData.city;
            request.phone = self.contentData.phone;
            request.fax = self.contentData.fax;
            request.contactPerson = self.contentData.contactPerson;
            request.accountNo = self.contentData.accountNo;
            request.accountName = self.contentData.accountName;
            request.bank = self.contentData.bank;
            request.bankBranchId = self.contentData.bankBranchId;
            request.bankBranch = self.contentData.bankBranch;
            request.balance = self.contentData.balance;
            request.kliringCode = self.contentData.kliringCode;
            request.notaryType = self.notaryType.value;
            request.npwpStatus = self.contentData.npwpStatus;
            request.npwpNo = self.contentData.npwpNo;
            request.npwpAdress = self.contentData.npwpAddress;
            request.skb = self.contentData.skb;
            request.ppn = self.contentData.ppn;
            console.log(request);
            ParameterNotaryService.createParameterNotary(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Parameter Notary berhasil ditambahkan',
                                        path: 'dashboard/setting-notary'
                                    };
                                }
                            }
                        });
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
       }
    };

    backToTop();

};
