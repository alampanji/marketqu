'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('nostraApp')
    .controller('createSettingLimitApproveCtrl', [
        '$scope','modalParam', '$modalInstance', '$modal','SettingLimitApproveService',
        createSettingLimitApproveCtrl
    ]);

function createSettingLimitApproveCtrl($scope ,modalParam, $modalInstance, $modal, SettingLimitApproveService) {

    var self = $scope;
    self.close = close;
    self.confirm = confirm;

    self.message = modalParam.message;
    self.judul='Tambah Periode Setting Limit';
    self.currentpage = 1;
    self.entrylimit = 5;

    var today = new Date();
    self.minDate = today.setDate(today.getDate() + 1);
    function close() {
        $modalInstance.dismiss('close');
    };

    self.isDisabled = false;

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    };


    function close (){
        $modalInstance.dismiss('no');
    }
    function confirm(){
        $modalInstance.dismiss('yes');
    }

    self.input={};

    var checking = function () {
        self.periodInvalid = false;
        self.effectiveDateInvalid = false;

        var bool = false;

        if (self.input.period == null) {
            self.periodInvalid = true;
            bool = true;
        }
        if(self.input.effectiveDate == null){
            self.input.effectiveDateInvalid = true;
            bool = true;
        }
        return bool;
    };

    var invalidModal = function(message){
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size    : 'sm',
            resolve : {
                modalParam: function(){
                    return{
                        title: 'Peringatan',
                        message: message,
                    };
                }
            }
        });
    };

    self.submit=function() {

        var hasil = checking();
        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }
        else {
            self.input.effectiveDate = Date.parse(self.input.effectiveDate);
            SettingLimitApproveService.createSettingLimitApprovePeriod(self.input).then(
                function (response) {
                    $modalInstance.close('close');
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            backdrop: 'static',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Success',
                                        message: 'Setting Periode Limit Approve Baru Telah Dibuat',
                                        path: 'dashboard/setting-limit-approve'
                                    };
                                }

                            }

                        });
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            backdrop: 'static',

                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                }
            );
        }
    }

};