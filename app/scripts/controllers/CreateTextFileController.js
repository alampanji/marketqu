'use strict';
angular.module('nostraApp')
    .controller('createTextFileCtrl', [
        '$scope', '$modal', '$window',
        '$location',
        'InvoiceService','ParameterNotaryService','FinanceService',
        createTextFileCtrl
    ]);

function createTextFileCtrl($scope, $modal, $window, $location, InvoiceService,ParameterNotaryService,FinanceService) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    self.currentPage = 0;
    self.numPages = 1;
    // var date = new Date();
    // var test = Date.parse(date);
    // console.log(test)
    self.search = [
        {"keyName" : "appId", "value":"APP ID"},
        {"keyName" : "npwp", "value":"NPWP"},
        {"keyName" : "companyName", "value":"Nama Perusahaan"}
    ];
    self.totalFee = 0;
    self.totalData = 0;
    self.tampung=[];
    self.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };
    self.start = {
        opened: false
    };

    self.end = {
        opened: false
    };

    self.documentDate = {
        opened: false
    };

    self.documentPeriod = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    }; 
    self.openStart = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.start.opened)
            self.start.opened = false;
        else
            self.start.opened = true;
    };  
    self.openEnd = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.end.opened)
            self.end.opened = false;
        else
            self.end.opened = true;
    };    
    self.openAkhir = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.akhir.opened)
            self.akhir.opened = false;
        else
            self.akhir.opened = true;
    };
 
    self.openDocumentDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.documentDate.opened)
            self.documentDate.opened = false;
        else
            self.documentDate.opened = true;
    };
    self.openDocumentPeriod = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.documentPeriod.opened)
            self.documentPeriod.opened = false;
        else
            self.documentPeriod.opened = true;
    };
    self.goToDetail = function(id){
        $location.path('/dashboard/payment-detail').search({id:id});
    };
    self.goToDetailApprove = function(id){
        $location.path('/dashboard/approve-payment-detail').search({id:id});
    };
    self.submit = function(){
        var request = [];
        for (var i=0;i < self.tampung.length; i++){
            var id = {"id": self.tampung[i].id}
            request.push(id)
        }
        console.log(request)
        var input = {};
        input.invoiceList = request;
        FinanceService.processTextFile(input).then(
            function (response) {

                if (response.message !== 'ERROR') {
                    var tampung = '';
                    console.log(response.result.length)
                    // for(var i=0;i<response.result.length;i++){
                        if(response.result.length == 1){
                        var split = response.result[0].fileName.split(",")
                        tampung = split[0];
                        }
                        else if (response.result.length == 2){

                        var split = response.result[0].fileName.split(",");
                        var split2 = response.result[1].fileName.split(",");
                        tampung = split[0]+', '+split2[0];
                        }

                    // }
                    // console.log(response.result[0].fileName)
                    console.log(tampung)
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'File Telah Berhasil Tergenerate',
                                    message: tampung,
                                    path: ''
                                };
                            }
                        }
                    });
                    // $location.path('/dashboard/security-admin');
                }else{
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Informasi File Gagal Tergenerate',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            }
        );
        // console.log(input)
    }

    self.getListInvoice = function(notary) {
        FinanceService.getlistOfBatchNumberCreateTextFile(notary.id).then(
            function (response) {
                if (response.message === 'OK') {
                    self.invoices = response.result;
                    // console.log(self.invoices[0])
                    self.itemInvoice = self.invoices[0];
                    if(self.itemInvoice != undefined){
                        self.getListReceiveInvoice(self.itemInvoice.batchNumber, self.itemNotary);
                    }
                }
            }
        );
    };

    self.getListReceiveInvoice = function(itemInvoice, itemNotary) {
        if(itemInvoice!=undefined && itemNotary!=undefined){
            FinanceService.getlistOfTextFile(itemInvoice, itemNotary.id).then(
                function (response) {
                    if (response.message === 'OK') {
                        self.listRequestResumes = response.result;

                        // if(self.listRequestResumes){
                            // for(var i=0; i<self.listRequestResumes.resumeInvoiceList.length; i++){
                            //     self.listRequestResumes.resumeInvoiceList[i].value = false;
                            // }
                        // }
                    }
                }
            );
        }

    };
// https://resume-ahu.nostratech.com:23443//invoice/input-invoice-list?startDate=1452416732000&endDate=1484038790000&access_token=b87ba11b-ca89-3c1e-8735-8f304e45c2a6
    var getListOfRequestCancelResume = function () {
        console.log(self.input)
        var request = {};
        request.startDate = 1480525200000;
        request.endDate = 1484240400000;
        // console.log(request)
        // endDate=1484240400000&startDate=1480525200000
//https://resume-ahu.nostratech.com:23443/invoice/input-invoice-list?access_token=e8958ea7-69e4-3e56-baa7-36188317c99e&endDate=1484240400000&startDate=1480525200000
        InvoiceService.getlistOfInvoice(request).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.listRequestResumes = response.result;
                    for (var i=0;i<self.listRequestResumes.resumeInvoiceList.length;i++){
                        self.listRequestResumes.resumeInvoiceList[i].value = false;
                    }

                }
            });
    };


    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    function getListNotary() {

        backToTop();

        ParameterNotaryService.getAllNotaryInvoiceName().then(
            function (response) {
                if (response.message === 'OK') {
                    var notaries = response.result;
                    var constructNotary = [{"id":"", "version":"","name":"Silahkan Pilih"}];
                    angular.forEach(notaries, function(notary){
                        constructNotary.push(notary);
                    });
                    self.notaries = constructNotary;
                    self.itemNotary = self.notaries[0];
                }
            }
        );
    };

    self.lovGroupPrivilege = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovGroupPrivilege.html',
            controller: 'modalGroupPrivilegeCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData.group
                    };
                }
            }
        });
        modalInstance.result.then(function (listGroup) {
            self.contentData.group = listGroup;
        });
    };

    self.deleteGroup = function(index){
        self.contentData.group.splice(index,1);
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.value = function (isSelected, item,index) {
        if (isSelected == true) {
            self.tampung.push(item);

        } else {
            self.tampung.splice(index, 1);
        }
        // console.log(self.tampung)
    }

    self.checkedAll = function() {
        if(self.document){
            for(var i=0; i<self.listRequestResumes.length; i++){
                self.listRequestResumes[i].value = false;
                self.document = false;

                if(self.tampung.length > 0){
                // console.log(self.tampung)
                    self.tampung.splice(i,1)
                }
            }

        }
        else{
            for(var i=0; i<self.listRequestResumes.length; i++) {

                // if (self.listRequestResumes.resumeInvoiceList[i].value = false){
                    // console.log(i)
                    self.listRequestResumes[i].value = true;
                    self.document = true;
                    self.tampung.push(self.listRequestResumes[i])
                // }
                // else{

                // }
            }

        }

    // hitung();
    };
    self.totalData = self.tampung.length;
    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: '/views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah user?',
                        path: 'dashboard/security-admin'
                    };
                }
            }
        });

    };

    self.doSearch = function () {
       
        getListOfRequestCancelResume()
    };

    getListNotary();
};
