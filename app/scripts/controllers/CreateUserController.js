'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('createUserCtrl', [
        '$scope', '$modal', 'localStorageService', 'CONF', '$filter',
        '$location',
        'UserService',
        '$window',
        createUserCtrl
    ]);

function createUserCtrl($scope, $modal, localStorageService, CONF, $filter, $location, UserService, $window) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    var request = {
        "branchList" : [],
        "branchCentralList": [],
        "groupList": []
    };

    // self.emailDomain = "@infra.bcafinance.co.id";
    self.emailDomain = "@mailinator.com";

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };
    
    self.retrieveUser = function () {
        UserService.getRetrieveUser(self.contentData.username).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataUser = response.result;
                    self.contentData.nip = self.dataUser.nip;
                    self.contentData.fullName = self.dataUser.fullName;
                    self.contentData.division = self.dataUser.division;
                    self.contentData.position = self.dataUser.position;
                    self.boolean = true;
                }
            });
    };
    

    self.lovBranch = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovBranch.html',
            controller: 'modalLovBranchCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData.branch
                    };
                }
            }
        });
        modalInstance.result.then(function (listBranch) {
            self.contentData.branch = listBranch;
        });
    };

    self.deleteBranch = function(index){
        self.contentData.branch.splice(index,1);
    };

    self.lovBranchCentral = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovBranchCentral.html',
            controller: 'modalLovBranchCentralCtrl',
            size: 'md',
            // if(self.contentData.centralBranch.name!=undefined){
                resolve: {
                    modalParam: function () {
                        return {
                            values: self.contentData.centralBranch
                        }
                    }
                }
            // }
        });
        modalInstance.result.then(function (listBranchCentral) {
            self.contentData.centralBranch = listBranchCentral;
        });
    };

    self.deleteBranchCentral = function(index){
        // self.contentData.centralBranch = null;
        self.contentData.centralBranch.splice(index,1);
    };

    self.lovGroupPrivilege = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovGroupPrivilege.html',
            controller: 'modalGroupPrivilegeCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData.group
                    };
                }
            }
        });
        modalInstance.result.then(function (listGroup) {
           self.contentData.group = listGroup;
        });
    };

    self.deleteGroup = function(index){
        self.contentData.group.splice(index,1);
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah user?',
                        path: 'dashboard/security-admin'
                    };
                }
            }
        });

    };

    var checking = function () {
        self.nipInvalid = false;
        self.userNameInvalid = false;
        self.confirmationPasswordInvalid = false;
        self.fullNameInvalid = false;
        self.divisionInvalid = false;
        self.positionInvalid = false;
        self.emailInvalid = false;
        self.branchInvalid = false;
        self.centralBranchInvalid = false;
        self.groupInvalid = false;

        var bool = false;

        if (self.contentData.username == null) {
            self.userNameInvalid = true;
            bool = true;
        }
        if(self.contentData.nip == null){
            self.nipInvalid = true;
            bool = true;
        }
        if(self.contentData.fullName == null){
            self.fullNameInvalid = true;
            bool = true;
        }
        if(self.contentData.division == null){
            self.divisionInvalid = true;
            bool = true;
        }
        if(self.contentData.position == null){
            self.positionInvalid = true;
            bool = true;
        }
        if(self.contentData.branch == null){
            self.branchInvalid = true;
            bool = true;
        }
        if(self.contentData.centralBranch == null){
            self.centralBranchInvalid = true;
            bool = true;
        }
        if (self.contentData.group == null){
            self.groupInvalid = true;
            bool = true;
        }
        if (self.contentData.email == null){
            self.emailInvalid = true;
            bool = true;
        }
        return bool;
    };

    self.createUser = function () {
        var hasil = checking();


        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }else {
            if(self.contentData.branch != null){
                for(var i=0; i<self.contentData.branch.length; i++){
                    var listId = {
                        'code' : self.contentData.branch[i].code,
                        'name'  : self.contentData.branch[i].name
                    };
                    request.branchList.push(listId);
                }
            }

            for(var i=0; i<self.contentData.centralBranch.length; i++){
                var listId = {
                    'id' : self.contentData.centralBranch[i].id
                }
                request.branchCentralList.push(listId);
            }

            for(var i=0; i<self.contentData.group.length; i++){
                var listId = {
                    'id' : self.contentData.group[i].id
                };
                request.groupList.push(listId);
            }

            request.nip = self.contentData.nip;
            request.fullName = self.contentData.fullName;
            request.username = self.contentData.username;
            request.email = self.contentData.email+self.emailDomain;
            request.division = self.contentData.division;
            request.position = self.contentData.position;
            request.firstName = "First Name";
            request.lastName = "Last Name";
            request.role = "BCA";
            console.log(request);
            UserService.createUserInternal(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'User berhasil ditambahkan',
                                        path: ''
                                    };
                                }
                            }
                        });
                        $location.path('/dashboard/security-admin');
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        }
    }

    backToTop();

};
