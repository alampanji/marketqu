'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('createUserNotaryCtrl', [
        '$scope', '$modal', 'UserService', '$window', '$sessionStorage',
        createUserNotaryCtrl
    ]);

function createUserNotaryCtrl($scope, $modal, UserService, $window, $sessionStorage) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    var request = {
        "groupList": []
    };

    var email = $sessionStorage.email;;
    self.email = email;

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    self.lovListNotary = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovListMasterNotary.html',
            controller: 'modalLovMasterNotaryCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData.notary
                    };
                }
            }
        });
        modalInstance.result.then(function (data) {
            self.contentData = data;
        });
    };

    self.lovGroupPrivilege = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovGroupPrivilege.html',
            controller: 'modalGroupPrivilegeCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData.group
                    };
                }
            }
        });
        modalInstance.result.then(function (listGroup) {
            self.contentData.group = listGroup;
        });
    };

    self.deleteGroup = function(index){
        self.contentData.group.splice(index,1);
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    var checking = function () {
        self.nameInvalid = false;
        self.fullNameInvalid = false;
        self.notaryIdInvalid = false;
        self.userIdInvalid = false;
        self.divisionInvalid = false;
        self.emailInvalid = false;
        self.groupPrivilegeInvalid = false;

        var bool = false;

        if(self.contentData.name == null){
            self.nameInvalid = true;
            bool = true;
        }

        if (self.contentData.fullName == null) {
            self.fullNameInvalid = true;
            bool = true;
        }
        if(self.contentData.notaryId == null){
            self.notaryIdInvalid = true;
            bool = true;
        }
        if(self.contentData.userId == null){
            self.userIdInvalid = true;
            bool = true;
        }
        if(self.email == null){
            self.emailInvalid = true;
            bool = true;
        }

        if(self.contentData.group == null){
            self.groupPrivilegeInvalid = true;
            bool = true;
        }

        return bool;
    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah user notary?',
                        path: 'dashboard/user-notary'
                    };
                }
            }
        });

    };

    self.createUserNotary = function () {
        var hasil = checking();

        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }else {

            for(var i=0; i<self.contentData.group.length; i++){
                var listId = {
                    'id' : self.contentData.group[i].id
                };
                request.groupList.push(listId);
            }
            request.fullName = self.contentData.fullName;
            request.notaryId = self.contentData.id;
            request.userId = self.contentData.userId;
            request.email = self.email;

            console.log(request);

            UserService.createUserNotary(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'User Notary berhasil ditambahkan',
                                        path: 'dashboard/user-notary'
                                    };
                                }
                            }
                        });
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
            
                }
            );
        }
    };

    backToTop();

};
