'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('nostraApp')
    .controller('createVoucherCtrl', [
        '$scope', 'FidusiaService', '$location', '$modal', '$modalInstance', 'modalParam',
        createVoucherCtrl
    ]);

function createVoucherCtrl($scope, FidusiaService, $location, $modal, $modalInstance, modalParam) {

    var self = $scope;
    self.close = close;

    var modelpassing = modalParam;
    self.userId = modelpassing.id;
    self.version = modelpassing.version;
    self.name = modelpassing.name;
    self.teacherVersion = modelpassing.version;

    function close() {
        $modalInstance.dismiss('close');
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.isDisabled = false;

    self.addNotary = function () {
        console.log(notaris);
        $modalInstance.close(notaris);
    };
    self.doUpload = function () {

        if(self.file==null){
            invalidModal("File Harus Di isi");
        }

        else{
            var extn = self.file.name.split(".").pop();

            if(extn!='xls' && extn!='xlsx'){
                invalidModal("Format File Harus Excel (XLS/XLSX)");
                self.file = '';
            }
            else{
                var file = self.file;

                var fd = new FormData();
                fd.append('file', file);

                self.isDisabled = true;
                FidusiaService.uploadVoucher(fd).then(
                    function (response) {
                        $modalInstance.close('close');
                        if (response.message !== 'ERROR') {
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                backdrop: 'static',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Success',
                                            message: 'Voucher Baru Telah Dibuat!',
                                            path: ''
                                        };
                                    }

                                }

                            });
                            $location.path('/dashboard/upload-code-voucher');
                        } else {
                            self.isDisabled = false;
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                backdrop: 'static',

                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Gagal',
                                            message: response.result,
                                            path: 'dashboard/upload-code-voucher'
                                        };
                                    }
                                }
                            });
                        }

                    }
                );
            }
        }

    }


};