'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('nostraApp')
    .controller('deleteParameterBranchCentralCtrl', [
        '$scope', 'BranchCentralService', '$location', '$modal', '$modalInstance', 'modalParam',
        deleteParameterBranchCentralCtrl
    ]);

function deleteParameterBranchCentralCtrl($scope, BranchCentralService, $location, $modal, $modalInstance, modalParam) {

    var self = $scope;
    self.close = close;

    var modelpassing = modalParam;
    self.userId = modelpassing.id;
    self.version = modelpassing.version;
    self.name = modelpassing.name;
    var request = {};

    function close() {
        $modalInstance.dismiss('close');
    };

    self.isDisabled = false;

    var detailBranchCentral = function () {
        BranchCentralService.getDetailBranchCentral(self.userId).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.details = response.result;
                    request={
                        "id": self.details.id,
                        "version": self.details.version
                    };
                }
            }
        );
    };

    detailBranchCentral();

    self.doDeleteUser = function () {
        self.isDisabled = true;
        BranchCentralService.deleteBranchCentral(request).then(
            function (response) {
                $modalInstance.dismiss('close');
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Informasi',
                                    message: 'Cabang Sentral telah terhapus',
                                    path: ''
                                };
                            }
                        }
                    });
                    $location.path('dashboard/setting-branch-central');
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    }


};