'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('nostraApp')
    .controller('deleteParameterLimitNotaryInvoiceCtrl', [
        '$scope', 'ParameterLimitNotaryInvoiceService', '$location', '$modal', '$modalInstance', 'modalParam',
        deleteParameterLimitNotaryInvoiceCtrl
    ]);

function deleteParameterLimitNotaryInvoiceCtrl($scope, ParameterLimitNotaryInvoiceService, $location, $modal, $modalInstance, modalParam) {

    var self = $scope;
    self.close = close;

    var modelpassing = modalParam;
    self.userId = modelpassing.id;
    self.version = modelpassing.version;
    self.name = modelpassing.name;
    console.log(self.name)
    console.log(self.userId)
    console.log(self.version)
    self.teacherVersion = modelpassing.version;

    function close() {
        $modalInstance.dismiss('close');
    };

    self.isDisabled = false;

    self.doDeleteUser = function () {
        self.isDisabled = true;

        var request={
            "id": self.userId
        };
        ParameterLimitNotaryInvoiceService.deleteParameterNotary(request).then(
            function (response) {
                $modalInstance.dismiss('close');

                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Informasi',
                                    message: 'Parameter Notary telah terhapus',
                                    path: ''
                                };
                            }
                        }
                    });
                    $location.path('/dashboard/setting-limit-notary-invoice');
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    }


};