'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('nostraApp')
    .controller('deleteSettingLimitApproveCtrl', [
        '$scope', 'SettingLimitApproveService', '$location', '$modal', '$modalInstance', 'modalParam',
        deleteSettingLimitApproveCtrl
    ]);

function deleteSettingLimitApproveCtrl($scope, SettingLimitApproveService, $location, $modal, $modalInstance, modalParam) {

    var self = $scope;
    self.close = close;

    var modelpassing = modalParam;
    self.userId = modelpassing.id;
    self.version = modelpassing.version;
    self.name = modelpassing.name;
    var request = {};

    function close() {
        $modalInstance.dismiss('close');
    };

    self.isDisabled = false;


    self.doDeleteUser = function () {
        request = {
          "id":self.userId
        };
        self.isDisabled = true;
        SettingLimitApproveService.deleteSettingLimitApprovePeriod(request).then(
            function (response) {
                $modalInstance.dismiss('close');
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Informasi',
                                    message: 'SLA Period terhapus',
                                    path: 'dashboard/setting-limit-approve'
                                };
                            }
                        }
                    });
                    $location.path('dashboard/setting-limit-approve');
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    }


};