'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('nostraApp')
    .controller('deleteUserNotaryCtrl', [
        '$scope', 'UserService', '$location', '$modal', '$modalInstance', 'modalParam',
        deleteUserNotaryCtrl
    ]);

function deleteUserNotaryCtrl($scope, UserService, $location, $modal, $modalInstance, modalParam) {

    var self = $scope;
    self.close = close;

    var modelpassing = modalParam;
    self.userId = modelpassing.id;
    var request = {};

    function close() {
        $modalInstance.dismiss('close');
    };

    var detailUserNotary = function () {
        UserService.getDetailUserNotary(self.userId).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.details = response.result;
                    request={
                        "id": self.details.id
                    };
                    self.name = self.details.fullName;
                }
            }
        );
    };

    detailUserNotary();

    self.isDisabled = false;
        
        self.doDeleteUserNotary = function () {
        self.isDisabled = true;
        UserService.deleteUserNotary(request).then(
            function (response) {
                $modalInstance.dismiss('close');

                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Informasi',
                                    message: 'User notary telah terhapus',
                                    path: ''
                                };
                            }
                        }
                    });
                    $location.path('/dashboard/user-notary');
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    }


};