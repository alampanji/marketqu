'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('ForgotPassCtrl', [
        '$scope',
        '$modal',
        'CONF','messageService','$location', 'UserService',
        ForgotPassCtrl
    ]);

function ForgotPassCtrl($scope, $modal, CONF,messageService,$location, UserService) {

    var self = $scope;

    self.backToLogin = function () {
      $location.path('/user/home');
    };

    self.submit = function () {
            if (self.userId) {
                var request = {
                    "username": self.userId
                };

                UserService.forgotPassword(JSON.stringify(request)).then(
                    function (response) {
                        if (response.status == 'OK') {
                            // messageService.toasterMessage(CONF.TOASTER_TOP_CENTER, CONF.TOASTER_SUCCESS, "Success Forgot Password , Please check you email to get new password")
                            // $location.path('/login');
                            $modal.open({
                                templateUrl: '/views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                backdrop: 'static',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Information',
                                            message: 'Your password has been reset, please check your email',
                                            path: '/user/home'
                                        };
                                    }
                                }
                            });

                        }
                        else {
                            $modal.open({
                                templateUrl: '/views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                backdrop: 'static',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Forgot Password Failed',
                                            message: response.message,
                                            path: ''
                                        };
                                    }
                                }
                            });
                        }

                    }
                );

            }
    };
    

};