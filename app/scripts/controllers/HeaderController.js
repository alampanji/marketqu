/**
 * Created by derryaditiya on 5/9/16.
 */


app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
 
                event.preventDefault();
            }
        });
    };
});

angular.module('marketplaceApp')
    .controller('HeaderCtrl' , [
            '$scope',
            '$modal',
            'authService',
            '$location',
            '$sessionStorage',
            'localStorageService',
            'sharedPropertiesService',
            'CONF',
            HeaderCtrl] );

    function HeaderCtrl($scope, $modal, authService, $location, $sessionStorage,localStorageService,sharedPropertiesService,CONF) {
        var self = $scope;
        
        self.location;
        self.fullName = authService.getFullName();

        
        self.signIn = function () {
             self.fullName = authService.getFullName();
             return authService.isAuthenticated();
        };

        self.notUser = function () {
            if(localStorageService.cookie.get(CONF.domain) == "admin" || localStorageService.cookie.get(CONF.domain) == "bbOwner"){
                return true;
            }else if(localStorageService.cookie.get(CONF.domain) == "bbUser"){
                return false;
            }
        };

        self.redirectToSignUp = function(){
            $location.path('/signUp');
        }

        self.showModalLogin = function() {
            $modal.open({
                templateUrl: '/views/auth/Login.html',
                controller: 'LoginCtrl',
                size: 'sm',
                resolve: {
                    modalParam: function () {
                        return {
                            message: "/billboard/home"
                        };
                    }
                }
            });
        };

        self.doSearch = function(){
            sharedPropertiesService.setSearchValue(self.location);
            $location.path('/billboard/search');
        };

        self.doLogout = function() {
            authService.logout().then(

            );
            $location.path('/login');
        };

        self.goProfile = function () {
        
            $location.path('/dashboard/profile');
    
        }
        self.goChangePassword = function () {
        
            $location.path('/dashboard/change-password');
    
        }

        self.showModalMap = function () {
            $location.path('/article/map');
        };

        self.showTentangProgram = function() {
            $modal.open({
                templateUrl: 'views/modal/AHMPMELS004m01.html',
                controller: 'ModalCtrl',
                size: 'lg',
                resolve: {
                    modalParam: function () {
                        return {};
                    }
                }
            });
        };

        self.gotoDashboard = function (page) {
            var accessToken = localStorageService.cookie.get(CONF.accessToken);

            if (accessToken != null) {
                $location.path('/dashboard/user-bo');
            } else {
                showModalLogin(page);
            }

        };

        self.addBillboard = function () {
            $location.path('/billboard/add-billboard');

        };

        self.gotoEDatabase = function (page) {
            var accessToken = $sessionStorage.accessToken;

            if (accessToken != null) {
                $location.path('/dashboard/notifications');
            } else {
                showModalLogin(page);
            }

        };

        self.gotoLowongan = function () {
            $location.path('/article/galery-lowongan');
        };



    };
