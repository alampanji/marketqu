'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('HomeCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'InstallService', 'localStorageService', 'UserService',
        HomeCtrl
    ]);

function HomeCtrl($scope, CONF, $location, $modal, $window, InstallService, localStorageService, UserService) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.siteInfo = {};

    // self.bannerContainer = {
    //     "width" : "100%",
    //     "background": "url('img/banner.jpeg')"
    //     "height": "200px"
    // };

    var paramValue = $location.search().id;

    function checkActivated() {
        if (paramValue != undefined) {
            UserService.activation(paramValue).then(
                function (response) {
                    if (response.status == 'OK') {
                        $modal.open({
                            templateUrl: '/views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            backdrop: 'static',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Information',
                                        message: response.message,
                                        path: ''
                                    };
                                }
                            }
                        });

                    }
                }
            );
        }
    }
    

    function reload() {
        // window.location.reload();

        $route.reload();

    }


    function statusChecking () {

        InstallService.getPing().then(
            function(response) {
                if (response.status === 'OK') {
                    $location.path('user/home');
                    if(localStorageService.get(CONF.SITE_INFO) == undefined){
                        InstallService.getSiteInfo().then(
                            function (response) {
                                if(response.status == 'OK'){
                                    angular.forEach(response.result, function (data) {
                                        localStorageService.set(CONF.SITE_INFO, data);
                                        self.siteInfo = data;
                                        constructBanner(self.siteInfo.site_banner);
                                    })
                                }
                            }
                        )
                    }
                    else{
                        self.siteInfo = localStorageService.get(CONF.SITE_INFO);
                        constructBanner(self.siteInfo.site_banner);
                    }

                }
                else{
                    $location.path('install/database');
                }
            }
        );
    }

    function constructBanner(banner) {
        self.bannerContainer = {
            "color" : "white",
            "background" : "url("+CONF.IMAGE_PATH + CONF.PATH_FILES_BANNER + banner +")",
            "width":"100%",
            "background-repeat":"no-repeat",
            "height:":"400px",
            "background-size": "cover",
            "background-position": "center",
            "padding" : "200px"
        };
    };

    statusChecking();
    checkActivated();

    // reload();
};