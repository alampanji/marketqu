'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('inquiryHistoryDetailResumeCtrl', [
        '$scope','CONF','$location','$modal','ResumeService','$sessionStorage','$window','DokumenKelengkapanService',
        inquiryHistoryDetailResumeCtrl
    ]);


function inquiryHistoryDetailResumeCtrl($scope,CONF,$location,$modal,ResumeService, $sessionStorage,$window,DokumenKelengkapanService) {
    var self = $scope;

    self.close = close;
    self.contentData = {};
    // self.tester = [];
    var paramId = $location.search().id;
    function close() {
        $modalInstance.dismiss('close');
    }
    self.goToList = function(id){
        $location.path('/dashboard/input-resume-tambahan');

        // $location.path('/dashboard/matching-result').search({id: id});
    }
    self.testDate = /^\d+$/;
    self.save = function(){
        console.log(self.users)
        // self.contentData = {};
        self.contentData.id = self.users.id;
        self.contentData.version = self.users.version;
        self.contentData.address = self.users.address;
        self.contentData.position = self.users.position;
        if(self.itemSearch == undefined){
            self.contentData.method = '';
        }
        else {
            self.contentData.method = self.itemSearch.keyName.toUpperCase();
        }
        self.contentData.stockList = self.users.stockList;
        if(self.contentData.category == undefined){
            // {
            // id: "1",
            // version: 0,
            // name: "Terbuka",
            // description: "Terbuka",
            // value: false
            // }
            self.contentData.category = self.users.category;
        } 
        // if(self.users.)
        if(self.contentData.stockType == undefined){
            self.contentData.stockType = self.users.stockType;
        } 

        // self.contentData.stockType = self.users.stockType;
        self.contentData.articleAssociationList = self.users.articleAssociationList;
        if(self.contentData.articleAssociationList != null){
            for(var i=0;i<self.contentData.articleAssociationList.length;i++){
                if(!self.testDate.test(self.contentData.articleAssociationList[i].approvalDate) && !self.testDate.test(self.contentData.articleAssociationList[i].certificateDate) ){
                    self.contentData.articleAssociationList[i].approvalDate = Date.parse(self.contentData.articleAssociationList[i].approvalDate)
                    self.contentData.articleAssociationList[i].certificateDate = Date.parse(self.contentData.articleAssociationList[i].certificateDate)
                }
            }
        }
        self.contentData.directorList = self.users.directorList;
        if(self.contentData.directorList != null){
            for(var j=0;j<self.contentData.directorList.length;j++){
                console.log(self.testDate.test(self.contentData.directorList[j].endDate))
                if(!self.testDate.test(self.contentData.directorList[j].endDate) && !self.testDate.test(self.contentData.directorList[j].startDate) ){

                    self.contentData.directorList[j].endDate = Date.parse(self.contentData.directorList[j].endDate)
                    self.contentData.directorList[j].startDate = Date.parse(self.contentData.directorList[j].startDate)
                }
            }
        }
        self.contentData.additionalDataList = self.users.additionalDataList;
        if(self.contentData.additionalDataList != null){
            for(var k=0;k<self.contentData.additionalDataList.length;k++){
                if(!self.testDate.test(self.contentData.additionalDataList[k].documentDate) && !self.testDate.test(self.contentData.additionalDataList[k].documentPeriod) ){
                    self.contentData.additionalDataList[k].documentDate = Date.parse(self.contentData.additionalDataList[k].documentDate)
                    self.contentData.additionalDataList[k].documentPeriod = Date.parse(self.contentData.additionalDataList[k].documentPeriod)
                }
            }
        }
        self.contentData.directorJob = self.users.directorJob;
        self.contentData.description = self.users.description;
        self.contentData.stockHolderList = self.users.stockHolderList;

        // self.input.effectiveDate = Date.parse(self.input.effectiveDate);
        console.log(self.contentData)
        var fd = new FormData();
        if(self.users.file != undefined){
            console.log(self.users.file)
            fd.append('file',self.users.file);
        }
        fd.append('contentData', angular.toJson(self.contentData));
        // console.log(fd)
            ResumeService.saveInputResumeTambahan(fd).then(
                function(response){
                        console.log(response)
                    if(response.message !== 'ERROR'){
                    }
            })

    }
    self.submit = function(){
        // console.log(self.contentData)
        self.contentData.id = self.users.id;
        self.contentData.version = self.users.version;
        self.contentData.address = self.users.address;
        self.contentData.position = self.users.position;
        if(self.itemSearch == undefined){
            self.contentData.method = '';
        }
        else {
            self.contentData.method = self.itemSearch.keyName.toUpperCase();
        }
        self.contentData.stockList = self.users.stockList;
        if(self.contentData.category == undefined){
            // {
            // id: "1",
            // version: 0,
            // name: "Terbuka",
            // description: "Terbuka",
            // value: false
            // }
            self.contentData.category = self.users.category;
        } 
        // if(self.users.)
        if(self.contentData.stockType == undefined){
            self.contentData.stockType = self.users.stockType;
        } 

        // self.contentData.stockType = self.users.stockType;
        self.contentData.articleAssociationList = self.users.articleAssociationList;
        if(self.contentData.articleAssociationList != null){
            for(var i=0;i<self.contentData.articleAssociationList.length;i++){
                if(!self.testDate.test(self.contentData.articleAssociationList[i].approvalDate) && !self.testDate.test(self.contentData.articleAssociationList[i].certificateDate) ){
                    self.contentData.articleAssociationList[i].approvalDate = Date.parse(self.contentData.articleAssociationList[i].approvalDate)
                    self.contentData.articleAssociationList[i].certificateDate = Date.parse(self.contentData.articleAssociationList[i].certificateDate)
                }
            }
        }
        self.contentData.directorList = self.users.directorList;
        if(self.contentData.directorList != null){
            for(var j=0;j<self.contentData.directorList.length;j++){
                console.log(self.testDate.test(self.contentData.directorList[j].endDate))
                if(!self.testDate.test(self.contentData.directorList[j].endDate) && !self.testDate.test(self.contentData.directorList[j].startDate) ){

                    self.contentData.directorList[j].endDate = Date.parse(self.contentData.directorList[j].endDate)
                    self.contentData.directorList[j].startDate = Date.parse(self.contentData.directorList[j].startDate)
                }
            }
        }
        self.contentData.additionalDataList = self.users.additionalDataList;
        if(self.contentData.additionalDataList != null){
            for(var k=0;k<self.contentData.additionalDataList.length;k++){
                if(!self.testDate.test(self.contentData.additionalDataList[k].documentDate) && !self.testDate.test(self.contentData.additionalDataList[k].documentPeriod) ){
                    self.contentData.additionalDataList[k].documentDate = Date.parse(self.contentData.additionalDataList[k].documentDate)
                    self.contentData.additionalDataList[k].documentPeriod = Date.parse(self.contentData.additionalDataList[k].documentPeriod)
                }
            }
        }
        self.contentData.directorJob = self.users.directorJob;
        self.contentData.description = self.users.description;
        self.contentData.stockHolderList = self.users.stockHolderList;

        // self.input.effectiveDate = Date.parse(self.input.effectiveDate);
        console.log(self.contentData)
        var fd = new FormData();
        if(self.users.file != undefined){
            console.log(self.users.file)
            fd.append('file',self.users.file.file);
        }
        fd.append('contentData', angular.toJson(self.contentData));
        ResumeService.submitInputResumeTambahan(fd).then(
                function(response){
                        console.log(response)
                    if(response.message !== 'ERROR'){
                        // console.log()
                    }
                    else{
                       $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    }); 
                    }
            })
    }
    self.tab = 1;

    self.setTab = function (tabId) {
        self.tab = tabId;
    };

    self.isSet = function (tabId) {
        return self.tab === tabId;
    };
    self.doUpdateUser = function(){
        $location.path('/dashboard/update-user');
    };

    var nip = '';
    var name = '';

    self.search = [
        { "type":"nip", "label": "NIP"},
        { "type":"name", "label": "Nama"},
    ];


    var getListOfDocumentSetting = function () {
        DokumenKelengkapanService.getListOfDocumentSetting().then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.listDocument = response.result;
                }
            });
    };


    self.doAddOtherFile = function (bool) {
        getListOfDocumentSetting();
        if(bool == undefined){
            return self.otherDocument = true;
        }
        else{
            return self.otherDocument = false;
        }

    };
    self.lovBranch = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalAddDocument.html',
            controller: 'modalLovAddDocumentCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.users.documentList
                    };
                }
            }
        });
        modalInstance.result.then(function (listBranch) {

            self.users.documentList = listBranch;
        });
        
    };
        self.method = [
        {"keyName" : "input", "value":"Input"},
        {"keyName" : "upload", "value":"Upload"},
    ];

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;

    self.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };
    self.start = {
        opened: false
    };

    self.end = {
        opened: false
    };

    self.documentDate = {
        opened: false
    };

    self.documentPeriod = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    }; 
    self.openStart = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.start.opened)
            self.start.opened = false;
        else
            self.start.opened = true;
    };  
    self.openEnd = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.end.opened)
            self.end.opened = false;
        else
            self.end.opened = true;
    };    
    self.openAkhir = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.akhir.opened)
            self.akhir.opened = false;
        else
            self.akhir.opened = true;
    };
 
    self.openDocumentDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.documentDate.opened)
            self.documentDate.opened = false;
        else
            self.documentDate.opened = true;
    };
    self.openDocumentPeriod = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.documentPeriod.opened)
            self.documentPeriod.opened = false;
        else
            self.documentPeriod.opened = true;
    };


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.status = [
        {keyName: 'true', name: 'Aktif'},
        {keyName: 'false', name: 'Tidak Aktif'}
    ];

    self.pageChanged = function () {
        getListUser();
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

     var getListUser = function (paramId) {

         backToTop();
    
        ResumeService.getInputResumeTambahanDetail(paramId).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.users = response.result;
                    var access_token = $sessionStorage.accessToken;
                    
                        for (var j=0; j< self.users.documentList.length;j++){

                        var urlConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_DOCUMENT +'?access_token='+access_token+'&resumeId='+self.users.id+'&docType='+self.users.documentList[j].documentCode;
                       
                        self.users.documentList[j].url = urlConstruct;
                        }

                    var urlAhuConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_RESUME +'?access_token='+access_token+'&resumeId='+self.users.id;
                    self.users.resumeAhuUrl = urlAhuConstruct;
                        if(self.users.category == null){
                            self.users.categoryList = [
                                                                        {
                                                                        id: "1",
                                                                        version: 0,
                                                                        name: "TERBUKA",
                                                                        description: "TERBUKA",
                                                                        value: false
                                                                        },
                                                                        {
                                                                        id: "0",
                                                                        version: 0,
                                                                        name: "TERTUTUP",
                                                                        description: "TERTUTUP",
                                                                        value: false
                                                                        }
                                                                    ];

                        } else if(self.users.category == 'TERBUKA' || self.users.category == 'Terbuka'){
                            self.users.categoryList = [
                                                                        {
                                                                        id: "1",
                                                                        version: 0,
                                                                        name: "TERBUKA",
                                                                        description: "TERBUKA",
                                                                        value: true
                                                                        },
                                                                        {
                                                                        id: "0",
                                                                        version: 0,
                                                                        name: "TERTUTUP",
                                                                        description: "TERTUTUP",
                                                                        value: false
                                                                        }
                                                                    ];
                        } else if(self.users.category == 'TERTUTUP' ||self.users.category == 'Tertutup'  ){
                            self.users.categoryList = [
                                                                        {
                                                                        id: "1",
                                                                        version: 0,
                                                                        name: "TERBUKA",
                                                                        description: "TERBUKA",
                                                                        value: false
                                                                        },
                                                                        {
                                                                        id: "0",
                                                                        version: 0,
                                                                        name: "TERTUTUP",
                                                                        description: "TERTUTUP",
                                                                        value: true
                                                                        }
                                                                    ];
                        }
                        if(self.users.stockType == null){

                            self.users.stockTypeList = [
                                                                        {
                                                                        id: "1",
                                                                        version: 0,
                                                                        name: "PMA",
                                                                        description: "PMA",
                                                                        value: false
                                                                        },
                                                                        {
                                                                        id: "0",
                                                                        version: 0,
                                                                        name: "PMDN",
                                                                        description: "PMDN",
                                                                        value: false
                                                                        }
                                                                    ];

                        }  else if(self.users.stockType == 'PMA'){

                            self.users.stockTypeList = [
                                                                        {
                                                                        id: "1",
                                                                        version: 0,
                                                                        name: "PMA",
                                                                        description: "PMA",
                                                                        value: true
                                                                        },
                                                                        {
                                                                        id: "0",
                                                                        version: 0,
                                                                        name: "PMDN",
                                                                        description: "PMDN",
                                                                        value: false
                                                                        }
                                                                    ];
                        } else if(self.users.stockType == 'PMDN'){

                            self.users.stockTypeList = [
                                                                        {
                                                                        id: "1",
                                                                        version: 0,
                                                                        name: "PMA",
                                                                        description: "PMA",
                                                                        value: false
                                                                        },
                                                                        {
                                                                        id: "0",
                                                                        version: 0,
                                                                        name: "PMDN",
                                                                        description: "PMDN",
                                                                        value: true
                                                                        }
                                                                    ];
                        }
                        
                }
            });
    };
    self.changeValue = function (id,index) {
        angular.forEach(self.users.categoryList, function (notary) {
                if(notary.id != id){
                    notary.value = false;
                    if(id == 1){
                    self.users.category = true;
                    self.contentData.category = 'TERBUKA';
                    }
                    else{
                    self.users.category= false;
                    self.contentData.category = 'TERTUTUP';
                    }
                }
        });

    };
    self.changeValuePenanamanModal = function (id,index) {
        angular.forEach(self.users.stockTypeList, function (notary) {
                if(notary.id != id){
                    notary.value = false;
                    if(id == 1){
                    self.users.stockType = true;
                    self.contentData.stockType = 'PMA';
                    }
                    else{
                        self.users.stockType= false;
                    self.contentData.stockType = 'PMDN';
                    }
                }
        });

    };
    self.reject = function () {
        var request = {
            id: self.users.id,
            version : self.users.version,
            description : self.users.description
        };
        ResumeService.rejectRequestResume(request).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: '/views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Sukses',
                                    message: 'Resume Sukses di Reject',
                                    path: ''
                                };
                            }
                        }
                    });

        $location.path('/dashboard/approval-request-resume');
                }
                else {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            });
    };
    self.Modal=0;
    self.PemegangSaham=0;
    self.Anggaran=0;
    self.Pengurus=0;
    self.DataPendukung=0;
    self.doRetrieve = function(input){
        $location.path('/dashboard/matching-result').search({id: input});
    }
    self.addModal = function(index){
        if(self.users.stockList == null || self.users.stockList == undefined){
            self.users.stockList = [];
            var add = {"name":'',"nominal":'',"stockQuantity":''} 
            self.users.stockList.push(add);
        }
        else if(self.users.stockList != null && self.users.stockList[index] != undefined){
                
                if(self.users.stockList[index].name != '' && self.users.stockList[index].nominal != '' && self.users.stockList[index].stockQuantity != ''){
                        var add = {"name":'',"nominal":'',"stockQuantity":''}            
                        self.users.stockList.push(add);
                        self.Modal = index + 1;
                    }
        }
        else if (self.users.stockList[index] == undefined) {
            var add = {"name":'',"nominal":'',"stockQuantity":''} 
            self.users.stockList.push(add);
        }
    }
    self.addAnggaran = function(index){
        if(self.users.articleAssociationList == null || self.users.articleAssociationList == undefined){
            self.users.articleAssociationList = [];
            var add = {"certificateDate": '',"notary": '',"certificateNumber": '',"about": '',"approvalNumber": '',"approvalDate": ''}
            
            self.users.articleAssociationList.push(add);
        }
        else if(self.users.articleAssociationList != null && self.users.articleAssociationList[index] != undefined){
            if(self.users.articleAssociationList[index].certificateDate != '' && self.users.articleAssociationList[index].notary != '' && self.users.articleAssociationList[index].certificateNumber != '' && self.users.articleAssociationList[index].about != '' && self.users.articleAssociationList[index].approvalNumber != '' && self.users.articleAssociationList[index].approvalDate != ''){
            var add = {"certificateDate": '',"notary": '',"certificateNumber": '',"about": '',"approvalNumber": '',"approvalDate": ''}
            
                        self.users.articleAssociationList.push(add);
                        self.Anggaran = index + 1;
                    }
        }
        else if (self.users.articleAssociationList[index] == undefined) {
            var add = {"certificateDate": '',"notary": '',"certificateNumber": '',"about": '',"approvalNumber": '',"approvalDate": ''}
            
            self.users.articleAssociationList.push(add);
        }
    }
    self.addPemegangSaham = function(index){
        if(self.users.stockHolderList == null || self.users.stockHolderList == undefined){
            self.users.stockHolderList = [];
            var add = {"name":'',"stockQuantity":'',"stockNominal":''}
            self.users.stockHolderList.push(add);
        }
        else if (self.users.stockHolderList != null && self.users.stockHolderList[index] != undefined){
            if(self.users.stockHolderList[index].name != '' && self.users.stockHolderList[index].nominal != '' && self.users.stockHolderList[index].stockQuantity != ''){
                var add = {"name":'',"stockQuantity":'',"stockNominal":''}
                self.users.stockHolderList.push(add);
                self.PemegangSaham = index + 1;
            }
           
        }
        else if (self.users.stockHolderList[index] == undefined) {
            var add = {"name":'',"stockQuantity":'',"stockNominal":''}
            self.users.stockHolderList.push(add);
        }
    }
    self.addDataPendukung = function(index){
        if(self.users.additionalDataList == null || self.users.additionalDataList == undefined){
            console.log("pertama")
            self.users.additionalDataList = [];
            var add = {"documentDate": '',"documentPeriod": '',"documentNumber": '',"documentName": ''}
            self.users.additionalDataList.push(add);
        }
        else if (self.users.additionalDataList != null && self.users.additionalDataList[index] != undefined){
            console.log("nambah")
            console.log(self.users.additionalDataList[index])
            if(self.users.additionalDataList[index].documentDate != '' && self.users.additionalDataList[index].documentPeriod != '' && self.users.additionalDataList[index].documentNumber != ''&& self.users.additionalDataList[index].documentName != ''){    
            var add = {"documentDate": '',"documentPeriod": '',"documentNumber": '',"documentName": ''}
                self.users.additionalDataList.push(add);
                self.DataPendukung = index + 1;
            }
           
        }
        else if (self.users.additionalDataList[index] == undefined) {
            console.log("nambahdelete")
            var add = {"documentDate": '',"documentPeriod": '',"documentNumber": '',"documentName": ''}
            self.users.additionalDataList.push(add);
        }
    }
    self.addPengurus = function(index){
        if(self.users.directorList == null || self.users.directorList == undefined){
            self.users.directorList = [];
            var add = {"approver": false,"signer": false,"name": '',"position": '',"startDate": '',"endDate":''}
            self.users.directorList.push(add);
        }
        else if (self.users.directorList != null && self.users.directorList[index] != undefined){
            if(self.users.directorList[index].name != '' && self.users.directorList[index].position != '' && self.users.directorList[index].startDate != ''&& self.users.directorList[index].endDate != ''){       
            var add = {"approver": false,"signer": false,"name": '',"position": '',"startDate": '',"endDate":''}
                self.users.directorList.push(add);
                self.Pengurus = index + 1;
            }
           
        }
        else if (self.users.directorList[index] == undefined) {
            var add = {"approver": false,"signer": false,"name": '',"position": '',"startDate": '',"endDate":''}
            self.users.directorList.push(add);
        }
    }
    self.approve = function () {
        var request = {
            id: self.users.id,
            version : self.users.version ,
            description : self.users.description
        };
        ResumeService.rejectRequestResume(request).then(
function (response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Sukses',
                                    message: 'Resume Sukses di Approve',
                                    path: ''
                                };
                            }
                        }
                    });

        $location.path('/dashboard/approval-request-resume');
                    // self.bigTotalItems = response.elements;
                    // self.numPages = response.pages;
                }
                else {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            });
    };

    self.doUpdateUser = function (id) {
        $location.path('/dashboard/update-user').search({id: id});
    };

    self.doDeleteModal = function (id) {
        // console.log(id)
        self.users.stockList.splice(id,1);
        if(self.Modal != 0){
            self.Modal = self.Modal -1;
        }
        
    };
    self.doDeletePemegangSaham = function (id) {
        // console.log(id)
        self.users.stockHolderList.splice(id,1);
        if(self.PemegangSaham != 0){
            self.PemegangSaham = self.PemegangSaham -1;
        }
        
    };
    self.doDeleteAnggaran = function (id) {
        // console.log(id)
        self.users.articleAssociationList.splice(id,1);
        if(self.Anggaran != 0){
            self.Anggaran = self.Anggaran -1;
        }
        
    };
    self.doDeletePengurus = function (id) {
        // console.log(id)
        self.users.directorList.splice(id,1);
        if(self.Pengurus != 0){
            self.Pengurus = self.Pengurus -1;
        }
        
    };
    self.doDeleteDataPendukung = function (id) {
        // console.log(id)
        self.users.additionalDataList.splice(id,1);
        if(self.DataPendukung != 0){
            self.DataPendukung = self.DataPendukung -1;
        }
        
    };

    self.doSearch = function (type,value) {
        self.currentPage = 1;
        getListUser(type,value);
    };
    self.process = function(){
        angular.forEach(self.users.documentList, function (data) {
            // console.log(data)
            var request = {"resumeId" : self.users.id,
            "documentCode":data.code,
            "verified" : data.verified,
            "pendingDescription" : data.pendingDescription}
            // console.log(request)
            DokumenKelengkapanService.processFileKelengkapanDokumen(request).then(
                function(response){
                        // console.log(response)
                    if(response.message !== 'ERROR'){
                    }
            })
        });
        processResume();
    }

    
    self.pending = function(){
        angular.forEach(self.users.documentList, function (data) {
            // console.log(data)
            var request = {"resumeId" : self.users.id,
            "documentCode":data.code,
            "verified" : data.verified,
            "pendingDescription" : data.pendingDescription}
            // console.log(request)
            DokumenKelengkapanService.pendingFileKelengkapanDokumen(request).then(
                function(response){
                        console.log(response)
                    if(response.message !== 'ERROR'){
                    }
            })
        });
        pendingResume();
    }
    function processResume (){
        var request = {"id":self.users.id,"version":self.users.version};
        DokumenKelengkapanService.processResumeKelengkapanDokumen(request).then(
                function(response){
                    if(response.message !== 'ERROR'){

        $location.path('/dashboard/check-kelengkapan-document')
                    }

            })
    }
    function pendingResume (){
        var request = {"id":self.users.id,"version":self.users.version};
        DokumenKelengkapanService.pendingResumeKelengkapanDokumen(request).then(
                function(response){
                    if(response.message !== 'ERROR'){

        $location.path('/dashboard/check-kelengkapan-document')
                    }

            })
    }
    getListUser(paramId);
};