'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('inquiryHistoryResumeCtrl', [
        '$scope', '$modal', '$window',
        '$location',
        'FidusiaService',
        inquiryHistoryResumeCtrl
    ]);

function inquiryHistoryResumeCtrl($scope, $modal, $window, $location, FidusiaService) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;

    self.search = [
        { "keyName": "appId", "value": "APP ID" },
        { "keyName": "npwp", "value": "NPWP" },
        { "keyName": "companyName", "value": "Nama Perusahaan" }
    ];

    self.goToDetail = function(id) {
        $location.path('/dashboard/inquiry-history-resume-detail').search({ id: id });
    };

    var backToTop = function() {
        $window.scrollTo(0, 0);
    };

    var loadSearch = function () {
        backToTop();
        var indexFound = self.search.map(function(e) {
            return e.keyName;
        }).indexOf(self.search);
        self.itemSearch = self.search[0];
    };

    var getListOfRequestCancelResume = function(type, value) {


        var appId = '';
        var npwp = '';
        var companyName = '';
        if (type && value != undefined) {
            if (type == 'appId') {
                appId = value;
                npwp = '';
                companyName = '';
            } else if (type == 'npwp') {
                npwp = value;
                appId = '';
                companyName = '';
            } else if (type == 'companyName') {
                companyName = value;
                appId = '';
                npwp = '';
            }
        }

        FidusiaService.inquiryHistoryResumeList(self.currentPage - 1, self.limit, appId, npwp, companyName).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.listRequestResumes = response.result;
                }
            });
    };


    self.lovGroupPrivilege = function() {
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovGroupPrivilege.html',
            controller: 'modalGroupPrivilegeCtrl',
            size: 'md',
            resolve: {
                modalParam: function() {
                    return {
                        values: self.contentData.group
                    };
                }
            }
        });
        modalInstance.result.then(function(listGroup) {
            self.contentData.group = listGroup;
        });
    };

    self.deleteGroup = function(index) {
        self.contentData.group.splice(index, 1);
    };

    var invalidModal = function(message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function() {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };


    self.doSearch = function(type, value) {
        if(value==null){
            invalidModal("Field Pencarian Harus Diisi");
        }
        else{
            self.currentPage = 1;
            getListOfRequestCancelResume(type.keyName, value);
        }

    };

    loadSearch();

    // getListOfRequestCancelResume();

};