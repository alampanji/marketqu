'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('inquiryStatusResumeCtrl', [
        '$scope', '$modal',
        '$location',
        'ResumeService', 'CONF', '$sessionStorage', '$window',
        inquiryStatusResumeCtrl
    ]);

function inquiryStatusResumeCtrl($scope, $modal, $location, ResumeService, CONF, $sessionStorage, $window) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    self.input = {};
    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    var request = {
        "branchList": [],
        "branchCentralList": [],
        "groupList": []
    };
    $scope.exportDataExcel = function() {
        var accessToken = $sessionStorage.accessToken;
        var appId = '';
        var name = '';
        var branchId = '';
        var status = '';
        var branchCentralId = '';
        if (self.appId != undefined) {
            appId = self.appId;
        }
        if (self.name != undefined) {
            name = self.name;
        }
        if (self.branchId != undefined) {
            branchId = self.branchId;
        }
        if (self.status != undefined) {
            status = self.status;
        }
        if (self.branchCentralId != undefined) {
            branchCentralId = self.branchCentralId;
        }
        var urlConstructExcel = CONF.MASTER_PATH + CONF.URL_DOWNLOAD_INQUIRY_EXCEL + '?access_token=' + accessToken + '&appId=' + appId + '&name=' + name + '&branch=' + branchId + '&status=' + status + '&branchCentral=' + branchCentralId;
        $window.open(urlConstructExcel, '_blank');
    };
    $scope.exportDataPdf = function() {
        var accessToken = $sessionStorage.accessToken;
        var appId = '';
        var name = '';
        var branchId = '';
        var status = '';
        var branchCentralId = '';
        if (self.appId != undefined) {
            appId = self.appId;
        }
        if (self.name != undefined) {
            name = self.name;
        }
        if (self.branchId != undefined) {
            branchId = self.branchId;
        }
        if (self.status != undefined) {
            status = self.status;
        }
        if (self.branchCentralId != undefined) {
            branchCentralId = self.branchCentralId;
        }
        var urlConstructExcel = CONF.MASTER_PATH + CONF.URL_DOWNLOAD_INQUIRY_PDF + '?access_token=' + accessToken + '&appId=' + appId + '&name=' + name + '&branch=' + branchId + '&status=' + status + '&branchCentral=' + branchCentralId;
        $window.open(urlConstructExcel, '_blank');
    };
    self.arr = [];

    self.retrieveAppId = function() {
        ResumeService.getRetrieveResumeApp(self.contentData.appId).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.dataUser = response.result;
                    self.contentData.companyName = self.dataUser.companyName;
                    self.contentData.npwp = self.dataUser.npwp;
                }
            });
    };

    var backToTop = function() {
        $window.scrollTo(0, 0);
    };

    var getListOfDocumentSetting = function() {

        backToTop();
        // console.log(self.input)
        var appId = '';
        var name = '';
        var branchId = '';
        var status = '';
        var branchCentralId = '';
        if (self.appId != undefined) {
            appId = self.appId;
        }
        if (self.name != undefined) {
            name = self.name;
        }
        if (self.branchId != undefined) {
            branchId = self.branchId;
        }
        if (self.status != undefined) {
            status = self.status;
        }
        if (self.branchCentralId != undefined) {
            branchCentralId = self.branchCentralId;
        }
        console.log(self.status)
        ResumeService.inquiryStatusResume(self.currentPage - 1, self.limit, name, appId, branchId, status, branchCentralId).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.listDocument = response.result;
                }
            });
    };
    self.doSearch = function() {
        if (self.name == null && self.npwp == null && self.appId == null) {
            invalidModal("Parameter Search Mandatory Salah Satu");
        } else {
            self.currentPage = 1;
            getListOfDocumentSetting();
        }
    };
    self.getDetail = function(input, id) {
        if (input === 'REJECTED') {
            $modal.open({
                templateUrl: 'views/modal/Modal.html',
                controller: 'ModalCtrl',
                size: 'sm',
                backdrop: 'static',

                resolve: {
                    modalParam: function() {
                        return {
                            title: 'Alert',
                            message: 'Permintaan Anda ditolak karena data tidak lengkap',
                            // path: 'dashboard/setting-tax-notary'
                        };
                    }
                }
            });
        } else if (input === 'SUBMITTED') {

            $modal.open({
                templateUrl: 'views/modal/Modal.html',
                controller: 'ModalCtrl',
                size: 'sm',
                backdrop: 'static',

                resolve: {
                    modalParam: function() {
                        return {
                            title: 'Alert',
                            message: 'Permintaan Anda dibatalkan karena data tidak lengkap',
                            // path: 'dashboard/setting-tax-notary'
                        };
                    }
                }
            });
        } else if (input === 'Cancel') {

            $modal.open({
                templateUrl: 'views/modal/Modal.html',
                controller: 'ModalCtrl',
                size: 'sm',
                backdrop: 'static',

                resolve: {
                    modalParam: function() {
                        return {
                            title: 'Alert',
                            message: 'Permintaan Anda sedang dalam process validasi',
                            // path: 'dashboard/setting-tax-notary'
                        };
                    }
                }
            });
        } else if (input === 'Done') {

            $location.path('/dashboard/matching-result').search({ id: id });
        }
    }

    self.lovBranch = function() {
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovBranchRadio.html',
            controller: 'modalLovBranchRadioCtrl',
            size: 'md',
            resolve: {
                modalParam: function() {
                    return {
                        values: self.branch
                    };
                }
            }
        });
        modalInstance.result.then(function(listGroup) {
            self.branch = listGroup.name;
            self.branchId = listGroup.id;
        });
    };
    self.lovBranchCentral = function() {
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovBranchCentralRadio.html',
            controller: 'modalLovBranchCentralRadioCtrl',
            size: 'md',
            resolve: {
                modalParam: function() {
                    return {
                        values: self.branchCentral
                    };
                }
            }
        });
        modalInstance.result.then(function(listGroup) {
            self.branchCentral = listGroup.name;
            self.branchCentralId = listGroup.id;
        });
    };

    self.deleteGroup = function(index) {
        self.contentData.group.splice(index, 1);
    };

    var invalidModal = function(message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function() {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.addFile = function(ele, index) {
        var files = ele.files[0];
        // self.listDocument[index].file = files;
        console.log(self.arr);
        //console.log(self.listDocument);
    };

    // var doUploadFile = function () {
    //     // var file = self.myFile;
    //     // console.log(self.contentData);
    //     //
    //     // var fd = new FormData();
    //     // fd.append('file', file);
    //     for(var i=0; i<self.document.file.length; i++){
    //         console.log(i);
    //     }
    // };

    self.uploadFile = function() {

        var file = self.myFile;

        var fd = new FormData();
        fd.append('file', file);
        fd.append('contentData', angular.toJson(self.contentData));
        console.log(fd);
        console.log(self.listDocument);

        ResumeService.submitResume(fd).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    var result = response.result;
                    if (result.match == true) {
                        //$location.path('/dashboard/matching-result').search({id: result.resumeId});
                        for (var i = 0; i < self.listDocument.file.length; i++) {
                            console.log(i);
                        }
                    } else {
                        // console.log(self.listDocument);
                        //   doUploadFile();
                        var request = {};
                        for (var i = 0; i < self.listDocument.length; i++) {
                            // console.log(self.listDocument[i].files);
                            var fd = new FormData();
                            request.resumeId = result.resumeId;
                            request.documentCode = self.listDocument[i].code;
                            request.documentDesc = self.listDocument[i].name;
                            fd.append('file', self.listDocument[i].file);
                            fd.append('contentData', angular.toJson(request));
                            console.log(fd);
                            ResumeService.submitDocument(fd).then(
                                function(response) {
                                    if (response.message !== 'ERROR') {
                                        console.log("Uploaded");
                                    } else {
                                        self.isDisabled = false;
                                        $modal.open({
                                            templateUrl: 'views/modal/Modal.html',
                                            controller: 'ModalCtrl',
                                            size: 'sm',
                                            resolve: {
                                                modalParam: function() {
                                                    return {
                                                        title: 'Gagal',
                                                        message: response.result,
                                                        path: ''
                                                    };
                                                }
                                            }
                                        });
                                    }
                                }
                            );

                        }
                    }
                    // $modal.open({
                    //     templateUrl: 'views/modal/Modal.html',
                    //     controller: 'ModalCtrl',
                    //     size: 'sm',
                    //     resolve: {
                    //         modalParam: function () {
                    //             return {
                    //                 title: 'Informasi',
                    //                 message: 'User berhasil disubmit',
                    //                 path: ''
                    //             };
                    //         }
                    //     }
                    // });
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function() {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );

        // var hasil = checking();
        //
        //
        // if (hasil == true) {
        //     invalidModal("Ada yang tidak valid, mohon periksa kembali");
        // }else {
        //
        //     for(var i=0; i<self.contentData.branch.length; i++){
        //         var listId = {
        //             'code' : self.contentData.branch[i].code,
        //             'name'  : self.contentData.branch[i].name
        //         };
        //         request.branchList.push(listId);
        //     }
        //
        //     for(var i=0; i<self.contentData.centralBranch.length; i++){
        //         var listId = {
        //             'id' : self.contentData.centralBranch[i].id
        //         }
        //         request.branchCentralList.push(listId);
        //     }
        //
        //     for(var i=0; i<self.contentData.group.length; i++){
        //         var listId = {
        //             'id' : self.contentData.group[i].id
        //         }
        //         request.groupList.push(listId);
        //     }
        //
        //     request.nip = self.contentData.nip;
        //     request.fullName = self.contentData.fullName;
        //     request.username = self.contentData.username;
        //     request.email = self.contentData.email;
        //     request.division = self.contentData.division;
        //     request.position = self.contentData.position;
        //     request.firstName = "First Name";
        //     request.lastName = "Last Name";
        //     request.role = "BCA";
        //
        //     console.log(request);
        //
        //     UserService.createUserInternal(request).then(
        //         function (response) {
        //             if (response.message !== 'ERROR') {
        //                 $modal.open({
        //                     templateUrl: '/views/modal/Modal.html',
        //                     controller: 'ModalCtrl',
        //                     size: 'sm',
        //                     resolve: {
        //                         modalParam: function () {
        //                             return {
        //                                 title: 'Informasi',
        //                                 message: 'User berhasil ditambahkan',
        //                                 path: 'dashboard/security-admin'
        //                             };
        //                         }
        //                     }
        //                 });
        //             }else{
        //                 self.isDisabled = false;
        //                 $modal.open({
        //                     templateUrl: '/views/modal/Modal.html',
        //                     controller: 'ModalCtrl',
        //                     size: 'sm',
        //                     resolve: {
        //                         modalParam: function () {
        //                             return {
        //                                 title: 'Gagal',
        //                                 message: response.result,
        //                                 path: ''
        //                             };
        //                         }
        //                     }
        //                 });
        //             }
        //
        //         }
        //     );
        // }
    };

    // getListOfDocumentSetting();

};