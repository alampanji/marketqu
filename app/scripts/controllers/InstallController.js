'use strict';
/**
 * @ngdoc function
 * @name marketplaceApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the marketplaceApp
 */

angular.module('marketplaceApp')
    .controller('InstallCtrl', [
        '$scope', '$modal', '$filter',
        '$location',
        '$window',
        'InstallService',
        InstallCtrl
    ]);

function InstallCtrl($scope, $modal, $filter, $location, $window,InstallService) {
    var self = $scope;
    var contentData = {};

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var isNewUser = true;

    function checking() {
        var isEmpty = false;
        self.dbNameInvalid = false;
        self.dbUsernameInvalid = false;
        self.dbHostInvalid = false;
        if(self.dbName == '' || self.dbName == null){
            self.dbNameInvalid = true;
            isEmpty = true;
        }
        if(self.dbUsername == '' || self.dbUsername == null){
            self.dbUsernameInvalid = true;
            isEmpty = true;
        }
        if(self.dbHost == '' || self.dbHost == null){
            self.dbHostInvalid = true;
            isEmpty = true;
        }
        return isEmpty;
    }

    self.cancel = function () {
      self.dbName = '';
      self.dbUsername = '';
      self.dbPassword = '';
      self.dbHost = '';
    };



    function invalidModal(message) {
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: message,
                        path: ''
                    };
                }
            }
        });
    }

    self.submit = function () {

        var validation = checking();

        if(validation == true){
            invalidModal("Data is not valid, please check your input text");
        }
        else {
            contentData.database = self.dbName;
            contentData.username = self.dbUsername;
            if(self.dbPassword == undefined){
                contentData.password = '';
            }
            else{
                contentData.password = self.dbPassword;
            }

            contentData.hostname = self.dbHost;
            // contentData.action = 'database';

            InstallService.createDatabase(JSON.stringify(contentData)).then(
                function (response) {
                    console.log(response);
                    if (response.status === 200) {
                        if (response.status === 200) {
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Sukses',
                                            message: 'Sukses Setting Database',
                                            path: 'install/setting-account'
                                        };
                                    }
                                }
                            });
                        }
                        else{
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Gagal',
                                            message: response.message,
                                            path: ''
                                        };
                                    }
                                }
                            });
                        }
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.message,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        }
    };

    function statusChecking () {

        InstallService.getPing().then(
            function(response) {
                if (response.status === 'OK') {
                    console.log("true");
                    $location.path('user/home');
                }

            });

        
        // call service which return userStatus
        // if(!isNewUser){
            // $location.path('/login');

        // }
    }

    statusChecking();

    backToTop();

};
