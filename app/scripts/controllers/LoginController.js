/**
 * Created by panjialam on 5/10/16.
 */
angular.module('marketplaceApp')
    .controller('LoginCtrl', [
                '$scope',
                '$location',
                'authService',
                'messageService',
                '$modalInstance',
                'CONF',
                LoginCtrl]);

        function LoginCtrl($scope, $location, authService, messageService, $modalInstance, CONF) {

        var self = $scope;

        self.crendetial={
            "username":"",
            "password":""
        };
        self.username='';
        self.password='';
        // self.crendetial.method = 'login';
        self.gotoForgotPassword = gotoForgotPassword;
        self.doLogin = doLogin;

        function gotoForgotPassword() {
            $location.path('/forgot-password');
            $modalInstance.dismiss();
        }

        function doLogin(){
            //edit to push
                if(self.username=='' ||  self.password==''){
                    messageService.toasterMessage(CONF.TOASTER_TOP_CENTER,CONF.TOASTER_ERROR,'Please input username and password');
                }
                else{

                    self.crendetial={
                        "username":self.username,
                        "password":self.password
                    };

                    authService.login(JSON.stringify(self.crendetial)).then(
                        function (response) {
                            $modalInstance.dismiss('close');
                            if (response.response.status == 200) {
                                messageService.toasterMessage(CONF.TOASTER_TOP_CENTER,CONF.TOASTER_SUCCESS,"Successfully Login");
                                if(response.new_user){
                                    $location.path('/admin/profile');
                                }
                                else {
                                    $location.path('/admin/home');
                                }
                            }else{
                                messageService.toasterMessage(CONF.TOASTER_TOP_CENTER,CONF.TOASTER_ERROR,response.response.message);
                            }
                        }
                    );
                }


        }
    };

angular.module('ui.bootstrap.modal').directive('modalWindow', function ($timeout) {
    return {
        priority: 1,
        link: function (scope, element) {
            $timeout(function () {
                element.find('[autofocus]').focus();
            });
        }
    };
});