'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('matchingResultCtrl', [
        '$scope', '$modal', '$filter',
        '$location',
        'ResumeService',
        '$sessionStorage',
        'CONF',
        matchingResultCtrl
    ]);

function matchingResultCtrl($scope, $modal, $filter, $location, ResumeService, $sessionStorage, CONF) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    var paramValue = $location.search().id;

    var getDetailResume = function() {
        ResumeService.getDetailByResumeId(paramValue).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;

                    if (self.dataResult.method == 'INPUT') {
                        var indexFound = self.method.map(function(e) {
                            return e.keyName;
                        }).indexOf(self.method);
                        self.itemSearch = self.method[0];
                    } else {
                        var indexFound = self.method.map(function(e) {
                            return e.keyName;
                        }).indexOf(self.method);
                        self.itemSearch = self.method[1];
                    }

                    var access_token = $sessionStorage.accessToken;

                    for (var j = 0; j < self.dataResult.documentList.length; j++) {

                        var urlConstruct = CONF.MASTER_PATH + CONF.URL_DOWNLOAD_DOCUMENT + '?access_token=' + access_token + '&resumeId=' + self.dataResult.id + '&docType=' + self.dataResult.documentList[j].documentCode;

                        self.dataResult.documentList[j].url = urlConstruct
                    }

                    var urlAhuConstruct = CONF.MASTER_PATH + CONF.URL_DOWNLOAD_RESUME + '?access_token=' + access_token + '&resumeId=' + self.dataResult.id;
                    self.dataResult.resumeAhuUrl = urlAhuConstruct;


                    var urlnpwpConstruct = CONF.MASTER_PATH + CONF.URL_DOWNLOAD_NPWP + '?access_token=' + access_token + '&resumeId=' + self.dataResult.id;
                    self.dataResult.npwpUrl = urlnpwpConstruct;
                }
            });
    };

    self.method = [
        { "keyName": "INPUT", "value": "Input" },
        { "keyName": "UPLOAD", "value": "Upload" },
    ];




    var constructSelect = function() {
        var indexFound = self.method.map(function(e) {
            return e.keyName;
        }).indexOf(self.method);
        self.itemSearch = self.method[0];
    };

    constructSelect();


    self.lovGroupPrivilege = function() {
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovGroupPrivilege.html',
            controller: 'modalGroupPrivilegeCtrl',
            size: 'md',
            resolve: {
                modalParam: function() {
                    return {
                        values: self.contentData.group
                    };
                }
            }
        });
        modalInstance.result.then(function(listGroup) {
            self.contentData.group = listGroup;
        });
    };

    self.deleteGroup = function(index) {
        self.contentData.group.splice(index, 1);
    };

    var invalidModal = function(message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function() {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });
    };

    self.showModalBack = function() {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function() {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses matching resume?',
                        path: 'dashboard/request-and-matching-resume'
                    };
                }
            }
        });
    };

    self.tab = 1;

    self.setTab = function(tabId) {
        self.tab = tabId;
    };

    self.isSet = function(tabId) {
        return self.tab === tabId;
    };

    self.process = function() {
        if (self.moDescription == null) {
            invalidModal("Keterangan MO wajib di isi!");
        } else {
            var request = {};
            request.id = paramValue;
            request.moDescription = self.moDescription;
            ResumeService.processSubmitResumeMatch(request).then(
                function(response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function() {
                                    return {
                                        title: 'Informasi',
                                        message: 'User berhasil ditambahkan',
                                        path: 'dashboard/request-and-matching-resume'
                                    };
                                }
                            }
                        });
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function() {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        }
    };

    getDetailResume();

};