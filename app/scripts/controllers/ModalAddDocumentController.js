angular.module('nostraApp')
    .controller('modalLovAddDocumentCtrl', [
        '$scope',
        '$modalInstance',
        '$modal',
        '$location',
        'modalParam',
        'DokumenKelengkapanService',
        modalLovAddDocumentCtrl]);

function modalLovAddDocumentCtrl($scope, $modalInstance, $modal, $location, modalParam, DokumenKelengkapanService) {

    var self = $scope;

    self.accessLov = modalParam.tipe;

    self.close = close;

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    var search_mdId = modalParam.search_mdId;
    var domain = "bbOwner";
    var listOfBranch = modalParam.values;

    self.clearCode = function() {
        self.searchCode = null;
        document.getElementById("searchCode").focus();
    };

    self.clearName = function() {
        self.searchName = null;
        document.getElementById("searchName").focus();
    };

    self.pageChanged = function () {
        storeData();
        modalLOV();
    };

    var lovBranch = function () {
        // var code = '';
        // var name = '';
        // if(codeSearch!=undefined && nameSearch!=undefined){
        //     code = codeSearch;
        //     name = nameSearch;
        // }
        // else if(codeSearch!=undefined){
        //     code = codeSearch;
        //     name = '';
        // }
        // else if(nameSearch!=undefined){
        //     name = nameSearch;
        //     code = '';
        // }
        DokumenKelengkapanService.getListOfDocumentSetting().then(
                    function (response) {
                        if (response.message !== 'ERROR') {
                            console.log(listOfBranch)
                                if(listOfBranch == undefined){
                                    console.log("1");
                                self.dataResult = response.result;
                                for(var i=0;i<self.dataResult.length;i++){
                                    self.dataResult[i].privileges = [
                                                                        {
                                                                        id: "1",
                                                                        version: 0,
                                                                        name: "yes",
                                                                        description: "Yes",
                                                                        value: false
                                                                        },
                                                                        {
                                                                        id: "0",
                                                                        version: 0,
                                                                        name: "no",
                                                                        description: "No",
                                                                        value: false
                                                                        }
                                                                    ];
                                }
                                }
                                else {

                                    console.log("2");
                                // console.log(id)
                                // console.log(listOfBranch)
                                        // console.log(self.dataResult)
                                        
                                self.dataResult = response.result;

                                for(var i=0;i<self.dataResult.length;i++){
                                    self.dataResult[i].delete = true;
                                    self.dataResult[i].privileges = [
                                                                        {
                                                                        id: "1",
                                                                        version: 0,
                                                                        name: "yes",
                                                                        description: "Yes",
                                                                        value: false
                                                                        },
                                                                        {
                                                                        id: "0",
                                                                        version: 0,
                                                                        name: "no",
                                                                        description: "No",
                                                                        value: false
                                                                        }
                                                                    ];
                                    for(var j=0;j<listOfBranch.length;j++){
                                        // console.log(listOfBranch[j])
                                        // console.log(self.dataResult[i].privileges)
                                        

                                        if(self.dataResult[i].code == listOfBranch[j].code){
                                        // console.log(i)
                                        self.dataResult.splice(i,1);

                                        }

                                    }
                                    // console.log(self.dataResult)

                                }
                                }
                        }
                    });
        // BranchService.getlistBranch(self.currentPage-1, self.limit).then(
        //     function (response) {
        //         if (response.message !== 'ERROR') {
        //             self.dataResult = response.result;
        //             self.bigTotalItems = response.elements;
        //             self.numPages = response.pages;

        //             for(var i=0; i<self.dataResult.length; i++){
        //                 self.dataResult[i].value = false;
        //             }
        //             checkExistValue();
        //         }
        //     });
    };

    var storeData = function () {
        console.log(listOfBranch);
        if(listOfBranch!=undefined) {
            angular.forEach(self.dataResult, function(branch){
                if (branch.value) {
                    var found = listOfBranch.map(function (e) {
                        return e.code;
                    }).indexOf(branch.code);
                    if (found == -1) {
                        console.log(branch)
                        listOfBranch.push(branch);
                    }
                }
            });
        }
        else{
            listOfBranch=[];
            angular.forEach(self.dataResult, function(branch){
                if (branch.value) {
                    listOfBranch.push(branch);
                }
            });
        }

    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.checkedAll = function() {
        if(self.branch){
            for(var i=0; i<self.dataResult.length; i++){
                self.dataResult[i].value = false;
                self.branch = false;
            }

        }
        else{
            for(var i=0; i<self.dataResult.length; i++) {
                self.dataResult[i].value = true;
                self.branch = true;
            }

        }
    };

    self.getnotary = function (index) {

    };
    self.check =function(){
        
    }
    self.checkedChild = function (value){
        var check = true;
        if(!value){
            self.branch = false;
        }
        else{
            for(var i=0; i<self.dataResult.length; i++) {
                if(!self.dataResult[i].value){
                    check = false;
                }

            }
            self.branch=check;
        }
    };

    self.addBranch = function () {

        for(var i=0; i<self.dataResult.length; i++){
            if(self.dataResult[i].value==true && self.dataResult[i].description==null){
                invalidModal("Deskripsi Tidak Boleh Kosong");
                break;
            }
            else {
                storeData();
                $modalInstance.close(listOfBranch);
            }
        }

        // storeData();
        // $modalInstance.close(listOfBranch);
    };

    var checkExistValue = function () {
        if(listOfBranch!=undefined) {
            angular.forEach(self.dataResult, function (branch) {
                var found = listOfBranch.map(function (e) {
                    return e.code;
                }).indexOf(branch.code);
                if (found != -1) {
                    branch.value = true;
                }
            });
        }

    };

    function close() {
        $modalInstance.dismiss('close');
        if(modalParam.path){
            $location.path(modalParam.path);
        }
    };

    var modalLOV = function(){
        self.judul = "List Dokumen";
        lovBranch();
    };

    modalLOV();

};
