/**
 * Created by derryaditiya on 5/11/16.
 */
angular.module('marketplaceApp')
    .controller('ModalCategoryCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        '$modal',
        'ProductService',
        'CONF',
        ModalCategoryCtrl]);

function ModalCategoryCtrl($scope, $modalInstance, $location, modalParam, $modal, ProductService, CONF) {

    var self = $scope;

    self.modalParam = modalParam;

    self.close = close;

    var paramCategory = modalParam.category;

    function close() {
        $modalInstance.dismiss('close');
        if(self.modalParam.path){

            if(self.modalParam.path == 'reload'){
                window.location.reload();
            }
            else{
                $location.path(self.modalParam.path);
            }

        }
    }
    
    function getCategory() {
        ProductService.getListCategory().then(function (response) {
            if(response.status == 'OK'){
                self.category = response.data;
                self.background = [];
                angular.forEach(self.category, function (data) {
                    data.category.img = CONF.IMAGE_PATH + CONF.PATH_FILES_CATEGORY + data.category.image;
                    data.isClicked = false;
                    data.background = {
                        "cursor":"pointer",
                        "background":"url("+data.category.img+")",
                        "background-repeat":"no-repeat",
                        "background-size": "cover",
                        "background-position": "center",
                        "height": "150px",
                        "padding": "5px"
                    };
                    self.background.push(data.background);
                });
            }
        });
    }

    // self.background = function (image) {
    //      var background = {
    //          "cursor":"pointer",
    //          "background-":"url("+image+")",
    //         // "background":"blue"
    //      };
    //     return background;
    // };

    self.backgroundColor = [];

    self.addBackground = function (index) {
        self.backgroundColor[index] = true;
    };

    self.removeBackground = function (index) {
        if(!self.category[index].isClicked) {
            self.backgroundColor[index] = false
        }
    };

    self.storeCategory = function (index) {

        for(var i = 0; i<self.category.length; i++){
            if(self.category[i].isClicked == true){
                self.category[i].isClicked = false;
                self.backgroundColor[i] = false;

            }
        }

        self.category[index].isClicked = true;
        paramCategory = self.category[index];
        self.backgroundColor[index] = true;
    };

    self.doSearch = function () {
        $modalInstance.close(paramCategory);
        console.log(paramCategory);
    };

    self.removeClass = function () {
        self.style = {
            "cursor":"default",
            "background-color":"none"
            // "background":"blue"
        };
    }

    self.getOpacity = function () {
    }

    getCategory();
};
