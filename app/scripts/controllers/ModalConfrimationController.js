'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the marketplaceApp
 */

angular.module('marketplaceApp')
    .controller('modalConfirmationCtrl', [
        '$scope', 'modalParam', '$modalInstance', '$location',
        modalConfirmationCtrl
    ]);
function modalConfirmationCtrl($scope, modalParam , $modalInstance, $location) {

    var self = $scope;
    self.close = close;
    self.confirm = confirm;
    self.modalParam = modalParam;

    self.message = modalParam.message;
    function close() {
        $modalInstance.dismiss('no');
    }

    function confirm() {
        $modalInstance.dismiss('yes');
        if(self.modalParam.path){
            $location.path(self.modalParam.path);
        }
    }


};