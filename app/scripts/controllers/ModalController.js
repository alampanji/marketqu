/**
 * Created by derryaditiya on 5/11/16.
 */
angular.module('marketplaceApp')
    .controller('ModalCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        '$modal',
        ModalCtrl]);

function ModalCtrl($scope, $modalInstance, $location, modalParam, $modal) {

    var self = $scope;

    self.modalParam = modalParam;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
        if(self.modalParam.path){

            if(self.modalParam.path == 'reload'){
                window.location.reload();
            }
            else{
                if(self.modalParam.paramSearch != undefined){
                    $location.path(self.modalParam.path).search({id:self.modalParam.paramSearch});
                }
                else{

                    $location.path(self.modalParam.path);
                    
                }
            }

        }
    }
};
