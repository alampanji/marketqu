/**
 * Created by derryaditiya on 5/11/16.
 */
angular.module('nostraApp')
    .controller('ModalDelMasaBerlakuCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        'MasaBerlakuService','$modal',
        ModalDelMasaBerlakuCtrl]);

function ModalDelMasaBerlakuCtrl($scope, $modalInstance, $location, modalParam, MasaBerlakuService,$modal) {

    var self = $scope;

    self.modalParam = modalParam;
    // console.log(modalParam);

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
        
    }

    self.gotoContactUs = function () {
        $location.path('/article/hubungi-kami');
        $modalInstance.dismiss('close');
    };

    self.confirm = function(){
        var request = {
            "id": modalParam.id
        };
        MasaBerlakuService.deleteMasaBerlaku(request).then(
            function(response){
                $modalInstance.close('close');

                if(response.message !== 'ERROR'){
                    $modal.open({
                        templateUrl:'views/modal/Modal.html',
                        controller:'ModalCtrl',
                        size:'sm',
                        backdrop:'static',

                        resolve: {
                            modalParam: function(){
                                return {
                                    title:'Informasi',
                                    message: 'Masa Berlaku Telah Terhapus',
                                    // path: 'dashboard/setting-tax-notary'
                                };
                            }
                        }
                    });
                }
                else{
                    $modal.open({
                        templateUrl:'views/modal/Modal.html',
                        controller:'modalCtrl',
                        size:'sm',
                        backdrop:'static',

                        resolve: {
                            modalParam: function(){
                                return {
                                    title:'Gagal',
                                    message: response.result
                                };
                            }
                        }
                    });
                }
            }
        );
    }
};
