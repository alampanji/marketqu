/**
 * Created by derryaditiya on 5/11/16.
 */
angular.module('nostraApp')
    .controller('deleteTaxCtrl', [
        '$scope',
        '$modalInstance',
        '$modal',
        '$location',
        'modalParam',
        'TaxService',
        deleteTaxCtrl]);

function deleteTaxCtrl($scope, $modalInstance, $modal, $location, modalParam, TaxService) {

    var self = $scope;

    self.modalParam = modalParam;
    var modelpassing = modalParam;
    self.taxId = modelpassing.id;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
        
    }

    self.gotoContactUs = function () {
        $location.path('/article/hubungi-kami');
        $modalInstance.dismiss('close');
    };

    self.deleteTax = function(){
        var request = {
            "id":self.taxId
        };

        TaxService.deleteTax(request).then(
            function(response){
                $modalInstance.dismiss('close');

                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Informasi',
                                    message: 'Tax telah terhapus',
                                    path: ''
                                };
                            }
                        }
                    });
                    $location.path('dashboard/setting-tax-notary');
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

                // if(response.message !== 'ERROR'){
                //     $modal.open({
                //         templateUrl:'/views/modal/Modal.html',
                //         controller:'modalCtrl',
                //         size:'sm',
                //         backdrop:'static',
                //
                //         resolve: {
                //             modalParam: function(){
                //                 return {
                //                     title:'Informasi',
                //                     message: 'Tax Telah Terhapus',
                //                     path: 'dashboard/setting-tax-notary'
                //                 };
                //             }
                //         }
                //     });
                // }
                // else{
                //     $modal.open({
                //         templateUrl:'/views/modal/Modal.html',
                //         controller:'modalCtrl',
                //         size:'sm',
                //         backdrop:'static',
                //
                //         resolve: {
                //             modalParam: function(){
                //                 return {
                //                     title:'Gagal',
                //                     message: response.result
                //                 };
                //             }
                //         }
                //     });
                // }
            }
        );
    }
};
