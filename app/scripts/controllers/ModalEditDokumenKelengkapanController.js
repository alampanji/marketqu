/**
 * Created by derryaditiya on 5/11/16.
 */
angular.module('nostraApp')
    .controller('ModalEditDokumenKelengkapanCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        'DokumenKelengkapanService','$modal',
        ModalEditDokumenKelengkapanCtrl]);

function ModalEditDokumenKelengkapanCtrl($scope, $modalInstance, $location, modalParam, DokumenKelengkapanService,$modal) {

    var self = $scope;

    self.modalParam = modalParam;
    // console.log(modalParam);

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
        
    }

    self.gotoContactUs = function () {
        $location.path('/article/hubungi-kami');
        $modalInstance.dismiss('close');
    };

    self.modalParam.display=='RESUME'?self.modalParam.display=true:self.modalParam.display=false;

    self.confirm = function(){
        var display;

        modalParam.display==true? display='RESUME' : display='RESUME_TAMBAHAN';

        var request = {
            "id": modalParam.id,
            "version":modalParam.version + 1,
            "name":modalParam.name,
            "fileType": modalParam.fileType,
            "mandatory": modalParam.mandatory,
            "display": display,
            "defaultFlag": modalParam.defaultFlag,
            "maxSize":modalParam.maxSize
        };
        DokumenKelengkapanService.editDocKelengkapan(request).then(
            function(response){
                $modalInstance.close('close');

                if(response.message !== 'ERROR'){
                    $modal.open({
                        templateUrl:'views/modal/Modal.html',
                        controller:'ModalCtrl',
                        size:'sm',
                        backdrop:'static',

                        resolve: {
                            modalParam: function(){
                                return {
                                    title:'Informasi',
                                    message: 'Dokumen Telah Terupdate',
                                    // path: 'dashboard/setting-tax-notary'
                                };
                            }
                        }
                    });
                }
                else{
                    $modal.open({
                        templateUrl:'views/modal/Modal.html',
                        controller:'ModalCtrl',
                        size:'sm',
                        backdrop:'static',

                        resolve: {
                            modalParam: function(){
                                return {
                                    title:'Gagal',
                                    message: response.result
                                };
                            }
                        }
                    });
                }
            }
        );
    }
};
