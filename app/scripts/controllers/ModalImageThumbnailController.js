/**
 * Created by derryaditiya on 5/11/16.
 */
angular.module('marketplaceApp')
    .controller('ModalImageThumbnailCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        '$modal',
        'ProductService',
        'CONF',
        ModalImageThumbnailCtrl]);

function ModalImageThumbnailCtrl($scope, $modalInstance, $location, modalParam, $modal, ProductService, CONF) {

    var self = $scope;

    self.modalParam = modalParam;
    self.image = self.modalParam.message;
    self.index = self.modalParam.index;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
        if(self.modalParam.path){

            if(self.modalParam.path == 'reload'){
                window.location.reload();
            }
            else{
                $location.path(self.modalParam.path);
            }

        }
    }

    self.goToNext = function (index){
        self.index = index % self.image.length;
        if(self.index == 0){
            self.index = 1;
        }

    };

    self.goToPrev = function (index){
        self.index = index % self.image.length;
        if(self.index == 0){
            self.index = self.image.length-1;
        }
    };

};
