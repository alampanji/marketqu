angular.module('nostraApp')
    .controller('modalLovBranchCentralCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        'BranchCentralService',
        modalLovBranchCentralCtrl]);

function modalLovBranchCentralCtrl($scope, $modalInstance, $location, modalParam, BranchCentralService) {

    var self = $scope;

    self.accessLov = modalParam.tipe;

    self.close = close;

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;

    var listOfBranchCentral = modalParam.values;

    self.clearText = function() {
        self.searchName = null;
        document.getElementById("myField").focus();
    };
    self.pageChanged = function () {
        storeData();
        modalLOV();
    };

    var lovBranchCentral = function (searchValue) {
        var name = '';
        if(searchValue!=undefined){
            name = searchValue;
        }
        BranchCentralService.getlistBranchCentral(self.currentPage-1, self.limit, name).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                    for(var i=0; i<self.dataResult.length; i++){
                        self.dataResult[i].value = false;
                    }
                    checkExistValue();
                }
            });
    };

    var storeData = function () {
        if(listOfBranchCentral!=undefined) {
            angular.forEach(self.dataResult, function(branchCentral){
                if (branchCentral.value) {
                    var found = listOfBranchCentral.map(function (e) {
                        return e.id;
                    }).indexOf(branchCentral.id);
                    if (found == -1) {
                        listOfBranchCentral.push(branchCentral);
                    }
                }
            });
        }
        else{
            listOfBranchCentral=[];
            angular.forEach(self.dataResult, function(branchCentral){
                if (branchCentral.value) {
                    listOfBranchCentral.push(branchCentral);
                }
            });
        }

    };
    
    self.getDomainName = function(domainName){
        console.log(domainName);
        $modalInstance.close(domainName);
    };

    self.checkedAll = function() {
        if(self.branchCentral){
            for(var i=0; i<self.dataResult.length; i++){
                self.dataResult[i].value = false;
                self.branchCentral = false;
            }
        }
        else{
            for(var i=0; i<self.dataResult.length; i++) {
                self.dataResult[i].value = true;
                self.branchCentral = true;
            }

        }
    };


    self.checkedChild = function (value){
        var check = true;
        if(!value){
            self.branchCentral = false;
        }
        else{
            for(var i=0; i<self.dataResult.length; i++) {
                if(!self.dataResult[i].value){
                    check = false;
                }

            }
            self.branchCentral=check;
        }

    };

    self.addBranchCentral = function () {
        storeData();
        $modalInstance.close(listOfBranchCentral);
    };

    var checkExistValue = function () {
        if(listOfBranchCentral!=undefined) {
            angular.forEach(self.dataResult, function (branchCentral) {
                var found = listOfBranchCentral.map(function (e) {
                    return e.id;
                }).indexOf(branchCentral.id);
                if (found != -1) {
                    branchCentral.value = true;
                }
            });
        }

    };

    function close() {
        $modalInstance.dismiss('close');
        if(modalParam.path){
            $location.path(modalParam.path);
        }
    };

    var modalLOV = function(){
        self.judul = "List Cabang Sentral";
        lovBranchCentral();
    };

    self.findValue = function (value) {
        lovBranchCentral(value);
    };

    modalLOV();

};
