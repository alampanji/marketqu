angular.module('nostraApp')
    .controller('modalLovBranchCentralCreateUserCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        'BranchCentralService',
        modalLovBranchCentralCreateUserCtrl]);

function modalLovBranchCentralCreateUserCtrl($scope, $modalInstance, $location, modalParam, BranchCentralService) {

    var self = $scope;
    self.accessLov = modalParam.tipe;
    self.close = close;

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;

    var listOfBranchCentral = modalParam.values;

    self.clearText = function() {
        self.searchName = null;
        document.getElementById("myField").focus();
    };
    self.pageChanged = function () {
        storeData();
        modalLOV();
    };

    // console.log(listOfBranchCentral)
    var lovBranchCentral = function (searchValue) {
        var name = '';
        if(searchValue!=undefined){
            name = searchValue;
        }

        BranchCentralService.getlistBranchCentral(self.currentPage-1, self.limit, name).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                    // for(var i=0; i<self.dataResult.length; i++){
                    //     self.dataResult[i].value = false;
                    // }
                    angular.forEach(self.dataResult, function (notary) {
                        // console.log(notary.name)
                        // console.log(listOfBranchCentral)
                        if(listOfBranchCentral != undefined && notary.id == listOfBranchCentral.id){
                            notary.value = true;
                        }else{
                            notary.value = false;
                        }
                    });
                }
            });
    };

    var storeData = function () {
        if(listOfBranchCentral!=undefined) {
            angular.forEach(self.dataResult, function(branchCentral){
                if (branchCentral.value) {
                    var found = listOfBranchCentral.map(function (e) {
                        return e.id;
                    }).indexOf(branchCentral.id);
                    if (found == -1) {
                        listOfBranchCentral.push(branchCentral);
                    }
                }
            });
        }
        else{
            listOfBranchCentral=[];
            angular.forEach(self.dataResult, function(branchCentral){
                if (branchCentral.value) {
                    listOfBranchCentral.push(branchCentral);
                }
            });
        }

    };

    self.getDomainName = function(domainName){
        console.log(domainName);
        $modalInstance.close(domainName);
    };

    self.checkedAll = function() {
        if(self.branchCentral){
            for(var i=0; i<self.dataResult.length; i++){
                self.dataResult[i].value = false;
                self.branchCentral = false;
            }

        }
        else{
            for(var i=0; i<self.dataResult.length; i++) {
                self.dataResult[i].value = true;
                self.branchCentral = true;
            }

        }
    };


    self.checkedChild = function (value){
        var check = true;
        if(!value){
            self.branchCentral = false;
        }
        else{
            for(var i=0; i<self.dataResult.length; i++) {
                if(!self.dataResult[i].value){
                    check = false;
                }

            }
            self.branchCentral=check;
        }

    };

    self.changeValue = function (id) {
        // self.dataResult[index].value=true;
        // console.log(id)
        angular.forEach(self.dataResult, function (notary) {
            // console.log(notary)
            if (notary.id != id) {
                notary.value = false;
            }
        });

    };
    // self.addBranchCentral = function () {
    //     storeData();
    //     $modalInstance.close(listOfBranchCentral);
    // };

    self.addBranchCentral = function () {
        angular.forEach(self.dataResult, function (notary) {
            if (notary.value) {
                notaris = notary;
            }
        });
        $modalInstance.close(notaris);
    };
    var checkExistValue = function () {
        if(listOfBranchCentral!=undefined) {
            angular.forEach(self.dataResult, function (branchCentral) {
                var found = listOfBranchCentral.map(function (e) {
                    return e.id;
                }).indexOf(branchCentral.id);
                if (found != -1) {
                    branchCentral.value = true;
                }
            });
        }

    };

    self.findValue = function (value) {
        lovBranchCentral(value);
    };

    function close() {
        $modalInstance.dismiss('close');
        if(modalParam.path){
            $location.path(modalParam.path);
        }
    };

    var modalLOV = function(){
        self.judul = "List Cabang Sentral";
        self.lovName = "mdName";
        self.lovId = "mdId";
        lovBranchCentral();
    };

    modalLOV();

};
