
angular.module('nostraApp').filter('pagination', function()
{
    return function(input, start)
    {
        start = +start;
        console.log("input: " +input);
        return input.slice(start);
    };
});


angular.module('nostraApp')
    .controller('modalLovBranchCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        'BranchService',
        modalLovBranchCtrl]);


function modalLovBranchCtrl($scope, $modalInstance, $location, modalParam, BranchService) {

    var self = $scope;

    self.accessLov = modalParam.tipe;

    self.close = close;

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    $scope.curPage = 1;// current Page
    self.pageSize = 5;
    var search_mdId = modalParam.search_mdId;
    var domain = "bbOwner";
    var listOfBranch = modalParam.values;

    self.clearCode = function() {
        self.searchCode = null;
        document.getElementById("searchCode").focus();
    };

    self.clearName = function() {
        self.searchName = null;
        document.getElementById("searchName").focus();
    };

    self.pageChanged = function () {
        storeData();
        modalLOV();
    };

    var lovBranch = function (codeSearch,nameSearch) {
        var code = '';
        var name = '';
        if(codeSearch!=undefined && nameSearch!=undefined){
            code = codeSearch;
            name = nameSearch;
        }
        else if(codeSearch!=undefined){
            code = codeSearch;
            name = '';
        }
        else if(nameSearch!=undefined){
            name = nameSearch;
            code = '';
        }

        BranchService.getlistBranch(self.currentPage-1, self.limit, name,code).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;
                    //
                    // self.bigTotalItems = response.elements;
                    // self.numPages = response.pages;
                    //



                    self.itemsPerPage = 10;
                    self.currentPage = 0;

                    for(var i=0; i<self.dataResult.length; i++){
                        self.dataResult[i].value = false;
                    }

                    $scope.range = function() {
                        var rangeSize = 4;
                        var ps = [];
                        var start;

                        start = self.currentPage;
                        if ( start > self.pageCount()-rangeSize ) {
                            start = self.pageCount()-rangeSize+1;
                        }

                        for (var i=start; i<start+rangeSize; i++) {
                            ps.push(i);
                        }
                        return ps;
                    };

                    $scope.prevPage = function() {
                        if (self.currentPage > 0) {
                            self.currentPage--;
                        }
                    };

                    self.DisablePrevPage = function() {
                        return self.currentPage === 0 ? "disabled" : "";
                    };

                    self.pageCount = function() {
                        return Math.ceil(self.dataResult.length/self.itemsPerPage)-1;
                    };

                    self.nextPage = function() {
                        if (self.currentPage < $scope.pageCount()) {
                            self.currentPage++;
                        }
                    };

                    self.DisableNextPage = function() {
                        return self.currentPage === self.pageCount() ? "disabled" : "";
                    };

                    self.setPage = function(n) {
                        $scope.currentPage = n;
                    };

                    checkExistValue();
                }
            });
    };

    self.pageSize = 1;

    self.numberOfPages = function() {
        return Math.ceil(self.dataResult.length / self.pageSize);
    };

    var storeData = function () {
        if(listOfBranch!=undefined) {
            angular.forEach(self.dataResult, function(branch){
                if (branch.value) {
                    var found = listOfBranch.map(function (e) {
                        return e.code;
                    }).indexOf(branch.code);
                    if (found == -1) {
                        listOfBranch.push(branch);
                    }
                }
            });
        }
        else{
            listOfBranch=[];
            angular.forEach(self.dataResult, function(branch){
                if (branch.value) {
                    listOfBranch.push(branch);
                }
            });
        }

    };

    self.checkedAll = function() {
        if(self.branch){
            for(var i=0; i<self.dataResult.length; i++){
                self.dataResult[i].value = false;
                self.branch = false;
            }

        }
        else{
            for(var i=0; i<self.dataResult.length; i++) {
                self.dataResult[i].value = true;
                self.branch = true;
            }

        }
    };

    self.getnotary = function (index) {

    };

    self.checkedChild = function (value){
        var check = true;
        if(!value){
            self.branch = false;
        }
        else{
            for(var i=0; i<self.dataResult.length; i++) {
                if(!self.dataResult[i].value){
                    check = false;
                }

            }
            self.branch=check;
        }
    };

    self.addBranch = function () {
        storeData();
        $modalInstance.close(listOfBranch);
    };

    var checkExistValue = function () {
        if(listOfBranch!=undefined) {
            angular.forEach(self.dataResult, function (branch) {
                var found = listOfBranch.map(function (e) {
                    return e.code;
                }).indexOf(branch.code);
                if (found != -1) {
                    branch.value = true;
                }
            });
        }

    };

    function close() {
        $modalInstance.dismiss('close');
        if(modalParam.path){
            $location.path(modalParam.path);
        }
    };

    var modalLOV = function(){
        self.judul = "List Cabang";
        lovBranch();
    };

    self.findValue = function (searchCode,searchName) {
        lovBranch(searchCode,searchName);
    };

    modalLOV();

};
