angular.module('nostraApp')
    .controller('modalLovBranchRadioCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        'BranchCentralService',
        modalLovBranchRadioCtrl
    ]);

function modalLovBranchRadioCtrl($scope, $modalInstance, $location, modalParam, BranchService) {

    var self = $scope;
    self.accessLov = modalParam.tipe;
    self.close = close;

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;

    var listOfBranch = modalParam.values;

    self.clearText = function() {
        self.searchName = null;
        document.getElementById("myField").focus();
    };
    self.pageChanged = function() {
        storeData();
        modalLOV();
    };

    // console.log(listOfBranchCentral)
    var lovBranchCentral = function(searchValue) {
        var name = '';
        if (searchValue != undefined) {
            name = searchValue;
        }

        BranchService.getlistBranch(self.currentPage - 1, self.limit, name).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                    angular.forEach(self.dataResult, function(notary) {
                        // console.log(notary.name)
                        // console.log(listOfBranchCentral)
                        if (listOfBranch != undefined && notary.name == listOfBranchCentral) {
                            notary.value = true;
                        } else {
                            notary.value = false;
                        }
                    });
                }
            });
    };

    var storeData = function() {
        if (listOfBranch != undefined) {
            angular.forEach(self.dataResult, function(branchCentral) {
                if (branchCentral.value) {
                    var found = listOfBranchCentral.map(function(e) {
                        return e.id;
                    }).indexOf(branchCentral.id);
                    if (found == -1) {
                        listOfBranch.push(branchCentral);
                    }
                }
            });
        } else {
            listOfBranchCentral = [];
            angular.forEach(self.dataResult, function(branchCentral) {
                if (branchCentral.value) {
                    listOfBranchCentral.push(branchCentral);
                }
            });
        }

    };

    self.getDomainName = function(domainName) {
        console.log(domainName);
        $modalInstance.close(domainName);
    };

    self.checkedAll = function() {
        if (self.branchCentral) {
            for (var i = 0; i < self.dataResult.length; i++) {
                self.dataResult[i].value = false;
                self.branchCentral = false;
            }

        } else {
            for (var i = 0; i < self.dataResult.length; i++) {
                self.dataResult[i].value = true;
                self.branchCentral = true;
            }

        }
    };


    self.checkedChild = function(value) {
        var check = true;
        if (!value) {
            self.branchCentral = false;
        } else {
            for (var i = 0; i < self.dataResult.length; i++) {
                if (!self.dataResult[i].value) {
                    check = false;
                }

            }
            self.branchCentral = check;
        }

    };

    self.changeValue = function(id) {
        // self.dataResult[index].value=true;
        // console.log(id)
        angular.forEach(self.dataResult, function(notary) {
            // console.log(notary)
            if (notary.id != id) {
                notary.value = false;
            }
        });

    };
    // self.addBranchCentral = function () {
    //     storeData();
    //     $modalInstance.close(listOfBranchCentral);
    // };

    self.addBranch = function() {
        angular.forEach(self.dataResult, function(notary) {
            if (notary.value) {
                notaris = notary;
            }
        });
        $modalInstance.close(notaris);
    };
    var checkExistValue = function() {
        if (listOfBranch != undefined) {
            angular.forEach(self.dataResult, function(branchCentral) {
                var found = listOfBranch.map(function(e) {
                    return e.id;
                }).indexOf(branchCentral.id);
                if (found != -1) {
                    branchCentral.value = true;
                }
            });
        }

    };

    self.findValue = function(value) {
        lovBranchCentral(value);
    };

    function close() {
        $modalInstance.dismiss('close');
        if (modalParam.path) {
            $location.path(modalParam.path);
        }
    };

    var modalLOV = function() {
        self.judul = "List Cabang Sentral";
        self.lovName = "mdName";
        self.lovId = "mdId";
        lovBranchCentral();
    };

    modalLOV();

};