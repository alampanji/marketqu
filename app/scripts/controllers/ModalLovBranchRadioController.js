angular.module('nostraApp')
    .controller('modalLovBranchRadioCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        'BranchService',
        modalLovBranchRadioCtrl
    ]);

function modalLovBranchRadioCtrl($scope, $modalInstance, $location, modalParam, BranchService) {

    var self = $scope;

    self.accessLov = modalParam.tipe;

    self.close = close;

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    var requests = [];
    var search_mdId = modalParam.search_mdId;
    var domain = "bbOwner";
    var listOfBranch = modalParam.values;

    self.clearText = function() {
        self.search.searchName = null;
        document.getElementById("myField").focus();
    };
    self.pageChanged = function() {
        storeData();
        modalLOV();
    };

    var lovBranch = function() {
        var code = '';
        var name = '';

        BranchService.getlistBranch(self.currentPage - 1, self.limit, name, code).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                    angular.forEach(self.dataResult, function(notary) {
                        if (listOfBranch != undefined && notary.name == listOfBranch.name) {
                            notary.value = true;
                        } else {
                            notary.value = false;
                        }
                    });
                }
            });
    };

    var storeData = function() {
        if (listOfBranch != undefined) {
            angular.forEach(self.dataResult, function(branch) {
                if (branch.value) {
                    var found = listOfBranch.map(function(e) {
                        return e.code;
                    }).indexOf(branch.code);
                    if (found == -1) {
                        listOfBranch.push(branch);
                    }
                }
            });
        } else {
            listOfBranch = [];
            angular.forEach(self.dataResult, function(branch) {
                if (branch.value) {
                    listOfBranch.push(branch);
                }
            });
        }

    };

    self.checkedAll = function() {
        if (self.privilege) {
            for (var i = 0; i < self.dataResult.length; i++) {
                self.dataResult[i].value = false;
                self.branch = false;
            }

        } else {
            for (var i = 0; i < self.dataResult.length; i++) {
                self.dataResult[i].value = true;
                self.branch = true;
            }

        }
    };

    self.getnotary = function(index) {

    };

    self.checkedChild = function(value) {
        var check = true;
        if (!value) {
            self.branch = false;
        } else {
            for (var i = 0; i < self.dataResult.length; i++) {
                if (!self.dataResult[i].value) {
                    check = false;
                }

            }
            self.branch = check;
        }
    };

    self.changeValue = function(id) {
        // self.dataResult[index].value=true;
        angular.forEach(self.dataResult, function(notary) {
            if (notary.id != id) {
                notary.value = false;
            }
        });

    };
    // self.addBranchCentral = function () {
    //     storeData();
    //     $modalInstance.close(listOfBranchCentral);
    // };

    self.addBranch = function() {
        angular.forEach(self.dataResult, function(notary) {
            if (notary.value) {
                notaris = notary;
            }
        });
        $modalInstance.close(notaris);
    };

    var checkExistValue = function() {
        if (listOfBranch != undefined) {
            angular.forEach(self.dataResult, function(branch) {
                var found = listOfBranch.map(function(e) {
                    return e.code;
                }).indexOf(branch.code);
                if (found != -1) {
                    branch.value = true;
                }
            });
        }

    };

    function close() {
        $modalInstance.dismiss('close');
        if (modalParam.path) {
            $location.path(modalParam.path);
        }
    };

    var modalLOV = function() {
        self.judul = "List Cabang";
        lovBranch();
    };

    modalLOV();

};