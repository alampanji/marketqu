angular.module('marketplaceApp')
    .controller('modalGroupPrivilegeCtrl', [
        '$scope',
        'GroupService',
        '$modalInstance',
        '$location',
        'modalParam',
        '$sessionStorage',
        'CONF',
        modalGroupPrivilegeCtrl]);

function modalGroupPrivilegeCtrl($scope, GroupService, $modalInstance, $location, modalParam,$sessionStorage, CONF) {

    var self = $scope;

    self.accessLov = modalParam.tipe;
    var requests = [];

    self.close = close;
    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    var listOfPrivilege = modalParam.values;



    self.clearText = function() {
        self.searchName = null;
        document.getElementById("myField").focus();
    };
    self.pageChanged = function () {

        storeData();
        modalLOV();
    };

    var listGroupPrivilege = function (searchName) {
        var user=0;
        var name = '';
        var code = '';
        if(searchName!=undefined){
            name = searchName;
        }

         if($sessionStorage.domain==CONF.USER_NOTARY){
             user = 1;
         }

        GroupService.getListGroup(self.currentPage-1, self.limit, code, name, user).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.privileges = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                    for(var i=0; i<self.privileges.length; i++){
                        self.privileges[i].value = false;
                    }
                    checkExistValue();
                }
            });
    };

    var storeData = function () {
        if(listOfPrivilege!=undefined) {
            angular.forEach(self.privileges, function(privilege){
                if (privilege.value) {
                    var found = listOfPrivilege.map(function (e) {
                        return e.code;
                    }).indexOf(privilege.code);
                    if (found == -1) {
                        listOfPrivilege.push(privilege);
                    }
                }
            });
        }
        else{
            listOfPrivilege=[];
            angular.forEach(self.privileges, function(privilege){
                if (privilege.value) {
                    listOfPrivilege.push(privilege);
                }
            });
        }

    };

    self.listPrivilege=false;

    self.checkedAll = function() {
        if(self.privilege){
            for(var i=0; i<self.privileges.length; i++){
                self.privileges[i].value = false;
                self.privilege = false;
            }

        }
        else{
            for(var i=0; i<self.privileges.length; i++) {
                self.privileges[i].value = true;
                self.privilege = true;
            }

        }
    };

    self.checkedChild = function (value){
        var check = true;
        if(!value){
            self.privilege = false;
        }
        else{
            for(var i=0; i<self.privileges.length; i++) {
               if(!self.privileges[i].value){
                    check = false;
               }

            }
            self.privilege=check;
        }

    };
    
    self.addPrivilege = function () {
        storeData();
        $modalInstance.close(listOfPrivilege);
    };
    
    var checkExistValue = function () {
        if(listOfPrivilege!=undefined) {
            angular.forEach(self.privileges, function (privilege) {
                var found = listOfPrivilege.map(function (e) {
                    return e.code;
                }).indexOf(privilege.code);
                if (found != -1) {
                    privilege.value = true;
                }
            });
        }

    };

    self.getDomainName = function(domainName){
        $modalInstance.close(domainName);
    };

    function close() {
        $modalInstance.dismiss('close');
        if(modalParam.path){
            $location.path(modalParam.path);
        }
    };

    var modalLOV = function(){
        self.judul = "List Group Privilege";
        listGroupPrivilege();
    };

    self.findValue = function (value) {
        listGroupPrivilege(value);
    };

    modalLOV();

};
