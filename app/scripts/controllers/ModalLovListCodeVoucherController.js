angular.module('nostraApp')
    .controller('modalLovListCodeVoucherCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        'FidusiaService',
        modalLovListCodeVoucherCtrl]);

function modalLovListCodeVoucherCtrl($scope, $modalInstance, $location, modalParam, FidusiaService) {

    var self = $scope;

    self.accessLov = modalParam.tipe;

    self.close = close;

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.search = {
        searchName : modalParam.search_name
    };

    var notaris;
    var listOfNotary = modalParam.values;

    self.clearText = function() {
        self.search.searchName = null;
        document.getElementById("myField").focus();
    };
    self.pageChanged = function () {
        modalLOV();
    };

    var lovVoucher = function (searchValue) {

        var code = '';
        if(searchValue!=undefined){
            code = searchValue;
        }

        console.log(code);

        FidusiaService.getVoucherList(self.currentPage-1, self.limit,code).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                    angular.forEach(self.dataResult, function (notary) {
                        if(listOfNotary != undefined && notary.code == listOfNotary.code){
                             notary.value = true;
                        }else{
                             notary.value = false;
                        }
                    });
                }
            });
    };

    self.changeValue = function (id) {
        angular.forEach(self.dataResult, function (notary) {
            if (notary.id != id) {
                notary.value = false;
            }
        });
    };

    self.addNotary = function () {
        angular.forEach(self.dataResult, function (notary) {
            if (notary.value) {
                notaris = notary;
            }
        });
        $modalInstance.close(notaris);
    };

    self.clearText = function() {
        self.searchName = null;
        document.getElementById("myField").focus();
    };


    function close() {
        $modalInstance.dismiss('close');
        if(modalParam.path){
            $location.path(modalParam.path);
        }
    };

    var modalLOV = function(){
        self.judul = "List Kode Voucher";
        lovVoucher();
    };

    self.findValue = function (value) {
        lovVoucher(value);
    };

    modalLOV();

};
