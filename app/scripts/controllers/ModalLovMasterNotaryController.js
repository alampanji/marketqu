angular.module('nostraApp')
    .controller('modalLovMasterNotaryCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        'ParameterNotaryService',
        modalLovMasterNotaryCtrl]);

function modalLovMasterNotaryCtrl($scope, $modalInstance, $location, modalParam, ParameterNotaryService) {

    var self = $scope;

    self.accessLov = modalParam.tipe;

    self.close = close;

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;

    self.clearCode = function() {
        self.searchCode = null;
        document.getElementById("searchCode").focus();
    };

    self.clearName = function() {
        self.searchName = null;
        document.getElementById("searchName").focus();
    };

    var notaris;

    var listOfNotary = modalParam.values;

    self.clearText = function() {
        self.search.searchName = null;
        document.getElementById("myField").focus();
    };
    self.pageChanged = function () {
        modalLOV();
    };

    var code = '';
    var name = '';

    var lovNotary = function (codeSearch,nameSearch) {


        if(codeSearch!=undefined && nameSearch!=undefined){
            code = codeSearch;
            name = nameSearch;
        }
        else if(codeSearch!=undefined){
            code = codeSearch;
            name = '';
        }
        else if(nameSearch!=undefined){
            name = nameSearch;
            code = '';
        }

        ParameterNotaryService.getAllNotary(self.currentPage-1, self.limit, name, code).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                }
            });
    };

    self.changeValue = function (id) {
        angular.forEach(self.dataResult, function (notary) {
            if (notary.notaryId != id) {
                notary.value = false;
            }
        });
    };

    self.addNotary = function () {
        angular.forEach(self.dataResult, function (notary) {
            if (notary.value) {
                notaris = notary;
            }
        });
        $modalInstance.close(notaris);
    };

    // self.addNotary = function () {
    //     $modalInstance.close(notaris);
    // };

    function close() {
        $modalInstance.dismiss('close');
        if(modalParam.path){
            $location.path(modalParam.path);
        }
    };

    self.findValue = function (searchCode,searchName) {
        // self.currentPage = 1;
        // console.log(searchCode);
        // console.log(searchName);
        lovNotary(searchCode,searchName);
    };

    var modalLOV = function(){
        self.judul = "List Data Master Notary";
        self.lovName = "mdName";
        self.lovId = "mdId";
        lovNotary();
    };

    modalLOV();

};
