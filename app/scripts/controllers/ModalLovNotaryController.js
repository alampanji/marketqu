angular.module('nostraApp')
    .controller('modalLovNotaryCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        'ParameterNotaryService',
        modalLovNotaryCtrl]);

function modalLovNotaryCtrl($scope, $modalInstance, $location, modalParam, ParameterNotaryService) {

    var self = $scope;

    self.accessLov = modalParam.tipe;

    self.close = close;

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.search = {
        searchId : modalParam.searchId,
        searchName : modalParam.search_name
    };

    var notaris;

    var listOfNotary = modalParam.values;

    self.clearText = function() {
        self.searchName = null;
        document.getElementById("myField").focus();
    };

    self.pageChanged = function () {
        modalLOV();
    };

    var lovNotary = function (searchValue) {
        var name = '';
        if(searchValue!=undefined){
            name = searchValue;
        }

        ParameterNotaryService.getListNotary(self.currentPage-1, self.limit,name).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                    angular.forEach(self.dataResult, function (notary) {
                        if(listOfNotary != undefined && notary.notaryId == listOfNotary.notaryId){
                            notary.value = true;
                        }else{
                            notary.value = false;
                        }
                    });
                }
            });
    };

    self.findValue = function (value) {
        lovNotary(value);
    };
    
    self.changeValue = function (id) {
        angular.forEach(self.dataResult, function (notary) {
            if (notary.notaryId != id) {
                notary.value = false;
            }
        });

    };

    self.addNotary = function () {
        angular.forEach(self.dataResult, function (notary) {
            if (notary.value) {
                notaris = notary;
            }
        });
        $modalInstance.close(notaris);
    };
    
    function close() {
        $modalInstance.dismiss('close');
        if(modalParam.path){
            $location.path(modalParam.path);
        }
    };

    var modalLOV = function(){
        self.judul = "List Notary";
        self.lovName = "mdName";
        self.lovId = "mdId";
        lovNotary();
    };

    modalLOV();

};
