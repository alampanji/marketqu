/**
 * Created by derryaditiya on 5/11/16.
 */
angular.module('nostraApp')
    .controller('ModalRejectApproveCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        'ResumeService','$modal',
        ModalRejectApproveCtrl]);

function ModalRejectApproveCtrl($scope, $modalInstance, $location, modalParam, ResumeService,$modal) {

    var self = $scope;


    var modelpassing = modalParam;
    self.id = modelpassing.id;
    self.version = modelpassing.version;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');

    }

    self.confirm = function(){
        var request = {
            "id": modalParam.id,
            "version": modalParam.version,
            "description": self.cancelDescription
        };
        ResumeService.cancelResumeRejectApprove(request).then(
            function(response){
                $modalInstance.close('close');

                if(response.message !== 'ERROR'){
                    $modal.open({
                        templateUrl:'views/modal/Modal.html',
                        controller:'ModalCtrl',
                        size:'sm',
                        backdrop:'static',

                        resolve: {
                            modalParam: function(){
                                return {
                                    title:'Informasi',
                                    message: 'Berhasil kirim keterangan cancel approve resume'
                                };
                            }
                        }
                    });
                }
                else{
                    $modal.open({
                        templateUrl:'views/modal/Modal.html',
                        controller:'modalCtrl',
                        size:'sm',
                        backdrop:'static',

                        resolve: {
                            modalParam: function(){
                                return {
                                    title:'Gagal',
                                    message: response.result
                                };
                            }
                        }
                    });
                }
            }
        );
    }
};
