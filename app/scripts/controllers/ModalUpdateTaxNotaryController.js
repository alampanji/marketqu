'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('nostraApp')
    .controller('modalCreateTaxNotaryCtrl', [
        '$scope','modalParam', '$modalInstance', '$modal','TaxService',
        modalCreateTaxNotaryCtrl
    ]);

function modalCreateTaxNotaryCtrl($scope ,modalParam, $modalInstance, $modal, TaxService) {

    var self = $scope;
    self.close = close;
    self.confirm = confirm;

    self.message = modalParam.message;
    self.judul='Add Tax';
    self.currentpage = 1;
    self.entrylimit = 5;


    function close (){
        $modalInstance.dismiss('no');
    }
    function confirm(){
        $modalInstance.dismiss('yes');
    }

    self.contentData={
        "minAmount"  : "",
        "maxAmount"  : "",
        "npwp"        :"",
        "nonNpwp"     : ""
    };


    /* //inisialisasi status field kosong
     var contentData=function(){
     self.minAmountInvalid=false;
     self.maxAmountInvalid=false;
     self.npwpInvalid=false;
     self.nonNpwpInvalid=false;
     //Mengecek kondisi ketika field kosong
     var boolean = false;
     if(self.contentData.npwp === null){
     self.npwpInvalid = true;
     boolean=true;
     }else{
     self.contentData.npwp = parseFloat(self.contentData.npwp);
     }

     if(self.contentData.nonNpwp === null){
     self.nonNpwpInvalid = true;
     boolean=true;
     }else{
     self.contentData.npwp = parseFloat(self.contentData.npwp);
     }

     if(self.contentData.minAmount === null){
     self.minAmountInvalid = true;
     boolean=true;
     }
     if(self.contentData.maxAmount === null){
     self.maxAmountInvalid = true;
     boolean=true;
     }

     return boolean;
     };
     */


    var invalidModal = function(message){
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size    : 'sm',
            resolve : {
                modalParam: function(){
                    return{
                        title: 'Peringatan',
                        message: message,
                    };
                }
            }
        });
    }

    self.submit=function(){
        if(self.contentData !== null){
            console.log(self.contentData);
            TaxService.createTax(self.contentData).then(
                function(response){
                    console.log(self.contentData);
                    if(response.message !== 'ERROR'){
                        console.log("sukses");
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            backdrop: 'static',

                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Tax telah berhasil ditambahkan',
                                        path: ''
                                    };
                                }
                            }
                        });
                        $location.path('dashboard/setting-tax-notary');
                    }else{
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            backdrop: 'static',

                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: 'dashboard/setting-tax-notary'
                                    };
                                }
                            }
                        });
                    }
                }
            );
        }else{
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }
    }

};