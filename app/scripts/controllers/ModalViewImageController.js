/**
 * Created by derryaditiya on 5/11/16.
 */
angular.module('marketplaceApp')
    .controller('ModalViewImageCtrl', [
        '$scope',
        '$modalInstance',
        '$location',
        'modalParam',
        '$modal',
        ModalViewImageCtrl]);

function ModalViewImageCtrl($scope, $modalInstance, $location, modalParam, $modal) {

    var self = $scope;

    self.modalParam = modalParam;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
        if(self.modalParam.path){
            $location.path(self.modalParam.path);
        }
    }
};
