'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('orderResumeAhuDetailCtrl', [
        '$scope','CONF','$location','$modal','FidusiaService','$sessionStorage','$window',
        orderResumeAhuDetailCtrl
    ]);

function orderResumeAhuDetailCtrl($scope,CONF,$location,$modal,FidusiaService, $sessionStorage,$window) {
    var self = $scope;

    self.close = close;

    var paramId = $location.search().id;
    function close() {
        $modalInstance.dismiss('close');
    }
    self.goToDetail = function(id){
        $location.path('/dashboard/approval-request-resume-detail').search({id:id});

    };

    self.action = [
        {"keyName" : "ORDER_AHU", "value":"Order AHU"},
        {"keyName" : "INTERNAL", "value":"Internal"},
        {"keyName" : "UPDATE", "value":"Update"},
        {"keyName" : "FINISH", "value":"Finish"}
    ];


    self.toMatchingResult = function (id) {
        $location.path('/dashboard/matching-result').search({id: id});
    };

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.status = [
        {keyName: 'true', name: 'Aktif'},
        {keyName: 'false', name: 'Tidak Aktif'}
    ];

    self.pageChanged = function () {
        getListUser();
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var getListResume = function (paramId) {
        backToTop();
        FidusiaService.orderResumeDetail(paramId).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.users = response.result;
                    var access_token = $sessionStorage.accessToken;
                    var indexFound = self.action.map(function (e) {
                        return e.keyName;
                    }).indexOf(self.action);
                    self.users.action = self.action[0];

                    for (var j=0; j< self.users.documentList.length;j++){

                        var urlConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_DOCUMENT +'?access_token='+access_token+'&resumeId='+self.users.id+'&docType='+self.users.documentList[j].documentCode;

                        self.users.documentList[j].url = urlConstruct
                    }
                }
            });
    };

    var checking = function () {
        self.descriptionInvalid = false;

        var bool = false;

        if (self.users.description == null) {
            self.descriptionInvalid = true;
            bool = true;
        }
        return bool;
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.doRetrieve = function(input){
        $location.path('/dashboard/matching-result').search({id: input});
    };

    self.process = function () {

        var hasil = checking();

        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }

        else{
            var request = {
                id: self.users.id,
                version : self.users.version ,
                description : self.users.description,
                action : self.users.action.keyName,
            };

            FidusiaService.processOrderResume(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Sukses',
                                        message: 'Berhasil Order Resume AHU',
                                        path: ''
                                    };
                                }
                            }
                        });
                        $location.path('/dashboard/order-resume-ahu');
                    }
                    else {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                });
        }


    };

    getListResume(paramId);
};