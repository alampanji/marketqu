'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('PaymentMethodCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'localStorageService', 'messageService', '$timeout', 'PaymentService', 'CartService',
        PaymentMethodCtrl
    ]);

function PaymentMethodCtrl($scope, CONF, $location, $modal, $window, localStorageService, messageService, $timeout, PaymentService, CartService) {
    var self = $scope;

    self.close = close;

    
    self.checkOutItems = [];

    self.deliveryFee;
    self.courier;
    self.nameReseller;
    self.addressReseller;


    // console.log(localStorageService.cookie.get(CONF.DELIVERY_COURIER));

    function close() {
        $modalInstance.dismiss('close');
    }

    self.orderCode = localStorageService.cookie.get(CONF.ORDER_CODE);

    function getItems(){
        if(localStorageService.get(CONF.CART) != undefined){
            self.checkOutItems = angular.copy(localStorageService.get(CONF.CART));
            self.deliveryFee = localStorageService.cookie.get(CONF.DELIVERY_COST);
            self.courier = localStorageService.cookie.get(CONF.DELIVERY_COURIER);
            self.deliveryDetail = localStorageService.cookie.get(CONF.DELIVERY_DETAIL);
            self.shippingData = angular.copy(localStorageService.cookie.get(CONF.SHIPPING_DATA));
            self.custId = localStorageService.cookie.get(CONF.dataProfile).id;
            self.nameReseller = self.deliveryDetail.first_name + " " + self.deliveryDetail.last_name;
            self.addressReseller = self.deliveryDetail.address;

        }
    }

    self.totalPrice = function () {
        var total = 0;
        for(var i=0; i<self.checkOutItems.length; i++){
            total += self.checkOutItems[i].price_low *  self.checkOutItems[i].quantity;
        }
        return total;
    };

    self.goChangePassword = function () {

        $location.path('/dashboard/change-password');

    };

    self.goProfile = function () {

        $location.path('/dashboard/profile');

    };

    self.goToDashboard = function () {
        $location.path('/admin/home');
    };

    self.goToHome= function () {
        $location.path('/user/home');
    };

    self.goToProduct= function () {
        $location.path('/product');
    };

    self.showModalMap = function () {
        $location.path('/article/map');
    };

    self.banner = 'img/banner.jpeg';

    // self.bannerContainer = {
    //     "width" : "100%",
    //     "background": "url('img/banner.jpeg')"
    //     "height": "200px"
    // };

    self.chartIsActive = false;
    self.chartNotNull = false;

    self.showCart = function () {
        self.chartIsActive = true;
        if(self.cartProduct.length > 0){
            self.chartNotNull = true;
        }
    };

    self.hideCart = function () {
        self.chartIsActive = false;
    };

    // function constructCart() {
    //
    //     localStorageService.cookie.remove(CONF.CART);
    //
    //     self.cartProduct = angular.copy(productToCart);
    //
    //     if(self.cartProduct != undefined){
    //
    //         self.cartProductItem = 0;
    //         self.totalPrice = 0;
    //         for(var i=0; i<self.cartProduct.length; i++){
    //             self.totalPrice += getPrice(self.cartProduct[i].quantity,self.cartProduct[i].price);
    //             self.cartProductItem += self.cartProduct[i].quantity;
    //         }
    //         self.showCart();
    //     }
    //
    // }


    function getPrice(quantity, price) {
        return quantity * price;
    }

    // constructCart();

    var productToCart = [];
    self.cartProduct = [];

    self.products = [
        {
            price:400000,
            img:'/img/pisang.jpg',
            name:'Produk A',
            id:'1',
            produsen:'Produsen C'
        },
        {
            price:500000,
            img:'/img/pisang.jpg',
            name:'Produk B',
            id:'112',
            produsen:'Produsen X'
        },
        {
            price:10000,
            img:'/img/pisang.jpg',
            name:'Produk C',
            id:'11',
            produsen:'Produsen X'
        },
        {
            price:10000,
            img:'/img/pisang.jpg',
            name:'Produk D',
            id:'2',
            produsen:'Produsen Y'
        },
        {
            price:5000,
            img:'/img/pisang.jpg',
            name:'Produk E',
            id:'9',
            produsen:'Produsen X'
        },
        {
            price:9000,
            img:'/img/pisang.jpg',
            name:'Produk F',
            id:'10',
            produsen:'Produsen Z'
        },
        {
            price:6500,
            img:'/img/pisang.jpg',
            name:'Produk X',
            id:'19',
            produsen:'Produsen A'
        }
    ];

    self.produsenLabel = function (index) {
        self.products[index].status = true;
    };

    self.gotToCheckOut = function () {
        localStorageService.set(CONF.CART, self.cartProduct);
        $location.path('/checkout');
    };


    self.produsenLabelHide = function (index) {
        self.products[index].status = false;
    };

    self.addToCart = function (product) {

        if(productToCart.length < 1){
            product.quantity = 1;
            productToCart.push(product);
            // localStorageService.cookie.set(CONF.CART, productToCart);
            messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_SUCCESS,'Berhasil menambahkan ' + product.name + ' ke keranjang belanja');
            constructCart();
        }

        else{
            var next = true;
            for(var i=0; i<productToCart.length; i++){
                if(productToCart[i].id == product.id){
                    productToCart[i].quantity = productToCart[i].quantity+1;
                    next = false;
                }
            }
            if(next){
                product.quantity = 1;
                productToCart.push(product);
            }

            localStorageService.set(CONF.CART, productToCart);
            messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_SUCCESS,'Berhasil menambahkan ' + product.name + ' ke keranjang belanja');
            constructCart();
        }
    };

    // function getCourier() {
    //     self.shippingType = [];
    //
    //     angular.forEach(localStorageService.cookie.get(CONF.DELIVERY_COST), function (data) {
    //             angular.forEach(data.costs, function (dataShipping) {
    //                 self.shippingType.push(dataShipping);
    //             })
    //
    //     });
    //
    //     self.courier = localStorageService.cookie.get(CONF.DELIVERY_COURIER);
    //     self.itemShipping = self.shippingType[0];
    //
    //     if(self.courier == 'pos'){
    //         self.courier == 'pos indonesi'
    //     }
    //    // console.log(self.shippingType);
    // }
    
    
    self.sendPayment = function() {

        var request = {
            data_product:[]
        };

        angular.forEach(localStorageService.get(CONF.CART), function (cart) {
            // console.log(cart);
            var dataProduct = {
                "id_product":cart.id,
                "qty":cart.quantity,
                "subtotal":cart.quantity*cart.price_low
            };
            request.data_product.push(dataProduct);
        });
        console.log(self.shippingData);
        request.total = self.totalPrice()+self.deliveryFee;
        request.id_customer = self.custId;
        request.notes = self.shippingData.notes;
        request.shipping_fee_detail = self.deliveryDetail;
        request.shipping_fee = self.deliveryFee;
        request.shipping_date = self.shippingData.shipping_date;
        request.shipping_type = self.courier.toUpperCase();
        request.data_shipping = self.shippingData;
        console.log(request);
        if(localStorageService.get(CONF.CART) != undefined){
            PaymentService.checkOutProcess(JSON.stringify(request)).then(
                function(response) {
                    if (response.status == 'OK') {
                        localStorageService.cookie.remove(CONF.DELIVERY_COST);
                        localStorageService.cookie.remove(CONF.DELIVERY_COURIER);
                        localStorageService.remove(CONF.CART);
                        localStorageService.cookie.remove(CONF.DELIVERY_DETAIL);
                        localStorageService.remove(CONF.SHIPPING_DATA);
                        $window.location.href = response.url;
                        // $location.path('/payment-method');
                    }
                    else{
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Failed',
                                        message: response.message,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                });
        }
        else{
            $modal.open({
                templateUrl: 'views/modal/Modal.html',
                controller: 'ModalCtrl',
                size: 'sm',
                resolve: {
                    modalParam: function () {
                        return {
                            title: 'Failed',
                            message: 'Your cart is empty' ,
                            path: 'user/home'
                        };
                    }
                }
            });
        }
    };

    getItems();

    //getCourier();
};