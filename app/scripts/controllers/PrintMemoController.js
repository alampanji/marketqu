'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('printMemoCtrl', [
        '$scope','$location','$modal','CONF', 'UserService',
        printMemoCtrl
    ]);

function printMemoCtrl ($scope,$location,$modal,CONF, UserService) {

    var self = $scope;
    var paramValue = $location.search().id;
    console.log(paramValue);
    var request = {
        "branchList" : [],
        "branchCentralList": [],
        "groupList": []
    };

    self.status = [
        {keyName: '0', name: 'Tidak Aktif'},
        {keyName: '1', name: 'Aktif'},
        {keyName: '2', name: 'Lock'}
    ];

    var detailUser = function () {
        UserService.getDetailUserInternal(paramValue).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.user = response.result;
                }
            }
        );
    };

    self.lovBranch = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovBranch.html',
            controller: 'modalLovBranchCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.user.branchList
                    };
                }
            }
        });
        modalInstance.result.then(function (listBranch) {
            self.user.branchList = listBranch;
        });
    };

    self.deleteBranch = function(index){
        self.user.branchList.splice(index,1);
    }

    // self.lovBranch = function(){
    //     var modalInstance = $modal.open({
    //         templateUrl: '/views/modal/ModalLovBranch.html',
    //         controller: 'modalLovBranchCtrl',
    //         size: 'md',
    //         resolve: {
    //             modalParam: function () {
    //                 return {
    //                     values: self.branch
    //                 };
    //             }
    //         }
    //     });
    //     modalInstance.result.then(function (listBranch) {
    //         self.user.branch = listBranch;
    //     });
    // };
    //
    // self.deleteBranch = function(index){
    //     self.user.branch.splice(index,1);
    // }

    self.lovBranchCentral = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovBranchCentral.html',
            controller: 'modalLovBranchCentralCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.user.branchCentralList
                    };
                }
            }
        });
        modalInstance.result.then(function (listBranchCentral) {
            self.user.branchCentralList = listBranchCentral;
        });
    };

    self.deleteBranchCentral = function(index){
        self.user.branchCentralList.splice(index,1);
    };

    self.lovGroupPrivilege = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovGroupPrivilege.html',
            controller: 'modalGroupPrivilegeCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.user.groupList
                    };
                }
            }
        });
        modalInstance.result.then(function (listGroup) {
            self.user.groupList = listGroup;
        });
    };

    self.deleteGroup = function(index){
        self.user.groupList.splice(index,1);
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses ubah user?',
                        path: 'dashboard/security-admin'
                    };
                }
            }
        });

    };

    var checking = function () {

        self.statusInvalid = false;
        self.groupInvalid = false;
        self.branchCentralInvalid = false;

        var bool = false;

        if (self.user.status == null) {
            self.userNameInvalid = true;
            bool = true;
        }
        if(self.user.branchCentralList.length == 0){
            self.branchCentralInvalid = true;
            bool = true;
        }
        if(self.user.groupList.length == 0){
            self.branchCentralInvalid = true;
            bool = true;
        }
        return bool;
    };

    self.updateUser = function () {
        var hasil = checking();
        for(var i=0; i<self.user.branchList.length; i++){
            var listId = {
                'code' : self.user.branchList[i].code,
                'name'  : self.user.branchList[i].name
            }
            request.branchList.push(listId);
        }

        for(var i=0; i<self.user.branchCentralList.length; i++){
            var listId = {
                'id' : self.user.branchCentralList[i].id,
            };
            request.branchCentralList.push(listId);
        }

        for(var i=0; i<self.user.groupList.length; i++){
            var listId = {
                'id' : self.user.groupList[i].id
            };
            request.groupList.push(listId);
        }


        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }else {
            request.id = self.user.id;
            request.version = self.user.version;
            request.status = self.user.status;
            UserService.updateUserInternal(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'User berhasil disimpan',
                                        path: 'dashboard/security-admin'
                                    };
                                }
                            }
                        });
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        }
    }

    detailUser();

};