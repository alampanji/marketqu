'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('receiveInvoiceCtrl', [
        '$scope', '$location', '$modal', 'InvoiceService', 'ParameterNotaryService', '$window',
        receiveInvoiceCtrl
    ]);

function receiveInvoiceCtrl($scope, $location, $modal, InvoiceService, ParameterNotaryService, $window) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    };

    var backToTop = function() {
        $window.scrollTo(0, 0);
    };

    var request = {
        "resumeList": []
    };

    self.goToDetail = function(id) {
        $location.path('/dashboard/approval-request-resume-detail').search({ id: id });

        // $location.path('/dashboard/matching-result').search({id: id});
    };
    // self.listMemo = function() {
    //     $location.path('/dashboard/data-memo');
    // };

    self.goToDetail = function(id) {
        $location.path('/dashboard/detail-receive-invoice').search({ id: id });
    };

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus = function(status) {
        if (status) return "active";
        else return "not active";
    };

    self.status = [
        { keyName: 'true', name: 'Aktif' },
        { keyName: 'false', name: 'Tidak Aktif' }
    ];

    self.search = [
        { "keyName": "appId", "value": "APP ID" },
        { "keyName": "name", "value": "Nama Badan Usaha" },
        { "keyName": "npwp", "value": "NPWP" },
    ];
    self.pageChanged = function() {
        getListUser();
    };

    var getListUser = function(type, value) {

        var indexFound = self.search.map(function(e) {
            return e.keyName;
        }).indexOf(self.search);
        self.itemSearch = self.search[0];

        var appId = '';
        var name = '';
        var npwp = '';

        if (type != undefined && value != undefined) {
            if (type == 'appId') {
                appId = value;
                name = '';
                npwp = '';
            } else if (type == 'name') {
                name = value;
                appId = '';
                npwp = '';
            } else if (type == 'npwp') {
                npwp = value;
                appId = '';
                name = '';
            }
        }

        InvoiceService.getlistOfReceiveInvoice(self.currentPage - 1, self.limit, appId, name, npwp).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.users = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                }
            });
    };

    var invalidModal = function(message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function() {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.doUpdateUser = function(id) {
        $location.path('/dashboard/update-user').search({ id: id });
    };

    self.doDeleteUser = function(id, name) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteUser.html',
            controller: 'deleteUserInternalCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function() {
                    return {
                        id: id,
                        name: name,
                    };
                }
            }
        });
        modalDelete.result.then(function() {}, function(data) {
            if (data == "close") {
                if (self.users.length == 1 && self.currentPage != 1) {
                    self.currentPage--;
                }
                getListUser();
            }
        });
    };

    function getListNotary() {
        backToTop();
        ParameterNotaryService.getAllNotaryInvoiceName().then(
            function(response) {
                if (response.message === 'OK') {
                    var notaries = response.result;
                    var constructNotary = [{ "id": "", "version": "", "name": "Pilih Notaris" }];
                    angular.forEach(notaries, function(notary) {
                        constructNotary.push(notary);
                    });
                    self.notaries = constructNotary;
                    self.itemNotary = self.notaries[0];
                }
            }
        );
    };

    self.getListInvoice = function(notary) {
        InvoiceService.getlistOfInvoiceByNotaryId(notary.id).then(
            function(response) {
                if (response.message === 'OK') {
                    self.invoices = response.result;
                    self.itemInvoice = self.invoices[0];
                    if (self.itemInvoice != undefined) {
                        self.getListReceiveInvoice(self.itemInvoice, self.itemNotary);
                    }
                }
            }
        );
    };

    self.getListReceiveInvoice = function(itemNotary, itemInvoice) {
        var notary = '';
        var invoice = '';
        if (itemNotary.id == '') {
            invalidModal("Notaris Wajib Di Isi");
        } else if (itemNotary.id != '' && itemInvoice == undefined) {
            notary = itemNotary.id;
            InvoiceService.getlistOfReceiveInvoice(invoice, notary).then(
                function(response) {
                    if (response.message === 'OK') {
                        self.dataResult = response.result;

                        if (self.dataResult) {
                            for (var i = 0; i < self.dataResult.length; i++) {
                                self.dataResult[i].typeFee = true;
                            }
                        }
                    }
                }
            );
        } else if (itemNotary.id != '' && itemInvoice != undefined) {
            if (itemInvoice.id != '') {
                notary = itemNotary.id;
                invoice = itemInvoice.id;
                InvoiceService.getlistOfReceiveInvoice(invoice, notary).then(
                    function(response) {
                        if (response.message === 'OK') {
                            self.dataResult = response.result;

                            if (self.dataResult) {
                                for (var i = 0; i < self.dataResult.length; i++) {
                                    self.dataResult[i].typeFee = true;
                                }
                            }
                        }
                    }
                );
            }
        }

    };

    self.checkedAll = function() {
        if (self.invoice) {
            for (var i = 0; i < self.dataResult.length; i++) {
                self.dataResult[i].value = true;
                self.invoice = true;
            }
        } else {
            for (var i = 0; i < self.dataResult.length; i++) {
                self.dataResult[i].value = false;
                self.invoice = false;
            }

        }
    };

    self.checkedChild = function(value) {
        var check = true;
        if (!value) {
            self.invoice = false;
        } else {
            for (var i = 0; i < self.dataResult.length; i++) {
                if (!self.dataResult[i].value) {
                    check = false;
                }

            }
            self.invoice = check;
        }

        console.log()

    };

    // self.checkedAll = function() {
    //     if(self.branchCentral){
    //         for(var i=0; i<self.dataResult.length; i++){
    //             self.dataResult[i].value = false;
    //             self.branchCentral = false;
    //         }
    //     }
    //     else{
    //         for(var i=0; i<self.dataResult.length; i++) {
    //             self.dataResult[i].value = true;
    //             self.branchCentral = true;
    //         }
    //
    //     }
    // };
    //
    //
    // self.checkedChild = function (value){
    //     var check = true;
    //     if(!value){
    //         self.branchCentral = false;
    //     }
    //     else{
    //         for(var i=0; i<self.dataResult.length; i++) {
    //             if(!self.dataResult[i].value){
    //                 check = false;
    //             }
    //
    //         }
    //         self.branchCentral=check;
    //     }
    //
    // };

    self.changed = function(index, value) {
        self.dataResult[index].typeFee = value;
    };

    // self.receiveInvoiceProcess = function() {
    //     console.log("CLICKED");
    //
    //     var totalFalse = 0;
    //     var totalTrue = 0;
    //     var invoiceNum = "";
    //     for(var i = 0; i < self.dataResult.length ; i ++){
    //         if(i == 0){
    //             invoiceNum = self.dataResult[i].invoiceNumber;
    //             console.log(invoiceNum);
    //         }else if(self.dataResult[i].invoiceNumber != invoiceNum){
    //             if(totalTrue != 0 && totalFalse != 0){
    //                 console.log("ERROR");
    //             }else{
    //                 invoiceNum = self.dataResult[i].invoiceNumber;
    //                 totalTrue = 0;
    //                 totalFalse = 0;
    //             }
    //
    //         }
    //         if(self.dataResult[i].value){
    //             totalTrue++;
    //         }else{
    //             totalFalse++;
    //         }
    //     }
    //
    //     console.log(totalTrue);
    //     console.log(totalFalse);
    //
    // };

    self.receiveInvoiceProcess = function() {
        var checklistValue = 0;
        // for (var i = 0; i < self.dataResult.length; i++) {
            // if (self.dataResult[i].value == undefined) {
            //     invalidModal("Pilih Invoice yang Akan di Receive!");
            //     break;
            // } else {

                // if(self.dataResult[i].invoiceNumber==self.dataResult[i+1].invoiceNumber){
                //     console.log(self.dataResult[i].invoiceNumber);
                //     console.log(self.dataResult[i+1].invoiceNumber);
                // }
                // if (self.dataResult[i].value == true) {
                //     if (self.dataResult[i].invoiceNumber)
                //         angular.forEach(self.dataResult[i], function(branch) {
                //             if (branch.value) {
                //                 listOfBranch.push(branch);
                //             }
                //         });
                //     console.log("Checklist True");
                //     if (self.dataResult[i].typeFee == false) {
                //         if (self.dataResult[i].feeManual != null) {
                //             var listId = {
                //                 'id': self.dataResult[i].id,
                //                 'feeType': 2,
                //                 'fee': self.dataResult[i].feeManual
                //             };
                //             request.resumeList.push(listId);
                //             console.log("Fee Manual True");
                //             console.log(request.resumeList);
                //         } else {
                //             invalidModal("Lengkapi field yang feenya diubah");
                //             break;
                //         }
                //     } else {
                //         var listId = {
                //             'id': self.dataResult[i].id,
                //             'feeType': 1,
                //             'fee': self.dataResult[i].fee
                //         };
                //         request.resumeList.push(listId);
                //     }
                // }
                var totalFalse = 0;
                var totalTrue = 0;
                var invoiceNum = "";
                for(var i = 0; i < self.dataResult.length; i ++){
                    if(i == 0){
                        invoiceNum = self.dataResult[i].invoiceNumber;
                    }else if(self.dataResult[i].invoiceNumber != invoiceNum){
                        if(totalTrue != 0 && totalFalse != 0){
                            console.log(invoiceNum);
                            invalidModal("Invoice ID Yang Sama Harus Di Checklist");
                            break;
                        }else{
                            invoiceNum = self.dataResult[i].invoiceNumber;
                            totalTrue = 0;
                            totalFalse = 0;
                        }

                    }
                    if(self.dataResult[i].value){
                        // if (self.dataResult[i].invoiceNumber)
                                // angular.forEach(self.dataResult[i], function(branch) {
                                //     if (branch.value) {
                                //         listOfBranch.push(branch);
                                //     }
                                // });
                            console.log("Checklist True");
                            if (self.dataResult[i].typeFee == false) {
                                if (self.dataResult[i].feeManual != null) {
                                    if(self.dataResult[i].description==null){
                                        invalidModal("Deskripsi wajib diisi");
                                    }
                                    else{
                                        var listId = {
                                            'id': self.dataResult[i].id,
                                            'feeType': 2,
                                            'fee': self.dataResult[i].feeManual,
                                            'invoiceNumber':self.dataResult[i].invoiceNumber,
                                            'description': self.dataResult[i].description
                                        };
                                        request.resumeList.push(listId);
                                        console.log("Fee Manual True");
                                        console.log(request.resumeList);
                                    }
                                } else {
                                    if(self.dataResult[i].feeManual==null){
                                        invalidModal("Lengkapi field yang feenya diubah");
                                    }
                                    break;
                                }
                            } else {
                                var listId = {
                                    'id': self.dataResult[i].id,
                                    'feeType': 1,
                                    'fee': self.dataResult[i].fee,
                                    'invoiceNumber':self.dataResult[i].invoiceNumber,
                                    'description': self.dataResult[i].description
                                };
                                request.resumeList.push(listId);
                            }

                        request.notaryId = self.itemNotary.id;
                        // request.invoiceId = self.itemInvoice.id;
                        console.log(request);
                        totalTrue++;
                        checklistValue++;

                    }else{
                        totalFalse++;
                    }
                }
                // if(totalTrue==0){
                //     invalidModal("Invoice Wajib Dipilih");
                // }
                // else{
                //     if(totalFalse==0){
                        InvoiceService.processReceiveInvoice(request).then(
                            function(response) {
                                if (response.message !== 'ERROR') {
                                    $modal.open({
                                        templateUrl: 'views/modal/Modal.html',
                                        controller: 'ModalCtrl',
                                        size: 'sm',
                                        resolve: {
                                            modalParam: function() {
                                                return {
                                                    title: 'Informasi',
                                                    message: 'Berhasil Receive Invoice',
                                                    path: ''
                                                };
                                            }
                                        }
                                    });
                                    request = {};
                                    // $location.path('/dashboard/data-memo');
                                } else {
                                    self.isDisabled = false;
                                    $modal.open({
                                        templateUrl: 'views/modal/Modal.html',
                                        controller: 'ModalCtrl',
                                        size: 'sm',
                                        resolve: {
                                            modalParam: function() {
                                                return {
                                                    title: 'Gagal',
                                                    message: response.result,
                                                    path: ''
                                                };
                                            }
                                        }
                                    });
                                }

                            }
                        );
                    // }
                // }


            };
        // }
        // request.notaryId = self.itemNotary.id;
        // request.invoiceId = self.itemInvoice.id;
        // console.log(request);
        //
        // InvoiceService.processReceiveInvoice(request).then(
        //     function(response) {
        //         if (response.message !== 'ERROR') {
        //             $modal.open({
        //                 templateUrl: '/views/modal/Modal.html',
        //                 controller: 'ModalCtrl',
        //                 size: 'sm',
        //                 resolve: {
        //                     modalParam: function() {
        //                         return {
        //                             title: 'Informasi',
        //                             message: 'Berhasil Receive Invoice',
        //                             path: ''
        //                         };
        //                     }
        //                 }
        //             });
        //             $location.path('/dashboard/data-memo');
        //         } else {
        //             self.isDisabled = false;
        //             $modal.open({
        //                 templateUrl: '/views/modal/Modal.html',
        //                 controller: 'ModalCtrl',
        //                 size: 'sm',
        //                 resolve: {
        //                     modalParam: function() {
        //                         return {
        //                             title: 'Gagal',
        //                             message: response.result,
        //                             path: ''
        //                         };
        //                     }
        //                 }
        //             });
        //         }
        //
        //     }
        // );
    // };

    self.rejectReceiveInvoice = function() {
        for (var i = 0; i < self.dataResult.length; i++) {
            if (self.dataResult[i].value == true) {
                if (self.dataResult[i].typeFee == false) {
                    if (self.dataResult[i].feeManual != null) {
                        var listId = {
                            'id': self.dataResult[i].id,
                            'feeType': 2,
                            'fee': self.dataResult[i].feeManual,
                        };
                        request.resumeList.push(listId);
                    } else {
                        invalidModal("Lengkapi field yang feenya diubah");
                        break;
                    }
                } else {
                    var listId = {
                        'id': self.dataResult[i].id,
                        'feeType': 1,
                        'fee': self.dataResult[i].fee
                    };
                    request.resumeList.push(listId);
                }
            }
        }
        request.notaryId = self.itemNotary.id;
        request.invoiceId = self.itemInvoice.id;

        InvoiceService.rejectReceiveInvoice(request).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: '/views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function() {
                                return {
                                    title: 'Informasi',
                                    message: 'Berhasil Reject Invoice',
                                    path: ''
                                };
                            }
                        }
                    });
                    $location.path('/dashboard/receive-invoice');
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: '/views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function() {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    };

    self.doSearch = function(type, value) {
        self.currentPage = 1;
        getListUser(type.keyName, value);
    };
    getListNotary();
};