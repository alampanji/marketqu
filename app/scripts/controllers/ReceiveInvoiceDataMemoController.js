'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('receiveInvoiceDataMemoCtrl', [
        '$scope','$location','$modal','InvoiceService', 'ParameterNotaryService', '$window',
        receiveInvoiceDataMemoCtrl
    ]);

function receiveInvoiceDataMemoCtrl($scope,$location,$modal,InvoiceService, ParameterNotaryService, $window) {
    var self = $scope;

    self.close = close;
    self.printStatus = false;

    function close() {
        $modalInstance.dismiss('close');
    }
    self.goToDetail = function(id){
        $location.path('/dashboard/approval-request-resume-detail').search({id:id});

        // $location.path('/dashboard/matching-result').search({id: id});
    };
    self.createUser = function(){
        $location.path('/dashboard/create-user');
    };

    self.doUpdateUser = function(){
        $location.path('/dashboard/update-user');
    };

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.status = [
        {keyName: 'true', name: 'Aktif'},
        {keyName: 'false', name: 'Tidak Aktif'}
    ];

    self.search = [
        {"keyName" : "appId", "value":"APP ID"},
        {"keyName" : "name", "value":"Nama Badan Usaha"},
        {"keyName" : "npwp", "value":"NPWP"},
    ];
    self.pageChanged = function () {
        getListUser();
    };

    function getListNotary() {
        ParameterNotaryService.getAllNotaryInvoiceName().then(
            function (response) {
                if (response.message === 'OK') {
                    var notaries = response.result;
                    var constructNotary = [{"id":"", "version":"","name":"Silahkan Pilih"}];
                    angular.forEach(notaries, function(notary){
                        constructNotary.push(notary);
                    });
                    self.notaries = constructNotary;
                    self.itemNotary = self.notaries[0];

                    if(self.notaries[0]){
                        self.getListInvoice(0);
                    }
                }
            }
        );
    };



    self.getListInvoice = function(notary) {
        InvoiceService.getlistOfInvoiceMemoByNotaryId(notary.id).then(
            function (response) {
                if (response.message === 'OK') {
                    self.invoices = response.result;
                    self.itemInvoice = self.invoices[0];
                }
            }
        );
    };

    var getListMemo = function (notary,invoice) {

        InvoiceService.getlistOfMemoInvoice(notary, invoice).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                }
            });
    };

    self.doUpdateUser = function (id) {
        $location.path('/dashboard/update-user').search({id: id});
    };

    self.doDeleteUser = function (id,name) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteUser.html',
            controller: 'deleteUserInternalCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id,
                        name:name,
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.users.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListUser();
            }
        });
    };

    // self.printMemo = function (divName,invoiceId) {
    //     // InvoiceService.getDataPrint(invoiceId).then(
    //     //     function (response) {
    //     //         if (response.message !== 'ERROR') {
    //     //             self.dataMemo = response.result;
    //     //             console.log(self.dataMemo);
    //     //
    //     //
    //     //
    //     //
    //     //         }
    //     //     });
    //
    //     var printContents = document.getElementById(divName).innerHTML;
    //     var popupWin = window.open('', '_blank', 'width=500,height=500');
    //     popupWin.document.open();
    //     popupWin.document.write('<!DOCTYPE html><html><head><title>TITLE OF THE PRINT OUT</title>'
    //         +'<link rel="stylesheet" type="text/css" href="app/directory/file.css" />'
    //         +'</head><body"><div>'
    //         + printContents + '</div></html>');
    //     popupWin.document.close();
    //     var printUrl = window.location.origin + '/#/dashboard/print-memo/' + invoiceId;
    //     var popupWin = window.open(printUrl.toString(), '_blank', 'width=800,height=700');
    // };
    self.printMemo = function(divName, invoiceId) {

        InvoiceService.getDataPrint(invoiceId).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        self.dataMemo = response.result;

                        var printContents = document.getElementById(divName).innerHTML;
                        var date = self.dataMemo.memoDate;
                        var number = self.dataMemo.memoNumber;
                        var invoiceNumber = self.dataMemo.invoiceNumber;
                        var notaryName = self.dataMemo.notaryName;
                        var bank = self.dataMemo.bank;
                        var accountNumber = self.dataMemo.accountNumber;
                        var accountName = self.dataMemo.accountName;
                        var totalFee = self.dataMemo.totalFee;
                        var textFee = self.dataMemo.textFee;

                        var date = new Date(date);

                        var year = date.getFullYear();
                        var month = date.getMonth('') + 1;
                        var day = date.getDate();
                        var hours = date.getHours();
                        var minutes = date.getMinutes();
                        var seconds = date.getSeconds();


                        var sampleTitle = "<h3 style='text-align: center;font-family: Arial'> PERMOHONAN PEMBAYARAN RESUME KE NOTARIS </h3><br>";
                        var sampleDate = "<h4 style='text-align: center;margin-bottom: 120px; font-family: Arial'> Tanggal: "+day + "-" + month + "-" + year+"</h4>";
                        var sampleNumber = "<h4 style='text-align: center;font-family: Arial'> No: "+number;+"</h4><br>";
                        var samplePar = "<p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'> Sehubungan dengan adanya tagihan pembuatan resume dari Notaris, Mohon dilakukan pembayaran yang ditujukan kepada:</p><br>";
                        var sampleInvoiceNumber = "<table><tr><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'> No Invoice </p></td><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>:</p></td><td> <p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>"+ invoiceNumber + "</p></td></tr> ";
                        var sampleNotaryName = "<tr><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'> Nama Notaris </p></td><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>:</p></td><td> <p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>"+ notaryName + "</p></td></tr>";
                        var sampleBank = "<tr><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'> Bank </p></td><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>:</p></td><td> <p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>"+ bank + "</p></td></tr>";
                        var sampleAccountNumber = "<tr><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'> No Rekening </p></td><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>:</p></td><td> <p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>"+ accountNumber + "</p></td></tr>";
                        var sampleAccountName = "<tr><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'> Atas Nama </p></td><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>:</p></td><td> <p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>"+ accountName + "</p></td></tr>";
                        var sampleNominal = "<tr><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'> Nominal </p></td><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>:</p></td><td> <p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>"+ totalFee + "</p></td></tr>";
                        var sampleNominalWord = "<tr><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'> Terbilang </p></td><td><p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>:</p></td><td> <p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>"+ textFee + "</p></td></tr></table><br><br>";
                        var constructHeaderTable = "<table class='table'><th>No</th><th>Nama Badan Usaha</th><th>No NPWP</th><th>Resume ID</th><th>Fee</th>";
                        var constructContentTable = [];
                        var footer = "<table width='90%' cellspacing='200'><tr><th width='40%'><h4 style='text-align: left'><h4>Menyetujui</h4></th><th width='30%'></th></tr><tr><td style='padding-top: 120px; padding-right: 150px'><hr color='black'></td><td style='padding-top: 120px; margin-left: 200px;'><hr color='black'></td></tr></table>"
                        var total = 0;
                        var tableNum = 1;
                        for(var i=0; i < self.dataMemo.resumeList.length; i++){
                            var number = "<td style='text-align: left'>" + tableNum +"</td>"
                            var companyName = "<td style='text-align: left'>" + self.dataMemo.resumeList[i].companyName + "</td>";
                            var npwp = "<td style='text-align: left'>" +  self.dataMemo.resumeList[i].npwp + "</td>";
                            var resumeId ="<td style='text-align: left'>" +  self.dataMemo.resumeList[i].resumeId + "</td>";
                            var fee = "<td style='text-align: left'>" +  self.dataMemo.resumeList[i].fee + "</td>";
                            var listContent = "<tr>" + number+companyName+npwp+resumeId+fee + "</tr>";

                            total += self.dataMemo.resumeList[i].fee;
                            constructContentTable.push(listContent);
                             tableNum++;
                        }

                        var endTable = "</table>";
                        var constructTotal = "<tr><td colspan='5' style='text-align: left'>Total : Rp. " +total+ "</td></tr>";
                        // var sampleNotaryName = "<p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'> Nama Notaris: " + notaryName+ "</p>";
                        // var sampleBank = "<p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'>Bank: " + bank+ "</p>";
                        //var sampleAccountNumber = "<p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'> No Rekening: " + accountNumber+ "</p>";
                        //var sampleAccountName = "<p style='text-align: left;font-family: Arial; font-size: 11pt; font-weight: 300'> Atas Name: " + accountName+ "</p>";
                        var popupWin = window.open('', '_blank', 'width=500,height=500');
                        popupWin.document.open();
                        popupWin.document.write('<!DOCTYPE html><html><head>'
                            +'<link rel="stylesheet" type="text/css" href="app/directory/file.css" />'
                            + '<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css" />'
                            +'</head><body onload="window.print(); window.close();"><div>'
                            + sampleTitle + sampleNumber + sampleDate + samplePar + sampleInvoiceNumber + sampleNotaryName +sampleBank+sampleAccountNumber+sampleAccountName+sampleNominal+sampleNominalWord+ constructHeaderTable+ constructContentTable +constructTotal+ endTable+footer+'</div></html>');
                        popupWin.document.close();
                    }
                });
        // var date = memoDate

    // var printUrl = window.location.origin + '/#/dashboard/print-memo/' + invoiceId;
    // var popupWin = window.open(printUrl.toString(), '_blank', 'width=800,height=700');

    // setTimeout(function () {
    //     popupWin.print();
    // }, 1000);
};


    self.doSearch = function (itemNotary,itemInvoice) {
        self.currentPage = 1;
        getListMemo(itemNotary.id,itemInvoice.id);
    };
    getListNotary();
};