'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('receiveInvoiceDetailCtrl', [
        '$scope', '$modal',
        '$location',
        'ResumeService',
        'FidusiaService',
        '$sessionStorage',
        '$window',
        'CONF',
        approvalResumeNotaryDetailCtrl
    ]);

function approvalResumeNotaryDetailCtrl($scope, $modal, $location, ResumeService, FidusiaService, $sessionStorage, $window, CONF) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    var paramValue = $location.search().id;

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var getDetailResume = function () {
        backToTop();
        ResumeService.getInputResumeTambahanDetail(paramValue).then(
            function (response) {
                if (response.message !== 'ERROR') {

                    self.dataResult = response.result;
                    var access_token = $sessionStorage.accessToken;
                    var urlConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_RESUME +'?access_token='+access_token+'&resumeId='+self.dataResult.id;
                    self.dataResult.resumeAhuUrl = urlConstruct;

                    if(self.dataResult.documentList != undefined){
                        for (var j=0; j< self.dataResult.documentList.length;j++){

                            var urlConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_DOCUMENT +'?access_token='+access_token+'&resumeId='+self.dataResult.id+'&docType='+self.dataResult.documentList[j].documentCode;
                            self.dataResult.documentList[j].url = urlConstruct;

                        }
                    }
                }
            });
    };

    self.method = [
        {"keyName" : "input", "value":"Input"},
        {"keyName" : "upload", "value":"Upload"},
    ];

    var constructSelect =  function () {
        var indexFound = self.method.map(function (e) {
            return e.keyName;
        }).indexOf(self.method);
        self.itemSearch = self.method[0];
    };

    constructSelect();

    self.lovGroupPrivilege = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovGroupPrivilege.html',
            controller: 'modalGroupPrivilegeCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData.group
                    };
                }
            }
        });
        modalInstance.result.then(function (listGroup) {
            self.contentData.group = listGroup;
        });
    };

    self.deleteGroup = function(index){
        self.contentData.group.splice(index,1);
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses approve resume notary?',
                        path: 'dashboard/approval-resume-notary'
                    };
                }
            }
        });
    };

    self.tab = 1;

    self.setTab = function (tabId) {
        self.tab = tabId;
    };

    self.isSet = function (tabId) {
        return self.tab === tabId;
    };

    self.approveResume = function () {

        var request = {};

        request.id = self.dataResult.id;
        request.version = self.dataResult.version;

        console.log(request);

        FidusiaService.approvalResumeNotary(request).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Informasi',
                                    message: 'Berhasil Approve Resume',
                                    path: 'dashboard/approval-resume-notary'
                                };
                            }
                        }
                    });
                }else{
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    };

    self.rejectResume = function () {

        if(self.rejectDescription==null){
            invalidModal("Keterangan Reject Wajib Diisi!");
        }
        else{
            var request = {};
            request.id = self.dataResult.id;
            request.version = self.dataResult.version;
            request.rejectDescription = self.rejectDescription;

            FidusiaService.rejectResumeNotary(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Berhasil Reject Resume',
                                        path: 'dashboard/approval-resume-notary'
                                    };
                                }
                            }
                        });
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        }


    };

    getDetailResume();

};
