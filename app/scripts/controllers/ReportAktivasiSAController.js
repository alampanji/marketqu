'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */
// angular.module('nostraApp').filter('getprice', function () {
//     return function (value, property) {
//         // console.log(value)
//         // console.log(property)
//         var total = 0;
//         angular.forEach(value, function (val, index) {
//             // console.log(val)
//             // console.log(index)
//             total = total + parseInt(val.fee)
//         });
//         return total;
//     }
// });
angular.module('nostraApp')
    .controller('reportAktivasiSACtrl', [
        '$scope', '$modal', '$window',
        '$location',
        'InvoiceService','ReportService','$sessionStorage','CONF',
        reportAktivasiSACtrl
    ]);

function reportAktivasiSACtrl($scope, $modal, $window, $location, InvoiceService,ReportService,$sessionStorage,CONF) {
 var self = $scope;
    self.contentData = {};
    self.boolean = false;
    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.download = {};
    // var date = new Date();
    // var test = Date.parse(date);
    // console.log(test)
    self.type = [
        {"keyName" : "USER", "value":"User"},
        {"keyName" : "PARAMETER", "value":"Parameter"},
        {"keyName" : "PARAMETER_GROUP", "value":"Parameter Group"},
        {"keyName" : "PARAMETER_VOUCHER", "value":"Parameter Voucher"},
        {"keyName" : "PARAMETER_SLA", "value":"Parameter SLA"},
        {"keyName" : "PARAMETER_LIMIT_APPROVE", "value":"Parameter Limit Approve"},
        {"keyName" : "PARAMETER_NOTARY", "value":"Parameter Notaris"},
        {"keyName" : "PARAMETER_DOCUMENT_TYPE", "value":"Parameter Dokumen Kelengkapan"},
        {"keyName" : "PARAMETER_EFFECTIVE_PERIOD", "value":"Parameter Tanggal Efektif"},
        {"keyName" : "PARAMETER_BRANCH_CENTRAL", "value":"Parameter Cabang Sentral"},

    ];

    self.totalFee = 0;
    self.totalData = 0;
    self.tampung=[];
    self.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    function generateSearch(){
        var indexFound = self.type.map(function(e) {
            return e.keyName;
        }).indexOf(self.type);
        self.itemType = self.type[0];
    };

    generateSearch();

    self.exportDataPdf = function () {
        var accessToken = $sessionStorage.accessToken;
        
        var urlConstructExcel=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_REPORT_REPORT_SA_ACTIVITY_LIST_PDF +'?access_token='+accessToken+'&type='+self.download.type+'&updateDate='+self.download.updateDate;
        $window.open(urlConstructExcel, '_blank');              
    };
    self.exportDataExcel = function () {
        var accessToken = $sessionStorage.accessToken;
        
        var urlConstructExcel=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_REPORT_REPORT_SA_ACTIVITY_LIST_EXCEL +'?access_token='+accessToken+'&type='+self.download.type+'&updateDate='+self.download.updateDate;
        $window.open(urlConstructExcel, '_blank');              
    };
    self.branchCentralList = {};
    self.lovBranchCentral = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovBranchCentralReport.html',
            controller: 'modalLovBranchCentralRadioCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.branchCentralList.name
                    };
                }
            }
        });
        modalInstance.result.then(function (listBranchCentral) {
            // console.log(listBranchCentral)
            self.branchCentralList = listBranchCentral;
            console.log(listBranchCentral)
            doSearch();
        });
    };

    self.deleteBranchCentral = function(index){
        self.user.branchCentralList.splice(index,1);
    };

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };
    self.start = {
        opened: false
    };

    self.end = {
        opened: false
    };

    self.documentDate = {
        opened: false
    };

    self.documentPeriod = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    }; 
    self.openStart = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.start.opened)
            self.start.opened = false;
        else
            self.start.opened = true;
    };  
    self.openEnd = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.end.opened)
            self.end.opened = false;
        else
            self.end.opened = true;
    };    
    self.openAkhir = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.akhir.opened)
            self.akhir.opened = false;
        else
            self.akhir.opened = true;
    };
 
    self.openDocumentDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.documentDate.opened)
            self.documentDate.opened = false;
        else
            self.documentDate.opened = true;
    };
    self.openDocumentPeriod = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.documentPeriod.opened)
            self.documentPeriod.opened = false;
        else
            self.documentPeriod.opened = true;
    };
    self.goToList = function(id){
        $location.path('/dashboard/order-resume-ahu-detail').search({id:id});
    };
    self.submit = function(){
        var request = {};
        request.invoiceNumber = self.input.invoiceNumber;
        request.bank = self.listRequestResumes.bank;
        request.bankBranch = self.listRequestResumes.bankBranch;
        request.accountNumber = self.listRequestResumes.accountNumber;
        request.accountName = self.listRequestResumes.accountName;
        request.resumeInvoiceList = [];
        for (var i=0;i < self.tampung.length; i++){
            var id = {"id": self.tampung[i].id}
            request.resumeInvoiceList.push(id)
        }
        // console.log(request)
        InvoiceService.processInvoice(request).then(
            function (response) {
                console.log(response)
                if (response.message !== 'ERROR') {
                    // self.listRequestResumes = response.result;
                    // for (var i=0;i<self.listRequestResumes.resumeInvoiceList.length;i++){
                    //     self.listRequestResumes.resumeInvoiceList[i].value = false;
                    // }
                }
            });
    }

    self.pageChanged = function () {
        // storeData();
        getListOfRequestCancelResume();
    };

// https://resume-ahu.nostratech.com:23443//invoice/input-invoice-list?startDate=1452416732000&endDate=1484038790000&access_token=b87ba11b-ca89-3c1e-8735-8f304e45c2a6
    var getListOfRequestCancelResume = function (input) {
        // console.log(input)
        // var request = {};
        // request.startDate = Date.parse(input.startDate)
        // request.endDate = Date.parse(input.endDate)
        // console.log(request)
        // https://resume-ahu.nostratech.com:23443/resume/request-resume-list?access_token=81922c6f-0c64-3866-a82a-e6302c4434a9&appId=&companyName=&limit=10&npwp=&page=0
        
        ReportService.searchReportAktivasiSA(self.currentPage-1,self.limit,input.type,input.updateDate).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.listRequestResumes = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                    self.download.type = input.type;
                    self.download.updateDate = input.updateDate;
                    self.updateDate=null;
                    // var indexFound = self.search.map(function (e) {
                    //     return e.keyName;
                    // }).indexOf(self.search);
                    // self.itemSearch = self.search[0];
                }
            });
        // InvoiceService.getlistOfInvoice(input).then(
        //     function (response) {
        //         if (response.message !== 'ERROR') {
        //             self.listRequestResumes = response.result;
        //             for (var i=0;i<self.listRequestResumes.resumeInvoiceList.length;i++){
        //                 self.listRequestResumes.resumeInvoiceList[i].value = false;
        //             }
        //         }
        //     });
    };


    self.lovGroupPrivilege = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovGroupPrivilege.html',
            controller: 'modalGroupPrivilegeCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData.group
                    };
                }
            }
        });
        modalInstance.result.then(function (listGroup) {
            self.contentData.group = listGroup;
        });
    };

    self.deleteGroup = function(index){
        self.contentData.group.splice(index,1);
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.value = function (isSelected, item,index) {
        if (isSelected == true) {
            self.tampung.push(item);
            self.totalFee = self.totalFee + item.fee;
            self.totalData = self.totalData + 1;
        } else {
            self.tampung.splice(index, 1);
            self.totalFee = self.totalFee - item.fee;
            self.totalData = self.totalData - 1;
        }
    }
    self.checkedChild = function (value,isi){
        // console.log(value)
        var check = true;
        if(!value.value){
            console.log("cancel dari isi")
            self.document = false;
            kurang(self.listRequestResumes.resumeInvoiceList[index].fee);
        }
        else{
            console.log("tambah?")
            for(var i=0; i<self.listRequestResumes.resumeInvoiceList.length; i++) {

                if(!self.listRequestResumes.resumeInvoiceList[i].value){
                    console.log("didalem if")
                    check = false;
                    // console.log(self.tampung)
                    if(self.tampung.length > 0){
                    console.log("didalem if2")
                        console.log(self.tampung)
                        self.tampung.splice(index,1)
                        kurang(self.listRequestResumes.resumeInvoiceList[index].fee);
                    }
                }

            }
            self.document=check;
            self.tampung.push(self.listRequestResumes.resumeInvoiceList[index]);
            hitung(self.listRequestResumes.resumeInvoiceList[index].fee);
                    // console.log(self.tampung)
        }

    // hitung();
    };

    self.checkedAll = function() {
        // console.log(self.document)
            self.totalFee = 0;
            self.totalData = 0;
        if(self.document){
            for(var i=0; i<self.listRequestResumes.resumeInvoiceList.length; i++){
                self.listRequestResumes.resumeInvoiceList[i].value = false;
                self.document = false;

                if(self.tampung.length > 0){
                // console.log(self.tampung)
                    self.tampung.splice(i,1)
                }
            }

        }
        else{
            for(var i=0; i<self.listRequestResumes.resumeInvoiceList.length; i++) {

                // if (self.listRequestResumes.resumeInvoiceList[i].value = false){
                    // console.log(i)
                    self.listRequestResumes.resumeInvoiceList[i].value = true;
                    self.document = true;
                    self.tampung.push(self.listRequestResumes.resumeInvoiceList[i])
                    self.totalFee = self.totalFee + self.listRequestResumes.resumeInvoiceList[i].fee;
                    self.totalData = self.totalData + 1;
                // }
                // else{

                // }
            }

        }

    // hitung();
    };
    self.totalData = self.tampung.length;
    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah user?',
                        path: 'dashboard/security-admin'
                    };
                }
            }
        });

    };

     function doSearch(input) {
        getListOfRequestCancelResume(input)
    };
    self.Search = function(){
        if(self.updateDate==undefined || self.updateDate==null){
            invalidModal("Tanggal Update Wajib Di isi");
        }
        else{
            self.updateDate = Date.parse(self.updateDate);
            // console.log(self.search.startDate);
            var request = {};
            request.type = self.itemType.keyName;
            request.updateDate = self.updateDate;
            doSearch(request);
        }

    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    backToTop();

    // getListOfRequestCancelResume();

};
