'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */
// angular.module('nostraApp').filter('getprice', function () {
//     return function (value, property) {
//         // console.log(value)
//         // console.log(property)
//         var total = 0;
//         angular.forEach(value, function (val, index) {
//             // console.log(val)
//             // console.log(index)
//             total = total + parseInt(val.fee)
//         });
//         return total;
//     }
// });
angular.module('nostraApp')
    .controller('reportPenerimaanResumeSLACtrl', [
        '$scope', '$modal',
        '$location',
        'InvoiceService','ReportService','$sessionStorage','CONF','$window',
        reportPenerimaanResumeSLACtrl
    ]);

function reportPenerimaanResumeSLACtrl($scope, $modal, $location, InvoiceService,ReportService,$sessionStorage,CONF,$window) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.download = {};
    // var date = new Date();
    // var test = Date.parse(date);
    // console.log(test)
    self.search = [
        {"keyName" : "appId", "value":"APP ID"},
        {"keyName" : "npwp", "value":"NPWP"},
        {"keyName" : "companyName", "value":"Nama Perusahaan"}
    ];
    self.totalFee = 0;
    self.totalData = 0;
    self.tampung=[];
    self.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    self.branchCentralList = {};
    self.lovBranchCentral = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovBranchCentralReport.html',
            controller: 'modalLovBranchCentralRadioCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.branchCentralList.name
                    };
                }
            }
        });
        modalInstance.result.then(function (listBranchCentral) {
            // console.log(listBranchCentral)
            self.branchCentralList = listBranchCentral;
            console.log(listBranchCentral)
            doSearch();
        });
    };

    self.deleteBranchCentral = function(index){
        self.user.branchCentralList.splice(index,1);
    };

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };
    self.start = {
        opened: false
    };

    self.end = {
        opened: false
    };

    self.documentDate = {
        opened: false
    };

    self.documentPeriod = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    }; 
    self.openStart = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.start.opened)
            self.start.opened = false;
        else
            self.start.opened = true;
    };  
    self.openEnd = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.end.opened)
            self.end.opened = false;
        else
            self.end.opened = true;
    };    
    self.openAkhir = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.akhir.opened)
            self.akhir.opened = false;
        else
            self.akhir.opened = true;
    };
 
    self.openDocumentDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.documentDate.opened)
            self.documentDate.opened = false;
        else
            self.documentDate.opened = true;
    };
    self.openDocumentPeriod = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.documentPeriod.opened)
            self.documentPeriod.opened = false;
        else
            self.documentPeriod.opened = true;
    };
    self.goToList = function(id){
        $location.path('/dashboard/order-resume-ahu-detail').search({id:id});
    };
    self.submit = function(){
        var request = {};
        request.invoiceNumber = self.input.invoiceNumber;
        request.bank = self.listRequestResumes.bank;
        request.bankBranch = self.listRequestResumes.bankBranch;
        request.accountNumber = self.listRequestResumes.accountNumber;
        request.accountName = self.listRequestResumes.accountName;
        request.resumeInvoiceList = [];
        for (var i=0;i < self.tampung.length; i++){
            var id = {"id": self.tampung[i].id}
            request.resumeInvoiceList.push(id)
        }
        // console.log(request)
        InvoiceService.processInvoice(request).then(
            function (response) {
                console.log(response)
                if (response.message !== 'ERROR') {
                    // self.listRequestResumes = response.result;
                    // for (var i=0;i<self.listRequestResumes.resumeInvoiceList.length;i++){
                    //     self.listRequestResumes.resumeInvoiceList[i].value = false;
                    // }
                }
            });
    }
    self.exportDataPdf = function () {
        var accessToken = $sessionStorage.accessToken;
        
        var urlConstructExcel=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_REPORT_SLA_OVERTIME_LIST_PDF +'?access_token='+accessToken+'&startDate='+self.download.startDate+'&endDate='+self.download.endDate;
        $window.open(urlConstructExcel, '_blank');              
    };
    self.exportDataExcel = function () {
        var accessToken = $sessionStorage.accessToken;
        
        var urlConstructExcel=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_REPORT_SLA_OVERTIME_LIST_EXCEL +'?access_token='+accessToken+'&startDate='+self.download.startDate+'&endDate='+self.download.endDate;
        $window.open(urlConstructExcel, '_blank');              
    };

    self.pageChanged = function () {
        // storeData();
        getListOfRequestCancelResume();
    };

// https://resume-ahu.nostratech.com:23443//invoice/input-invoice-list?startDate=1452416732000&endDate=1484038790000&access_token=b87ba11b-ca89-3c1e-8735-8f304e45c2a6
    var getListOfRequestCancelResume = function (input) {
        // console.log(input)
        // var request = {};
        // request.startDate = Date.parse(input.startDate)
        // request.endDate = Date.parse(input.endDate)
        // console.log(request)
        // https://resume-ahu.nostratech.com:23443/resume/request-resume-list?access_token=81922c6f-0c64-3866-a82a-e6302c4434a9&appId=&companyName=&limit=10&npwp=&page=0
        ReportService.searchReportPenerimaanResumeSLA(self.currentPage-1,self.limit,input).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.listRequestResumes = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages; 
                    self.download.startDate = self.search.startDate;
                    self.download.endDate = self.search.endDate;
                    self.search.startDate = undefined;
                    self.search.endDate = undefined;
                    // var indexFound = self.search.map(function (e) {
                    //     return e.keyName;
                    // }).indexOf(self.search);
                    // self.itemSearch = self.search[0];
                }
            });
        // InvoiceService.getlistOfInvoice(input).then(
        //     function (response) {
        //         if (response.message !== 'ERROR') {
        //             self.listRequestResumes = response.result;
        //             for (var i=0;i<self.listRequestResumes.resumeInvoiceList.length;i++){
        //                 self.listRequestResumes.resumeInvoiceList[i].value = false;
        //             }
        //         }
        //     });
    };


    self.lovGroupPrivilege = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovGroupPrivilege.html',
            controller: 'modalGroupPrivilegeCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData.group
                    };
                }
            }
        });
        modalInstance.result.then(function (listGroup) {
            self.contentData.group = listGroup;
        });
    };

    self.deleteGroup = function(index){
        self.contentData.group.splice(index,1);
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.value = function (isSelected, item,index) {
        if (isSelected == true) {
            self.tampung.push(item);
            self.totalFee = self.totalFee + item.fee;
            self.totalData = self.totalData + 1;
        } else {
            self.tampung.splice(index, 1);
            self.totalFee = self.totalFee - item.fee;
            self.totalData = self.totalData - 1;
        }
    }
    self.checkedChild = function (value,isi){
        // console.log(value)
        var check = true;
        if(!value.value){
            console.log("cancel dari isi")
            self.document = false;
            kurang(self.listRequestResumes.resumeInvoiceList[index].fee);
        }
        else{
            console.log("tambah?")
            for(var i=0; i<self.listRequestResumes.resumeInvoiceList.length; i++) {

                if(!self.listRequestResumes.resumeInvoiceList[i].value){
                    console.log("didalem if")
                    check = false;
                    // console.log(self.tampung)
                    if(self.tampung.length > 0){
                    console.log("didalem if2")
                        console.log(self.tampung)
                        self.tampung.splice(index,1)
                        kurang(self.listRequestResumes.resumeInvoiceList[index].fee);
                    }
                }

            }
            self.document=check;
            self.tampung.push(self.listRequestResumes.resumeInvoiceList[index]);
            hitung(self.listRequestResumes.resumeInvoiceList[index].fee);
                    // console.log(self.tampung)
        }

    // hitung();
    };

    self.checkedAll = function() {
        // console.log(self.document)
            self.totalFee = 0;
            self.totalData = 0;
        if(self.document){
            for(var i=0; i<self.listRequestResumes.resumeInvoiceList.length; i++){
                self.listRequestResumes.resumeInvoiceList[i].value = false;
                self.document = false;

                if(self.tampung.length > 0){
                // console.log(self.tampung)
                    self.tampung.splice(i,1)
                }
            }

        }
        else{
            for(var i=0; i<self.listRequestResumes.resumeInvoiceList.length; i++) {

                // if (self.listRequestResumes.resumeInvoiceList[i].value = false){
                    // console.log(i)
                    self.listRequestResumes.resumeInvoiceList[i].value = true;
                    self.document = true;
                    self.tampung.push(self.listRequestResumes.resumeInvoiceList[i])
                    self.totalFee = self.totalFee + self.listRequestResumes.resumeInvoiceList[i].fee;
                    self.totalData = self.totalData + 1;
                // }
                // else{

                // }
            }

        }

    // hitung();
    };
    self.totalData = self.tampung.length;
    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah user?',
                        path: 'dashboard/security-admin'
                    };
                }
            }
        });

    };

     function doSearch(input) {
        // var request = {};
        // request.startDate = 1480525200000
        // request.endDate = 1483981200000
        getListOfRequestCancelResume(input);
    };
    self.Search = function(reportDate){
        reportDate = Date.parse(reportDate);
        getListOfRequestCancelResume(reportDate);
        // self.search.endDate = Date.parse(self.search.endDate);
        // console.log(self.search.startDate);
        // var request = {};
        // request.startDate = self.search.startDate;
        // // request.endDate = self.search.endDate;
        // doSearch(request.startDate);
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    backToTop();

    // getListOfRequestCancelResume();

};
