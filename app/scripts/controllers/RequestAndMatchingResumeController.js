'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('requestAndMatchingResumeCtrl', [
        '$scope', '$modal',
        '$location',
        '$window',
        'ResumeService',
        'DokumenKelengkapanService',
        requestAndMatchingResumeCtrl
    ]);

function requestAndMatchingResumeCtrl($scope, $modal, $location, $window, ResumeService, DokumenKelengkapanService) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    var request = {
        "branchList": [],
        "branchCentralList": [],
        "groupList": []
    };

    self.arr = [];

    self.otherDocument = false;

    self.retrieveAppId = function() {
        ResumeService.getRetrieveResumeApp(self.contentData.appId).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.dataUser = response.result;
                    self.contentData.companyName = self.dataUser.companyName;
                    self.contentData.npwp = self.dataUser.npwp;
                }
            });
    };

    var backToTop = function() {
        $window.scrollTo(0, 0);
    };

    var getListOfDocumentSetting = function() {
        backToTop();
        DokumenKelengkapanService.getListOfDocumentSetting().then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.listDocument = response.result;
                }
            });
    };


    self.doAddOtherFile = function(bool) {
        if (bool == undefined) {
            return self.otherDocument = true;
        } else {
            return self.otherDocument = false;
        }

    };


    var invalidModal = function(message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function() {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.showModalBack = function() {
        setNull();
    };

    var checking = function() {
        self.nipInvalid = false;
        self.userNameInvalid = false;
        self.confirmationPasswordInvalid = false;
        self.fullNameInvalid = false;
        self.divisionInvalid = false;
        self.positionInvalid = false;
        self.emailInvalid = false;
        self.branchInvalid = false;
        self.centralBranchInvalid = false;
        self.groupInvalid = false;

        var bool = false;

        if (self.contentData.username == null) {
            self.userNameInvalid = true;
            bool = true;
        }
        if (self.contentData.nip == null) {
            self.nipInvalid = true;
            bool = true;
        }
        if (self.contentData.fullName == null) {
            self.fullNameInvalid = true;
            bool = true;
        }
        if (self.contentData.division == null) {
            self.divisionInvalid = true;
            bool = true;
        }
        if (self.contentData.position == null) {
            self.positionInvalid = true;
            bool = true;
        }
        if (self.contentData.centralBranch == null) {
            self.centralBranchInvalid = true;
            bool = true;
        }
        if (self.contentData.group == null) {
            self.groupInvalid = true;
            bool = true;
        }
        return bool;
    };

    self.addFile = function(ele, index) {
        var files = ele.files[0];

        setTimeout(function() {
            self.listDocument[index].file = files;
            console.log(self.listDocument[index]);
        }, 2);


        // self.listDocument[index].file = files;
    };

    function setNull() {
        self.contentData.appId = null;
        self.contentData.companyName = null;
        self.contentData.npwp = null;
        if (self.myFile != null || self.myFile2 != null) {
            self.myFile = null;
            self.myFile2 = null;
        }
        if (self.listDocument != null) {
            for (var index = 0; index < self.listDocument.length; index++) {
                self.listDocument[index].file = null;
            }
        }

    };

    $scope.fileInputs = [1, 2, 3];
    self.uploadFile = function() {
        // for (var indexFile=0; indexFile<self.listDocument.length; indexFile++){
        //     console.log(self.listDocument[indexFile].file);    
        // }
        if (self.myFile == null || self.myFile2 == null) {
            invalidModal("File AHU dan NPWP Wajib Terisi");
        } else {
            var file = self.myFile;
            var file2 = self.myFile2;

            var fileExt1 = self.myFile.name.split(".").pop();
            fileExt1 = fileExt1.toUpperCase();

            var fileExt2 = self.myFile2.name.split(".").pop();
            fileExt2 = fileExt2.toUpperCase();

            if ((fileExt1 != "PDF" && fileExt1 != "JPG" && fileExt1 != "JPEG" && fileExt1 != "TIF") || (fileExt2 != "PDF" && fileExt2 != "JPG" && fileExt2 != "JPEG" && fileExt2 != "TIF")) {
                invalidModal("Format File AHU & NPWP Harus PDF/JPG/JPEG/TIF");
            }

            var fd = new FormData();
            fd.append('file1', file);
            fd.append('file2', file2);
            fd.append('contentData', angular.toJson(self.contentData));


            ResumeService.submitResume(fd).then(
                function(response) {
                    if (response.message !== 'ERROR') {
                        var result = response.result;
                        if (result.match == true) {
                            var date = new Date(result.effectiveDate);
                            var year = date.getFullYear();
                            var month = date.getMonth('') + 1;
                            var day = date.getDate();
                            var textMonth;
                            if (month == 1) {
                                textMonth = 'Januari';
                            } else if (month == 2) {
                                textMonth = 'Februari';
                            } else if (month == 3) {
                                textMonth = 'Maret';
                            } else if (month == 4) {
                                textMonth = 'April';
                            } else if (month == 5) {
                                textMonth = 'Mei';
                            } else if (month == 6) {
                                textMonth = 'Juni';
                            } else if (month == 7) {
                                textMonth = 'Juli';
                            } else if (month == 8) {
                                textMonth = 'Agustus';
                            } else if (month == 9) {
                                textMonth = 'September';
                            } else if (month == 10) {
                                textMonth = 'Oktober';
                            } else if (month == 11) {
                                textMonth = 'November';
                            } else if (month == 12) {
                                textMonth = 'Desember';
                            }

                            $modal.open({
                                templateUrl: 'views/modal/ModalConfirmationMatch.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function() {
                                        return {
                                            title: 'Informasi',
                                            message: "Data Match! Sama dengan " + result.resumeId + ' Masa Berlaku Hingga ' + day + " " + textMonth + " " + year,
                                            path: ''
                                        };
                                    }
                                }
                            });
                            $location.path('/dashboard/matching-result').search({ id: result.resumeId });
                        } else {
                            var counting = 0;
                            var successDoc = 0;
                            var request = {};
                            for (var i = 0; i < self.listDocument.length; i++) {

                                if (self.listDocument[i].mandatory == true && self.listDocument[i].file == null) {
                                    invalidModal(self.listDocument[i].name + " Tidak Boleh Kosong");
                                    break;
                                } else {
                                    var invalidFileType = false;
                                    var fd = new FormData();
                                    request.resumeId = result.resumeId;
                                    request.documentCode = self.listDocument[i].code;
                                    if (self.listDocument[i].file != null) {
                                        counting++;
                                        request.documentDesc = self.listDocument[i].name;
                                        var extn = self.listDocument[i].file.name.split(".").pop();
                                        extn = extn.toUpperCase();

                                        var fileType = self.listDocument[i].fileType.split(",");

                                        for (var z = 0; z < fileType.length; z++) {
                                            if (fileType[z] == extn) {
                                                invalidFileType = true;
                                                break;
                                            }
                                        }

                                        if (invalidFileType == false) {
                                            invalidModal("Format File " + self.listDocument[i].name + " Harus " + self.listDocument[i].fileType);
                                            break;
                                        } else {
                                            fd.append('file', self.listDocument[i].file);
                                            fd.append('contentData', angular.toJson(request));
                                            if (self.listDocument[i].file != null) {
                                                ResumeService.submitDocument(fd).then(
                                                    function(response) {
                                                        if (response.message !== 'ERROR') {
                                                            successDoc++;
                                                            if (counting == successDoc) {
                                                                $modal.open({
                                                                    templateUrl: 'views/modal/Modal.html',
                                                                    controller: 'ModalCtrl',
                                                                    size: 'sm',
                                                                    resolve: {
                                                                        modalParam: function() {
                                                                            return {
                                                                                title: 'Informasi',
                                                                                message: 'Berhasil Upload Dokumen',
                                                                                path: ''
                                                                            };
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            setNull();
                                                        } else {
                                                            self.isDisabled = false;
                                                            $modal.open({
                                                                templateUrl: 'views/modal/Modal.html',
                                                                controller: 'ModalCtrl',
                                                                size: 'sm',
                                                                resolve: {
                                                                    modalParam: function() {
                                                                        return {
                                                                            title: 'Gagal',
                                                                            message: response.result,
                                                                            path: ''
                                                                        };
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function() {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                }
            );
        }
    };

    getListOfDocumentSetting();

};