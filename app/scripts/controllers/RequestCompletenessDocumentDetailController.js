'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('requestCompletenessDocumentDetailCtrl', [
        '$scope', '$modal',
        '$location',
        'FidusiaService',
        '$sessionStorage',
        'CONF',
        '$window',
        requestCompletenessDocumentDetailCtrl
    ]);

function requestCompletenessDocumentDetailCtrl($scope, $modal, $location, FidusiaService, $sessionStorage, CONF, $window) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    var paramValue = $location.search().id;

    var request = {
        "documentList" : [],
        "documentTypeList": []
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var getDetailResume = function () {

        backToTop();

        FidusiaService.getRequestOtherDocumentDetail(paramValue).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;
                    var access_token = $sessionStorage.accessToken;
                    if(self.dataResult.documentList != undefined){
                        for (var j=0; j< self.dataResult.documentList.length;j++){

                            var urlConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_DOCUMENT +'?access_token='+access_token+'&resumeId='+self.dataResult.id+'&docType='+self.dataResult.documentList[j].documentCode;
                            self.dataResult.documentList[j].url = urlConstruct;

                        }
                    }

                    if(self.dataResult.ahuFile == true){
                        var urlAhuConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_RESUME +'?access_token='+access_token+'&resumeId='+self.dataResult.id;
                        self.dataResult.resumeAhuUrl = urlAhuConstruct;
                    }

                    if(self.dataResult.npwpFile == true){
                        var urlnpwpConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_NPWP +'?access_token='+access_token+'&resumeId='+self.dataResult.id;
                        self.dataResult.npwpUrl = urlnpwpConstruct;
                    }

                }
            });
    };

    self.method = [
        {"keyName" : "input", "value":"Input"},
        {"keyName" : "upload", "value":"Upload"},
    ];

    var constructSelect =  function () {
        var indexFound = self.method.map(function (e) {
            return e.keyName;
        }).indexOf(self.method);
        self.itemSearch = self.method[0];
    };

    if(self.verificationAhu==true){
        self.ahuDescription = "";
    };

    if(self.verificationNpwp==true){
        self.npwpDescription = "";
    };

    constructSelect();


    self.lovGroupPrivilege = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovGroupPrivilege.html',
            controller: 'modalGroupPrivilegeCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData.group
                    };
                }
            }
        });
        modalInstance.result.then(function (listGroup) {
            self.contentData.group = listGroup;
        });
    };

    self.deleteGroup = function(index){
        self.contentData.group.splice(index,1);
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.checkedAll = function() {
        if(self.documentTypeList){
            for(var i=0; i<self.dataResult.documentTypeList.length; i++){
                self.dataResult.documentTypeList[i].value = false;
                self.documentTypeList = false;
            }
        }
        else{
            for(var i=0; i<self.dataResult.documentTypeList.length; i++) {
                self.dataResult.documentTypeList[i].value = true;
                self.documentTypeList = true;
            }

        }
    };

    self.changed = function (index, value) {
        self.dataResult.documentList[index].verificationStatus = value;
        if(self.dataResult.documentList[index].verificationStatus==true){
            self.dataResult.documentList[index].description = "";
        }
    };

    self.changedAhu = function (value) {
        self.verificationAhu = value;
        if(self.verificationAhu==true){
            self.ahuDescription = "";
        }
    };

    self.changedNpwp = function (value) {
        self.verificationNpwp = value;
        if(self.verificationNpwp==true){
            self.npwpDescription = "";
        }
    };


    self.checkedChild = function (value){
        var check = true;
        if(!value){
            self.documentTypeList = false;
        }
        else{
            for(var i=0; i<self.dataResult.documentTypeList.length; i++) {
                if(!self.dataResult.documentTypeList[i].value){
                    check = false;
                }

            }
            self.documentTypeList=check;
        }

        console.log()

    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses request dokumen kelengkapan?',
                        path: 'dashboard/request-document-kelengkapan'
                    };
                }
            }
        });
    };

    self.tab = 1;

    self.setTab = function (tabId) {
        self.tab = tabId;
    };

    self.isSet = function (tabId) {
        return self.tab === tabId;
    };


    self.documentCompleted = function () {

        if(self.dataResult.documentList != null){
            var counting = 0;
            for(var i=0; i<self.dataResult.documentList.length; i++) {

                if (self.dataResult.documentList[i].verificationStatus == true) {
                    counting++;
                }
            }

            if (counting == self.dataResult.documentList.length) {

                if(self.dataResult.ahuFile == true && self.verificationAhu!=true){
                    invalidModal("Pilih OK untuk proses dokumen yang telah lengkap");
                }
                else if(self.dataResult.npwpFile == true && self.verificationNpwp!=true){
                    invalidModal("Pilih OK untuk proses dokumen yang telah lengkap");
                }
                else{
                    var contentData = {
                        'id': self.dataResult.id,
                        'version': self.dataResult.version
                    };

                    console.log("Document List NOT NULL");

                    FidusiaService.completedResume(contentData).then(
                        function (response) {
                            if (response.message !== 'ERROR') {
                                $modal.open({
                                    templateUrl: 'views/modal/Modal.html',
                                    controller: 'ModalCtrl',
                                    size: 'sm',
                                    resolve: {
                                        modalParam: function () {
                                            return {
                                                title: 'Informasi',
                                                message: 'Document telah lengkap',
                                                path: ''
                                            };
                                        }
                                    }
                                });
                                $location.path('/dashboard/request-document-kelengkapan');
                            }else{
                                self.isDisabled = false;
                                $modal.open({
                                    templateUrl: 'views/modal/Modal.html',
                                    controller: 'ModalCtrl',
                                    size: 'sm',
                                    resolve: {
                                        modalParam: function () {
                                            return {
                                                title: 'Gagal',
                                                message: response.result,
                                                path: ''
                                            };
                                        }
                                    }
                                });
                            }

                        }
                    );
                }

            }

            else{
                invalidModal("Pilih OK untuk proses dokumen yang telah lengkap");
            }

        }
        else{
            if(self.dataResult.ahuFile == true && self.verificationAhu!=true){
                invalidModal("Pilih OK untuk proses dokumen yang telah lengkap");
            }
            else if(self.dataResult.npwpFile == true && self.verificationNpwp!=true){
                invalidModal("Pilih OK untuk proses dokumen yang telah lengkap");
            }
            else{
                var contentData = {
                    'id': self.dataResult.id,
                    'version': self.dataResult.version
                };

                FidusiaService.completedResume(contentData).then(
                    function (response) {
                        if (response.message !== 'ERROR') {
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Informasi',
                                            message: 'Document telah lengkap',
                                            path: ''
                                        };
                                    }
                                }
                            });
                            $location.path('/dashboard/request-document-kelengkapan');
                        }else{
                            self.isDisabled = false;
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Gagal',
                                            message: response.result,
                                            path: ''
                                        };
                                    }
                                }
                            });
                        }

                    }
                );
            }

        }
            

    };

    self.requestDocument = function () {
        
        for(var i=0; i<self.dataResult.documentTypeList.length; i++){

            if(self.dataResult.documentTypeList[i].value==true){
                if(self.dataResult.documentTypeList[i].documentDescRequest==null){
                    invalidModal("Keterangan File yang Di request Tidak Boleh Kosong");
                    break;
                }
                else{
                    var listId = {
                        'documentCode' : self.dataResult.documentTypeList[i].code,
                        'description': self.dataResult.documentTypeList[i].documentDescRequest
                    };
                    request.documentTypeList.push(listId);
                }
            }
        }

        for(var i=0; i<self.dataResult.documentList.length; i++) {
                var listId = {
                    'documentCode' : self.dataResult.documentList[i].documentCode,
                    'verify' : self.dataResult.documentList[i].verificationStatus,
                    'description': self.dataResult.documentList[i].description
                };
                request.documentList.push(listId);
        };

        if(self.ahuDescription==undefined){
            request.ahuDesc = "";
        }
        else{
            request.ahuDesc = self.ahuDescription;
        }

        if(self.npwpDescription==undefined){
            request.npwpDesc = "";
        }
        else{
            request.npwpDesc = self.npwpDescription;
        }

        request.ahuVerify = self.verificationAhu;
        request.npwpVerify = self.verificationNpwp;
        request.id = self.dataResult.id;
        request.version = self.dataResult.version;



        // var hasil = checking();
        //
        // if (hasil == true) {
        //     invalidModal("Ada yang tidak valid, mohon periksa kembali");
        // }else {

            FidusiaService.requestOtherDocumentProcess(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Berhasil Request Dokumen Kelengkapan',
                                        path: ''
                                    };
                                }
                            }
                        });
                        $location.path('/dashboard/request-document-kelengkapan');
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        // }
    };

    getDetailResume();

};
