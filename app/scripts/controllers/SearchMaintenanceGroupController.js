'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('searchMaintenanceGroupCtrl', [
        '$scope','CONF', 'GroupService', '$location','$sessionStorage',
        '$modal', '$window',
        searchMaintenanceGroupCtrl
    ]);

function searchMaintenanceGroupCtrl($scope,CONF,GroupService,$location,$sessionStorage,$modal, $window) {
    var self = $scope;

    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;

    self.close = close;

    self.createGroup =  function () {
        $location.path('/dashboard/create-group');
    };

    self.doUpdateGroup = function (id) {
        $location.path('/dashboard/update-group').search({id: id});
    };

    self.search = [
        {"keyName" : "id", "value":"ID"},
        {"keyName" : "groupName", "value":"Nama Group"},
    ];

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var getListGroup = function (type,value) {
        backToTop();
        var user = '';
        var id = '';
        var name='';

        if($sessionStorage.domain==CONF.USER_NOTARY){
            user = 1;
        }

        if(type && value != undefined){
            console.log(type);
            console.log(value);
            if(type=='id'){
                id = value;
                name='';

                var indexFound = self.search.map(function (e) {
                    return e.keyName;
                }).indexOf(self.search);
                self.itemSearch = self.search[0];

            } else if(type=='groupName'){
               name = value;
               id = '';

                var indexFound = self.search.map(function (e) {
                    return e.keyName;
                }).indexOf(self.search);
                self.itemSearch = self.search[1];
            }

        }
        else{
            var indexFound = self.search.map(function (e) {
                return e.keyName;
            }).indexOf(self.search);
            self.itemSearch = self.search[0];

        }

        GroupService.getListGroup(self.currentPage-1, self.limit,id,name,user).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.contentData = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                }
            });
    };

    self.doDeleteGroup = function (id,name) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteGroup.html',
            controller: 'deleteGroupCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id,
                        name: name
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.contentData.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListGroup();
            }
        });
    };

    self.doSearch = function (type,value) {
        self.currentPage = 1;
        getListGroup(type.keyName,value);
    };

    getListGroup();

 }

 
