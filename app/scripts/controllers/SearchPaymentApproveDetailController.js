'use strict';

angular.module('nostraApp')
    .controller('searchPaymentApproveDetailCtrl', [
        '$scope', '$modal', '$window',
        '$location',
        'InvoiceService','ParameterNotaryService','FinanceService',
        searchPaymentApproveDetailCtrl
    ]);

function searchPaymentApproveDetailCtrl($scope, $modal, $window, $location, InvoiceService,ParameterNotaryService,FinanceService) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    self.currentPage = 0;
    self.numPages = 1;

    var paramId = $location.search().id;
    // var date = new Date();
    // var test = Date.parse(date);
    // console.log(test)
    self.search = [
        {"keyName" : "appId", "value":"APP ID"},
        {"keyName" : "npwp", "value":"NPWP"},
        {"keyName" : "companyName", "value":"Nama Perusahaan"}
    ];
    self.totalFee = 0;
    self.totalData = 0;
    self.tampung=[];
    self.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    self.request = {};
    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };
    self.start = {
        opened: false
    };

    self.end = {
        opened: false
    };

    self.documentDate = {
        opened: false
    };

    self.documentPeriod = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    }; 
    self.openStart = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.start.opened)
            self.start.opened = false;
        else
            self.start.opened = true;
    };  
    self.openEnd = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.end.opened)
            self.end.opened = false;
        else
            self.end.opened = true;
    };    
    self.openAkhir = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.akhir.opened)
            self.akhir.opened = false;
        else
            self.akhir.opened = true;
    };
 
    self.openDocumentDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.documentDate.opened)
            self.documentDate.opened = false;
        else
            self.documentDate.opened = true;
    };
    self.openDocumentPeriod = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.documentPeriod.opened)
            self.documentPeriod.opened = false;
        else
            self.documentPeriod.opened = true;
    };
    self.goToDetail = function(id){
        $location.path('/dashboard/payment-detail').search({id:id});
    };
    self.goToDetailApprove = function(id){
        $location.path('/dashboard/approve-payment-detail').search({id:id});
    };
    self.submit = function(){
        self.request.id = self.listRequestResumes.id;
        self.request.version = self.listRequestResumes.version;
        console.log(self.request)
        FinanceService.approveFinance(self.request).then(
            function (response) {
                    console.log(response)
                if (response.message === 'OK') {
                    $location.path('/dashboard/search-payment-approve');
                }
            }
        );
        // console.log(self.request)
    }

    self.getListInvoice = function(notary) {
        FinanceService.getBankAccount(notary.id).then(
            function (response) {
                if (response.message === 'OK') {
                    self.invoices = response.result;
                    console.log(self.invoices)
                    self.itemInvoice = self.invoices[0];
                    self.request.bank = notary.id;
                    self.request.bankName = notary.bankName;
                    self.request.accountNumber = self.itemInvoice.accountNumber;
                }
            }
        );
    };

    self.getListReceiveInvoice = function(input) {
        self.request.accountNumber = input.accountNumber;

    };
    var getListOfRequestCancelResume = function () {
        console.log(self.input)
        var request = {};
        request.startDate = 1480525200000;
        request.endDate = 1484240400000;
        InvoiceService.getlistOfInvoice(request).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.listRequestResumes = response.result;
                    for (var i=0;i<self.listRequestResumes.resumeInvoiceList.length;i++){
                        self.listRequestResumes.resumeInvoiceList[i].value = false;
                    }

                }
            });
    };

    var getAccountNumber = function (notary,id) {
        
        FinanceService.getBankAccount(notary.id).then(
            function (response) {
                if (response.message === 'OK') {
                    self.invoices = response.result;

                    var indexFound = self.invoices.map(function (e) {
                           return e.accountNumber;
                       }).indexOf(id);
                    // console.log(notary)
                    // console.log(id)

                    // console.log(self.invoices)
                       self.itemInvoice = self.invoices[indexFound];
                    self.request.bank = notary.id;
                    self.request.bankName = notary.bankName;
                    self.request.accountNumber = self.itemInvoice.accountNumber;
                }
            }
        );
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    function getDetailFinance(input) {

        backToTop();

        FinanceService.getFinanceApproveDetail(input).then(
            function (response) {
                if (response.message === 'OK') {
                    
                    self.notaries = response.result.bankList;
                    var indexFound = self.notaries.map(function (e) {
                           return e.id;
                       }).indexOf(response.result.bankPayment);
                       self.itemNotary = self.notaries[indexFound];
                    getAccountNumber(self.itemNotary,response.result.accountPayment)
                    // self.notaries = response.result.bankList;
                    // var indexFound = self.notaries.map(function (e) {
                    //        return e.id;
                    //    }).indexOf(response.result.bankPayment);
                    //    self.itemNotary = self.notaries[indexFound];
                    // self.itemNotary = self.notaries[0];
                    self.listRequestResumes = response.result;
                }
            }
        );
    };

    self.lovGroupPrivilege = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovGroupPrivilege.html',
            controller: 'modalGroupPrivilegeCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData.group
                    };
                }
            }
        });
        modalInstance.result.then(function (listGroup) {
            self.contentData.group = listGroup;
        });
    };

    self.deleteGroup = function(index){
        self.contentData.group.splice(index,1);
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.value = function (isSelected, item,index) {
        if (isSelected == true) {
            self.tampung.push(item);
            self.totalFee = self.totalFee + item.fee;
            self.totalData = self.totalData + 1;

        } else {
            self.tampung.splice(index, 1);
            self.totalFee = self.totalFee - item.fee;
            self.totalData = self.totalData - 1;
        }
        console.log(self.tampung)
    }
    self.checkedChild = function (value,isi){
        // console.log(value)
        var check = true;
        if(!value.value){
            console.log("cancel dari isi")
            self.document = false;
            kurang(self.listRequestResumes.resumeInvoiceList[index].fee);
        }
        else{
            console.log("tambah?")
            for(var i=0; i<self.listRequestResumes.resumeInvoiceList.length; i++) {

                if(!self.listRequestResumes.resumeInvoiceList[i].value){
                    console.log("didalem if")
                    check = false;
                    // console.log(self.tampung)
                    if(self.tampung.length > 0){
                    console.log("didalem if2")
                        console.log(self.tampung)
                        self.tampung.splice(index,1)
                        kurang(self.listRequestResumes.resumeInvoiceList[index].fee);
                    }
                }

            }
            self.document=check;
            self.tampung.push(self.listRequestResumes.resumeInvoiceList[index]);
            hitung(self.listRequestResumes.resumeInvoiceList[index].fee);
                    // console.log(self.tampung)
        }

    // hitung();
    };

    self.checkedAll = function() {
        // console.log(self.document)
            self.totalFee = 0;
            self.totalData = 0;
        if(self.document){
            for(var i=0; i<self.listRequestResumes.resumeInvoiceList.length; i++){
                self.listRequestResumes.resumeInvoiceList[i].value = false;
                self.document = false;

                if(self.tampung.length > 0){
                // console.log(self.tampung)
                    self.tampung.splice(i,1)
                }
            }

        }
        else{
            for(var i=0; i<self.listRequestResumes.resumeInvoiceList.length; i++) {

                // if (self.listRequestResumes.resumeInvoiceList[i].value = false){
                    // console.log(i)
                    self.listRequestResumes.resumeInvoiceList[i].value = true;
                    self.document = true;
                    self.tampung.push(self.listRequestResumes.resumeInvoiceList[i])
                    self.totalFee = self.totalFee + self.listRequestResumes.resumeInvoiceList[i].fee;
                    self.totalData = self.totalData + 1;
                // }
                // else{

                // }
            }

        }

    // hitung();
    };
    self.totalData = self.tampung.length;
    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah user?',
                        path: 'dashboard/security-admin'
                    };
                }
            }
        });

    };

    self.doSearch = function () {
       
        getListOfRequestCancelResume()
    };

    getDetailFinance(paramId);

};
