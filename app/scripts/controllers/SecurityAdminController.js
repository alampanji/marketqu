'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('securityAdminCtrl', [
        '$scope', 'CONF', '$location', '$modal', 'UserService', '$window',
        securityAdminCtrl
    ]);

function securityAdminCtrl($scope, CONF, $location, $modal, UserService, $window) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.createUser = function() {
        $location.path('/dashboard/create-user');
    };

    self.doUpdateUser = function() {
        $location.path('/dashboard/update-user');
    };

    self.search = [
        { "type": "nip", "label": "NIP" },
        { "type": "name", "label": "Nama" },
    ];

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus = function(status) {
        if (status) return "active";
        else return "not active";
    };

    self.search = [
        { "keyName": "nip", "value": "NIP" },
        { "keyName": "name", "value": "Nama User" },
    ];

    self.status = [
        { keyName: 'true', name: 'Aktif' },
        { keyName: 'false', name: 'Tidak Aktif' }
    ];

    self.pageChanged = function() {
        getListUser();
    };

    var backToTop = function() {
        $window.scrollTo(0, 0);
    };

    var getListUser = function(type, value) {

        backToTop();

        var nip = '';
        var name = '';

        if (type != undefined && value != undefined) {
            if (type == 'nip') {
                name = '';
                nip = value;
                var indexFound = self.search.map(function(e) {
                    return e.keyName;
                }).indexOf(self.search);
                self.itemSearch = self.search[0];
            } else if (type == 'name') {
                name = value;
                nip = '';
                var indexFound = self.search.map(function(e) {
                    return e.keyName;
                }).indexOf(self.search);
                self.itemSearch = self.search[1];
            }
        } else {
            var indexFound = self.search.map(function(e) {
                return e.keyName;
            }).indexOf(self.search);
            self.itemSearch = self.search[0];

        }
        UserService.getListUserInternal(self.currentPage - 1, self.limit, nip, name).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.users = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                }
            });
    };

    self.doUpdateUser = function(id) {
        $location.path('/dashboard/update-user').search({ id: id });
    };

    self.doDeleteUser = function(id, name) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteUser.html',
            controller: 'deleteUserInternalCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function() {
                    return {
                        id: id,
                        name: name,
                    };
                }
            }
        });
        modalDelete.result.then(function() {}, function(data) {
            if (data == "close") {
                if (self.users.length == 1 && self.currentPage != 1) {
                    self.currentPage--;
                }
                getListUser();
            }
        });
    };

    self.doSearch = function(type, value) {
        self.currentPage = 1;
        getListUser(type.keyName, value);
    };

    getListUser();
};