'use strict';
/**
 * @ngdoc function
 * @name marketplaceApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the marketplaceApp
 */

angular.module('marketplaceApp')
    .controller('SettingAccountCtrl', [
        '$scope', '$modal', 'InstallService', 'SiteService', '$filter',
        '$location',
        '$window',
        SettingAccountCtrl
    ]);

function SettingAccountCtrl($scope, $modal, InstallService, SiteService, $filter, $location, $window) {
    var self = $scope;
    var contentData = {};
    self.contentData = {};
    var bank;

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    self.state = 'FIRST_STEP';

    function checking() {
        var isEmpty = false;
        self.marketplaceNameInvalid = false;
        self.marketplaceEmailInvalid = false;
        self.marketplacePhoneInvalid = false;
        self.marketplaceAddressInvalid = false;
        self.usernameInvalid = false;
        self.passwordInvalid = false;
        self.logoInvalid = false;
        self.iconInvalid = false;
        self.adminNameInvalid = false;
        self.emailInvalid = false;
        self.phoneNumberInvalid = false;
        self.avatarInvalid = false;
        self.institutionInvalid = false;
        self.departmentInvalid = false;
        self.accountNameInvalid = false;
        self.accountNumberInvalid = false;
        if(self.dbName == '' || self.dbName == null){
            self.dbNameInvalid = true;
            isEmpty = true;
        }
        if(self.dbUsername == '' || self.dbUsername == null){
            self.dbUsernameInvalid = true;
            isEmpty = true;
        }
        if(self.dbHost == '' || self.dbHost == null){
            self.dbHostInvalid = true;
            isEmpty = true;
        }
        return isEmpty;
    }

    self.cancel = function () {
        self.dbName = '';
        self.dbUsername = '';
        self.dbPassword = '';
        self.dbHost = '';
    };

    self.checkPassword = function () {
        var boolean = false;
        if(self.contentData.password != self.contentData.confirmPassword){
            boolean = true;
        }
        return boolean
    };

    function invalidModal(message) {
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });
    }

    var listBank = function () {
        SiteService.getListBank().then(
            function (response) {
                if (response.status === 'OK') {
                    self.banks = response.result;
                    self.itemBank = self.banks[0];
                    bank = self.banks[0];
                }
            });
    };

    self.setBank = function (bank) {
        bank = bank.id;
    };

    self.submit = function () {

        var validation = checking();
        //
        // if(validation == true){
        //     invalidModal()
        // }
        // else {
            contentData.action = 'site';
            contentData.site_name = self.contentData.marketplaceName;
            contentData.site_email = self.contentData.marketplaceEmail;
            contentData.site_phone = self.contentData.marketplacePhone;
            contentData.site_address = self.contentData.marketplaceAddress;
            contentData.username = self.contentData.username;
            contentData.password = self.contentData.password;
            contentData.name = self.contentData.name;
            contentData.email = self.contentData.email;
            contentData.contact = self.contentData.contact;
            contentData.country = self.contentData.country;
            contentData.province = self.contentData.province;
            contentData.city = self.contentData.city;
            contentData.institution = self.contentData.institution;
            contentData.department = self.contentData.department;
            contentData.bank_id = bank;
            contentData.name_rek = self.contentData.accountName;
            contentData.no_rek = self.contentData.accountNumber;
        // }

        var formDt = new FormData();
        formDt.append('logo',self.contentData.logo);
        formDt.append('icon',self.contentData.icon);
        formDt.append('photo',self.contentData.avatar);
        formDt.append('site_name', angular.toJson(self.contentData.marketplaceName));
        formDt.append('site_email', angular.toJson(contentData.site_email));
        formDt.append('site_phone', angular.toJson(contentData.site_phone));
        formDt.append('site_address', angular.toJson(contentData.site_address));
        formDt.append('username', angular.toJson(self.contentData.username));
        formDt.append('password', angular.toJson(self.contentData.password));
        formDt.append('name', angular.toJson(self.contentData.adminName));
        formDt.append('email', angular.toJson(contentData.email));
        formDt.append('contact', angular.toJson(contentData.phoneNumber));
        formDt.append('country', angular.toJson(contentData.country));
        formDt.append('province', angular.toJson(contentData.province));
        formDt.append('city', angular.toJson(contentData.city));
        formDt.append('institution', angular.toJson(contentData.institution));
        formDt.append('department', angular.toJson(contentData.department));
        formDt.append('address', angular.toJson(self.contentData.address));
        formDt.append('bank_id', angular.toJson(contentData.bank_id));
        formDt.append('name_rek', angular.toJson(contentData.name_rek));
        formDt.append('no_rek', angular.toJson(contentData.no_rek));
        InstallService.createSiteInstallation(formDt).then(
            function (response) {
                if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Berhasil Daftar',
                                        path: 'user/home'
                                    };
                                }
                            }
                        });
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            }
        );

        // console.log(contentData);
    };

    self.next = function (step) {
        self.state = step;
        return self.state;
    };

    self.back = function () {
        self.state = 'FIRST_STEP';
        console.log (self.state);
        return self.state;
    };
    
    function getMigration() {
        InstallService.getMigration().then(
            function(response) {
                console.log(response.status);
            }
        );
    }
    
    getMigration();

    listBank();

    backToTop();

};
