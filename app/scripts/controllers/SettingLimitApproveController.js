'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('settingLimitApproveCtrl', [
        '$scope','$modal','SettingLimitApproveService','$window',
        settingLimitApproveCtrl
    ]);

function settingLimitApproveCtrl($scope,$modal,SettingLimitApproveService, $window) {
    var self = $scope;

    self.maxSize = 5;
    self.bigTotalItems = self.index;
   
    self.newLine = false;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var today = new Date();
    self.minDate = today.setDate(today.getDate() + 1);
    function close() {
        $modalInstance.dismiss('close');
    };

    self.isDisabled = false;

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    };

    self.createSLA = function(){
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalCreateSettingLimitApprove.html',
            controller: 'createSettingLimitApproveCtrl',
            size: 'md',
            backdrop:'static',
            resolve: {
                modalParam: function () {
                    return {
                        path: 'dashboard/setting-limit-approve'
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                getListSLA();
            }
        });
    };

    self.doDeleteSLA = function (id,name,version) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteSettingLimitApprove.html',
            controller: 'deleteSettingLimitApproveCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id,
                        name:name,
                        version:version
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.datas.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListSLA();
            }
        });
    };

    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.pageChanged = function () {
        getListBranchCentral();
    };

    var getListSLA = function () {
        backToTop();
        SettingLimitApproveService.getlistSLA().then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.datas = response.result;
                }
            });
    };

    getListSLA();
};