'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */
angular.module('nostraApp').filter('idr', function () {
        return function (val) {
            while (/(\d+)(\d{3})/.test(val.toString())){
                val = val.toString().replace(/(\d+)(\d{3})/, '$1'+'.'+'$2');
            }
            var val = val +',00';
            return val;
          };
});
angular.module('nostraApp')
    .controller('settingBiayaResumeAHUCtrl', [
        '$scope','CONF','$location','$modal','BiayaResumeService', '$window',
        settingBiayaResumeAHUCtrl
    ]);

function settingBiayaResumeAHUCtrl($scope,CONF,$location,$modal,BiayaResumeService, $window) {
    var self = $scope;
    self.close = close;
    self.user= {};
    function close() {
        $modalInstance.dismiss('close');
    }

    self.checkValidasi = function(input){
    var today = Date.parse(new Date());
        if(parseInt(input) > today){
            return true;
        }
        else{
            return false;
        }
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    self.doCreateBiayaResume = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalAddParameterBiayaResume.html',
            controller: 'addParameterBiayaResumeCtrl',
            size: 'md',
            backdrop:'static',
            resolve: {
                modalParam: function () {
                    return {
                    };
                }
            }
        });
        modalInstance.result.then(function (listBranchCentral) {
            getListNotary();
        });
    };
    self.checkValidasi = function(input){
    var today = Date.parse(new Date());
        if(parseInt(input) > today){
            return true;
        }
        else{
            return false;
        } 
    }
    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.pageChanged = function () {
        getListTransaction();
    };


    self.doDeleteBiayaResume = function(id,effectiveDate){
            var modalHapus = $modal.open({
                templateUrl: 'views/modal/ModalDeleteBiayaResume.html',
                controller: 'ModalDelBiayaResumeCtrl',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    modalParam: function () {
                        return {
                            id: id,
                            effectiveDate : effectiveDate
                        };
                    }
                }
            });
            modalHapus.result.then(function () {
                getListNotary();
            })
            };

        
        


    var getListNotary = function () {
        backToTop();
        self.name='';
        BiayaResumeService.getlistBiayaResume(self.currentPage-1, self.limit,self.name,self.idNotary).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.branches = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                }
            });
    }

    getListNotary();
};