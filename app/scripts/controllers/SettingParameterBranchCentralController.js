'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('settingParameterBranchCentralCtrl', [
        '$scope','CONF','$location','$modal','BranchCentralService', '$window',
        settingParameterBranchCentralCtrl
    ]);

function settingParameterBranchCentralCtrl($scope,CONF,$location,$modal,BranchCentralService, $window) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    self.createParameterBranchCentral = function(){
        $location.path('/dashboard/create-branch-central');
    };

    self.doUpdateParameterBranchCentral = function(id){
        $location.path('/dashboard/update-branch-central').search({id: id});
    };

    self.doDeleteParameterBranchCentral = function (id,name,version) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteParameterBranchCentral.html',
            controller: 'deleteParameterBranchCentralCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id,
                        name:name,
                        version:version
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.branches.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListBranchCentral();
            }
        });
    };

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.pageChanged = function () {
        getListBranchCentral();
    };

    var getListBranchCentral = function (value) {
        backToTop();
        name = '';
        if(value != undefined){
            name = value;
        }
        BranchCentralService.getlistBranchCentral(self.currentPage-1, self.limit,name).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.branches = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                }
            });
    };

    self.doSearch = function (value) {
        self.currentPage = 1;
        getListBranchCentral(value);
    };

    getListBranchCentral();
};