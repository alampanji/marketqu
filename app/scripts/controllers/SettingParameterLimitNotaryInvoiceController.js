'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('settingParameterLimitApprove', [
        '$scope','$location','$modal','ParameterLimitNotaryInvoiceService','$window',
        settingParameterLimitApprove
    ]);

function settingParameterLimitApprove($scope,$location,$modal,ParameterLimitNotaryInvoiceService, $window) {
    var self = $scope;

    self.close = close;

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    function close() {
        $modalInstance.dismiss('close');
    }

    self.createParameterNotary = function(){
        $location.path('/dashboard/create-parameter-notary');
    };

    self.doUpdateParameterNotary = function(id){
        $location.path('/dashboard/update-notary-area').search({id: id});
    };

    self.search = [
        { "type":"notaryID", "label": "Notary ID"},
        { "type":"name", "label": "Nama"}
    ];

    self.doDeleteParameterLimitNotaryInvoice = function (id,name,version) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteParameterLimitNotaryInvoice.html',
            controller: 'deleteParameterLimitNotaryInvoiceCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id,
                        name:name,
                        version:version
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.notaries.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListNotary();
            }
        });
    };

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;

    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.pageChanged = function () {
        getListTransaction();
    };

    self.doCreateLimitNotary = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalAddParameterLimitNotaryInvoice.html',
            controller: 'addParameterLimitNotaryInvoiceCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                    };
                }
            }
        });
        modalInstance.result.then(function (listBranchCentral) {
            getListNotary();
        });
    };
    var getListNotary = function () {
        backToTop();
        self.name='';
        ParameterLimitNotaryInvoiceService.getAllNotary(self.currentPage-1, self.limit,self.name,self.idNotary).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.notaries = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                }
            });
    };

    self.doSearch = function (type,value) {
        console.log(type,value);
        self.currentPage = 1;
        getListNotary(type,value);
    }

    getListNotary();
};