'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('settingParameterMasaBerlakuCtrl', [
        '$scope','CONF','$location','$modal','MasaBerlakuService',
        settingParameterMasaBerlakuCtrl
    ]);

function settingParameterMasaBerlakuCtrl($scope,CONF,$location,$modal,MasaBerlakuService) {
    var self = $scope;
    // console.log("masaberlaku")
    self.close = close;
    self.user= {};
    function close() {
        $modalInstance.dismiss('close');
    }

    self.createParameterBranchCentral = function(){
        $location.path('/dashboard/create-branch-central');
    };

    self.doCreateMasaBerlaku = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalAddParameterMasaBerlaku.html',
            controller: 'addParameterMasaBerlakuCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                    };
                }
            }
        });
        modalInstance.result.then(function (listBranchCentral) {
            getListNotary();
        });
    };

    self.checkValidasi = function(input){
    var today = Date.parse(new Date());
        if(parseInt(input) > today){
            return true;
        }
        else{
            return false;
        }
    }
    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.pageChanged = function () {
        getListTransaction();
    };


    self.doDeleteMasaBerlaku = function(id,effectiveDate){
            var modalHapus = $modal.open({
                templateUrl: 'views/modal/ModalDeleteMasaBerlaku.html',
                controller: 'ModalDelMasaBerlakuCtrl',
                size: 'sm',
                backdrop: 'static',
                resolve: {
                    modalParam: function () {
                        return {
                            id: id,
                            effectiveDate : effectiveDate
                        };
                    }
                }
            });
            modalHapus.result.then(function () {
                getListNotary();
            })
            };

        
        


    var getListNotary = function () {
        self.name='';
        MasaBerlakuService.getlistMasaBerlaku(self.currentPage-1, self.limit,self.name,self.idNotary).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.branches = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                }
            });
    }

    getListNotary();
};