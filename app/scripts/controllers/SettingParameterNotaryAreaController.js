'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('settingParameterNotaryAreaCtrl', [
        '$scope','$location','$modal','ParameterNotaryService', '$window',
        settingParameterNotaryAreaCtrl
    ]);

function settingParameterNotaryAreaCtrl($scope,$location,$modal,ParameterNotaryService, $window) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    self.createParameterNotary = function(){
        $location.path('/dashboard/create-parameter-notary');
    };

    self.doUpdateParameterNotary = function(id){
        $location.path('/dashboard/update-notary-area').search({id: id});
    };

    self.search = [
        { "keyName":"notaryId", "value": "Notary ID"},
        { "keyName":"name", "value": "Nama"}
    ];

    self.doDeleteParameterNotaryArea = function (id,name,version) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteParameterNotaryArea.html',
            controller: 'deleteParameterNotaryAreaCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id,
                        name:name,
                        version:version
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.notaries.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListNotary();
            }
        });
    };

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.pageChanged = function () {
        getListTransaction();
    };

    var getListNotary = function (type, value) {
        backToTop();

        if(type==undefined && value==undefined){
            var indexFound = self.search.map(function (e) {
                return e.keyName;
            }).indexOf(self.search);
            self.itemSearch = self.search[0];
        }

        var name='';
        var notaryId = '';
        if(type=='notaryId'){
            notaryId =value;
            name = '';
            var indexFound = self.search.map(function (e) {
                return e.keyName;
            }).indexOf(self.search);
            self.itemSearch = self.search[0];
        }
        else if(type=='name'){
            name = value;
            notaryId = '';
            self.itemSearch = self.search[1];
        }


        self.name='';
        ParameterNotaryService.getAllNotary(self.currentPage-1, self.limit,name,notaryId).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.notaries = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                }
            });
    };

    self.doSearch = function (type,value) {
        self.currentPage = 1;
        getListNotary(type.keyName,value);

    };

    getListNotary();
};