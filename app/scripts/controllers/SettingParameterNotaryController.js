'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('settingParameterNotaryCtrl', [
        '$scope','CONF','$location','$modal','ParameterNotaryService', '$window',
        settingParameterNotaryCtrl
    ]);

function settingParameterNotaryCtrl($scope,CONF,$location,$modal,ParameterNotaryService, $window) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.createParameterNotary = function(){
        $location.path('/dashboard/create-parameter-notary');
    };

    self.doUpdateParameterNotary = function(id){
        $location.path('/dashboard/update-parameter-notary').search({id: id});
    };

    self.search = [
        { "keyName":"notaryId", "value": "Notary ID"},
        { "keyName":"name", "value": "Nama"}
    ];

    self.doDeleteParameterNotary = function (id,name,version) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteParameterNotary.html',
            controller: 'deleteParameterNotaryCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id,
                        name:name,
                        version:version
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.notaries.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListNotary();
            }
        });
    };

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.pageChanged = function () {
        getListTransaction();
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var getListNotary = function (type, value) {
        backToTop();
        var name='';
        var notaryId = '';
        if(type=='notaryId'){
            notaryId =value;
            name = '';
        }
        else if(type=='name'){
            name = value;
            notaryId = '';
        }

        ParameterNotaryService.getAllNotary(self.currentPage-1, self.limit,name,notaryId).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.notaries = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                    var indexFound = self.search.map(function (e) {
                        return e.keyName;
                    }).indexOf(self.search);
                    self.itemSearch = self.search[0];
                }
            });
    };

    self.doSearch = function (type,value) {
        self.currentPage = 1;
        getListNotary(type.keyName,value);
    };

    getListNotary();
};