'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('settingTaxNotaryCtrl', [
        '$scope','CONF','$location','$modal','TaxService',
        settingTaxNotaryCtrl
    ]);

function settingTaxNotaryCtrl($scope,CONF,$location,$modal, TaxService) {
    var self = $scope;

/**/

    self.doCreateTax = function(){
            var invalidModalWarning = $modal.open({
                templateUrl: 'views/modal/ModalCreateTaxNotary.html',
                controller: 'modalCreateTaxNotaryCtrl',
                size: 'md',
                backdrop: 'static',
                resolve: {
                    modalParam: function () {
                        return {};
                    }
                }
            });
            invalidModalWarning.result.then(function () {
            },function (data) {
                if (data == "yes") {
                    self.doDeleteTax();
                }
            });
    };

    self.doDeleteTax = function (id) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteTax.html',
            controller: 'deleteTaxCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.dataResult.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListOfTax();
            }
        });
    };

    // var doDeleteTax= function(){
    //     console.log("DELETE HERE");
    // };

    // self.doDeleteTax = function(id){
    //         var modalHapus = $modal.open({
    //             templateUrl: 'views/modal/ModalDeleteTax.html',
    //             controller: 'ModalDelTaxCtrl',
    //             size: 'sm',
    //             backdrop: 'static',
    //             resolve: {
    //                 modalParam: function () {
    //                     return {
    //                         id: id
    //                     };
    //                 }
    //             }
    //         });
    //         modalHapus.result.then(function () {
    //         },function (data) {
    //             if (data == "close") {
    //                 doDeleteTax();
    //             }
    //         });
    //     };



    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

   

    var getListOfTax = function () {
        TaxService.getlistOfTax(self.currentPage-1, self.limit).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.dataResult = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                }
            });
    };

    getListOfTax();
};
