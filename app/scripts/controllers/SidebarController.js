/**
 * Created by alampanji on 3/12/17.
 */

'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */
angular.module('marketplaceApp')
    .controller('CoreCtrl', [
        '$scope',
        '$modal',
        'authService',
        '$location',
        SideCtrl
    ]);

function CoreCtrl($scope, $modal, authService, $location) {
    var self = $scope;

    self.fullName = authService.getFullName();
    self.doLogout = doLogout;

    self.addBillboard = function () {
        $location.path('/billboard/add-billboard');
    };

    function doLogout() {
        authService.logout().then(
            // function (response) {
            //     if (response.message == 'OK' && response.result) {
            //         $location.path('/article/home');
            //     }
            // }
        );

        $location.path('/article/home');
    }

    self.showModalLogin = function() {
        $modal.open({
            templateUrl: '/views/auth/Login.html',
            controller: 'LoginCtrl',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        message: ""
                    };
                }
            }
        });
    };

    self.goChangePassword = function () {

        $location.path('/dashboard/change-password');

    }

    self.goProfile = function () {

        $location.path('/dashboard/profile');

    }


    self.showModalMap = function () {
        $location.path('/article/map');
    };
};
