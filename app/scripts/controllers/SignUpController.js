'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:SignUpCtrl
 * @description
 * # SignUpCtrl
 * Controller of the nostraApp
 */
angular.module('nostraApp')
    .controller('SignUpCtrl', [
        '$scope','$modal','profilesService','CONF','messageService','$location',
        SignUpCtrl
    ]);

function SignUpCtrl ($scope,$modal,profilesService,CONF,messageService,$location) {
    $scope.pw = '';
    var self = $scope;
    self.step1 = true;
    self.step2 = false;

    self.confirmed = false;
    var reg = CONF.REGEX_PASSWORD;
    self.register = {
        email : "",
        username : "",
        password : "",
        confirmationPassword : "",
        fullName : "",
        role : "bbUser",
        phoneNumber : "",
        companyName : "",
        positionTitle : ""
    }

    self.showModalLogin = function() {
        $modal.open({
            templateUrl: '/views/auth/Login.html',
            controller: 'LoginCtrl',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        message: "/billboard/home"
                    };
                }
            }
        });
    }

    self.nextStep = function () {
        if(!self.confirmed || !reg.test(self.register.password)){
            messageService.toasterMessage(CONF.TOASTER_TOP_FULL,CONF.TOASTER_ERROR,"Password not valid. Password minimum 8 character and must contain number")
        }else{
            self.step1 = false;
            self.step2 = true;
        }
    }

    self.checkConfirmation = function () {
        if(self.register.confirmationPassword != null && self.register.password == self.register.confirmationPassword){
            self.confirmed = true;
        }else{
            self.confirmed = false;
        }
    }

    self.back = function () {
        self.step1 = true;
        self.step2 = false;
    }

    self.submitRegister = function () {

        profilesService.registration(self.register).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    $location.path('/finishSignUp');
                }else{
                    messageService.toasterMessage(CONF.TOASTER_TOP_CENTER,CONF.TOASTER_ERROR,response.result);
                }
            }
        );
    }

};