'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('updateGroupCtrl', [
        '$scope', '$modal', '$location',
        'GroupService',
        'AccessLevelService',
        '$window',
        updateGroupCtrl
    ]);

function updateGroupCtrl($scope, $modal, $location, GroupService, AccessLevelService, $window) {

    var self = $scope;

    var request = {
        "privilegeList" : []
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    self.canceled = function () {
        $location.path('/dashboard/search-maintenance-group');
    };

    var paramValue = $location.search().id;

    var getDetailGroup = function () {
        backToTop();
        GroupService.getDetailGroup(paramValue).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.privileges = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                    getListAccessLevel();

                }
            });
    };

    var getListAccessLevel = function () {
        AccessLevelService.getListAccessLevel().then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.accessLevel = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                    var indexFound = self.accessLevel.map(function (e) {
                        return e.keyName;
                    }).indexOf(self.privileges.accessLevel);
                    self.itemAccessLevel = self.accessLevel[indexFound];
                }
            });
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    var checking = function () {
        self.nameInvalid = false;

        var bool = false;

        if (self.privileges.name == null) {
            self.nameInvalid = true;
            bool = true;
        }
        return bool;
    };

    self.changed = function (parents, parent, id) {
        angular.forEach(self.privileges.privilegeList[parents].subCategoryPrivilegeList[parent].privilegeList, function (listOfPrivileges) {
            if (listOfPrivileges.id != id) {
                listOfPrivileges.value = false;
            }
        });
    };

    var checkIdPrivilege = function () {
        angular.forEach(self.privileges.privilegeList, function (masterPrivilege) {
            angular.forEach(masterPrivilege.subCategoryPrivilegeList, function (subCategoryPrivilegeList) {
                angular.forEach(subCategoryPrivilegeList.privilegeList, function (listOfPrivileges) {
                        if (listOfPrivileges.value && listOfPrivileges.id != "0") {
                            var listId = {"id":listOfPrivileges.id};
                            request.privilegeList.push(listId);
                        }
                });
            });
        });
    };

    self.updateGroup = function () {
        checkIdPrivilege();
        var hasil = checking();

        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }else {

            request.groupId = self.privileges.id;
            request.newName = self.privileges.name;
            GroupService.updateGroup(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Group berhasil diubah',
                                        path: 'dashboard/search-maintenance-group'
                                    };
                                }
                            }
                        });
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        }
    };

    getDetailGroup();


}