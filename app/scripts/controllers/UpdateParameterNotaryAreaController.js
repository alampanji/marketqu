'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('updateParameterNotaryAreaCtrl', [
        '$scope', '$modal', '$location', 'ParameterNotaryService', '$window',
        updateParameterNotaryAreaCtrl
    ]);

function updateParameterNotaryAreaCtrl($scope, $modal, $location, ParameterNotaryService, $window) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    var paramValue = $location.search().id;
    var request = {
        "branchList" : []
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    self.lovBranch = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovBranch.html',
            controller: 'modalLovBranchCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.details.branchList
                    };
                }
            }
        });
        modalInstance.result.then(function (listBranch) {
            self.contentData.branch = listBranch;

        });
    };

    self.deleteBranch = function(index){
        self.details.branchList.splice(index,1);
    };

    var detailNotaryArea = function () {
        backToTop();
        ParameterNotaryService.getDetailNotaryArea(paramValue).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.details = response.result;
                }
            }
        );
    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses ubah wilayah notary?',
                        path: 'dashboard/setting-notary-area'
                    };
                }
            }
        });

    };


    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    var checking = function () {
        self.nameInvalid = false;
        self.branchInvalid = false;

        var bool = false;

        if (self.details.name == null) {
            self.userNameInvalid = true;
            bool = true;
        }
        if(self.details.branchList.length == 0){
            self.branchInvalid = true;
            bool = true;
        }
        return bool;
    };

    self.updateNotaryArea = function () {
        var hasil = checking();
        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }else {
            for(var i=0; i<self.details.branchList.length; i++){
                var listId = {
                    'code' : self.details.branchList[i].code,
                    'name'  : self.details.branchList[i].name
                };
                request.branchList.push(listId);
            }
            request.name = self.details.name;
            request.id = self.details.id;
            request.version = self.details.version;

            console.log(request);

            ParameterNotaryService.updateParameterNotaryArea(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Notary are berhasil diubah',
                                        path: 'dashboard/setting-notary-area'
                                    };
                                }
                            }
                        });
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        }
    };

    detailNotaryArea();

};
