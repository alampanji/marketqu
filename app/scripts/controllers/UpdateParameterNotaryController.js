'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('updateParameterNotaryCtrl', [
        '$scope','$location','$modal','CONF', 'ParameterNotaryService',
        updateParameterNotaryCtrl
    ]);

function updateParameterNotaryCtrl ($scope,$location,$modal,CONF, ParameterNotaryService) {

    var self = $scope;
    var paramValue = $location.search().id;
    var contentData = {};
    self.notaryFeeResumeList = [];
    var request = {
        notaryFeeResumeList : []
    };


    var detailUser = function () {
        ParameterNotaryService.getDetailNotary(paramValue).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.details = response.result;
                }
            }
        );
    };

    self.notaryType = [
        {value: 'PERORANGAN', name: 'Perorangan'},
        {value: 'BADAN_USAHA', name: 'Badan Usaha'}

    ];

    self.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    };

    self.addResume = function(fee,start){
        console.log(fee,start);
        if(fee!=undefined && start != undefined){
            var tempStart = angular.copy(self.tglEfektif);
            var tempFee = angular.copy(self.feeResume);
            console.log(tempStart);
            var insert={"effectiveDate":Date.parse(tempStart),"feeResume":tempFee};
            console.log(insert);
            self.details.notaryFeeResumeList.push(insert);
            self.tglEfektif = null;
            self.feeResume = null;

        }
    };

    self.deleteFeeResume = function (index) {
        self.details.notaryFeeResumeList.splice(index,1);
    };

    var checking = function () {
        self.notaryResumeListInvalid = false;
        self.notaryIdInvalid = false;
        self.notaryTypeInvalid = false;
        self.kliringCodeInvalid = false;
        self.npwpStatusInvalid = false;
        self.skbInvalid = false;
        self.ppnInvalid = false;

        var bool = false;

        if (self.details.notaryFeeResumeList == 0) {
            self.nameInvalid = true;
            bool = true;
        }
        if(self.details.kliringCode == null){
            self.kliringCodeInvalid = true;
            bool = true;
        }
        if(self.details.notaryType == null){
            self.notaryTypeInvalid = true;
            bool = true;
        }
        if(self.details.npwpStatus == null){
            self.npwpStatusInvalid = true;
            bool = true;
        }
        if(self.details.skb == null){
            self.skbInvalid = true;
            bool = true;
        }
        if(self.details.ppn == null){
            self.ppnInvalid = true;
            bool = true;
        }

        return bool;
    };

    self.updateSettingNotary = function () {

        var hasil = checking();
        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }else {
            request.notaryFeeResumeList = self.details.notaryFeeResumeList;
            request.id = self.details.id;
            request.version = self.details.version;
            request.kliringCode = self.details.kliringCode;
            request.notaryType = self.details.notaryType;
            request.npwpStatus = self.details.npwpStatus;
            request.npwpNo = self.details.npwpNo;
            request.npwpAdress = self.details.npwpAdress;
            request.skb = self.details.skb;
            request.ppn = self.details.ppn;
            if(!request.npwpStatus){
                request.npwpNo = '';
                request.npwpAdress = '';
            }
            ParameterNotaryService.updateParameterNotary(request).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Parameter Notary berhasil diubah',
                                        path: 'dashboard/setting-notary'
                                    };
                                }
                            }
                        });
                    }else{
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        }
    };

    detailUser();

};