'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('updateUserNotaryCtrl', [
        '$scope', '$modal', '$window', '$location', 'UserService',
        updateUserNotaryCtrl
    ]);

function updateUserNotaryCtrl($scope, $modal, $window, $location, UserService) {

    var self = $scope;
    self.contentData = {};
    self.boolean = false;
    var listGroup;
    var paramValue = $location.search().id;
    var request = {
        "groupList": []
    };

    var backToTop = function() {
        $window.scrollTo(0, 0);
    };

    self.lovListNotary = function() {
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovListMasterNotary.html',
            controller: 'modalLovMasterNotaryCtrl',
            size: 'md',
            resolve: {
                modalParam: function() {
                    return {
                        values: self.contentData.notary
                    };
                }
            }
        });
        modalInstance.result.then(function(data) {
            self.contentData = data;
            console.log(self.contentData);
        });
    };

    self.lovGroupPrivilege = function() {
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovGroupPrivilege.html',
            controller: 'modalGroupPrivilegeCtrl',
            size: 'md',
            resolve: {
                modalParam: function() {
                    return {
                        values: self.user.groupList
                    };
                }
            }
        });
        modalInstance.result.then(function(listGroup) {
            self.contentData.group = listGroup;
        });
    };

    self.deleteGroup = function(index) {
        self.user.groupList.splice(index, 1);
    };

    var invalidModal = function(message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function() {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };



    var detailUserNotary = function() {
        backToTop();
        UserService.getDetailUserNotary(paramValue).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.user = response.result;
                }
            }
        );
    };

    var checking = function() {
        self.nameInvalid = false;
        self.fullNameInvalid = false;
        self.notaryIdInvalid = false;
        self.userIdInvalid = false;
        self.divisionInvalid = false;
        self.emailInvalid = false;
        self.groupPrivilegeInvalid = false;
        // self.statusInvalid = false;
        // self.groupInvalid = false;

        var bool = false;

        if (self.user.notaryName == null) {
            self.nameInvalid = true;
            bool = true;
        }

        if (self.user.fullName == null) {
            self.fullNameInvalid = true;
            bool = true;
        }
        if (self.user.notaryId == null) {
            self.notaryIdInvalid = true;
            bool = true;
        }
        if (self.user.userId == null) {
            self.userIdInvalid = true;
            bool = true;
        }
        if (self.user.email == null) {
            self.emailInvalid = true;
            bool = true;
        }
        if (self.user.status == null) {
            self.statusInvalid = true;
            bool = true;
        }
        // if(self.user.groupList.length == 0){
        //     self.groupInvalid = true;
        //     bool = true;
        // }
        return bool;
    };

    self.showModalBack = function() {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function() {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses edit user notary?',
                        path: 'dashboard/user-notary'
                    };
                }
            }
        });

    };

    self.updateUserNotary = function() {
        var hasil = checking();

        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        } else {

            for (var i = 0; i < self.user.groupList.length; i++) {
                var listId = {
                    'id': self.user.groupList[i].id
                };
                request.groupList.push(listId);
            }
            request.fullName = self.user.fullName;
            request.id = self.user.id;
            request.notaryId = self.user.notaryId;
            request.userId = self.user.userId;
            request.email = self.user.email;
            request.status = self.user.status;
            request.version = self.user.version;

            UserService.updateUserNotary(request).then(
                function(response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function() {
                                    return {
                                        title: 'Informasi',
                                        message: 'User Notary berhasil diubah',
                                        path: 'dashboard/user-notary'
                                    };
                                }
                            }
                        });
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function() {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }

                }
            );
        }
    };

    detailUserNotary();

};