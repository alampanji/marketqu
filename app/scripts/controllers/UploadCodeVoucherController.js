'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('uploadCodeVoucherCtrl', [
        '$scope', '$location', '$modal', 'FidusiaService', '$window',
        uploadCodeVoucherCtrl
    ]);

function uploadCodeVoucherCtrl($scope, $location, $modal, FidusiaService, $window) {
    var self = $scope;

    self.close = close;

    var backToTop = function() {
        $window.scrollTo(0, 0);
    };

    function close() {
        $modalInstance.dismiss('close');
    }

    self.createUser = function() {
        $location.path('/dashboard/create-user');
    };

    self.doUpdateUser = function() {
        $location.path('/dashboard/update-user');
    };

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus = function(status) {
        if (status) return "active";
        else return "not active";
    };

    self.search = [
        { "keyName": "voucherCode", "value": "Kode Voucher" }
    ];


    self.pageChanged = function() {
        getListVoucher();
    };

    self.doCreateVoucher = function() {
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalCreateVoucher.html',
            controller: 'createVoucherCtrl',
            size: 'md',
            resolve: {
                modalParam: function() {
                    return {};
                }
            }
        });
    };

    var getListVoucher = function(type, value) {

        backToTop();

        var indexFound = self.search.map(function(e) {
            return e.keyName;
        }).indexOf(self.search);
        self.itemSearch = self.search[0];

        var voucher = '';

        if (type != undefined && value != undefined) {
            if (type == 'voucherCode') {
                voucher = value;
            }
        }
        FidusiaService.getVoucherList(self.currentPage - 1, self.limit, voucher).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.users = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;
                }
            });
    };

    self.doSearch = function(type, value) {
        self.currentPage = 1;
        getListVoucher(type.keyName, value);
    };

    getListVoucher();
};