'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('uploadDokumenKelengkapanDetailCtrl', [
        '$scope','CONF','$location','$modal','ResumeService','$sessionStorage','$window','DokumenKelengkapanService',
        uploadDokumenKelengkapanDetailCtrl
    ]);

function uploadDokumenKelengkapanDetailCtrl($scope,CONF,$location,$modal,ResumeService, $sessionStorage,$window,DokumenKelengkapanService) {
    var self = $scope;

    self.close = close;
    var paramId = $location.search().id;
    function close() {
        $modalInstance.dismiss('close');
    }
    
    self.createUser = function(){
        $location.path('/dashboard/create-user');
    };

    self.doUpdateUser = function(){
        $location.path('/dashboard/update-user');
    };

    var nip = '';
    var name = '';

    self.search = [
        { "type":"nip", "label": "NIP"},
        { "type":"name", "label": "Nama"},
    ];
    
    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.status = [
        {keyName: 'true', name: 'Aktif'},
        {keyName: 'false', name: 'Tidak Aktif'}
    ];

    // self.pageChanged = function () {
    //     getListUser();
    // };
    self.console = function(){
        console.log(self.users.documentList)
    }
self.addFile = function (ele,index) {
    console.log(ele.files)
        var files = ele.files[0];
        var file_name =  ele.files[0].name;
        self.users.documentList[index].file = files;
        self.users.documentList[index].file_name = file_name;
        console.log(self.users.documentList);
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var getListUser = function (paramId) {
        backToTop();
        // if(type!=undefined && value!=undefined){
        //     if(type=='NIP'){
        //         name='';
        //         nip=value;
        //     }
        //     else if(type=='Nama'){
        //         name = value;
        //         nip='';
        //     }
        // }
        DokumenKelengkapanService.getUploadDocumentKelengkapanDetail(paramId).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.users = response.result;
                    var access_token = $sessionStorage.accessToken;
                    
                        for (var j=0; j< self.users.documentList.length;j++){

                        var urlConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_DOCUMENT +'?access_token='+access_token+'&resumeId='+self.users.id+'&docType='+self.users.documentList[j].documentCode;
                       
                        self.users.documentList[j].url = urlConstruct
                        }
                }
            });
    };
    self.reject = function () {
        var request = {
            id: self.users.id,
            version : self.users.version,
            description : self.users.description
        };
        ResumeService.rejectRequestResume(request).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Sukses',
                                    message: 'Resume Sukses di Reject',
                                    path: ''
                                };
                            }
                        }
                    });

        $location.path('/dashboard/approval-request-resume');
                    // self.bigTotalItems = response.elements;
                    // self.numPages = response.pages;
                }
                else {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            });
    };
    self.doRetrieve = function(input){
        $location.path('/dashboard/matching-result').search({id: input});
    }
    self.approve = function () {
        var request = {
            id: self.users.id,
            version : self.users.version ,
            description : self.users.description
        };
        ResumeService.rejectRequestResume(request).then(
function (response) {
                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Sukses',
                                    message: 'Resume Sukses di Approve',
                                    path: ''
                                };
                            }
                        }
                    });

        $location.path('/dashboard/approval-request-resume');
                    // self.bigTotalItems = response.elements;
                    // self.numPages = response.pages;
                }
                else {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            });
    };

    self.doUpdateUser = function (id) {
        $location.path('/dashboard/update-user').search({id: id});
    };

    self.doDeleteUser = function (id,name) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteUser.html',
            controller: 'deleteUserInternalCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id,
                        name:name,
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.users.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListUser();
            }
        });
    };
self.uploadFile = function () {

    // var file = self.myFile;
    // var file2 = self.myFile2;
        var fd = new FormData();
    for (var i=0;i<self.users.documentList.length;i++){
        // console.log("masuk")
        self.contentData = {"resumeId" : self.users.documentList[i].resumeId,
        "documentCode" : self.users.documentList[i].documentCode };
        fd.append('file'+i,self.users.documentList[i].file);
        fd.append('contentData', angular.toJson(self.contentData));
    }

        console.log(fd)
        // console.log(fd)
    // var fd = new FormData();
    // fd.append('file1', file);
    // fd.append('file2', file2);
    // fd.append('contentData', angular.toJson(self.contentData));


    DokumenKelengkapanService.uploadDocumentKelengkapan(fd).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    var result = response.result;
                    // if(result.match==true){
                    //     $location.path('/dashboard/matching-result').search({id: result.resumeId});
                    // }
                    // else{
                    //     var request = {};
                    //     for(var i=0; i<self.users.documentList.length; i++){
                    //         var fd = new FormData();
                    //         request.resumeId = result.resumeId;
                    //         request.documentCode = self.users.documentList[i].documentCode;
                    //         var extn = self.users.documentList[i].file.name.split(".").pop();
                    //         extn = extn.toUpperCase();
                    //         console.log(self.users.documentList[i]);
                    //         console.log(extn);
                    //     }
                    // }
                }else{
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );

    };


    self.goToList = function(){
        $location.path('/dashboard/upload-dokumen-kelengkapan');
    }
    self.submitDokumen =function(){


    // var file = self.myFile;
    // var file2 = self.myFile2;
        var fd = new FormData();
    for (var i=0;i<self.users.documentList.length;i++){
        // console.log("masuk")
        self.contentData = {"resumeId" : self.users.documentList[i].resumeId,
        "documentCode" : self.users.documentList[i].documentCode };
        fd.append('file'+i,self.users.documentList[i].file);
        fd.append('contentData', angular.toJson(self.contentData));
    }

        console.log(fd)


                    

    DokumenKelengkapanService.uploadDocumentKelengkapanSubmit(fd).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    var result = response.result;
                    console.log(result)
                    var request = {
                                      "id" : paramId,
                                      "version" : self.users.version
                                  };
                                  submitResume(request);
                    // if(result.match==true){
                    //     $location.path('/dashboard/matching-result').search({id: result.resumeId});
                    // }
                    // else{
                    //     var request = {};
                    //     for(var i=0; i<self.users.documentList.length; i++){
                    //         var fd = new FormData();
                    //         request.resumeId = result.resumeId;
                    //         request.documentCode = self.users.documentList[i].documentCode;
                    //         var extn = self.users.documentList[i].file.name.split(".").pop();
                    //         extn = extn.toUpperCase();
                    //         console.log(self.users.documentList[i]);
                    //         console.log(extn);
                    //     }
                    // }
                }else{
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    }
    // self.doSearch = function (type,value) {
    //     self.currentPage = 1;
    //     getListUser(type,value);
    // };
    function submitResume (request){

    DokumenKelengkapanService.uploadDocumentKelengkapanSubmitResume(request).then(
            function (response1) {
                console.log(response1)
                if (response1.message !== 'ERROR') {
                    $location.path('/dashboard/upload-dokumen-kelengkapan');
                }else{
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response1.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
// DokumenKelengkapanService.uploadDocumentKelengkapanSubmitResume(request).then(
//                             function (response1) {
//                                     console.log(response1)
//                                 if(response1.message !== 'ERROR'){
//                                     console.log("sukses")
//                                 };
//                                 )
    };
    getListUser(paramId);
};