'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('uploadResumeAhuCtrl', [
        '$scope','$location','$modal','FidusiaService','$window',
        requestCompletenessDocumentCtrl
    ]);

function requestCompletenessDocumentCtrl($scope,$location,$modal,FidusiaService, $window) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }
    self.goToDetail = function(id){
        $location.path('/dashboard/detail-upload-resume-ahu').search({id:id});

        // $location.path('/dashboard/matching-result').search({id: id});
    };
    self.createUser = function(){
        $location.path('/dashboard/create-user');
    };

    self.doUpdateUser = function(){
        $location.path('/dashboard/update-user');
    };

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.status = [
        {keyName: 'true', name: 'Aktif'},
        {keyName: 'false', name: 'Tidak Aktif'}
    ];

    self.search = [
        {"keyName" : "appId", "value":"APP ID"},
        {"keyName" : "name", "value":"Nama Badan Usaha"},
        {"keyName" : "npwp", "value":"NPWP"},
    ];
    self.pageChanged = function () {
        getListUser();
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var getListUser = function (type,value) {

        backToTop();

        var indexFound = self.search.map(function (e) {
            return e.keyName;
        }).indexOf(self.search);
        self.itemSearch = self.search[0];

        var appId = '';
        var name = '';
        var npwp = '';

        if(type != undefined && value != undefined){
            if(type=='appId'){
                appId = value;
                name = '';
                npwp = '';
            }
            else if(type=='name'){
                name = value;
                appId = '';
                npwp = '';
            }
            else if(type=='npwp'){
                npwp = value;
                appId = '';
                name = '';
            }
        }

        FidusiaService.getUploadResumeAhuList(self.currentPage-1, self.limit, appId, name, npwp).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.users = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                }
            });
    };

    self.doUpdateUser = function (id) {
        $location.path('/dashboard/update-user').search({id: id});
    };

    self.doDeleteUser = function (id,name) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteUser.html',
            controller: 'deleteUserInternalCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id,
                        name:name,
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.users.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListUser();
            }
        });
    };


    self.doSearch = function (type,value) {
        self.currentPage = 1;
        getListUser(type.keyName,value);
    };
    getListUser();
};