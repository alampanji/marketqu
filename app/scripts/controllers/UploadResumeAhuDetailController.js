'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('uploadResumeAhuDetailCtrl', [
        '$scope', '$modal',
        '$location',
        'FidusiaService',
        '$sessionStorage',
        'CONF',
        '$window',
        uploadResumeAhuDetailCtrl
    ]);

function uploadResumeAhuDetailCtrl($scope, $modal, $location, FidusiaService, $sessionStorage, CONF, $window) {

    var self = $scope;

    self.close = close;

    var paramId = $location.search().id;
    function close() {
        $modalInstance.dismiss('close');
    }
    self.goToDetail = function(id){
        $location.path('/dashboard/approval-request-resume-detail').search({id:id});

    };

    self.contentData = {};

    var nip = '';
    var name = '';

    self.search = [
        { "type":"nip", "label": "NIP"},
        { "type":"name", "label": "Nama"},
    ];

    self.toMatchingResult = function (id) {
        $location.path('/dashboard/matching-result').search({id: id});
    };

    self.lovCodeVoucher = function(){
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalLovListCodeVoucher.html',
            controller: 'modalLovListCodeVoucherCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        values: self.contentData
                    };
                }
            }
        });
        modalInstance.result.then(function (data) {
            self.contentData = data;
        });
    };

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.status = [
        {keyName: 'true', name: 'Aktif'},
        {keyName: 'false', name: 'Tidak Aktif'}
    ];

    self.pageChanged = function () {
        getListUser();
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var getListResume = function (paramId) {
        backToTop();
        FidusiaService.getUploadResumeAhuDetail(paramId).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.users = response.result;
                    var access_token = $sessionStorage.accessToken;

                    for (var j=0; j< self.users.documentList.length;j++){

                        var urlConstruct=  CONF.MASTER_PATH + CONF.URL_DOWNLOAD_DOCUMENT +'?access_token='+access_token+'&resumeId='+self.users.id+'&docType='+self.users.documentList[j].documentCode;

                        self.users.documentList[j].url = urlConstruct
                    }
                }
            });
    };

    var checking = function () {
        self.fileInvalid = false;
        self.codeInvalid = false;

        var bool = false;

        if (self.file == null) {
            self.fileInvalid = true;
            bool = true;
        }

        if (self.contentData.code == null) {
            self.codeInvalid = true;
            bool = true;
        }

        return bool;
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.submit = function () {

        var hasil = checking();

        if (hasil == true) {
            invalidModal("Ada yang tidak valid, mohon periksa kembali");
        }

       else{

            var file = self.file;

            var request = {
                id: self.users.id,
                version : self.users.version ,
                voucherId : self.contentData.id
            };

            var fd = new FormData();
            fd.append('file', file);
            fd.append('contentData', angular.toJson(request));

            FidusiaService.processUploadResumeAhu(fd).then(
                function (response) {
                    if (response.message !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Sukses',
                                        message: 'Berhasil Upload Resume AHU, Kode Voucher: ' + response.result.voucherCode,
                                        path: ''
                                    };
                                }
                            }
                        });
                        $location.path('/dashboard/upload-resume-ahu');
                    }
                    else {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.result,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                });
        }
    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses upload resume ahu?',
                        path: 'dashboard/upload-resume-ahu'
                    };
                }
            }
        });

    };

    self.doSearch = function (type,value) {
        self.currentPage = 1;
        getListResume(type,value);
    };

    getListResume(paramId);

};
