'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('nostraApp')
    .controller('userNotaryCtrl', [
        '$scope','CONF','$location','$modal','UserService', '$window',
        userNotaryCtrl
    ]);

function userNotaryCtrl($scope,CONF,$location,$modal,UserService, $window) {
    var self = $scope;

    self.close = close;


    function close() {
        $modalInstance.dismiss('close');
    }

    self.createUserNotary = function(){
        $location.path('/dashboard/create-user-notary');
    };

    self.doUpdateParameterNotary = function(id){
        $location.path('/dashboard/update-user-notary').search({id: id});
    };

    self.search = [
        { "keyName":"notaryId", "value": "Notary ID"},
        { "keyName":"notaryName", "value": "Nama Notary"}
    ];

    self.doDeleteUserNotary = function (id,name,version) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteUserNotary.html',
            controller: 'deleteUserNotaryCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function () {
                    return {
                        id: id
                    };
                }
            }
        });
        modalDelete.result.then(function () {
        }, function (data) {
            if(data == "close"){
                if(self.notaries.length == 1 && self.currentPage != 1){
                    self.currentPage--;
                }
                getListUserNotary();
            }
        });
    };

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;


    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.pageChanged = function () {
        getListUserNotary();
    };

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    var getListUserNotary = function (type,value) {
        backToTop();
        var notaryId = '';
        var notaryName = '';

        if(type!=undefined && value!=undefined){
            if(type=='notaryId'){
                notaryId = value;
                notaryName = '';
                var indexFound = self.search.map(function (e) {
                    return e.keyName;
                }).indexOf(self.search);
                self.itemSearch = self.search[0];
            }
            else if(type=='notaryName'){
                notaryName = value;
                notaryId = '';
                var indexFound = self.search.map(function (e) {
                    return e.keyName;
                }).indexOf(self.search);
                self.itemSearch = self.search[1];
            }
        }
        else{
            var indexFound = self.search.map(function (e) {
                return e.keyName;
            }).indexOf(self.search);
            self.itemSearch = self.search[0];
        }



        UserService.getListUserNotary(self.currentPage-1, self.limit,notaryName,notaryId).then(
            function (response) {
                if (response.message !== 'ERROR') {
                    self.notaries = response.result;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;


                }
        });
    };

    self.doSearch = function (type,value) {
        self.currentPage = 1;
        getListUserNotary(type.keyName,value);
    };

    getListUserNotary();
};