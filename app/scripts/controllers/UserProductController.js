'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

angular.module('marketplaceApp')
    .controller('UserProductCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'localStorageService', 'messageService', 'ProductService', 'privilegeService',
        UserProductCtrl
    ]);

function UserProductCtrl($scope, CONF, $location, $modal, $window, localStorageService, messageService, ProductService, privilegeService) {
    var self = $scope;

    self.close = close;

    self.logoExtHeader = true;
    self.searchLogo = true;
    
    var paramValue = $location.search().product_name;


    function close() {
        $modalInstance.dismiss('close');
    }

    function doLogout() {
        authService.logout().then(
            // function (response) {
            //     if (response.message == 'OK' && response.result) {
            //         $location.path('/article/home');
            //     }
            // }
        );

        $location.path('/article/home');
    }

    self.showModalLogin = function() {
        $modal.open({
            templateUrl: '/views/auth/Login.html',
            controller: 'LoginCtrl',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        message: ""
                    };
                }
            }
        });
    };

    self.goToDetail = function (id) {

        $location.path('/product-detail').search({uuid:id});

    };

    self.goProfile = function () {

        $location.path('/dashboard/profile');

    };

    self.goToDashboard = function () {
        $location.path('/admin/home');
    };

    self.goToHome= function () {
        $location.path('/user/home');
    };

    self.goToProduct= function () {
        $location.path('/product');
    };

    self.showModalMap = function () {
        $location.path('/article/map');
    };

    self.chartIsActive = false;
    self.chartNotNull = false;

    var productToCart = [];
    self.cartProduct = [];

    self.showCart = function () {
        self.chartIsActive = true;
        if(self.cartProduct.length > 0){
            self.chartNotNull = true;
        }
    };

    self.hideCart = function () {
        self.chartIsActive = false;
    };

    function constructCart() {

        if(localStorageService.get(CONF.CART) != undefined){
            productToCart = angular.copy(localStorageService.get(CONF.CART));
            self.cartProduct = angular.copy(productToCart);
            if(localStorageService.get(CONF.CART).length == 0){
                self.cartProduct = angular.copy(productToCart);
            }
        }
        else{
            self.cartProduct = angular.copy(productToCart);
        }


        if(self.cartProduct.length > 0){
            self.cartProductItem = 0;
            self.totalPrice = 0;
            for(var i=0; i<self.cartProduct.length; i++){
                self.totalPrice += getPrice(self.cartProduct[i].quantity,self.cartProduct[i].price_low);
                self.cartProductItem += self.cartProduct[i].quantity;
            }
        }

    }

    function getPrice(quantity, price) {
        return quantity * price;
    }

    function getProduct(type,value) {

        if(type != undefined){

            var category = '';
            var name = '';


            if(type=='name'){
                name = value;
            }
            else if(type=='category'){
                category = value.id
            }

            ProductService.getListProductUser(category, name).then(
                function(response) {
                    if (response.status == 'OK') {
                        self.dataList = response.data;
                        for(var i=0; i<self.dataList.length; i++){
                            if(self.dataList[i].thumbnail != null){
                                self.dataList[i].img = CONF.IMAGE_PATH + CONF.PATH_FILES_PRODUCT + self.dataList[i].thumbnail.photo;
                            }
                        }
                        backToTop();
                    }
                }
            );
        }
        else{
            ProductService.getListProductUser().then(
                function(response) {
                    if (response.status == 'OK') {
                        self.dataList = response.data;
                        for(var i=0; i<self.dataList.length; i++){
                            if(self.dataList[i].thumbnail != null){
                                self.dataList[i].img = CONF.IMAGE_PATH + CONF.PATH_FILES_PRODUCT + self.dataList[i].thumbnail.photo;
                            }
                        }

                        backToTop();

                    }
                }
            );
        }
    }

    getProduct();

    constructCart();


    self.produsenLabel = function (index) {
        self.dataList[index].statusProdusen = true;
    };

    self.deleteCart = function (index) {
        messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_ERROR,'Item ' + productToCart[index].name + ' telah terhapus dari keranjang belanja');
        productToCart.splice(index,1);
        localStorageService.set(CONF.CART, productToCart);
        if (productToCart.length == 0){
            localStorageService.cookie.remove(CONF.CART);
            self.chartNotNull = false;
            self.cartProductItem = 0;
        }
        constructCart();
    };

    self.gotToCheckOut = function () {
        localStorageService.set(CONF.CART, self.cartProduct);
        $location.path('/checkout');
    };


    function isReseller () {
        return privilegeService.hasRoleIsReseller();
    };


    self.produsenLabelHide = function (index) {
        self.dataList[index].statusProdusen = false;
    };

    self.openModalCategory = function () {
       var modalInstance = $modal.open({
            templateUrl: 'views/modal/ModalCategory.html',
            controller: 'ModalCategoryCtrl',
            size: 'lg',
            windowClass: 'modal-lg-custom',
            resolve: {
                modalParam: function () {
                    return {
                        category: self.category

                    }
                }
            }
        });

        modalInstance.result.then(function (param) {
            self.category = param.category;
            if(self.category != undefined){
                getProduct("category",self.category);
            }
        });

    };

    self.addToCart = function (product) {

        if(isReseller()){

            if(productToCart.length < 1){
                console.log("masuk");
                product.quantity = 1;
                productToCart.push(product);
                localStorageService.set(CONF.CART, productToCart);
                console.log(localStorageService.get(CONF.CART));

                messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_SUCCESS,'Berhasil menambahkan ' + product.name + ' ke keranjang belanja');
                constructCart();
            }

            else{
                var next = true;
                for(var i=0; i<productToCart.length; i++){
                    if(productToCart[i].id == product.id){
                        productToCart[i].quantity = productToCart[i].quantity+1;
                        next = false;
                    }
                }
                if(next){
                    product.quantity = 1;
                    productToCart.push(product);
                    productToCart.reverse();
                }

                localStorageService.set(CONF.CART, productToCart);
                messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_SUCCESS,'Berhasil menambahkan ' + product.name + ' ke keranjang belanja');
                constructCart();
            }
        }
        else{
            messageService.toasterMessage(CONF.TOASTER_TOP_FULL,CONF.TOASTER_WARNING,'You have to login as Reseller');
        }
    };

    function getDataSite() {
        if(localStorageService.get(CONF.SITE_INFO) != undefined){
            self.dataSite = localStorageService.get(CONF.SITE_INFO);

            if(self.dataSite.site_logo != null){
                self.dataSite.site_logo = CONF.IMAGE_PATH + CONF.PATH_FILES_LOGO + self.dataSite.site_logo;
            }
        }
    }

    function backToTop() {
        $window.scrollTo(0, 0);
    }

    self.goToHome = function () {
        $location.path('user/home');
    };

    self.doSearchProduct = function (searchName) {
        getProduct("name",searchName);
    };

    getDataSite();

    angular.element($window).bind(
        "scroll", function() {
            if(window.pageYOffset > 20) {
                self.logoExtHeader = false;
                self.searchLogo = false;
                self.headerSearch = 'search-box-ext';
            } else {
                self.logoExtHeader = true;
                self.searchLogo = true;
                self.headerSearch = '';
            }
            self.$apply();
        });

    console.log(self.logoExtHeader);

    if(paramValue != undefined){
        getProduct("name",paramValue);
    }
};