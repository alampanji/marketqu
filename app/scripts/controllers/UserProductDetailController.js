'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('UserProductDetailCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'localStorageService', 'messageService', '$timeout', 'ProductService', 'privilegeService',
        UserProductDetailCtrl
    ]);

function UserProductDetailCtrl($scope, CONF, $location, $modal, $window, localStorageService, messageService, $timeout, ProductService, privilegeService) {
    var self = $scope;

    self.close = close;

    var paramId = $location.search().uuid;

    self.isReseller = isReseller();

    self.checkOutItems = [];

    self.contentData = {};

    self.tab = 1;

    self.setTab = function(tabId) {
        self.tab = tabId;
    };

    self.isSet = function(tabId) {
        if(self.tab==tabId){
            return true
        }
        // return self.tab === tabId;
    };

    function close() {
        $modalInstance.dismiss('close');
    }

    self.chartIsActive = false;
    self.chartNotNull = false;

    var productToCart = [];
    self.cartProduct = [];

    self.showCart = function () {
        self.chartIsActive = true;
        if(self.cartProduct.length > 0){
            self.chartNotNull = true;
        }
    };

    self.hideCart = function () {
        self.chartIsActive = false;
    };

    function constructCart() {

        if(localStorageService.get(CONF.CART) != undefined){
            productToCart = angular.copy(localStorageService.get(CONF.CART));
            self.cartProduct = angular.copy(productToCart);
            console.log(localStorageService.get(CONF.CART));
            console.log(self.cartProduct);
        }
        else{
            self.cartProduct = angular.copy(productToCart);
        }

        if(self.cartProduct.length > 0){
            self.cartProductItem = 0;
            self.totalPrice = 0;
            for(var i=0; i<self.cartProduct.length; i++){
                self.totalPrice += getPrice(self.cartProduct[i].quantity,self.cartProduct[i].price_low);
                self.cartProductItem += self.cartProduct[i].quantity;
            }
        }

    }


    function getPrice(quantity, price) {
        return quantity * price;
    }

    constructCart();

    self.showModalLogin = function() {
        $modal.open({
            templateUrl: '/views/auth/Login.html',
            controller: 'LoginCtrl',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        message: ""
                    };
                }
            }
        });
    };

    function getDetailProduct () {
        self.products = {
            price:400000,
            img:'/img/pisang.jpg',
            name:'Produk A',
            id:'1',
            produsen:'Produsen C',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec dapibus nisl. Duis nulla nulla, auctor vitae maximus in, bibendum sit amet neque. Proin interdum tempus nisl sit amet mattis. Phasellus aliquam felis ac turpis dapibus egestas. Integer at fermentum leo, et malesuada urna. Fusce congue imperdiet erat, in laoreet sem ultricies eget. Donec consectetur, mauris sollicitudin tincidunt lacinia, enim mauris tincidunt lacus, commodo tristique turpis',
            feedback:[
                {
                    "fullName": "Panji Alam",
                    "timestamp": "3 April 2017",
                    "description": "Barang sesuai yang digambar :D"
                },
                {
                    "fullName": "Joko Widodo",
                    "timestamp": "1 April 2017",
                    "description": "Tolong pengiriman lebih rapih lagi"
                }
            ]
        };
    };

    self.deleteCart = function (index) {
        messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_ERROR,'Item ' + productToCart[index].name + ' telah terhapus dari keranjang belanja');
        productToCart.splice(index,1);
        localStorageService.set(CONF.CART, productToCart);
        if (productToCart.length == 0){
            localStorageService.cookie.remove(CONF.CART);
            self.chartNotNull = false;
            self.cartProductItem = 0;
        }
        constructCart();
    };

    getDetailProduct();



    self.goToDashboard = function () {
        $location.path('/admin/home');
    };

    self.goToHome= function () {
        $location.path('/user/home');
    };

    self.goToProduct= function () {
        $location.path('/product');
    };

    self.goToPaymentMethod = function () {
        if(self.checkOutItems.length > 0){
            localStorageService.set(CONF.CART, self.checkOutItems);
            $location.path('/payment-method');
        }
    };

    self.produsenLabel = function (index) {
        self.products[index].status = true;
    };

    self.gotToCheckOut = function () {
        localStorageService.set(CONF.CART, self.cartProduct);
        $location.path('/checkout');
    };


    self.produsenLabelHide = function (index) {
        self.products[index].status = false;
    };

    function isReseller () {
        return privilegeService.hasRoleIsReseller();
    };

    self.addToCart = function () {

        if(isReseller()){
            if(productToCart.length < 1){
                self.dataList.quantity = 1;
                console.log(productToCart);
                productToCart.push(self.dataList);
                localStorageService.set(CONF.CART, productToCart);
                messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_SUCCESS,'Berhasil menambahkan ' + self.dataList.name + ' ke keranjang belanja');
                constructCart();
            }
            else {
                var newCopyProduct;
                var next = true;
                for(var i=0; i<productToCart.length; i++){
                    console.log(productToCart[i].id);
                    console.log(self.dataList.id);

                    if(productToCart[i].id == self.dataList.id){
                        console.log("masuk");
                        productToCart[i].quantity = productToCart[i].quantity+1;
                        next = false;
                    }
                }

                if(next){
                    self.dataList.quantity = 1;
                    console.log(productToCart);
                    productToCart.push(self.dataList);
                    console.log(productToCart);
                    productToCart.reverse();
                }

                console.log(productToCart);

                newCopyProduct = angular.copy(productToCart);

                localStorageService.set(CONF.CART, newCopyProduct);

                console.log(localStorageService.get(CONF.CART));

                messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_SUCCESS,'Berhasil menambahkan ' + self.dataList.name + ' ke keranjang belanja');
                constructCart();

            }
        }
        else{
            messageService.toasterMessage(CONF.TOASTER_TOP_FULL,CONF.TOASTER_WARNING,'You have to login as Reseller');
        }
    };

    //rating

    $scope.rating = 0;
    $scope.ratings = [{
        current: 3,
        max: 5
    }];

    $scope.getSelectedRating = function (rating) {
        console.log(rating);
    };

    function sendViewData() {
        if(localStorageService.cookie.get(CONF.accessToken) != undefined) {
            ProductService.viewOfProduct();
        }
    }

    function getDetailProduct() {

        ProductService.getDetailProductUser(paramId).then(
            function(response) {
                if (response.status == 'OK') {
                    
                    self.dataList = response.data;
                    angular.forEach(self.dataList.thumbnail, function (thumbnail) {
                        if(thumbnail.status == 1){
                            self.dataList.img = CONF.IMAGE_PATH + CONF.PATH_FILES_PRODUCT + thumbnail.photo
                        }
                        else{
                            thumbnail.img = CONF.IMAGE_PATH + CONF.PATH_FILES_PRODUCT + thumbnail.photo;
                        }
                    });

                }
            }
        )
        
    };
    
    self.viewImageThumbnail = function (thumbnail, index) {
        $modal.open({
            templateUrl: 'views/modal/ModalImageThumbnail.html',
            controller: 'ModalImageThumbnailCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        index :index,
                        message: thumbnail,
                        path: ''
                    };
                }
            }
        });
    }

    self.sendDiscussion = function () {

        self.contentData.id_product = self.dataList.id;

        console.log(JSON.stringify(self.contentData));

        ProductService.sendDiscussion(JSON.stringify(self.contentData)).then(
            function(response) {
                if (response.status == 'OK') {
                    messageService.toasterMessage(CONF.TOASTER_TOP_FULL,CONF.TOASTER_SUCCESS,'Success to send your discussion! ');
                    window.location.reload();
                }
                else{
                    messageService.toasterMessage(CONF.TOASTER_TOP_FULL,CONF.TOASTER_ERROR,response.message);
                }
            }
        )


    };

    function getDataSite() {
        if(localStorageService.get(CONF.SITE_INFO) != undefined){
            self.dataSite = localStorageService.get(CONF.SITE_INFO);

            if(self.dataSite.site_logo != null){
                self.dataSite.site_logo = CONF.IMAGE_PATH + CONF.PATH_FILES_LOGO + self.dataSite.site_logo;
            }
        }
    }

    sendViewData();

    getDataSite();

    getDetailProduct();

};