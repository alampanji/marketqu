'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('AdminCategoryCtrl', [
        '$scope', 'CONF', 'ProductService', '$location', '$modal', '$window',
        AdminCategoryCtrl
    ]);

function AdminCategoryCtrl($scope, CONF, ProductService, $location, $modal, $window) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;

    self.create = function () {
        $location.path('/admin/create-category');
    };

    function getListCategory() {

        ProductService.getListCategory(self.currentPage - 1, self.limit).then(
            function(response) {
                if (response.status == 'OK') {
                    self.dataList = response.data;
                    // self.bigTotalItems = response.elements;
                    // self.numPages = response.pages;

                }
            });
    }

    self.doUpdateCategory = function (id) {
        $location.path('/admin/update-category').search({ id: id });
    };

    self.doDelete = function (id, name) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteCategory.html',
            controller: 'DeleteCategoryCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function() {
                    return {
                        id: id,
                        name: name,
                    };
                }
            }
        });
        // modalDelete.result.then(function() {}, function(data) {
        //     if (data == "close") {
        //         if (self.users.length == 1 && self.currentPage != 1) {
        //             self.currentPage--;
        //         }
        //         getListUser();
        //     }
        // });
    };

    getListCategory();
};
