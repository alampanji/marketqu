'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('AdminProductCtrl', [
        '$scope', 'CONF', 'ProductService', '$location', '$modal', '$window', 'privilegeService', 'localStorageService',
        AdminProductCtrl
    ]);

function AdminProductCtrl($scope, CONF, ProductService, $location, $modal, $window, privilegeService, localStorageService) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;

    self.createProduct = function () {
        $location.path('/admin/create-product');
    };

    function getListProdusen() {

        ProductService.getListProduct(self.currentPage - 1, self.limit).then(
            function(response) {
                if (response.status == 'OK') {
                    self.dataList = response.data;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                }
            });
    }

    self.doUpdate = function (id) {
        $location.path('/admin/update-product').search({ id: id });
    };

    self.doDelete = function (id, name) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteProduct.html',
            controller: 'DeleteProductCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function() {
                    return {
                        id: id,
                        name: name,
                    };
                }
            }
        });
    };

    self.doFeedback = function (id) {
      $location.path('admin/produsen-feedback').search({product_id:id});
    };

    self.hasRoleProdusen = function () {
        privilegeService.hasRoleIsProdusen();

    };

    self.hasRoleAdmin = function () {
        privilegeService.hasRoleIsAdmin();

    };

    getListProdusen();
};
