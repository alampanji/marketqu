'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('AssignItemCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'ItemService', 'LocationService',
        AssignItemCtrl
    ]);

function AssignItemCtrl($scope, CONF, $location, $modal, $window, ItemService, LocationService) {
    var self = $scope;
    var paramValue = $location.search().id_group_user;

    self.close = close;

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    };


    function close() {
        $modalInstance.dismiss('close');
    }

    function getItem(){
        ItemService.getItemReseller().then(
            function (response) {
                if (response.status !== 'ERROR') {
                    self.product = response.data;
                    self.itemProduct = self.product[0];
                    self.stockItem = self.itemProduct.stock;
                }
            }
        );
    };

    function getReport(){
        ItemService.getReport(paramValue).then(
            function (response) {
                if (response.status !== 'ERROR') {
                    self.reports = response.data;
                }
            }
        );
    };

    self.setItem = function (data) {
        self.stockItem = data.stock;
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: 'Are you sure to cancel assign product process?',
                        path: 'admin/marketing'
                    };
                }
            }
        });

    };

    self.processSubmit = function () {

        self.stockInvalid = false;
        self.dateInvalid = false;
        var validation = false;

        if(self.stockAssign > self.stockItem){
            invalidModal("Stock Assign should not be greater than Stock Available");
            self.stockInvalid = true;
            validation = true;
        }

        if(self.date == undefined || self.date == ''){
            invalidModal("Date is not Empty");
            self.dateInvalid = true;
            validation = true;
        }

        if(validation == false){

            var request = {
                "id_item":self.itemProduct.id,
                "id_user_group":paramValue,
                "stock_start":self.stockAssign,
                "start_date":Date.parse(self.date)
            };

            ItemService.processAssignItem(JSON.stringify(request)).then(
                function (response) {
                    if (response.status !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Information',
                                        message: 'Assign Product has been success',
                                        path: 'reload'
                                    };
                                }
                            }
                        });
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.message,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                }
            );

        }
    };

    getItem();

    getReport();

};
