'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('BankCtrl', [
        '$scope', 'CONF', 'UserService', '$location', '$modal', '$window',
        BankCtrl
    ]);

function BankCtrl($scope, CONF, UserService, $location, $modal, $window) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;

    self.createProdusen = function () {
        $location.path('/admin/create-produsen');
    };

    function getListProdusen() {

        UserService.getListUserProdusen(self.currentPage - 1, self.limit).then(
            function(response) {
                if (response.status == 'OK') {
                    self.produsenList = response.data;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                }
            });
    }

    self.doUpdateProdusen = function () {
        $location.path('/admin/update-produsen');
    };

    self.doDeleteProdusen = function (id, name) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteProdusen.html',
            controller: 'DeleteProdusenCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function() {
                    return {
                        id: id,
                        name: name,
                    };
                }
            }
        });
        // modalDelete.result.then(function() {}, function(data) {
        //     if (data == "close") {
        //         if (self.users.length == 1 && self.currentPage != 1) {
        //             self.currentPage--;
        //         }
        //         getListUser();
        //     }
        // });
    };

    getListProdusen();
};
