'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('CreateCategoryCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'ProductService', 'LocationService',
        CreateCategoryCtrl
    ]);

function CreateCategoryCtrl($scope, CONF, $location, $modal, $window, ProductService, LocationService) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    function checkPassword() {
        var boolean = false;

        if(self.password != self.confirmPassword){
            boolean = true;
        }

        return boolean;
    }

    var checking = function () {
        self.nameInvalid = false;
        self.addressInvalid = false;

        var bool = false;

        // if(self.name == null || self.name == ''){
        //     self.nameInvalid = true;
        //     bool = true;
        // }
        // if(self.address == null || self.address == ''){
        //     self.addressInvalid = true;
        //     bool = true;
        // }

        return bool;
    };


    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah produsen?',
                        path: 'admin/produsen'
                    };
                }
            }
        });

    };

    self.processSubmit = function () {

        var hasil = checking();
        if(hasil){
            invalidModal("Data is not valid, please check your input text");
        }
        else{

            var formDt = new FormData();
            formDt.append('image',self.images);
            formDt.append('name', angular.toJson(self.name));
            formDt.append('description', angular.toJson(self.description));
            formDt.append('id_parent', angular.toJson('0'));

            ProductService.createCategory(formDt).then(
                    function (response) {
                        if (response.status !== 'ERROR') {
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Informasi',
                                            message: 'Create category has been success',
                                            path: 'admin/category'
                                        };
                                    }
                                }
                            });
                        } else {
                            self.isDisabled = false;
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Gagal',
                                            message: response.message,
                                            path: ''
                                        };
                                    }
                                }
                            });
                        }
                    }
                );
        }
    };

};
