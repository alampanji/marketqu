'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('CreateMarketingCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'ItemService', 'LocationService',
        CreateMarketingCtrl
    ]);

function CreateMarketingCtrl($scope, CONF, $location, $modal, $window, ItemService, LocationService) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    function checkPassword() {
        var boolean = false;

        if(self.password != self.confirmPassword){
            boolean = true;
        }

        return boolean;
    }

    var checking = function () {
        self.nameInvalid = false;
        self.addressInvalid = false;

        var bool = false;

        return bool;
    };


    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah produsen?',
                        path: 'admin/produsen'
                    };
                }
            }
        });

    };

    self.processSubmit = function () {

        var hasil = checking();
        if(hasil){
            invalidModal("Data is not valid, please check your input text");
        }
        else{
            var request = {
                "group_name":self.name,
                "description":self.description
            };

            ItemService.createMarketing(JSON.stringify(request)).then(
                function (response) {
                    if (response.status !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Create marketing has been success',
                                        path: 'admin/marketing'
                                    };
                                }
                            }
                        });
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.message,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                }
            );
        }
    };

};
