'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('CreatePrivilegeCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'GroupService',
        CreatePrivilegeCtrl
    ]);

function CreatePrivilegeCtrl($scope, CONF, $location, $modal, $window, GroupService) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    function getRole(){
        self.roles = [
            {"name":"Produsen", "value": "2"},
            {"name":"Reseller", "value": "3"}
        ];
        self.itemRole = self.roles[0];
        // LocationService.getListLocation(type, id).then(
        //     function(response) {
        //         if (response.message !== 'ERROR') {
        //             self.country = response.result;
        //             self.itemCountry = self.country[0];
        //             self.setProvince(self.itemCountry.country_id);
        //         }
        //     }
        // );
    }

    function getListCapabillities(){
        GroupService.getListCapabillities().then(
            function(response) {
                if (response.status == 'OK') {
                    self.capabillitesList = response.data;
                }
                console.log(self.capabillitesList);
            });
    }

    function getListCountry(){
        var type = 'country';
        var id = '';
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.country = response.result;
                    self.itemCountry = self.country[0];
                    self.setProvince(self.itemCountry.country_id);
                }
            });
    }

    self.setProvince = function(countryId){
        var type = 'province';
        var id = countryId;
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.province = response.result;
                    self.itemProvince = self.province[0];
                    self.setCity(self.itemProvince);
                }
            });
    };

    self.checkPassword = function () {
        var boolean = false;
        if(self.contentData.password != self.contentData.confirmPassword){
            boolean = true;
        }
        return boolean
    };

    self.setCity = function(provinceId){
        var type = 'city';
        var id = provinceId.province_id;
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.city = response.result;
                    self.itemCity = self.city[0];
                }
            });
    };

    self.selectCity = function (city) {
        self.itemCity = city
    };

    self.processSubmit = function () {

        var request = {};
        request.username = self.username;
        request.password = self.password;
        request.name = self.fullName;
        request.email = self.email;
        request.contact = self.contact;
        request.country = self.itemCountry.country_id;
        request.province = self.itemProvince.province_id;
        request.city = self.itemCity.city_id;
        request.address = self.address;
        request.role = '2';

        UserService.createUser(JSON.stringify(request)).then(
            function (response) {
                if (response.status !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Informasi',
                                    message: 'Berhasil Menambah User Produsen',
                                    path: 'admin/produsen'
                                };
                            }
                        }
                    });
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.message,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            }
        );
    };

    getRole();

    getListCapabillities();

    // getListCountry();

};
