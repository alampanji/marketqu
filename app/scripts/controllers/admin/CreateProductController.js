'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('CreateProductCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'ProductService', 'localStorageService',
        CreateProductCtrl
    ]);

function CreateProductCtrl($scope, CONF, $location, $modal, $window, ProductService, localStorageService) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    var checking = function () {
        self.nameInvalid = false;
        self.priceLowInvalid = false;
        self.priceSellInvalid = false;
        self.stockInvalid = false;
        self.imageDefaultInvalid = false;
        self.descriptionInvalid = false;

        var bool = false;

        if(self.name == null || self.name == ''){
            self.nameInvalid = true;
            bool = true;
        }
        if (self.priceLow == null || self.priceLow == '') {
            self.priceLowInvalid = true;
            bool = true;
        }
        if (self.priceSell == null || self.priceSell == '') {
            self.priceSellInvalid = true;
            bool = true;
        }
        if(self.stock == null || self.stock == ''){
            self.stockInvalid = true;
            bool = true;
        }
        if(self.images1 == undefined || self.images1 == null){
            self.imageDefaultInvalid = true;
            bool = true;
        }

        if(self.description == null || self.description == ''){
            self.descriptionInvalid = true;
            bool = true;
        }

        return bool;
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: 'Are you sure to cancel create product process?',
                        path: 'admin/product'
                    };
                }
            }
        });

    };

    self.processSubmit = function () {

        self.idCategory = [];

        angular.forEach(self.categoryList, function (categoryList) {
            self.idCategory.push(categoryList.id);
        })

        var result = checking();

        if(result){
            invalidModal("Data is not valid, please check your input text");
        }
        else {

            var formDt = new FormData();
            formDt.append('image1', self.images1);
            formDt.append('image2', self.images2);
            formDt.append('image3', self.images3);
            formDt.append('image4', self.images4);
            formDt.append('image5', self.images5);
            formDt.append('id_user', angular.toJson(localStorageService.cookie.get(CONF.id)));
            formDt.append('name', angular.toJson(self.name));
            formDt.append('price_low', angular.toJson(self.priceLow));
            formDt.append('price_sell', angular.toJson(self.priceSell));
            formDt.append('discount', angular.toJson(self.discount));
            formDt.append('type', angular.toJson(self.itemType.value));
            formDt.append('stock', angular.toJson(self.stock));
            formDt.append('status', angular.toJson(self.itemStatus.value));
            formDt.append('category', angular.toJson(self.idCategory.join(',')));
            formDt.append('featured', '1');
            formDt.append('description', angular.toJson(self.description));
            formDt.append('weight', angular.toJson(self.weight));
            formDt.append('volume', angular.toJson(self.itemVolume.value));

            ProductService.createProduct(formDt).then(
                function (response) {
                    if (response.status !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Information',
                                        message: 'Success to create product',
                                        path: 'admin/product'
                                    };
                                }
                            }
                        });
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Failed',
                                        message: response.message,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                }
            );
        }
    };

    self.status = [
        {'value': 1, display: 'Active'},
        {'value': 2, display: 'Not Active'}
    ];

    self.volume = [
        {'value': 'pcs', display: 'Pcs'},
        {'value': 'box', display: 'Box'},
        {'value': 'pack', display: 'Pack'}
    ];

    function getStatus(){
        self.itemStatus = self.status[0];
        getListCategory();
    }

    function getVolume(){
        self.itemVolume = self.volume[0];
    }

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    self.categoryList = [];

    self.addCategory = function () {
        if(self.itemCategory.id != ''){
            self.categoryList.push(self.itemCategory);
        }
    };

    self.deleteCategory = function (index) {
        self.categoryList.splice(index,1);
    };

    function getListCategory(){
        ProductService.getListCategory().then(
            function(response) {
                if (response.status == 'OK') {
                    self.category = response.data;
                    self.categoryConstruct = [
                        {
                                'name':'Please Select',
                                'id'  : ''

                        }
                    ];
                    angular.forEach(self.category, function(data) {
                        self.categoryConstruct.push(data.category);
                        self.itemCategory = self.categoryConstruct[0];
                    });
                    // self.bigTotalItems = response.elements;
                    // self.numPages = response.pages;

                }
            });
    }

    function generateType(){
        self.type = [{"name":"Baru", "value":"Baru"}, {"name":"Bekas", "value":"Bekas"}];
        self.itemType = self.type[0];
    };


    backToTop();

    getStatus();

    getVolume();

    generateType();

};
