'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('CreateProdusenCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'UserService', 'LocationService',
        CreateProdusenCtrl
    ]);

function CreateProdusenCtrl($scope, CONF, $location, $modal, $window, UserService, LocationService) {
    var self = $scope;

    self.close = close;

    getListCountry();

    var backToTop = function () {
        $window.scrollTo(0, 0);
    };

    backToTop();

    function close() {
        $modalInstance.dismiss('close');
    }

    function getListCountry(){
        var type = 'country';
        var id = '';
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.status == 'OK') {
                    self.country = response.result;
                    self.itemCountry = self.country[0];
                    self.setProvince(self.itemCountry.country_id);
                }
            });
    }

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.setProvince = function(countryId){
        var type = 'province';
        var id = countryId;
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.province = response.result;
                    self.itemProvince = self.province[0];
                    self.setCity(self.itemProvince);
                }
            });
    };

    self.setCity = function(provinceId){
        var type = 'city';
        var id = provinceId.province_id;
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.city = response.result;
                    self.itemCity = self.city[0];
                }
        });
    };

    self.selectCity = function (city) {
        self.itemCity = city
    };

    function checkPassword() {
        var boolean = false;

        if(self.password != self.confirmPassword){
            boolean = true;
        }

        return boolean;
    }

    var checking = function () {
        self.userNameInvalid = false;
        self.confirmationPasswordInvalid = false;
        self.fullNameInvalid = false;
        self.passwordInvalid = false;
        self.confirmPasswordInvalid = false;
        self.emailInvalid = false;
        self.phoneInvalid = false;
        self.addressInvalid = false;

        var bool = false;

        if(self.fullName == null || self.fullName == ''){
            self.fullNameInvalid = true;
            bool = true;
        }
        if (self.username == null || self.username == '') {
            self.userNameInvalid = true;
            bool = true;
        }
        if (self.password == null || self.password == '') {
            self.confirmPasswordInvalid = true;
            bool = true;
        }
        if (self.confirmPassword == null || self.confirmPassword == '') {
            self.passwordInvalid = true;
            bool = true;
        }
        if(self.email == null || self.email == ''){
            self.emailInvalid = true;
            bool = true;
        }
        if(self.contact == null || self.contact == ''){
            self.phoneInvalid = true;
            bool = true;
        }

        if(self.address == null || self.address == ''){
            self.addressInvalid = true;
            bool = true;
        }

        return bool;
    };


    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: '',
                        message: 'Are you sure to cancel create produsen process?',
                        path: 'admin/produsen'
                    };
                }
            }
        });

    };

    self.processSubmit = function () {

        var hasil = checking();
        var passwordInvalid = checkPassword();
        if(hasil){
            invalidModal("Data is not valid, please check your input text");
        }
        else{
            if(passwordInvalid){

                invalidModal("Password is not match!")
            }
            else{

                if(self.institution == undefined){
                    self.institution = '';
                }
                if(self.department == undefined){
                    self.department = '';
                }

                var request = {};
                request.username = self.username;
                request.password = self.password;
                request.name = self.fullName;
                request.last_name = self.lastName;
                request.email = self.email;
                request.contact = self.contact;
                request.country = self.itemCountry.country_id;
                request.province = self.itemProvince.province_id;
                request.city = self.itemCity.city_id;
                request.address = self.address;
                request.postcode = self.post;
                request.institution = self.institution;
                request.department = self.department;
                request.role = '2';

                UserService.createUser(JSON.stringify(request)).then(
                    function (response) {
                        if (response.status !== 'ERROR') {
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Information',
                                            message: 'Successfully to create User Produsen',
                                            path: 'admin/produsen'
                                        };
                                    }
                                }
                            });
                        } else {
                            self.isDisabled = false;
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Failed',
                                            message: response.message,
                                            path: ''
                                        };
                                    }
                                }
                            });
                        }
                    }
                );

            }
        }
    };
    
};
