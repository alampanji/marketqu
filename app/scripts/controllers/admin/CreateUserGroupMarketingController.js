'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('CreateUserGroupMarketingCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'ItemService', 'LocationService',
        CreateUserGroupMarketingCtrl
    ]);

function CreateUserGroupMarketingCtrl($scope, CONF, $location, $modal, $window, ItemService, LocationService) {
    var self = $scope;
    var paramValue = $location.search().id_mrkt;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.dataUser = [];

    self.addUser = function (username, email) {

        console.log(username);
        console.log(email);

       if(username!=undefined && email!=undefined) {
               var user = {
                   "username":username,
                   "email":email
               };

               self.dataUser.push(user);
       }
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    function checkPassword() {
        var boolean = false;

        if(self.password != self.confirmPassword){
            boolean = true;
        }

        return boolean;
    }

    var checking = function () {
        self.nameInvalid = false;
        self.addressInvalid = false;

        var bool = false;

        return bool;
    };


    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: 'Apakah anda ingin membatalkan proses tambah produsen?',
                        path: 'admin/produsen'
                    };
                }
            }
        });

    };

    self.processSubmit = function () {

        if(self.dataUser.length == 0){
            invalidModal("Data users are not empty");
        }
        else{
            var request = {
                "id_group":paramValue,
                "users":self.dataUser
            };

            console.log(request);

            ItemService.createUserMarketingGroup(JSON.stringify(request)).then(
                function (response) {
                    if (response.status !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Create marketing has been success',
                                        path: 'admin/marketing'
                                    };
                                }
                            }
                        });
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.message,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                }
            );
        }
    };

};
