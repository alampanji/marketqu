'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('marketplaceApp')
    .controller('DeleteCategoryCtrl', [
        '$scope', '$location', '$modal', '$modalInstance', 'modalParam', 'ProductService',
        DeleteCategoryCtrl
    ]);

function DeleteCategoryCtrl($scope, $location, $modal, $modalInstance, modalParam, ProductService) {

    var self = $scope;
    self.close = close;

    var modelpassing = modalParam;
    self.userId = modelpassing.id;
    self.name = modelpassing.name;

    console.log(self.userId);

    function close() {
        $modalInstance.dismiss('close');
    };

    self.isDisabled = false;

    self.doDeleteUser = function () {
        self.isDisabled = true;

        var request={
            "id": self.userId
        };

        ProductService.deleteCategory(JSON.stringify(request)).then(
            function (response) {
                $modalInstance.dismiss('close');

                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Information',
                                    message: 'Category has been deleted',
                                    path: 'reload'
                                };
                            }
                        }
                    });
                    $location.path('/admin/category');
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Failed',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    }


};