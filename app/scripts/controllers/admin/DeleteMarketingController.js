'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('marketplaceApp')
    .controller('DeleteMarketingCtrl', [
        '$scope', '$location', '$modal', '$modalInstance', 'modalParam', 'ItemService',
        DeleteMarketingCtrl
    ]);

function DeleteMarketingCtrl($scope, $location, $modal, $modalInstance, modalParam, ItemService) {

    var self = $scope;
    self.close = close;

    var modelpassing = modalParam;
    self.userId = modelpassing.id;
    self.name = modelpassing.name;

    console.log(self.userId);

    function close() {
        $modalInstance.dismiss('close');
    };

    self.isDisabled = false;

    self.doDeleteUser = function () {
        self.isDisabled = true;

        var request={
            "id_group": self.userId
        };

        ItemService.deleteMarketing(JSON.stringify(request)).then(
            function (response) {
                $modalInstance.dismiss('close');

                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Information',
                                    message: 'Marketing has been deleted',
                                    path: 'reload'
                                };
                            }
                        }
                    });
                    $location.path('/admin/marketing');
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Failed',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    }


};