'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('marketplaceApp')
    .controller('DeleteProductCtrl', [
        '$scope', '$location', '$modal', '$modalInstance', 'modalParam', 'ProductService',
        DeleteProductCtrl
    ]);

function DeleteProductCtrl($scope, $location, $modal, $modalInstance, modalParam, ProductService) {

    var self = $scope;
    self.close = close;

    var modelpassing = modalParam;
    self.userId = modelpassing.id;
    self.name = modelpassing.name;

    function close() {
        $modalInstance.dismiss('close');
    };

    self.isDisabled = false;

    self.doDeleteUser = function () {
        self.isDisabled = true;

        var request={
            "id": self.userId
        };

        ProductService.deleteProduct(request).then(
            function (response) {
                $modalInstance.dismiss('close');

                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Information',
                                    message: 'Product has been deleted',
                                    path: 'reload'
                                };
                            }
                        }
                    });
                    $location.path('/admin/product')
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    }


};