'use strict';
/**
 * @ngdoc function
 * @name ahmApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ahmApp
 */

angular.module('marketplaceApp')
    .controller('DeleteResellerCtrl', [
        '$scope', '$location', '$modal', '$modalInstance', 'modalParam', 'UserService',
        DeleteResellerCtrl
    ]);

function DeleteResellerCtrl($scope, $location, $modal, $modalInstance, modalParam, UserService) {

    var self = $scope;
    self.close = close;

    var modelpassing = modalParam;
    self.userId = modelpassing.id;
    self.name = modelpassing.name;
    self.teacherVersion = modelpassing.version;

    function close() {
        $modalInstance.dismiss('close');
    };

    self.isDisabled = false;

    self.doDeleteUser = function () {
        self.isDisabled = true;

        var request={
            "id": self.userId
        };

        UserService.deleteUser(JSON.stringify(request)).then(
            function (response) {
                $modalInstance.dismiss('close');

                if (response.message !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Information',
                                    message: 'Reseller has been deleted',
                                    path: 'reload'
                                };
                            }
                        }
                    });
                    $location.path('admin/produsen');
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        backdrop: 'static',

                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Failed',
                                    message: response.result,
                                    path: ''
                                };
                            }
                        }
                    });
                }

            }
        );
    }


};