'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('DetailComplainAdviceCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window',
        DetailComplainAdviceCtrl
    ]);

function DetailComplainAdviceCtrl($scope, CONF, $location, $modal, $window) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

};
