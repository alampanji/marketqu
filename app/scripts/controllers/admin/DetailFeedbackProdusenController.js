'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('DetailFeedbackDetailCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'ItemService', 'LocationService',
        DetailFeedbackDetailCtrl
    ]);

function DetailFeedbackDetailCtrl($scope, CONF, $location, $modal, $window, ItemService, LocationService) {
    var self = $scope;
    var paramValue = $location.search().product_id;
    var paramId = $location.search().id;


    self.close = close;

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    };


    function close() {
        $modalInstance.dismiss('close');
    }

    self.setItem = function (data) {
        self.stockItem = data.stock;
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };


    function validation() {
        self.titleInvalid = false;
        self.contentInvalid = false;
        var boolean = false;

        if(self.title == undefined || self.title == ''){
            self.titleInvalid = true;
            boolean = true
        }

        if(self.content == undefined || self.content == ''){
            self.contentInvalid = true;
            boolean = true
        }

        return boolean;
    }

    self.processSubmit = function () {

        if(validation() == true){
            invalidModal("Your input is not valid, please check first!")
        }

        else{

            var request = {
                "id_product":paramValue,
                "title":self.title,
                "content":self.content
            };

            ItemService.giveFeedback(JSON.stringify(request)).then(
                function (response) {
                    if (response.status !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Information',
                                        message: 'Give feedback has been success',
                                        path: 'admin/transaction-detail',
                                        paramSearch : paramId
                                    };
                                }
                            }
                        });
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.message,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                }
            );

        }
    };

};
