'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('DetailGroupMarketingCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'ItemService', 'LocationService',
        DetailGroupMarketingCtrl
    ]);

function DetailGroupMarketingCtrl($scope, CONF, $location, $modal, $window, ItemService, LocationService) {
    var self = $scope;
    var paramValue = $location.search().id_mrkt;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.create = function () {
        $location.path('/admin/create-user-group-marketing');
    };

    function getDetailMarketing(){
        ItemService.getDetailMarketing(paramValue).then(
            function (response) {
                if (response.status !== 'ERROR') {
                    self.data = response.data;
                }
            }
        );
    };

    self.assignItem = function (id) {
        $location.path('/admin/assign-item').search({id_group_user:id})
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    function checkPassword() {
        var boolean = false;

        if(self.password != self.confirmPassword){
            boolean = true;
        }

        return boolean;
    }

    var checking = function () {
        self.nameInvalid = false;
        self.descriptionInvalid = false;

        var bool = false;

        if(self.data.group_name == null || self.data.group_name == ''){
            self.nameInvalid = true;
            bool = true;
        }
        if(self.data.description == null || self.data.description == ''){
            self.descriptionInvalid = true;
            bool = true;
        }

        return bool;
    };


    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: 'Are you sure to cancel update category?',
                        path: 'admin/category'
                    };
                }
            }
        });

    };

    self.processSubmit = function () {
        var hasil = checking();
        console.log(hasil);
        if(hasil){
            invalidModal("Data is not valid, please check your input text");
        }
        else{
            var request = {
                "name":self.data.group_name,
                "description":self.data.description,
                "id_group":self.data.id,
                "status":self.data.status
            };

            ItemService.updateMarketing(JSON.stringify(request)).then(
                function (response) {
                    if (response.status !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Update marketing has been success',
                                        path: 'admin/marketing'
                                    };
                                }
                            }
                        });
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.message,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                }
            );
        }
    };

    getDetailMarketing();

};
