'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('FeedbackProdusenCtrl', [
        '$scope', 'CONF', 'ItemService', '$location', '$modal', '$window', 'privilegeService', 'localStorageService',
        FeedbackProdusenCtrl
    ]);

function FeedbackProdusenCtrl($scope, CONF, ItemService, $location, $modal, $window, privilegeService, localStorageService) {
    var self = $scope;

    self.close = close;
    var paramValue = $location.search().product_id;

    function close() {
        $modalInstance.dismiss('close');
    }


    self.createProduct = function () {
        $location.path('/admin/create-product');
    };

    function getListFeedback() {

        ItemService.getFeedbackProdusen(paramValue).then(
            function(response) {
                if (response.status == 'OK') {
                    self.dataList = response.data;

                }
            });
    }

    self.hasRoleProdusen = function () {
        privilegeService.hasRoleIsProdusen();

    };

    self.doDetailFeedback = function (id) {
        $location.path('admin/view-feedback-reseller').search({feedback_id:id});
    };

    self.hasRoleAdmin = function () {
        privilegeService.hasRoleIsAdmin();

    };

    getListFeedback();
};
