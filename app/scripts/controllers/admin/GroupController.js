'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('GroupCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window',
        GroupCtrl
    ]);

function GroupCtrl($scope, CONF, $location, $modal, $window) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.createCapabillities = function () {
        $location.path('/admin/create-privilege');
    };

    function getListProdusen() {
        self.produsenList = [
            {
                name :"User Produsen",
                email :"user@mail.com",
                balance :"Rp. 2000000"
            },
            {
                name :"User Produsen2",
                email :"user2@mail.com",
                balance :"Rp. 6000000"
            }
        ]
    }

    self.doUpdateGroup = function () {
        $location.path('/admin/update-produsen');
    };

    self.doDeleteGroup = function () {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteGroup.html',
            controller: 'DeleteGroupCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function() {
                    return {
                        // id: id,
                        // name: name,
                    };
                }
            }
        });
        // modalDelete.result.then(function() {}, function(data) {
        //     if (data == "close") {
        //         if (self.users.length == 1 && self.currentPage != 1) {
        //             self.currentPage--;
        //         }
        //         getListUser();
        //     }
        // });
    };

    self.createGroup = function () {
        $location.path('admin/create-privilege');
    }

    getListProdusen();
};
