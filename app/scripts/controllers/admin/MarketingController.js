'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('MarketingCtrl', [
        '$scope', 'CONF', 'ItemService', '$location', '$modal', '$window',
        MarketingCtrl
    ]);

function MarketingCtrl($scope, CONF, ItemService, $location, $modal, $window) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;

    self.create = function () {
        $location.path('/admin/create-marketing');
    };

    function getListMarketing() {

        ItemService.getListMarketing(self.currentPage - 1, self.limit).then(
            function(response) {
                if (response.status == 'OK') {
                    self.dataList = response.data;
                    // self.bigTotalItems = response.elements;
                    // self.numPages = response.pages;

                }
            });
    }

    self.detailGroup = function (id) {
        $location.path('/admin/detail-group-marketing').search({ id_mrkt: id });
    };

    self.doUpdate = function (id) {
        $location.path('/admin/update-marketing').search({ id_mrkt: id });
    };

    self.doDelete = function (id, name) {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteMarketing.html',
            controller: 'DeleteMarketingCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function() {
                    return {
                        id: id,
                        name: name,
                    };
                }
            }
        });
        // modalDelete.result.then(function() {}, function(data) {
        //     if (data == "close") {
        //         if (self.users.length == 1 && self.currentPage != 1) {
        //             self.currentPage--;
        //         }
        //         getListUser();
        //     }
        // });
    };

    getListMarketing();
};
