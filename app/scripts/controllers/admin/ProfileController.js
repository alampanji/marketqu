'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('ProfileCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'UserService', 'LocationService', 'privilegeService',
        ProfileCtrl
    ]);

function ProfileCtrl($scope, CONF, $location, $modal, $window, UserService, LocationService, privilegeService) {
    var self = $scope;

    self.close = close;

    self.tab = 1;

    self.setTab = function(tabId) {
        self.tab = tabId;
    };

    self.isSet = function(tabId) {
        if(self.tab==tabId){
            return true
        }
        // return self.tab === tabId;
    };

    function close() {
        $modalInstance.dismiss('close');
    }

    self.viewOldImage = function (image) {
        $modal.open({
            templateUrl: 'views/modal/ModalViewImage.html',
            controller: 'ModalViewImageCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'View Old Image',
                        message: image,
                        path: ''
                    };
                }
            }
        });
    };

    function checkingMarketplaceSite(){

        var bool = false;
        self.siteNameInvalid = false;
        self.siteEmailInvalid = false;
        self.siteAddressInvalid = false;
        self.sitePhoneInvalid = false;

        if(self.site.site_name == null || self.site.site_name == ''){
            self.siteNameInvalid = true;
            bool = true;
        }

        if(self.site.site_email == null || self.site.site_email == ''){
            self.siteEmailInvalid = true;
            bool = true;
        }

        if(self.site.site_address == null || self.site.site_address == ''){
            self.siteAddressInvalid = true;
            bool = true;
        }

        if(self.site.site_phone == null || self.site.site_phone == ''){
            self.sitePhoneInvalid = true;
            bool = true;
        }

        return bool;

    }


    function getDetailProfile() {
        UserService.getDetailProfile().then(
            function (response) {
                if (response.status !== 'ERROR') {
                    self.site = response.data_site;
                    self.user = response.data_user;
                    self.profile = response.data_profile;
                    if(self.profile.photo != null){
                        self.profile.img = CONF.IMAGE_PATH+CONF.PATH_FILES_USER+self.profile.photo
                    }

                    if(self.site.site_logo != null){
                        self.site.imgLogo = CONF.IMAGE_PATH+CONF.PATH_FILES_LOGO+self.site.site_logo
                    }

                    if(self.site.site_icon != null){
                        self.site.imgIcon = CONF.PATH_FILES_ICON+self.site.site_icon
                    }

                    if(self.site.site_banner != null){
                        self.site.imgBanner = CONF.IMAGE_PATH+CONF.PATH_FILES_BANNER+self.site.site_banner
                    }

                    self.bank = response.data_bank;
                }
            }
        );
    }

    function getListCountry(){
        var type = 'country';
        var id = '';
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.country = response.result;
                    self.itemCountry = self.country[0];
                    self.setProvince(self.itemCountry.country_id);
                }
            });
    }

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.setProvince = function(countryId){
        var type = 'province';
        var id = countryId;
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.province = response.result;
                    angular.forEach(self.province, function (provinceSet) {
                       if(provinceSet.province_id == self.profile.province){
                           self.itemProvince = provinceSet;
                       }
                    });
                    self.setCity(self.itemProvince);
                }
            });
    };

    self.setCity = function(provinceId){
        console.log(provinceId);
        var type = 'city';
        var id = provinceId.province_id;
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.city = response.result;
                    angular.forEach(self.city, function (citySet) {
                        if(citySet.city_id == self.profile.city){
                            self.itemCity = citySet;
                        }

                        self.itemCity = self.city[0];
                    })
                }
            });
    };

    self.selectCity = function (city) {
        self.itemCity = city
    };

    self.processUpdateProfile = function () {

        if(self.photo == undefined){
            self.photo = '';
        }


        console.log(self.user.uid);

        if(self.photo == undefined){
            self.photo = '';
        }

        var formDt = new FormData();
        formDt.append('photo', self.photo);
        formDt.append('name', angular.toJson(self.profile.name));
        formDt.append('uid', angular.toJson(self.user.uid));
        formDt.append('contact', angular.toJson(self.profile.contact));
        formDt.append('country', angular.toJson(self.itemCountry.country_id));
        formDt.append('province', angular.toJson(self.itemProvince.province_id));
        formDt.append('city', angular.toJson(self.itemCity.city_id));
        formDt.append('address', angular.toJson(self.profile.address));
        formDt.append('postcode', angular.toJson(self.profile.postcode));
        formDt.append('institution', angular.toJson(self.profile.institution));
        formDt.append('department', angular.toJson(self.profile.department));

        UserService.updateProfile(formDt).then(
            function (response) {
                if (response.status == 'OK') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Information',
                                    message: 'Success to update profile',
                                    path: 'reload'
                                };
                            }
                        }
                    });
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Failed',
                                    message: response.message,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            }
        );


    };

    self.processUpdateSite = function () {

        var result = checkingMarketplaceSite();

        if(result){
            invalidModal("Your input is not valid, please checking again");
        }
        else{

            if(self.site_logo == undefined){
                self.site_logo = '';
            }
            if(self.site_icon == undefined){
                self.site_icon = '';
            }
            if(self.site_banner == undefined){
                self.site_banner = '';
            }

            var formDt = new FormData();
            formDt.append('logo', self.site_logo);
            formDt.append('icon', self.site_icon);
            formDt.append('banner', self.site_banner);
            formDt.append('site_name', angular.toJson(self.site.site_name));
            formDt.append('site_email', angular.toJson(self.site.site_email));
            formDt.append('site_address', angular.toJson(self.site.site_address));
            formDt.append('site_about', angular.toJson(self.site.site_about));
            formDt.append('site_facebook', angular.toJson(self.site.site_facebook));
            formDt.append('site_twitter', angular.toJson(self.site.site_twitter));
            formDt.append('site_instagram', angular.toJson(self.site.site_instagram));
            formDt.append('site_google', angular.toJson(self.site.site_google));
            formDt.append('site_path', angular.toJson(self.site.site_path));
            formDt.append('site_youtube', angular.toJson(self.site.site_youtube));
            formDt.append('site_phone', angular.toJson(self.site.site_phone));

            UserService.updateSite(formDt).then(
                function (response) {
                    if (response.status == 'OK') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Information',
                                        message: 'Success to update site',
                                        path: 'reload'
                                    };
                                }
                            }
                        });
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Failed',
                                        message: response.message,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                }
            );

        }
    };

    self.hasRoleAdmin = function () {
        return privilegeService.hasRoleIsAdmin();
    };

    getDetailProfile();

    getListCountry();
};