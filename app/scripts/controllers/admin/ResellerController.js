'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('ResellerCtrl', [
        '$scope', 'CONF', '$location', 'UserService', '$modal', '$window',
        ResellerCtrl
    ]);

function ResellerCtrl($scope, CONF, $location, UserService, $modal, $window) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.createReseller = function () {
        $location.path('/admin/create-reseller');
    };

    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;

    function getListReseller() {

        UserService.getListUserReseller(self.currentPage - 1, self.limit).then(
            function(response) {
                if (response.status == 'OK') {
                    self.resellerList = response.data;
                    self.bigTotalItems = response.elements;
                    self.numPages = response.pages;

                }
            });

    }

    self.doUpdateReseller = function (id) {
        $location.path('/admin/update-reseller').search({id_reseller:id});
    };

    self.doDeleteReseller = function () {
        var modalDelete = $modal.open({
            templateUrl: 'views/modal/ModalDeleteReseller.html',
            controller: 'DeleteResellerCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                modalParam: function() {
                    return {
                        // id: id,
                        // name: name,
                    };
                }
            }
        });
    };

    

    getListReseller();
};