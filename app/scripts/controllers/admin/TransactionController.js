'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('TransactionCtrl', [
        '$scope','TransactionService','$modal','CONF', '$location', 'privilegeService', 'localStorageService',
        TransactionCtrl
    ]);

function TransactionCtrl($scope,TransactionService,$modal,CONF, $location, privilegeService, localStorageService) {
    var self = $scope;

    self.accesLevel = {};
    self.role = {};
    self.accesLevel.value = {};

    self.maxSize = 5;
    self.bigTotalItems = self.index;
    self.currentPage = 1;
    self.numPages = 1;
    self.limit = 10;
    self.perPage = self.limit;
    self.newLine = false;

    // self.pageChanged = function() {

    // };

    self.checkActivateStatus =function (status) {
        if(status) return "active";
        else return "not active";
    };

    self.pageChanged = function () {
        getListTransaction();
    };

    var getListTransaction = function () {
        TransactionService.getListTranscaction(self.currentPage-1,self.limit).then(
            function (response) {
                if (response.status == 'OK') {
                    self.transaction = response.data_transaction;
                    self.bigTotalItems = response.total_transaction;
                    self.numPages = response.total_page;
                    console.log(self.bigTotalItems);
                    console.log(self.numPages);
                }else{

                }
            }
        );
    };

    self.checkOrderStatus = function (data) {

        if(data != 'Challenge'){
            return 'isNotChallenge';
        }
        else{
            return 'isChallenge';
        }
    };


    self.approveTransaction = function (id) {
        // console.log(id)
        var request = {"order_code":id};
        TransactionService.approveTransaction(JSON.stringify(request)).then(
            function (response) {
                if (response.status == 'OK') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Information',
                                    message: 'Success to approve transaction',
                                    path: 'reload'
                                };
                            }
                        }
                    });

                }else{

                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Failed',
                                    message: response.message,
                                    path: ''
                                };
                            }
                        }
                    });

                }
            }
        );
    };

    self.goToDetail = function (id) {
        $location.path('admin/transaction-detail').search({id:id});
    };

    self.isAdmin = function () {
        return privilegeService.hasRoleIsAdmin();
    };

    self.isProdusen = function () {
        return privilegeService.hasRoleIsProdusen();
    };


    self.isReseller = function () {
        return privilegeService.hasRoleIsReseller();
    };

    self.pageChanged = function() {
        getListTransaction();
    };

    getListTransaction();
};