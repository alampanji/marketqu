'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('TransactionDetailCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'TransactionService', 'localStorageService',
        TransactionDetailCtrl
    ]);

function TransactionDetailCtrl($scope, CONF, $location, $modal, $window, TransactionService, localStorageService) {
    var self = $scope;
    var paramValue = $location.search().id;

    self.code = paramValue;

    self.close = close;

    function getDetail(){
        TransactionService.getDetailTransaction(paramValue).then(
            function (response) {
                if (response.status !== 'ERROR') {
                    self.data = response.data_transaction;
                }
            }
        );
    }

    self.goToFeedback = function (id) {
      $location.path('admin/transaction-feedback').search({product_id:id, id:paramValue});
    };

    self.goToViewFeedback = function (id) {
        $location.path('admin/view-feedback-reseller').search({feedback_id:id, id:paramValue});
    };



    function close() {
        $modalInstance.dismiss('close');
    }

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.disabledBtn = function (state) {
        if(state==true){
                return {"cursor":"not-allowed"}
        }
    };


    self.volume = [
        {'value': 'pcs', display: 'Pcs'},
        {'value': 'box', display: 'Box'},
        {'value': 'pack', display: 'Pack'}
    ];


    function getVolume(){
        self.itemVolume = self.volume[0];
    }

    self.status = [
        {'value': 1, display: 'Active'},
        {'value': 2, display: 'Not Active'}
    ];

    function getStatus(){
        self.itemStatus = self.status[0];
    }

    self.checkReseller = function () {

    };

    self.addCategory = function (itemCategory) {
        if(itemCategory.id != ''){
            var flag = false;
            angular.forEach(self.data.category, function (dataCategory) {
                if(itemCategory.id != dataCategory.id){
                    flag = true;
                }
            });
            if(flag){
                self.data.category.push(itemCategory);
            }

        }
    };

    self.deleteCategory = function (index) {
        self.data.category.splice(index,1);
    };

    function getListCategory(){
        ProductService.getListCategory().then(
            function(response) {
                if (response.status == 'OK') {
                    self.category = response.data;
                    self.categoryConstruct = [
                        {
                            'name':'Please Select',
                            'id'  : ''

                        }
                    ];
                    angular.forEach(self.category, function(data) {
                        self.categoryConstruct.push(data.category);
                        self.itemCategory = self.categoryConstruct[0];
                    });
                    // self.bigTotalItems = response.elements;
                    // self.numPages = response.pages;

                }
            });
    }

    getStatus();

    getVolume();

    getDetail();

};
