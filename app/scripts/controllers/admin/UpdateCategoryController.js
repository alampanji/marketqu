'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('UpdateCategoryCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'ProductService', 'LocationService',
        UpdateCategoryCtrl
    ]);

function UpdateCategoryCtrl($scope, CONF, $location, $modal, $window, ProductService, LocationService) {
    var self = $scope;
    var paramValue = $location.search().id;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    function getDetailCategory(){
        ProductService.getDetailCategory(paramValue).then(
            function (response) {
                if (response.status !== 'ERROR') {
                    self.user = response.data;
                    self.user.imgUrl = CONF.IMAGE_PATH + CONF.PATH_FILES_CATEGORY + self.user.image;
                }
            }
        );
    }

    self.viewOldImage = function () {
        $modal.open({
            templateUrl: 'views/modal/ModalViewImage.html',
            controller: 'ModalViewImageCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'View Old Image',
                        message: self.user.imgUrl,
                        path: ''
                    };
                }
            }
        });
    }

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.setProvince = function(countryId){
        var type = 'province';
        var id = countryId;
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.province = response.result;
                    self.itemProvince = self.province[0];
                    self.setCity(self.itemProvince);
                }
            });
    };

    self.setCity = function(provinceId){
        var type = 'city';
        var id = provinceId.province_id;
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.city = response.result;
                    self.itemCity = self.city[0];
                }
            });
    };

    self.selectCity = function (city) {
        self.itemCity = city
    };

    function checkPassword() {
        var boolean = false;

        if(self.password != self.confirmPassword){
            boolean = true;
        }

        return boolean;
    }

    var checking = function () {
        self.nameInvalid = false;
        self.addressInvalid = false;

        var bool = false;

        if(self.user.name == null || self.user.name == ''){
            self.nameInvalid = true;
            bool = true;
        }
        if(self.user.description == null || self.user.description == ''){
            self.addressInvalid = true;
            bool = true;
        }

        return bool;
    };


    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: 'Are you sure to cancel update category?',
                        path: 'admin/category'
                    };
                }
            }
        });

    };

    self.processSubmit = function () {

        var hasil = checking();
        if(hasil){
            invalidModal("Data is not valid, please check your input text");
        }
        else{

            if(self.image == undefined){
                self.image = '';
            }

            var formDt = new FormData();
            formDt.append('image',self.image);
            formDt.append('name', angular.toJson(self.user.name));
            formDt.append('id', angular.toJson(self.user.id));
            formDt.append('description', angular.toJson(self.user.description));
            formDt.append('id_parent', angular.toJson('0'));

            ProductService.updateCategory(formDt).then(
                function (response) {
                    if (response.status !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Informasi',
                                        message: 'Create category has been success',
                                        path: 'admin/category'
                                    };
                                }
                            }
                        });
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.message,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                }
            );
        }
    };

    getDetailCategory();

};
