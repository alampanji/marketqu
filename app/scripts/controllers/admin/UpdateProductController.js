'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('UpdateProductCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'ProductService', 'localStorageService', 'InstallService',
        UpdateProductCtrl
    ]);

function UpdateProductCtrl($scope, CONF, $location, $modal, $window, ProductService, localStorageService, InstallService) {
    var self = $scope;
    var paramValue = $location.search().id;

    self.close = close;

    self.viewOldImage = function (image) {
        $modal.open({
            templateUrl: 'views/modal/ModalViewImage.html',
            controller: 'ModalViewImageCtrl',
            size: 'md',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'View Old Image',
                        message: CONF.IMAGE_PATH + CONF.PATH_FILES_PRODUCT + image,
                        path: ''
                    };
                }
            }
        });
    }

    function getDetail(){
        ProductService.getDetailProduct(paramValue).then(
            function (response) {
                if (response.status !== 'ERROR') {
                    self.data = response.data;
                    getListCategory();
                    generateType();
                }
            }
        );
    }

    function close() {
        $modalInstance.dismiss('close');
    }

    var checking = function () {
        self.nameInvalid = false;
        self.priceLowInvalid = false;
        self.priceSellInvalid = false;
        self.stockInvalid = false;
        self.imageDefaultInvalid = false;
        self.descriptionInvalid = false;
        self.weightInvalid = false;
        self.categoryInvalid = false;

        var bool = false;

        if(self.data.name == null || self.data.name == ''){
            self.nameInvalid = true;
            bool = true;
        }
        if (self.data.price_low == null || self.data.price_low == '') {
            self.priceLowInvalid = true;
            bool = true;
        }
        if (self.data.price_sell == null || self.data.price_sell == '') {
            self.priceSellInvalid = true;
            bool = true;
        }
        if(self.data.current_stock == null || self.data.current_stock == ''){
            self.stockInvalid = true;
            bool = true;
        }
        if(self.data.description == null || self.data.description == ''){
            self.descriptionInvalid = true;
            bool = true;
        }

        if(self.data.weight == null || self.data.weight == ''){
            self.weightInvalid = true;
            bool = true;
        }
        
        if(self.data.category.length < 1){
            self.categoryInvalid = false;
            bool = true;
        }

        return bool;
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: 'Are you sure to cancel update product process?',
                        path: 'admin/product'
                    };
                }
            }
        });

    };

    self.processSubmit = function () {
        var result = checking();

        if(result){
            invalidModal("Data is not valid, please check your input text");
        }
        else {

            var featured = '';
            var newCat = [];

            angular.forEach(self.data.thumbnail, function (thumbnail) {
                if(thumbnail.status=='1'){
                    console.log(thumbnail);
                    featured = thumbnail.id;
                }
            });

            angular.forEach(self.data.category, function (categories) {
                newCat.push(categories.id)
            });

                var formDt = new FormData();
                formDt.append('image1', self.images1);
                formDt.append('image2', self.images2);
                formDt.append('image3', self.images3);
                formDt.append('image4', self.images4);
                formDt.append('image5', self.images5);
                formDt.append('id', angular.toJson(self.data.id));
                formDt.append('id_user', angular.toJson(localStorageService.cookie.get(CONF.id)));
                formDt.append('name', angular.toJson(self.data.name));
                formDt.append('price_low', angular.toJson(self.data.price_low));
                formDt.append('price_sell', angular.toJson(self.data.price_sell));
                formDt.append('discount', angular.toJson(self.data.discount));
                formDt.append('type', angular.toJson(self.data.type));
                formDt.append('stock', angular.toJson(self.data.current_stock));
                formDt.append('status', angular.toJson(self.itemStatus.value));
                formDt.append('category', angular.toJson(newCat.join(',')));
                formDt.append('featured', angular.toJson(featured));
                formDt.append('description', angular.toJson(self.data.description));
                formDt.append('volume', angular.toJson(self.itemVolume.value));
                formDt.append('weight', angular.toJson(self.data.weight));

                ProductService.updateProduct(formDt).then(
                    function (response) {
                        if (response.status !== 'ERROR') {
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Information',
                                            message: 'Success to update product',
                                            path: 'admin/product'
                                        };
                                    }
                                }
                            });
                        } else {
                            self.isDisabled = false;
                            $modal.open({
                                templateUrl: 'views/modal/Modal.html',
                                controller: 'ModalCtrl',
                                size: 'sm',
                                resolve: {
                                    modalParam: function () {
                                        return {
                                            title: 'Failed',
                                            message: response.message,
                                            path: ''
                                        };
                                    }
                                }
                            });
                        }
                    }
                );
            }
    };


    self.volume = [
        {'value': 'pcs', display: 'Pcs'},
        {'value': 'box', display: 'Box'},
        {'value': 'pack', display: 'Pack'}
    ];


    function getVolume(){
        self.itemVolume = self.volume[0];
    }

    self.status = [
        {'value': 1, display: 'Active'},
        {'value': 2, display: 'Not Active'}
    ];

    function getStatus(){
        self.itemStatus = self.status[0];
    }
    
    self.checkReseller = function () {
        
    };

    self.addCategory = function (itemCategory, index) {
        if(itemCategory.id != ''){
            // var flag = false;
            // angular.forEach(self.data.category, function (dataCategory) {
            //     if(itemCategory.id != dataCategory.id){
            //         flag = true;
            //     }
            // });
            // if(flag){
                self.data.category.push(itemCategory);
;
            // }

        }
    };

    self.deleteCategory = function (index) {
        self.data.category.splice(index,1);
    };

    function getListCategory(){
        ProductService.getListCategory().then(
            function(response) {
                if (response.status == 'OK') {
                    self.category = response.data;
                    self.categoryConstruct = [
                        {
                            'name':'Please Select',
                            'id'  : ''

                        }
                    ];
                    angular.forEach(self.category, function(data) {
                        self.categoryConstruct.push(data.category);
                        self.itemCategory = self.categoryConstruct[0];
                    });
                    // self.bigTotalItems = response.elements;
                    // self.numPages = response.pages;

                }
            });
    }

    function generateType(){
        self.type = [{"name":"Baru", "value":"Baru"}, {"name":"Bekas", "value":"Bekas"}];
        angular.forEach(self.type, function (type) {
            if(type.value == self.data.type){
                // self.type = type;
                self.itemType = type;
            }
        })
        // self.itemType = self.type[0];
    };

    getStatus();

    getVolume();
    
    getDetail();

};
