'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('UpdateResellerCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'UserService', 'LocationService',
        UpdateResellerCtrl
    ]);

function UpdateResellerCtrl($scope, CONF, $location, $modal, $window, UserService, LocationService) {
    var self = $scope;
    var paramValue = $location.search().id_reseller;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    function getDetail(){
        UserService.getDetailUserProfile(paramValue).then(
            function (response) {
                if (response.status !== 'ERROR') {
                    self.data = response.data_profile;
                    self.location = response.location;
                    self.setProvince(self.data.country);

                    if(self.data.photo != null){
                        self.data.img = CONF.IMAGE_PATH+CONF.PATH_FILES_USER+self.data.photo
                    }
                }
            }
        );
    }

    self.processUpdateProfile = function () {

        if(self.photo == undefined){
            self.photo = '';
        }

        if(self.photo == undefined){
            self.photo = '';
        }

        var formDt = new FormData();
        formDt.append('photo', self.photo);
        formDt.append('name', angular.toJson(self.data.name));
        formDt.append('uid', angular.toJson(self.data.uid));
        formDt.append('contact', angular.toJson(self.data.contact));
        formDt.append('country', angular.toJson(self.data.country));
        formDt.append('province', angular.toJson(self.itemProvince.province_id));
        formDt.append('city', angular.toJson(self.itemCity.city_id));
        formDt.append('address', angular.toJson(self.data.address));
        formDt.append('postcode', angular.toJson(self.data.postcode));
        formDt.append('institution', angular.toJson(self.data.institution));
        formDt.append('department', angular.toJson(self.data.department));

        UserService.updateProfile(formDt).then(
            function (response) {
                if (response.status == 'OK') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Information',
                                    message: 'Success to update profile',
                                    path: 'admin/reseller'
                                };
                            }
                        }
                    });
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Failed',
                                    message: response.message,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            }
        );


    };


    function getListCountry(){
        var type = 'country';
        var id = '';
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.country = response.result;
                    self.itemCountry = self.country[0];
                    self.setProvince(self.itemCountry.country_id);
                }
            });
    }

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };

    self.setProvince = function(countryId){
        var type = 'province';
        var id = countryId;
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.province = response.result;
                    angular.forEach(self.province, function (provinceSet) {
                        if(provinceSet.province_id == self.data.province){
                            self.itemProvince = provinceSet;
                        }
                    });
                    self.setCity(self.itemProvince);
                }
            });
    };

    self.setCity = function(provinceId){
        var type = 'city';
        var id = provinceId.province_id;
        LocationService.getListLocation(type, id).then(
            function(response) {
                if (response.message !== 'ERROR') {
                    self.city = response.result;
                    angular.forEach(self.city, function (citySet) {
                        if(citySet.city_id == self.location.city.city_id){
                            self.itemCity = citySet;
                        }

                        // self.itemCity = self.city[0];
                    })
                }
            });
    };

    self.selectCity = function (city) {
        self.itemCity = city
    };

    self.showModalBack = function () {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/ModalConfirmation.html',
            controller: 'modalConfirmationCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Warning',
                        message: 'Are you sure to cancel create reseller process?',
                        path: 'admin/reseller'
                    };
                }
            }
        });

    };

    self.selectCity = function (city) {
        self.itemCity = city
    };

    getListCountry();

    getDetail();

};
