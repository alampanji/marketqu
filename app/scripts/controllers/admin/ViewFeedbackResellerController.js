'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */

angular.module('marketplaceApp')
    .controller('ViewFeedbackResellerCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'ItemService', 'LocationService',
        ViewFeedbackResellerCtrl
    ]);

function ViewFeedbackResellerCtrl($scope, CONF, $location, $modal, $window, ItemService, LocationService) {
    var self = $scope;
    var paramValue = $location.search().feedback_id;
    var paramId = $location.search().id;


    self.close = close;

    self.awal = {
        opened: false
    };

    self.akhir = {
        opened: false
    };

    self.format = 'dd-MMM-yyyy';

    self.openAwal = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (self.awal.opened)
            self.awal.opened = false;
        else
            self.awal.opened = true;
    };


    function close() {
        $modalInstance.dismiss('close');
    }

    self.setItem = function (data) {
        self.stockItem = data.stock;
    };

    var invalidModal = function (message) {
        self.isDisabled = false;
        $modal.open({
            templateUrl: 'views/modal/Modal.html',
            controller: 'ModalCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        title: 'Peringatan',
                        message: message,
                        path: ''
                    };
                }
            }
        });

    };
    
    function getFeedbackDetails() {
        ItemService.getFeedbackResponse(paramValue).then(
            function (response) {
                self.feedback = response.data_feedback;
                self.response = response.data_response;

                angular.forEach(self.response, function (response) {
                    // response

                    if(response.photo != null){
                        response.img = CONF.IMAGE_PATH+CONF.PATH_FILES_USER+response.photo
                    }
                })
            }
        )
    }

    getFeedbackDetails();


    function validation() {
        self.responseInvalid = false;
        var boolean = false;

        if(self.responseData == undefined || self.responseData == ''){
            self.responseInvalid = true;
            boolean = true
        }


        return boolean;
    }

    self.processSubmit = function () {

        if(validation() == true){
            invalidModal("Your input is not valid, please check first!")
        }
        //
        else{

            var request = {
                "id_feedback":paramValue,
                "response":self.responseData
            };

            ItemService.giveResponse(JSON.stringify(request)).then(
                function (response) {
                    if (response.status !== 'ERROR') {
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Information',
                                        message: 'Give feedback has been success',
                                        path: 'reload'
                                        // paramSearch : paramId
                                    };
                                }
                            }
                        });
                    } else {
                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Gagal',
                                        message: response.message,
                                        path: ''
                                    };
                                }
                            }
                        });
                    }
                }
            );

        }
    };

};
