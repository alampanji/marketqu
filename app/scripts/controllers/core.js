'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nostraApp
 */
angular.module('marketplaceApp')
    .controller('CoreCtrl', [
        '$scope',
        '$modal',
        'authService',
        '$location',
        'CONF',
        'localStorageService',
        '$window',
        CoreCtrl
    ]);

function CoreCtrl($scope, $modal, authService, $location, CONF, localStorageService, $window) {
    var self = $scope;
    self.isLogin = isLogin();

    self.fullName = authService.getFullName();
    self.doLogout = doLogout;

    function doLogout() {
        authService.logout().then(
            function (response) {
                if (response.status == 'OK') {

                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        backdrop: 'static',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Information',
                                    message: response.message,
                                    path: '/article/home'
                                };
                            }
                        }
                    });
                }
            }
        );

        $location.path('/article/home');
    }

    self.showModalLogin = function() {
        $modal.open({
            templateUrl: '/views/auth/Login.html',
            controller: 'LoginCtrl',
            size: 'sm',
            resolve: {
                modalParam: function () {
                    return {
                        message: ""
                    };
                }
            }
        });
    };

    self.goChangePassword = function () {
        
        $location.path('/dashboard/change-password');
    
    };
    
    self.goToProductAdmin = function () {
        $location.path('/admin/product');
    };

    self.goToProduct = function () {
        $window.location.href = '/#/product';
    };

    self.goToTransaction = function () {
        $location.path('/admin/transaction');
    };

    self.goToHome= function () {
        $location.path('/user/home');
    };

    self.showModalMap = function () {
            $location.path('/article/map');
    };

    self.goToDashboard = function () {

        $window.location.href = '/#/admin/home';

        // $location.path('/admin/home');

    };

    function getDataSite() {
        if(localStorageService.get(CONF.SITE_INFO) != undefined){
            self.dataSite = localStorageService.get(CONF.SITE_INFO);

            if(self.dataSite.site_logo != null){
                self.dataSite.site_logo = CONF.IMAGE_PATH + CONF.PATH_FILES_LOGO + self.dataSite.site_logo;
            }
        }
    }


    function isLogin() {
        var login = true;
        if(!localStorageService.cookie.get(CONF.accessToken)){
            login = false
        }
        return login;
    }

    getDataSite();
};
