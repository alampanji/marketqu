'use strict';
/**
 * @ngdoc function
 * @name nostraApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the marketplaceApp
 */
angular.module('marketplaceApp')
    .controller('SideCtrl', [
        '$scope',
        '$position',
        'CONF',
        'localStorageService',
        '$location',
        'authService',
        '$sessionStorage',
        'privilegeService',
        '$window',
        function ($scope, $position, CONF, localStorageService, $location, authService, $sessionStorage,privilegeService, $window) {

            var self = $scope;

            // if($sessionStorage.domain==CONF.USER_INTERNAL){
                self.userHeader = "ADMIN MENU";
            // }

            self.goProfile = function () {

                $location.path('/admin/profile');

            };
            
            self.goToHome = function () {
                // $location.path('/user/home');

                $window.location.href = '/#/user/home';

            };

            function getDetailBalance() {
                self.balance = localStorageService.cookie.get(CONF.balance);
            }
            

            getDetailBalance();

            self.goToDashboard = function () {
                $location.path('/admin/home');
            };

            self.goToProduct = function () {
                $location.path('/admin/product');
            };

            self.goToProfile = function () {
                $location.path('/admin/profile');
            };

            self.goToProdusen = function () {
                $location.path('admin/produsen');
            };

            self.goToFavorite = function () {
                $location.path('admin/favorite');
            };

            self.goToReseller = function () {
                $location.path('admin/reseller');
            };

            self.goToGroup = function () {
                $location.path('admin/group');
            };

            self.goToCategory = function () {
                $location.path('/admin/category');
            };

            self.goToReport = function () {
                $location.path('/admin/sales-report');
            };

            self.goToMarketing = function () {
                $location.path('admin/marketing');
            };

            self.doLogout = function () {
                authService.logout().then(
                    function (response) {
                        if (response.status == 'OK') {
                            $location.path('user/home');
                        }
                    }
                );
            };

            self.goToFeedbackProdusen = function () {
                $location.path('admin/produsen-feedback');
            };

            self.hasRoleProdusen = function () {
                return privilegeService.hasRoleIsProdusen();

            };

            self.hasRoleReseller = function () {
                return privilegeService.hasRoleIsReseller();

            };

            self.hasRoleAdmin = function () {
               return privilegeService.hasRoleIsAdmin();
            };

            self.hasRoleMemberGroup = function () {
                return privilegeService.hasRoleIsMemberGroup();
            };



            privilegeService.generateMenu();
            self.menu = localStorageService.cookie.set(CONF.privileges);
            $scope.selectedMenu = 'dashboard';
            $scope.collapseVar = 0;
            $scope.multiCollapseVar = 0;
            $scope.check = function (x) {
                if (x == $scope.collapseVar)
                    $scope.collapseVar = 0;
                else
                    $scope.collapseVar = x;
            };
            $scope.multiCheck = function (y) {
                if (y == $scope.multiCollapseVar)
                    $scope.multiCollapseVar = 0;
                else
                    $scope.multiCollapseVar = y;
            };

        }]);
