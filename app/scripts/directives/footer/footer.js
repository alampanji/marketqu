     'use strict';

     /**
      * @ngdoc directive
      * @name izzyposWebApp.directive:adminPosHeader
      * @description
      * # adminPosHeader
      */

     angular.module('marketplaceApp')
         .controller('FooterCtrl', [
             '$scope', 'CONF', '$location', 'localStorageService',
             FooterCtrl
         ]);

     function FooterCtrl($scope, CONF, $location, localStorageService) {
         var self = $scope;

         function getSiteInfo() {
             self.siteInfo = localStorageService.get(CONF.SITE_INFO);
         }

         getSiteInfo();

         console.log(self.siteInfo)

     }

     angular.module('marketplaceApp')
       .directive('footer',['$location',function() {
           return {
           templateUrl:'scripts/directives/footer/footer.html',
           restrict: 'E',
           replace: true,
           controller: 'FooterCtrl'

         }
       }]);
