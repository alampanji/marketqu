'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */


angular.module('marketplaceApp')
	.controller('HeaderExternalCtrl', [
		'$scope', 'CONF', '$location', 'localStorageService',
		HeaderExternalCtrl
	]);

function HeaderExternalCtrl($scope, CONF, $location, localStorageService) {
	var self = $scope;

	self.goToHome= function () {
		$location.path('/user/home');
	};

	function getDataSite() {
		if(localStorageService.get(CONF.SITE_INFO) != undefined){
			self.dataSite = localStorageService.get(CONF.SITE_INFO);

			if(self.dataSite.site_logo != null){
				self.dataSite.site_logo = CONF.IMAGE_PATH + CONF.PATH_FILES_LOGO + self.dataSite.site_logo;
			}
		}
	}

	getDataSite();
}

angular.module('marketplaceApp')
	.directive('headerExternal',function(){
		return {
        templateUrl:'scripts/directives/header-external/header-external.html',
        restrict: 'E',
        replace: true,
		controller: 'HeaderExternalCtrl'
    	}
	});


