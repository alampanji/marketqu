'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

app.directive('ngEnter', function () {
	return function (scope, element, attrs) {
		element.bind("keydown keypress", function (event) {
			if(event.which === 13) {
				scope.$apply(function (){
					scope.$eval(attrs.ngEnter);
				});

				event.preventDefault();
			}
		});
	};
});

angular.module('marketplaceApp')
	.controller('HeaderHamburgerCtrl', [
		'$scope', 'CONF', '$location', 'localStorageService',
		HeaderHamburgerCtrl
	]);

function HeaderHamburgerCtrl($scope, CONF, $location, localStorageService) {
	var self = $scope;

	self.goToHome= function () {
		$location.path('/user/home');
	};

	self.isLogin = isLogin();

	self.navState = {
		"display":"none"
	};

	function getDataSite() {
		if(localStorageService.get(CONF.SITE_INFO) != undefined){
			self.dataSite = localStorageService.get(CONF.SITE_INFO);

			if(self.dataSite.site_logo != null){
				self.dataSite.site_logo = CONF.IMAGE_PATH + CONF.PATH_FILES_LOGO + self.dataSite.site_logo;
			}
		}
	}


	self.doSearchProduct = function (searchName) {
		$location.path('/product').search({product_name:searchName});
	};

	self.goToHome= function () {
		$location.path('/user/home');
	};

	self.openNav = function () {
		self.navState = {
			"display":"block"
		}
	};

	self.closeNav = function () {
		self.navState = {
			"display":"none"
		};
	};

	self.toProfile = function () {
		$location.path('/admin/profile');
	};

	function isLogin() {
		var login = true;
		if(!localStorageService.cookie.get(CONF.accessToken)){
			login = false
		}
		return login;
	}

	getDataSite();
}

angular.module('marketplaceApp')
	.directive('headerHamburger',function(){
		return {
        templateUrl:'scripts/directives/header-hamburger/header-hamburger.html',
        restrict: 'E',
        replace: true,
		controller: 'HeaderHamburgerCtrl'
    	}
	});


