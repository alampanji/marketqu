/**
 * Created by alampanji on 3/12/17.
 */

'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

angular.module('marketplaceApp')
    .controller('HeaderUserCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'localStorageService', 'messageService',
        HeaderUserCtrl
    ]);

function HeaderUserCtrl($scope, CONF, $location, $modal, $window, localStorageService, messageService) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.chartIsActive = false;
    self.chartNotNull = false;

    self.showCart = function () {
        self.chartIsActive = true;
        if(localStorageService.cookie.get(CONF.CART) != null){
            self.chartNotNull = true;
        }
    };

    

    self.hideCart = function () {
        self.chartIsActive = false;
    };

    function constructCart() {

        if(localStorageService.cookie.get(CONF.CART) != null || localStorageService.cookie.get(CONF.CART) != undefined){

            self.cartProductList = localStorageService.cookie.get(CONF.CART);
            self.cartProductItem = 0;
            self.totalPrice = 0;
            for(var i=0; i<self.cartProductList.length; i++){
                self.totalPrice += getPrice(self.cartProductList[i].quantity,self.cartProductList[i].price);
                self.cartProductItem += self.cartProductList[i].quantity;
            }
        }

    }


    function getPrice(quantity, price) {
        return quantity * price;
    }

    constructCart();



    self.goToDashboard  = function () {

        // $location.path('/admin/home');
        $window.location.href = '/#/admin/home';

    };


};

angular.module('marketplaceApp')
    .directive('headerUser',function(){
        return {
            templateUrl:'scripts/directives/user/view/header-user.html',
            restrict: 'E',
            replace: true,
            controller:'HeaderUserCtrl'
        }
    })