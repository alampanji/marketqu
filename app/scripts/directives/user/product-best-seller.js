/**
 * Created by alampanji on 3/22/17.
 */
'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

angular.module('marketplaceApp')
    .controller('ProductBestSellerCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'ProductService',
        ProductBestSellerCtrl
    ]);

function ProductBestSellerCtrl($scope, CONF, $location, $modal, $window, ProductService) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.limit = 6;


    function getListBestSeller() {
        ProductService.getListBestProduct(self.limit).then(
            function(response) {
                if (response.status == 'OK') {
                    self.dataList = response.data;

                    angular.forEach(self.dataList, function (data) {
                        if(data.thumbnail != null){

                            data.img =  CONF.IMAGE_PATH + CONF.PATH_FILES_PRODUCT + data.thumbnail.photo;

                        }
                    })
                }
            }
        );
    }

    getListBestSeller();

};

angular.module('marketplaceApp')
    .directive('productBestSeller',function(){
        return {
            templateUrl:'scripts/directives/user/view/product-best-seller.html',
            restrict: 'E',
            replace: true,
            controller: 'ProductBestSellerCtrl'
        }
    })