/**
 * Created by alampanji on 3/12/17.
 */

'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

angular.module('marketplaceApp')
    .controller('ProductCatalogCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'localStorageService', 'messageService',
        ProductCatalogCtrl
    ]);

function ProductCatalogCtrl($scope, CONF, $location, $modal, $window, localStorageService, messageService) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    var productToCart = [];

    self.products = [
        {
            price:400000,
            img:'/img/pisang.jpg',
            name:'Produk A',
            id:'1',
            produsen:'Produsen C'
        },
        {
            price:500000,
            img:'/img/pisang.jpg',
            name:'Produk B',
            id:'112',
            produsen:'Produsen X'
        },
        {
            price:10000,
            img:'/img/pisang.jpg',
            name:'Produk C',
            id:'11',
            produsen:'Produsen X'
        },
        {
            price:10000,
            img:'/img/pisang.jpg',
            name:'Produk D',
            id:'2',
            produsen:'Produsen Y'
        },
        {
            price:5000,
            img:'/img/pisang.jpg',
            name:'Produk E',
            id:'9',
            produsen:'Produsen X'
        },
        {
            price:9000,
            img:'/img/pisang.jpg',
            name:'Produk F',
            id:'10',
            produsen:'Produsen Z'
        },
        {
            price:6500,
            img:'/img/pisang.jpg',
            name:'Produk X',
            id:'19',
            produsen:'Produsen A'
        }
    ];

    self.produsenLabel = function (index) {
        self.products[index].status = true;
    }

    self.produsenLabelHide = function (index) {
        self.products[index].status = false;
    }

    self.addToCart = function (product) {

        if(localStorageService.cookie.get(CONF.CART) == undefined){
            product.quantity = 1;
            productToCart.push(product);
            localStorageService.cookie.set(CONF.CART, productToCart);
            messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_SUCCESS,'Berhasil menambahkan ' + product.name + ' ke keranjang belanja');
        }

        else{
            var next = true;
            for(var i=0; i<productToCart.length; i++){
                if(productToCart[i].id == product.id){
                    productToCart[i].quantity = productToCart[i].quantity+1;
                    next = false;
                }
            }
            if(next){
                product.quantity = 1;
                productToCart.push(product);
            }

            localStorageService.cookie.set(CONF.CART, productToCart);
            messageService.toasterMessage(CONF.TOASTER_BOTTOM_FULL,CONF.TOASTER_SUCCESS,'Berhasil menambahkan ' + product.name + ' ke keranjang belanja');
        }
    }


};

angular.module('marketplaceApp')
    .directive('productCatalog',function(){
        return {
            templateUrl:'scripts/directives/user/view/product-catalog.html',
            restrict: 'E',
            replace: true,
            controller: 'ProductCatalogCtrl'
        }
    })