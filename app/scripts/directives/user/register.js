/**
 * Created by alampanji on 3/12/17.
 */

'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

angular.module('marketplaceApp')
    .controller('RegisterCtrl', [
        '$scope', 'CONF', '$location', '$modal', '$window', 'UserService',
        RegisterCtrl
    ]);

function RegisterCtrl($scope, CONF, $location, $modal, $window, UserService) {
    var self = $scope;

    self.close = close;

    function close() {
        $modalInstance.dismiss('close');
    }

    self.register = function () {
        var request = {};
        request.username = self.username;
        request.email = self.email;
        request.role = self.itemRole.value;

        UserService.registerUser(JSON.stringify(request)).then(
            function (response) {
                if (response.status !== 'ERROR') {
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Informasi',
                                    message: 'Your password was sent to your email',
                                    path: ''
                                };
                            }
                        }
                    });
                } else {
                    self.isDisabled = false;
                    $modal.open({
                        templateUrl: 'views/modal/Modal.html',
                        controller: 'ModalCtrl',
                        size: 'sm',
                        resolve: {
                            modalParam: function () {
                                return {
                                    title: 'Gagal',
                                    message: response.message,
                                    path: ''
                                };
                            }
                        }
                    });
                }
            }
        );
    }

    function generateRole(){
        self.role = [{"name":"Produsen", "value":"2"}, {"name":"Reseller", "value":"3"}];
        self.itemRole = self.role[0];
    }

    generateRole();

};

angular.module('marketplaceApp')
    .directive('register',function(){
        return {
            templateUrl:'scripts/directives/user/view/register.html',
            restrict: 'E',
            replace: true,
            controller: 'RegisterCtrl'
        }
    })