'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('nostraApp')
    .service('AccessLevelService', [
        '$http',
        '$q',
        'CONF',
        'localStorageService',
        function ($http, $q, CONF) {
            return {
                getListAccessLevel: function () {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_ACCESS_LEVEL,
                        timeout: CONF.TIMEOUT,

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }
            };
        }]
    );
