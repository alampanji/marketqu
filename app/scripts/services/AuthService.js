'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('marketplaceApp')
    .service('authService', [
        '$http',
        '$q',
        'CONF',
        'localStorageService',
        '$sessionStorage',
        'privilegeService',
        function ($http, $q, CONF, localStorageService, $sessionStorage, privilegeService) {

            var authenticate = function () {
                var accessToken = $sessionStorage.accessToken;
                return (accessToken != null);
            };
            var fullName = function () {
                var fullName = $sessionStorage.fullName;
                return fullName;
            };
            var email = function () {
                var email = $sessionStorage.email;
                return email;
            };

            return {
                getFullName:fullName,
                isAuthenticated: authenticate,
                getEmail : email,

                logout: function () {

                    var deferred = $q.defer();
                    // var request = {
                    //     accessToken : localStorageService.cookie.get(CONF.accessToken)
                    // };

                    localStorageService.cookie.remove(CONF.name);
                    localStorageService.cookie.remove(CONF.accessToken);
                    localStorageService.cookie.remove(CONF.uid);
                    localStorageService.cookie.remove(CONF.id);
                    localStorageService.cookie.remove(CONF.privileges);
                    localStorageService.cookie.remove(CONF.dataUser);
                    localStorageService.cookie.remove(CONF.dataProfile);
                    localStorageService.cookie.remove(CONF.name);
                    localStorageService.remove(CONF.CART);
                    localStorageService.cookie.remove(CONF.ORDER_CODE);
                    localStorageService.cookie.remove(CONF.DELIVERY_COST);
                    localStorageService.cookie.remove(CONF.DELIVERY_COURIER);
                    localStorageService.cookie.remove(CONF.ORDER_CODE);
                    localStorageService.cookie.remove(CONF.balance);

                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_LOGOUT,
                        timeout: CONF.TIMEOUT,
                        data:''
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                login: function (request) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_LOGIN,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    }).success(function (response) {
                        if (response.response.status == 200) {
                            localStorageService.cookie.set(CONF.name, response.data_user.name);
                            localStorageService.cookie.set(CONF.accessToken, response.response.token);
                            localStorageService.cookie.set(CONF.uid, response.data_user.uid);
                            localStorageService.cookie.set(CONF.id, response.data_user.id);
                            localStorageService.cookie.set(CONF.privileges, response.user_capabillities);
                            localStorageService.cookie.set(CONF.dataUser, response.data_user);
                            localStorageService.cookie.set(CONF.dataProfile, response.data_profile);
                            localStorageService.cookie.set(CONF.balance, response.balance);
                        }

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }
            };
        }]);
