'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('nostraApp')
    .service('BiayaResumeService', [
        '$http',
        '$q',
        'CONF',
        '$sessionStorage',
        function($http, $q, CONF, $sessionStorage) {

            return {
                getlistBiayaResume: function(page, limit) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_BIAYA_RESUME,
                        timeout: CONF.TIMEOUT,
                        params: {
                            page: page,
                            limit: limit
                        }
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                createBiayaResume: function(request) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CREATE_BIAYA_RESUME,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params: {
                            access_token: access_token
                        }
                    }).success(function(response) {
                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                deleteBiayaResume: function(request) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken
                    $http({
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        url: CONF.MASTER_PATH + CONF.URL_DELETE_BIAYA_RESUME,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params: {
                            access_token: access_token
                        }
                    }).success(function(response) {
                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }
            };
        }
    ]);