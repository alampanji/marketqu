'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('nostraApp')
    .service('BranchService', [
        '$http',
        '$q',
        'CONF',
        '$sessionStorage',
        function ($http, $q, CONF, $sessionStorage) {

            return {
                getlistBranch: function (page,limit,name,code) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_LIST_BRANCH_INTERNAL,
                        timeout: CONF.TIMEOUT,
                        params: {
                            page : page,
                            limit : limit,
                            name : name,
                            code: code,
                            access_token:access_token
                        }
                    }).success(function (response) {



                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }
            };
        }]);
