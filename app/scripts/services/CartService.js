'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('marketplaceApp')
    .factory('CartService', [
        '$http',
        '$q',
        '$sessionStorage',
        'localStorageService',
        'CONF',
        function($http, $q, $sessionStorage, localStorageService, CONF) {
            var courier;
            var cost;

            return {
                getCourier: function() {
                    return courier;
                },

                setCourier : function (newCourier) {
                    courier = newCourier;
                    console.log(courier);
                },

                setCost : function (newCost) {
                    cost = newCost;
                },

                getCost : function () {
                  return cost;
                }
            };
        }
    ]);