'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('nostraApp')
    .service('ContactUsService', [
        '$http',
        '$q',
        'CONF',
        function ($http, $q, CONF) {

            return {
                sendContactUs: function (request) {
                    var deferred = $q.defer();

                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CONTACT_US,
                        timeout: CONF.TIMEOUT,
                        data: request
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },
            };
        }]);