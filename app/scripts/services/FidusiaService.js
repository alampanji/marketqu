'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('nostraApp')
    .service('FidusiaService', [
        '$http',
        '$q',
        'CONF',
        '$sessionStorage',
        function ($http, $q, CONF,$sessionStorage) {
            return {
                orderResumeList: function (page,limit,appId,npwp,companyName) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_ORDER_RESUME_LIST,
                        timeout: CONF.TIMEOUT,
                        params: {
                            access_token: accessToken,
                            page : page,
                            limit : limit,
                            appId : appId,
                            companyName: companyName,
                            npwp: npwp
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                orderResumeDetail: function (id) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET', 
                        url: CONF.MASTER_PATH + CONF.URL_GET_ORDER_RESUME_DETAIL,
                        timeout: CONF.TIMEOUT,
                        params: {
                            access_token: accessToken,
                            id : id
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                processOrderResume: function (contentData) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_ORDER_RESUME_PROCESS,
                        timeout: CONF.TIMEOUT,
                        data: contentData,
                        params : {
                            access_token :access_token
                        }

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                uploadVoucher: function (fd) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_UPLOAD_VOUCHER,
                        timeout: CONF.TIMEOUT,
                        data: fd,
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined},
                        params: {
                            access_token : accessToken
                        }

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getVoucherList: function (page,limit,voucher) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_VOUCHER,
                        timeout: CONF.TIMEOUT,
                        params: {
                            access_token: accessToken,
                            page : page,
                            limit : limit,
                            voucher : voucher
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getUploadResumeAhuList: function (page,limit,voucher) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_UPLOAD_RESUME_AHU,
                        timeout: CONF.TIMEOUT,
                        params: {
                            access_token: accessToken,
                            page : page,
                            limit : limit,
                            voucher : voucher
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getUploadResumeAhuDetail: function (id) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_DETAIL_UPLOAD_RESUME_AHU,
                        timeout: CONF.TIMEOUT,
                        params: {
                            access_token: accessToken,
                            id : id
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },
                
                processUploadResumeAhu: function (fd) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_UPLOAD_RESUME_AHU_PROCESS,
                        timeout: CONF.TIMEOUT,
                        data: fd,
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined},
                        params: {
                            access_token : accessToken
                        }

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getRequestOtherDocumentList: function (page,limit,appId,npwp,companyName) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_REQUEST_OTHER_DOCUMENT,
                        timeout: CONF.TIMEOUT,
                        params: {
                            access_token: accessToken,
                            page : page,
                            limit : limit,
                            appid : appId,
                            companyName: companyName,
                            npwp: npwp
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getRequestOtherDocumentDetail: function (id) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_DETAIL_REQUEST_OTHER_DOCUMENT,
                        timeout: CONF.TIMEOUT,
                        params: {
                            access_token: accessToken,
                            id : id
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },
               
                inquiryHistoryResumeList: function (page,limit,appId,npwp,companyName) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_INQUIRY_HISTORY_RESUME_LIST,
                        timeout: CONF.TIMEOUT,
                        params: {
                            access_token: accessToken,
                            page : page,
                            limit : limit,
                            appid : appId,
                            companyName: companyName,
                            npwp: npwp
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                requestOtherDocumentProcess: function (contentData) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_REQUEST_OTHER_DOCUMENT_PROCESS,
                        timeout: CONF.TIMEOUT,
                        data: contentData,
                        params : {
                            access_token :access_token
                        }

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },


                completedResume: function (contentData) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_REQUEST_COMPLETE_PROCESS,
                        timeout: CONF.TIMEOUT,
                        data: contentData,
                        params : {
                            access_token :access_token
                        }

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getApprovalResumeList: function (page,limit,appId,npwp,companyName) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_APPROVAL_RESUME,
                        timeout: CONF.TIMEOUT,
                        params: {
                            access_token: accessToken,
                            page : page,
                            limit : limit,
                            appId : appId,
                            companyName: companyName,
                            npwp: npwp
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                approvalResumeNotary: function (contentData) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_APPROVAL_RESUME_NOTARY,
                        timeout: CONF.TIMEOUT,
                        data: contentData,
                        params : {
                            access_token :access_token
                        }

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },


                rejectResumeNotary: function (contentData) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_REJECT_RESUME_NOTARY,
                        timeout: CONF.TIMEOUT,
                        data: contentData,
                        params : {
                            access_token :access_token
                        }

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },




            };
        }]
    );
