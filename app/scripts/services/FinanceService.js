'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('nostraApp')
    .service('FinanceService', [
        '$http',
        '$q',
        'CONF',
        '$sessionStorage',
        function ($http, $q, CONF, $sessionStorage) {

            return {
                getlistOfInvoice: function (request) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_INPUT_INVOICE_LIST,
                        timeout: CONF.TIMEOUT,
                        params: {
                            startDate : request.startDate,
                            endDate : request.endDate,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 
                getlistOfTextFile: function (batchNo,notaryId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_TEXT_FILE_LIST,
                        timeout: CONF.TIMEOUT,
                        params: {
                            notaryId : notaryId,
                            batchNo : batchNo,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 
                getlistOfBatchNumber: function (notaryId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_BATCH_NUMBER,
                        timeout: CONF.TIMEOUT,
                        params: {
                            notaryId : notaryId,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 
                getlistOfApproveBatchNumber: function (notaryId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_APPROVE_BATCH_NUMBER,
                        timeout: CONF.TIMEOUT,
                        params: {
                            notaryId : notaryId,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 
                getlistOfBatchNumberCreateTextFile: function (notaryId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_CREATE_TEXT_FILE_BATCH_NUMBER,
                        timeout: CONF.TIMEOUT,
                        params: {
                            notaryId : notaryId,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 

                getFinanceDetail: function (id) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_FINANCE_DETAIL,
                        timeout: CONF.TIMEOUT,
                        params: {
                            invoiceId : id,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 
                getFinanceApproveDetail: function (id) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_APPROVE_FINANCE_DETAIL,
                        timeout: CONF.TIMEOUT,
                        params: {
                            invoiceId : id,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 

                getBankAccount: function (id) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_BANK_ACCOUNT,
                        timeout: CONF.TIMEOUT,
                        params: {
                            bankId : id,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 

                getlistOfFinance: function (batchNo,notaryId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_FINANCE_PAYMENT_LIST,
                        timeout: CONF.TIMEOUT,
                        params: {
                            notaryId : notaryId,
                            batchNo : batchNo,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 

                getlistOfApproveFinance: function (batchNo,notaryId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_APPROVAL_FINANCE_PAYMENT_LIST,
                        timeout: CONF.TIMEOUT,
                        params: {
                            notaryId : notaryId,
                            batchNo : batchNo,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 

                processFinance: function (request) {
                    var deferred = $q.defer();

                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_PROCESS_FINANCE,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            access_token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 
                processTextFile: function (request) {
                    var deferred = $q.defer();

                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CREATE_TEXT_FILE,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            access_token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 


                approveFinance: function (request) {
                    var deferred = $q.defer();

                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_APPROVE_FINANCE,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            access_token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }

                /*deleteTax: function (request) {
                    var deferred = $q.defer();

                    $http({
                        method: 'DELETE',
                        headers: {
                            'Content-Type':'application/json'
                        }
                        url: CONF.MASTER_PATH + CONF.URL_DELETE_TAX,
                        timeout: CONF.TIMEOUT,
                        data: request,
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },*/
                
            };
        }]);
