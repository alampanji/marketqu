'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('marketplaceApp')
    .service('GroupService', [
        '$http',
        '$q',
        'CONF',
        '$sessionStorage',
        'localStorageService',
        function ($http, $q, CONF, $sessionStorage, localStorageService) {
            return {
                getListGroup: function (page,limit,code,name,user) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_GROUP,
                        timeout: CONF.TIMEOUT,
                        params: {
                            access_token: accessToken,
                            page : page,
                            limit : limit,
                            code:code,
                            name : name,
                            accessLevel:user
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getListCapabillities: function () {
                    var deferred = $q.defer();
                    var accessToken = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_CAPABILLITIES,
                        timeout: CONF.TIMEOUT,
                        params: {
                            role: 2
                        }

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getListPrivilege: function () {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_PRIVILEGE,
                        timeout: CONF.TIMEOUT,
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getDetailGroup: function (id) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_DETAIL_GROUP,
                        timeout: CONF.TIMEOUT,
                        params: {
                            groupsId : id
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                createGroup: function (contentData) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CREATE_GROUP,
                        timeout: CONF.TIMEOUT,
                        data: contentData,

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                updateGroup: function (contentData) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_UPDATE_GROUP,
                        timeout: CONF.TIMEOUT,
                        data: contentData,

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                deleteGroup: function (request) {
                    var deferred = $q.defer();
                    $http({
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        url: CONF.MASTER_PATH + CONF.URL_DELETE_GROUP,
                        timeout: CONF.TIMEOUT,
                        data: request,
                    }).success(function (response) {
                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }
            };
        }]
    );
