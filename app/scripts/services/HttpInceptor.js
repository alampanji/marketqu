'use strict';
/**
 * Created by nostra on 3/14/16.
 */
angular.module('marketplaceApp')
    .service('myHttpInterceptor', [
        '$q',
        'CONF',
        'localStorageService',
        '$log',
        '$location',
        '$window', '$injector',
        function ($q, CONF, localStorageService, $log, $location, $window, $injector) {
        var $modal = null;

            return {
                'request': function (config) {
                    return config;
                },

                'requestError': function (rejection) {
                    $log.error("request error");
                    return $q.reject(rejection);
                },
                'response': function (response) {
                    return response;
                },
                'responseError': function (rejection) {
                if($modal == null) $modal = $injector.get('$modal');
                    if (rejection.status == 401) {

                        self.isDisabled = false;
                        $modal.open({
                            templateUrl: 'views/modal/Modal.html',
                            controller: 'ModalCtrl',
                            backdrop: 'static',
                            size: 'sm',
                            resolve: {
                                modalParam: function () {
                                    return {
                                        title: 'Warning',
                                        message: 'Your token has been expired',
                                        path: ''
                                    };
                                }
                            }
                        });

                        localStorageService.cookie.remove(CONF.fullName);
                        localStorageService.cookie.remove(CONF.accessToken);
                        localStorageService.cookie.remove(CONF.domain);
                        localStorageService.cookie.remove(CONF.id);
                        localStorageService.cookie.remove(CONF.uid);
                        $location.path('/user/home');
                    }
                    return $q.reject(rejection);
                }
            };
        }]);