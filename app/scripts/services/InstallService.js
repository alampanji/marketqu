'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('marketplaceApp')
    .service('InstallService', [
        '$http',
        '$q',
        '$sessionStorage',
        'CONF',
        function($http, $q, $sessionStorage, CONF) {
            return {
                getMigration: function() {
                    var deferred = $q.defer();
                    var version = 20170308234100;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_MIGRATION,
                        timeout: CONF.TIMEOUTFILE,
                        params: {
                            version: version
                        }
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getPing: function() {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_PING,
                        timeout: CONF.TIMEOUTFILE
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getSiteInfo: function() {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_SITE_INFO,
                        timeout: CONF.TIMEOUTFILE
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getDetailUserInternal: function(id) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_DETAIL_USER_INTERNAL,
                        timeout: CONF.TIMEOUT,
                        params: {
                            id: id,
                            access_token: access_token
                        }
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },
                getDetailUserNotary: function(id) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_DETAIL_USER_NOTARY,
                        timeout: CONF.TIMEOUT,
                        params: {
                            access_token: access_token,
                            id: id
                        }
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },
                getRetrieveUser: function(username) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_RETRIEVE_USER,
                        timeout: CONF.TIMEOUT,
                        params: {
                            username: username,
                            access_token: access_token
                        }
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },
                getListUserNotary: function(page, limit, notaryName, notaryId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_LIST_USER_NOTARY,
                        timeout: CONF.TIMEOUT,
                        params: {
                            page: page,
                            limit: limit,
                            access_token: accessToken,
                            notaryId: notaryId,
                            name: notaryName
                        }
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },
                createDatabase: function(contentData) {
                    var deferred = $q.defer();
                    console.log(contentData);
                    $http({
                        method: 'POST',
                        // headers: {
                        //     'Content-Type': 'application/json'
                        // },
                        url: CONF.MASTER_PATH + CONF.URL_INSTALLATION_DB,
                        // url: CONF.URL_INSTALLATION,
                        timeout: CONF.TIMEOUT,
                        data: contentData
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                createSiteInstallation: function(fd) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    console.log(fd);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_INSTALLATION_SITE,
                        timeout: CONF.TIMEOUTFILE,
                        data: fd,
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                        // params: {
                        //     access_token: accessToken
                        // }

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                // createSiteInstallation: function(contentData) {
                //     var deferred = $q.defer();
                //     console.log(contentData);
                //     $http({
                //         method: 'POST',
                //         // headers: {
                //         //     'Content-Type': 'application/json'
                //         // },
                //         url: CONF.MASTER_PATH + CONF.URL_INSTALLATION_SITE,
                //         // url: CONF.URL_INSTALLATION,
                //         timeout: CONF.TIMEOUT,
                //         data: contentData
                //     }).success(function(response) {
                //
                //         deferred.resolve(response);
                //
                //     }).error(deferred.reject);
                //
                //     return deferred.promise;
                // },


                updateUserInternal: function(contentData) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    console.log(accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_UPDATE_USER_INTERNAL,
                        timeout: CONF.TIMEOUT,
                        data: contentData,
                        params: {
                            access_token: accessToken
                        }

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                deleteUserInternal: function(request) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken
                    $http({
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        url: CONF.MASTER_PATH + CONF.URL_DELETE_USER_INTERNAL,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params: {
                            access_token: access_token
                        }
                    }).success(function(response) {
                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                createUserNotary: function(contentData) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CREATE_USER_NOTARY,
                        timeout: CONF.TIMEOUT,
                        data: contentData,
                        params: {
                            access_token: access_token
                        }
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                updateUserNotary: function(contentData) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_UPDATE_USER_NOTARY,
                        timeout: CONF.TIMEOUT,
                        data: contentData,
                        params: {
                            access_token: access_token
                        }

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                deleteUserNotary: function(request) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken
                    $http({
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        url: CONF.MASTER_PATH + CONF.URL_DELETE_USER_NOTARY,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params: {
                            access_token: access_token
                        }
                    }).success(function(response) {
                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }
            };
        }
    ]);