'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('nostraApp')
    .service('InvoiceService', [
        '$http',
        '$q',
        'CONF',
        '$sessionStorage',
        function ($http, $q, CONF, $sessionStorage) {

            return {
                getlistOfInvoice: function (request) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_INPUT_INVOICE_LIST,
                        timeout: CONF.TIMEOUT,
                        params: {
                            startDate : request.startDate,
                            endDate : request.endDate,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 
                processInvoice: function (request) {
                    var deferred = $q.defer();

                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_PROCESS_INVOICE,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            access_token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getlistOfReceiveInvoice: function (invoiceId,notaryId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_RECEIVE_INVOICE_LIST,
                        timeout: CONF.TIMEOUT,
                        params: {
                            invoiceId : invoiceId,
                            notaryId : notaryId,
                            access_token: accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getlistOfInvoiceByNotaryId: function (notaryId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_INVOICE_BY_NOTARY_ID,
                        timeout: CONF.TIMEOUT,
                        params: {
                            notaryId : notaryId,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getlistOfInvoiceApprovalByNotaryId: function (notaryId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_INVOICE_APPROVAL_LIST,
                        timeout: CONF.TIMEOUT,
                        params: {
                            notaryId : notaryId,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getlistOfInvoiceMemoByNotaryId: function (notaryId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_MEMO_INVOICE_BY_NOTARY,
                        timeout: CONF.TIMEOUT,
                        params: {
                            notaryId : notaryId,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },



                getlistOfMemoInvoice: function (notaryId,invoiceId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_INVOICE_MEMO_LIST,
                        timeout: CONF.TIMEOUT,
                        params: {
                            invoiceId : invoiceId,
                            notaryId : notaryId,
                            access_token: accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getDataPrint: function (invoiceId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_INVOICE_MEMO_PRINT,
                        timeout: CONF.TIMEOUT,
                        params: {
                            invoiceId : invoiceId,
                            access_token: accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                processReceiveInvoice: function (request) {
                    var deferred = $q.defer();

                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_RECEIVE_INVOICE_PROCESS,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            access_token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                rejectReceiveInvoice: function (request) {
                    var deferred = $q.defer();

                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_RECEIVE_INVOICE_REJECT,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            access_token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getlistOfApproveInvoice: function (page, limit, invoiceId, notaryId, receiveDate) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_APPROVAL_INVOICE_LIST,
                        timeout: CONF.TIMEOUT,
                        params: {
                            page : page,
                            limit : limit,
                            invoiceId : invoiceId,
                            notaryId : notaryId,
                            receiveDate: receiveDate,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getDetailApproveInvoice: function (invoiceId) {
                    var deferred = $q.defer();
                    var accessToken = $sessionStorage.accessToken;
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_APPROVAL_INVOICE_DETAIL,
                        timeout: CONF.TIMEOUT,
                        params: {
                            invoiceId : invoiceId,
                            access_token : accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                approvalInvoiceProcess: function (contentData) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_PROCESS_APPROVAL_INVOICE,
                        timeout: CONF.TIMEOUT,
                        data: contentData,
                        params : {
                            access_token :access_token
                        }

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                rejectInvoiceProcess: function (contentData) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_PROCESS_APPROVAL_INVOICE,
                        timeout: CONF.TIMEOUT,
                        data: contentData,
                        params : {
                            access_token :access_token
                        }

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }

            };
        }]);
