'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('marketplaceApp')
    .service('ItemService', [
        '$http',
        '$q',
        '$sessionStorage',
        'localStorageService',
        'CONF',
        function($http, $q, $sessionStorage, localStorageService, CONF) {
            return {
                getListMarketing: function(page,limit) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_ITEM_GROUP,
                        timeout: CONF.TIMEOUT,
                        params:{
                            page:page,
                            limit:limit,
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getDetailMarketing: function(id) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_DETAIL_ITEM_GROUP_MARKETING,
                        timeout: CONF.TIMEOUT,
                        params:{
                            id:id,
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getItemReseller: function(id) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_ITEM_RESELLER,
                        timeout: CONF.TIMEOUT,
                        params:{
                            id:id,
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getFeedbackProdusen: function(id) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_FEEDBACK_PRODUSEN_PRODUCT,
                        timeout: CONF.TIMEOUT,
                        params:{
                            id_product:id,
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getListReportSales: function(page,limit) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_REPORT,
                        timeout: CONF.TIMEOUT,
                        params:{
                            page:page,
                            limit:limit,
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                processAssignItem: function (request) {
                    var deferred = $q.defer();

                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_ASSIGN_ITEM_PROCESS,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                giveFeedback: function (request) {
                    var deferred = $q.defer();

                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_ASSIGN_GIVE_FEEDBACK,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                giveResponse: function (request) {
                    var deferred = $q.defer();

                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_GIVE_RESPONSE,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getFeedbackResponse: function(id) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_FEEDBACK_RESPONSE,
                        timeout: CONF.TIMEOUT,
                        params:{
                            id_feedback:id,
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },


                createMarketing: function (request) {
                    var deferred = $q.defer();

                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CREATE_GROUP_MARKETING,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                createUserMarketingGroup: function (request) {
                    var deferred = $q.defer();

                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CREATE_USER_GROUP_MARKETING,
                        timeout: CONF.TIMEOUTFILE,
                        data: request,
                        params : {
                            token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                updateMarketing: function (request) {
                    var deferred = $q.defer();

                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'PUT',
                        url: CONF.MASTER_PATH + CONF.URL_UPDATE_GROUP_MARKETING,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                deleteMarketing: function (request) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        url: CONF.MASTER_PATH + CONF.URL_DELETE_MARKETING_GROUP,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params:{
                            token: access_token
                        }
                    }).success(function (response) {
                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },


            };
        }
    ]);