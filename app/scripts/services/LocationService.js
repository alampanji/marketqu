'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('marketplaceApp')
    .service('LocationService', [
        '$http',
        '$q',
        'localStorageService',
        'CONF',
        function($http, $q, localStorageService, CONF) {
            return {
                getListBank: function() {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_LIST_BANK,
                        timeout: CONF.TIMEOUT
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getListLocation: function(type,id) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LOCATION,
                        timeout: CONF.TIMEOUTFILE,
                        params:{
                            type:type,
                            id:id
                        }
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getDeliveryFeeCity: function(type,id) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_ONGKIR,
                        timeout: CONF.TIMEOUTFILE,
                        params:{
                            type:type,
                            id:id
                        }
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                countCostOngkir: function (request) {
                    var deferred = $q.defer();

                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_DELIVERY_COST,
                        timeout: CONF.TIMEOUTFILE,
                        data: request,
                        params : {
                            token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }
                
            };
        }
    ]);