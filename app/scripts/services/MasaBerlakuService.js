'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('nostraApp')
    .service('MasaBerlakuService', [
        '$http',
        '$q',
        'CONF',
        'localStorageService',
        function ($http, $q, CONF, localStorageService) {

            return {
                getlistMasaBerlaku: function (page,limit) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_MASA_BERLAKU,
                        timeout: CONF.TIMEOUT,
                        params: {
                            page : page,
                            limit : limit
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                createMasaBerlaku: function (request) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CREATE_MASA_BERLAKU,
                        timeout: CONF.TIMEOUT,
                        data: request,
                    }).success(function (response) {
                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                deleteMasaBerlaku: function (request) {
                    var deferred = $q.defer();
                    $http({
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        url: CONF.MASTER_PATH + CONF.URL_DELETE_MASA_BERLAKU,
                        timeout: CONF.TIMEOUT,
                        data: request,
                    }).success(function (response) {
                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }
            };
        }]);
