'use strict';

/**
 * @ngdoc service
 * @name nostraApp.messageService
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('marketplaceApp')
    .service('messageService', [
        '$http',
        '$q',
        'CONF',
        function ($http, $q, CONF) {

            return {
                toasterMessage: function (position,type,message) {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": position,
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "20000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut",
                    };
                    toastr[type](message);
                }
            };

        }]);