/**
 * Created by derryaditiya on 5/13/16.
 */
'use strict';

(function () {
    angular.module('marketplaceApp')
        .service('navigationMenuService', [ UserService]);

    function UserService() {
        var publicMenus = [
            {
                detail: [
                    {
                        name: 'Sign in',
                        icon: 'login',
                        route: '/login',
                        has_counter: false
                    }

                ],
                name: 'Authentication'
            }
        ];

        var otherPrivateMenu = [

            {
                name: 'Dashboard',
                route: '/dashboard/home'
            },
            {
                name: 'Notification',
                route: '/dashboard/notifications'
            },
            {
                name: 'Change password',
                route: '/article/changepassword'
            }

        ]


        var navigationPrivateMenus = [
            {
                detail: [
                    {
                        name: 'Search',
                        icon: 'search',
                        route: '/search',
                        has_counter: false
                    }
                ],
                name: 'Search'
            },
            {
                detail: [
                    {
                        name: 'Attention',
                        key:'ATTENTION_ALL',
                        icon: 'warning',
                        route: '/order/attention/accepted',
                        has_counter: true,
                        theme: 'attention'
                    },
                    {
                        name: 'Opened',
                        key : 'PENDING',
                        icon: 'open_in_new',
                        route: '/order/pending',
                        has_counter: true,
                        theme:'default'
                    },
                    {
                        name: 'Accepted',
                        key : 'ACCEPTED',
                        icon: 'toc',
                        route: '/order/accepted',
                        has_counter: true,
                        theme:'default'
                    },
                    {
                        name: 'Confirmed',
                        key : 'CONFIRMED',
                        icon: 'beenhere',
                        route: '/order/confirmed',
                        has_counter: true,
                        theme:'default'
                    },
                    {
                        name: 'Shipped',
                        key : 'SHIPPED',
                        icon: 'local_shipping',
                        route: '/order/shipped',
                        has_counter: true,
                        theme:'default'

                    },
                    {
                        name: 'Completed',
                        key : 'COMPLETED',
                        icon: 'redeem',
                        route: '/order/completed',
                        has_counter: true,
                        theme:'' +
                        '' +
                        ''
                    },
                    {
                        name: 'Closed',
                        key : 'CLOSED',
                        icon: 'done_all',
                        route: '/order/closed',
                        has_counter: true,
                        theme:'default'

                    },
                    {
                        name: 'Canceled',
                        key : 'CANCELED',
                        icon: 'cancel',
                        route: '/order/cancelled',
                        has_counter: true,
                        theme:'default'

                    }

                ],
                name: 'Order'
            },
            {
                detail: [
                    {
                        name: 'Sign out',
                        icon: 'logout',
                        route: '/logout',
                        has_counter: false
                    }
                ],
                name: 'Authentication'
            }
        ];

        return {
            loadAllPrivateMenus: function () {
                return navigationPrivateMenus;
            },
            loadAllPublicMenus: function () {
                return publicMenus;
            },
            loadOtherPrivateMenu: function () {
                return otherPrivateMenu;
            }
        };
    }

})();
