'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('marketplaceApp')
    .service('PaymentService', [
        '$http',
        '$q',
        'localStorageService',
        'CONF',
        function($http, $q, localStorageService, CONF) {
            return {

                checkOutProcess: function (request) {
                    var deferred = $q.defer();

                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CHECKOUT_MIDTRANS,
                        timeout: CONF.TIMEOUTFILE,
                        data: request,
                        params : {
                            token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }

            };
        }
    ]);