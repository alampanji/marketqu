var app = angular.module('marketplaceApp')
    .service('privilegeService', [
        '$http',
        '$q',
        'CONF',
        'localStorageService',
        '$sessionStorage',
        function($http, $q, CONF, localStorageService, $sessionStorage) {

            function generateMenus() {

                var menu = [{
                        "Menu": "Administrator",
                        "multiMenu": true,
                        "menuLevel2": [{
                                "menuName": "User BCA Finance",
                                "path": "dashboard.securityAdmin",
                                "privileges": [{
                                        "privilege": CONF.user_internal_read
                                    },
                                    {
                                        "privilege": CONF.user_internal_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "User Notary",
                                "path": "dashboard.userNotary",
                                "privileges": [{
                                        "privilege": CONF.user_notari_read
                                    },
                                    {
                                        "privilege": CONF.user_notari_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Paramater Group Admin",
                                "path": "dashboard.SearchMaintenanceGroup",
                                "privileges": [{
                                        "privilege": CONF.setting_group_read
                                    },
                                    {
                                        "privilege": CONF.setting_group_full_access
                                    }
                                ]
                            },
                            // {
                            //     "menuName": "Parameter Biaya Resume",
                            //     "path": "dashboard.settingBiayaResumeAHU",
                            //     "privileges": [{
                            //             "privilege": CONF.setting_resume_cost_ahu_read
                            //         },
                            //         {
                            //             "privilege": CONF.setting_resume_cost_full_access
                            //         }
                            //     ]
                            // },
                            {
                                "menuName": "Parameter Cabang Sentral",
                                "path": "dashboard.settingParameterBranchCentral",
                                "privileges": [{
                                        "privilege": CONF.setting_branch_central_full_access
                                    },
                                    {
                                        "privilege": CONF.setting_branch_central_read
                                    }
                                ]
                            },
                            {
                                "menuName": "Parameter Kelengkapan Dokumen",
                                "path": "dashboard.settingDokumenKelengkapan",
                                "privileges": [{
                                        "privilege": CONF.setting_document_type_read
                                    },
                                    {
                                        "privilege": CONF.setting_document_type_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Parameter Masa Berlaku",
                                "path": "dashboard.settingMasaBerlaku",
                                "privileges": [{
                                        "privilege": CONF.setting_resume_effective_period_read
                                    },
                                    {
                                        "privilege": CONF.setting_resume_effective_period_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Parameter Notary",
                                "path": "dashboard.settingParameterNotary",
                                "privileges": [{
                                        "privilege": CONF.setting_notary_read
                                    },
                                    {
                                        "privilege": CONF.setting_notary_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Setting Limit Approve Invoice",
                                "path": "dashboard.settingLimitApproveInvoiceNotary",
                                "privileges": [{
                                        "privilege": CONF.setting_limit_approve_full_access
                                    },
                                    {
                                        "privilege": CONF.setting_limit_approve_read
                                    }
                                ]
                            },
                            {
                                "menuName": "Parameter SLA",
                                "path": "dashboard.settingLimitApprove",
                                "privileges": [{
                                        "privilege": CONF.setting_sla_read
                                    },
                                    {
                                        "privilege": CONF.setting_sla_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Parameter Upload Voucher",
                                "path": "dashboard.uploadVoucherCode",
                                "privileges": [{
                                        "privilege": CONF.setting_voucher_read
                                    },
                                    {
                                        "privilege": CONF.setting_voucher_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Parameter Wilayah Notaris",
                                "path": "dashboard.settingParameterNotaryArea",
                                "privileges": [{
                                        "privilege": CONF.setting_notary_area_full_access
                                    },
                                    {
                                        "privilege": CONF.setting_notary_area_read
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "Menu": "Transaction",
                        "multiMenu": true,
                        "menuLevel2": [{
                                "menuName": "Approval Cancel Resume",
                                "path": "dashboard.approveCancelResume",
                                "privileges": [{
                                        "privilege": CONF.approval_cancel_resume_full_access
                                    },
                                    {
                                        "privilege": CONF.approval_cancel_resume_read
                                    }

                                ]
                            },
                            {
                                "menuName": "Approval Invoice",
                                "path": "dashboard.approvalInvoice",
                                "privileges": [{
                                        "privilege": CONF.approval_invoice_read
                                    },
                                    {
                                        "privilege": CONF.approval_invoice_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Approval Request Resume",
                                "path": "dashboard.approvalRequestResume",
                                "privileges": [{
                                        "privilege": CONF.approval_request_resume_read
                                    },
                                    {
                                        "privilege": CONF.approval_request_resume_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Approval Resume",
                                "path": "dashboard.approvalResume",
                                "privileges": [{
                                        "privilege": CONF.approval_resume_read
                                    },
                                    {
                                        "privilege": CONF.approval_resume_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Approve Payment",
                                "path": "dashboard.searchPaymentApprove",
                                "privileges": [{
                                        "privilege": CONF.approve_payment_full_access
                                    },
                                    {
                                        "privilege": CONF.approve_payment_read
                                    }
                                ]
                            },
                            {
                                "menuName": "Cek Dokumen Kelengkapan",
                                "path": "dashboard.checkKelengkapanDocument",
                                "privileges": [{
                                        "privilege": CONF.check_document_kelengkapan_read
                                    },
                                    {
                                        "privilege": CONF.check_document_kelengkapan_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Create Text File",
                                "path": "dashboard.createTextFile",
                                "privileges": [{
                                        "privilege": CONF.create_text_file_read
                                    },
                                    {
                                        "privilege": CONF.create_text_file_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Input Invoice",
                                "path": "dashboard.inputInvoiceTagihan",
                                "privileges": [{
                                        "privilege": CONF.input_invoice_read
                                    },
                                    {
                                        "privilege": CONF.input_invoice_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Input Resume Tambahan",
                                "path": "dashboard.inputResumeTambahan",
                                "privileges": [{
                                        "privilege": CONF.input_additional_resume_read
                                    },
                                    {
                                        "privilege": CONF.input_additional_resume_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Inquiry History Resume",
                                "path": "dashboard.inquiryHistoryResume",
                                "privileges": [{
                                        "privilege": CONF.inqiury_history_resume_read
                                    },
                                    {
                                        "privilege": CONF.inqiury_history_resume_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Inquiry Status Resume",
                                "path": "dashboard.inquiryStatusResume",
                                "privileges": [{
                                        "privilege": CONF.inquiry_status_resume_read
                                    },
                                    {
                                        "privilege": CONF.inquiry_status_resume_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Order Resume AHU",
                                "path": "dashboard.orderResumeAHU",
                                "privileges": [{
                                        "privilege": CONF.order_resume_ahu_read
                                    },
                                    {
                                        "privilege": CONF.order_resume_ahu_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Payment",
                                "path": "dashboard.searchPayment",
                                "privileges": [{
                                        "privilege": CONF.payment_read
                                    },
                                    {
                                        "privilege": CONF.payment_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Receive Invoice",
                                "path": "dashboard.receiveInvoice",
                                "privileges": [{
                                        "privilege": CONF.receive_invoice_read
                                    },
                                    {
                                        "privilege": CONF.receive_invoice_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Request and Matching",
                                "path": "dashboard.requestAndMatchingResume",
                                "privileges": [{
                                        "privilege": CONF.request_and_matching_full_access
                                    },
                                    {
                                        "privilege": CONF.request_and_matching_read
                                    }
                                ]
                            },
                            {
                                "menuName": "Request Cancel Resume",
                                "path": "dashboard.requestCancelResume",
                                "privileges": [{
                                        "privilege": CONF.request_cancel_resume_read
                                    },
                                    {
                                        "privilege": CONF.request_cancel_resume_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Request Dokumen Kelengkapan",
                                "path": "dashboard.requestCompletenessDocument",
                                "privileges": [{
                                        "privilege": CONF.request_document_kelengkapan_read
                                    },
                                    {
                                        "privilege": CONF.request_document_kelengkapan_full_access
                                    }
                                ]
                            },

                            {
                                "menuName": "Upload Dokumen Kelengkapan",
                                "path": "dashboard.uploadDokumenKelengkapan",
                                "privileges": [{
                                        "privilege": CONF.upload_document_kelengkapan_read
                                    },
                                    {
                                        "privilege": CONF.upload_document_kelengkapan_full_access
                                    }
                                ]
                            },
                            {
                                "menuName": "Upload Resume AHU",
                                "path": "dashboard.uploadResumeAhu",
                                "privileges": [{
                                        "privilege": CONF.upload_resume_ahu_read
                                    },
                                    {
                                        "privilege": CONF.upload_resume_ahu_full_access
                                    }
                                ]
                            }

                        ]
                    },

                    // {
                    //     "Menu": "Notary",
                    //     "multiMenu": true,
                    //     "menuLevel2": [{
                    //         "menuName": "Other Menu Notary",
                    //         "path": "dashboard.receiveInvoice",
                    //         "privileges": [{
                    //                 "privilege": CONF.receive_invoice_read
                    //             },
                    //             {
                    //                 "privilege": CONF.receive_invoice_full_access
                    //             }
                    //         ]
                    //     }]
                    // },
                    {
                        "Menu" : "Request Dokumen Kelengkapan",
                        "multiMenu" : false,
                        "menuLevel2" : [
                            {
                                "menuName"      : "Request Dokumen Kelengkapan",
                                "path"          : "dashboard.requestCompletenessDocument",
                                "privileges"    : [
                                    {
                                        "privilege"   : CONF.request_document_kelengkapan_read
                                    },
                                    {
                                        "privilege"   : CONF.request_document_kelengkapan_full_access
                                    },
                                ]
                            }
                        ]
                    },
                    // {
                    //     "Menu": "Report",
                    //     "multiMenu": true,
                    //     "menuLevel2": [{
                    //             "menuName": "Outstanding Pembuatan Resume",
                    //             "path": "dashboard.reportOutstandingResume",
                    //             "privileges": [{
                    //                 "privilege": CONF.report_create_outstanding_resume_read
                    //             }]
                    //         },
                    //         {
                    //             "menuName": "Cancel Resume",
                    //             "path": "dashboard.reportCancelResume",
                    //             "privileges": [{
                    //                 "privilege": CONF.report_cancel_resume_read
                    //             }]
                    //         },
                    //         {
                    //             "menuName": "Reimburse Saldo Resume AHU",
                    //             "path": "dashboard.reportReimburseSaldo",
                    //             "privileges": [{
                    //                 "privilege": CONF.report_reimburse_resume_saldo_read
                    //             }]
                    //         },
                    //         {
                    //             "menuName": "Konsumen yang Belum Realisasi",
                    //             "path": "dashboard.reportKonsumenRealisasi",
                    //             "privileges": [{
                    //                 "privilege": CONF.report_realization_consumen_read
                    //             }]
                    //         },
                    //         {
                    //             "menuName": "Penerimaan Resume  Badan Usaha",
                    //             "path": "dashboard.reportPenerimaanResumeBadanUsaha",
                    //             "privileges": [{
                    //                 "privilege": CONF.report_acceptance_resume_organization_read
                    //             }]
                    //         },
                    //         {
                    //             "menuName": "Penerimaan Resume yang lebih dari SLA",
                    //             "path": "dashboard.reportPenerimaanResumeSLA",
                    //             "privileges": [{
                    //                 "privilege": CONF.report_acceptance_resume_over_sla_read
                    //             }]
                    //         },
                    //         {
                    //             "menuName": "Outstanding Pembayaran Notaris",
                    //             "path": "dashboard.reportOutstandingPembayaranNotaris",
                    //             "privileges": [{
                    //                 "privilege": CONF.report_outstanding_paid_notary_read
                    //             }]
                    //         },
                    //         {
                    //             "menuName": "Laporan Aktifitas SA",
                    //             "path": "dashboard.reportAktivasiSLA",
                    //             "privileges": [{
                    //                 "privilege": CONF.report_sla_activity_read
                    //             }]
                    //         }
                    //     ]
                    // }



                ];

                // var privilegeList = $sessionStorage.privileges;
                // var menuFinal = [];
                // if (privilegeList != "undefined") {
                //     for (var i = 0; i < menu.length; i++) {
                //         var createMenu = {
                //             "mainMenu": menu[i].Menu,
                //             "multiMenu": menu[i].multiMenu,
                //             "menuLevel2": []
                //         }
                //         for (var j = 0; j < menu[i].menuLevel2.length; j++) {
                //             for (var k = 0; k < menu[i].menuLevel2[j].privileges.length; k++) {
                //                 var privilege = privilegeList.map(function(e) {
                //                     return e.name;
                //                 }).indexOf(menu[i].menuLevel2[j].privileges[k].privilege);
                //                 if (privilege != -1) {
                //                     var subMenu = {
                //                         "subMenu": menu[i].menuLevel2[j].menuName,
                //                         "path": menu[i].menuLevel2[j].path
                //                     };
                //                     createMenu.menuLevel2.push(subMenu);
                //                     break;
                //                 }
                //             }
                //         }
                //         if (createMenu.menuLevel2.length > 0)
                //             menuFinal.push(createMenu);
                //     }
                // }
                // return menuFinal;
            }

            return {

                generateMenu: function() {
                    return generateMenus();
                },

                refreshPrivilege: function(request) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_USER_PRIVILEGE,
                        timeout: CONF.TIMEOUT,
                        param: {
                            access_token: $sessionStorage.accessToken
                        }
                    }).success(function(response) {
                        deferred.resolve(response);
                        $sessionStorage.privileges = response.result.group.privilegeList;
                        $sessionStorage.menu = generateMenus();
                    }).error(deferred.reject);

                    return deferred.promise;
                },

                hasPrivilege: function(checkPrivilege) {
                    if ($sessionStorage.privileges) {
                        var listPriv = $sessionStorage.privileges;
                        var privilege = listPriv.map(function(e) {
                            return e.name;
                        }).indexOf(checkPrivilege);
                        if (privilege != -1) {
                            return true;
                        }
                        return false;
                    }
                    return false;
                },

                hasRoleIsAdmin: function() {

                    if(localStorageService.cookie.get(CONF.dataUser) == undefined){
                        return false;
                    }
                    else{
                        if (localStorageService.cookie.get(CONF.dataUser).name == 'admin') {
                            return true;
                        }
                    }
                    return false;
                },

                hasRoleIsProdusen: function() {

                    if(localStorageService.cookie.get(CONF.dataUser) == undefined){
                        return false;
                    }
                    else{
                        if (localStorageService.cookie.get(CONF.dataUser).name == 'produsen') {
                            return true;
                        }
                    }

                    return false;
                },

                hasRoleIsReseller: function() {
                    if(localStorageService.cookie.get(CONF.dataUser) == undefined){
                        return false;
                    }
                    else{
                        if (localStorageService.cookie.get(CONF.dataUser).name == 'reseller') {
                            return true;
                        }
                    }
                    return false;
                },

                hasRoleIsMemberGroup: function() {
                    if(localStorageService.cookie.get(CONF.dataUser) == undefined){
                        return false;
                    }
                    else{
                        if (localStorageService.cookie.get(CONF.dataUser).name == 'member_group') {
                            return true;
                        }
                    }
                    return false;
                }
            };

        }
    ]);