'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('marketplaceApp')
    .service('ProductService', [
        '$http',
        '$q',
        '$sessionStorage',
        'localStorageService',
        'CONF',
        function($http, $q, $sessionStorage, localStorageService, CONF) {
            return {
                getListBank: function() {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_LIST_BANK,
                        timeout: CONF.TIMEOUT,
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },
                getListCategory: function(page,limit) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_CATEGORY,
                        timeout: CONF.TIMEOUT,
                        params:{
                            page:page,
                            limit:limit,
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                createCategory: function(fd) {
                    var deferred = $q.defer();
                    var accessToken = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CREATE_CATEGORY,
                        timeout: CONF.TIMEOUTFILE,
                        data: fd,
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined },
                        params: {
                            token: accessToken
                        }

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                updateCategory: function(fd) {
                    var deferred = $q.defer();
                    var accessToken = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_UPDATE_CATEGORY,
                        timeout: CONF.TIMEOUTFILE,
                        data: fd,
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined },
                        params: {
                            token: accessToken
                        }

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },


                createProduct: function(fd) {
                    var deferred = $q.defer();
                    var accessToken = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CREATE_PRODUCT,
                        timeout: CONF.TIMEOUTFILE,
                        data: fd,
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined },
                        params: {
                            token: accessToken
                        }

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                updateProduct: function(fd) {
                    var deferred = $q.defer();
                    var accessToken = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_UPDATE_PRODUCT,
                        timeout: CONF.TIMEOUTFILE,
                        data: fd,
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined },
                        params: {
                            token: accessToken
                        }

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                deleteProduct: function (request) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        url: CONF.MASTER_PATH + CONF.URL_DELETE_PRODUCT,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params:{
                            token: access_token
                        }
                    }).success(function (response) {
                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getDetailCategory: function (id) {
                    var deferred = $q.defer();
                    var accessToken = localStorageService.cookie.get(CONF.accessToken);

                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_CATEGORY,
                        timeout: CONF.TIMEOUT,
                        params: {
                            id : id,
                            token: accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getListProduct: function(page,limit) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_PRODUCT,
                        timeout: CONF.TIMEOUTFILE,
                        params:{
                            page:page,
                            limit:limit,
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getListProductUser: function(category, name) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_PRODUCT_USER,
                        timeout: CONF.TIMEOUTFILE,
                        params:{
                            cat:category,
                            name:name
                        }
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getListBestProduct: function(limit) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_BEST_SELLER,
                        timeout: CONF.TIMEOUTFILE,
                        params:{
                            limit:limit
                        }
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getDetailProductUser: function (id) {
                    var deferred = $q.defer();

                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_PRODUCT_USER,
                        timeout: CONF.TIMEOUT,
                        params: {
                            id : id
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },



                getDetailProduct: function (id) {
                    var deferred = $q.defer();
                    var accessToken = localStorageService.cookie.get(CONF.accessToken);

                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_PRODUCT,
                        timeout: CONF.TIMEOUT,
                        params: {
                            id : id,
                            token: accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                createUserNotary: function(contentData) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken;
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CREATE_USER_NOTARY,
                        timeout: CONF.TIMEOUT,
                        data: contentData,
                        params: {
                            access_token: access_token
                        }
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                updateUser: function(contentData) {
                    var deferred = $q.defer();
                    var access_token = $sessionStorage.accessToken
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_UPDATE_USER_NOTARY,
                        timeout: CONF.TIMEOUT,
                        data: contentData,
                        params: {
                            access_token: access_token
                        }

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                deleteCategory: function (request) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        url: CONF.MASTER_PATH + CONF.URL_DELETE_CATEGORY,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params:{
                            token: access_token
                        }
                    }).success(function (response) {
                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                sendDiscussion: function (request) {
                    var deferred = $q.defer();

                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_SEND_DISCUSSION,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                viewOfProduct: function (request) {
                    var deferred = $q.defer();

                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_SEND_VIEW,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            token :access_token
                        }


                    })
                }

                // deleteUser: function(request) {
                //     var deferred = $q.defer();
                //     var access_token = localStorageService.cookie.get(CONF.accessToken);
                //     $http({
                //         method: 'DELETE',
                //         headers: {
                //             'Content-Type': 'application/json'
                //         },
                //         url: CONF.MASTER_PATH + CONF.URL_DELETE_USER,
                //         timeout: CONF.TIMEOUT,
                //         data: request
                //     }).success(function(response) {
                //         deferred.resolve(response);
                //
                //     }).error(deferred.reject);
                //
                //     return deferred.promise;
                // },
            };
        }
    ]);