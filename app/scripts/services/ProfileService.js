'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('nostraApp')
    .service('profilesService', [
        '$http',
        '$q',
        'CONF',
        'localStorageService', '$sessionStorage',
        function ($http, $q, CONF, localStorageService, $sessionStorage ) {

        return{

            registration: function (request) {
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: CONF.MASTER_PATH + CONF.URL_REGISTER,
                    timeout: CONF.TIMEOUT,
                    data: request
                    
                }).success(function (response) {

                    deferred.resolve(response);

                }).error(deferred.reject);

                return deferred.promise;
            },

            registrationAdmin: function (request) {
                var deferred = $q.defer();
                var accessToken = localStorageService.cookie.get(CONF.accessToken);
                $http({
                    method: 'POST',
                    url: CONF.MASTER_PATH + CONF.URL_REGISTER_ADMIN,
                    timeout: CONF.TIMEOUT,
                    data: request,
                    params: {
                        access_token: accessToken
                    }

                }).success(function (response) {

                    deferred.resolve(response);

                }).error(deferred.reject);

                return deferred.promise;
            },

            activateAccount: function (request) {
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: CONF.MASTER_PATH + CONF.URL_ACTIVATE_ACCOUNT,
                    timeout: CONF.TIMEOUT,
                    params: {
                        code : request
                    }

                }).success(function (response) {

                    deferred.resolve(response);

                }).error(deferred.reject);

                return deferred.promise;
            },
            changePassword: function (request) {
                var deferred = $q.defer();

                var accessToken = $sessionStorage.accessToken;
                $http({
                    method: 'POST',
                    url: CONF.MASTER_PATH + CONF.URL_PROFILE_CHANGE_PASSWORD,
                    timeout: CONF.TIMEOUT,
                    data: request,
                    params: {
                        access_token: accessToken
                    }
                }).success(function (response) {

                    deferred.resolve(response);

                }).error(deferred.reject);

                return deferred.promise;
            },
            changeProfile: function (request) {
                var deferred = $q.defer();

                var accessToken = localStorageService.cookie.get(CONF.accessToken);
                $http({
                    method: 'PUT',
                    url: CONF.MASTER_PATH + CONF.URL_PROFILE_CHANGE_PROFILE,
                    timeout: CONF.TIMEOUT,
                    data: request,
                    params: {
                        access_token: accessToken
                    }
                }).success(function (response) {
                    if (response.message != 'ERROR') {

                            localStorageService.cookie.remove(CONF.fullName, response.result.fullName);
                            localStorageService.cookie.set(CONF.fullName, request.fullName);
                        }

                    deferred.resolve(response);

                }).error(deferred.reject);

                return deferred.promise;
            },
            changeProfileAdmin: function (request) {
                var deferred = $q.defer();

                var accessToken = localStorageService.cookie.get(CONF.accessToken);
                $http({
                    method: 'PUT',
                    url: CONF.MASTER_PATH + CONF.URL_PROFILE_CHANGE_PROFILE_ADMIN,
                    timeout: CONF.TIMEOUT,
                    data: request,
                    params: {
                        access_token: accessToken
                    }
                }).success(function (response) {

                    deferred.resolve(response);

                }).error(deferred.reject);

                return deferred.promise;
            },
            getProfileAdmin: function () {
                var deferred = $q.defer();

                var accessToken = localStorageService.cookie.get(CONF.accessToken);
                $http({
                    method: 'GET',
                    url: CONF.MASTER_PATH + CONF.URL_PROFILE_ADMIN,
                    timeout: CONF.TIMEOUT,
                    params: {
                        access_token : accessToken
                    }

                }).success(function (response) {

                    deferred.resolve(response);

                }).error(deferred.reject);

                return deferred.promise;
            },
            getProfile: function () {
                var deferred = $q.defer();

                var accessToken = localStorageService.cookie.get(CONF.accessToken);
                $http({
                    method: 'GET',
                    url: CONF.MASTER_PATH + CONF.URL_PROFILE_USER,
                    timeout: CONF.TIMEOUT,
                    params: {
                        access_token : accessToken
                    }

                }).success(function (response) {

                    deferred.resolve(response);

                }).error(deferred.reject);

                return deferred.promise;
            },

            getProfileUserByAdmin: function (id) {
                var deferred = $q.defer();

                var accessToken = localStorageService.cookie.get(CONF.accessToken);
                $http({
                    method: 'GET',
                    url: CONF.MASTER_PATH + CONF.URL_PROFILE_USER + "/" + id,
                    timeout: CONF.TIMEOUT,
                    params: {
                        access_token : accessToken
                    }

                }).success(function (response) {

                    deferred.resolve(response);

                }).error(deferred.reject);

                return deferred.promise;
            },

            updateProfileAdmin: function () {
                var deferred = $q.defer();

                var accessToken = localStorageService.cookie.get(CONF.accessToken);
                $http({
                    method: 'PUT',
                    url: CONF.MASTER_PATH + CONF.URL_PROFILE_USER,
                    timeout: CONF.TIMEOUT,
                    params: {
                        access_token : accessToken
                    }

                }).success(function (response) {

                    deferred.resolve(response);

                }).error(deferred.reject);

                return deferred.promise;
            },

            forgotPassword:function (request) {
                var deferred = $q.defer();
                $http({
                    method: 'POST',
                    url: CONF.MASTER_PATH + CONF.URL_REGISTER_RESET_PASSWORD,
                    timeout: CONF.TIMEOUT,
                    data: request

                }).success(function (response) {

                    deferred.resolve(response);

                }).error(deferred.reject);
                return deferred.promise;
            },
            getListOfAdmin: function (page,limit) {
                var deferred = $q.defer();
                var accessToken = localStorageService.cookie.get(CONF.accessToken);
                $http({
                    method: 'GET',
                    url: CONF.MASTER_PATH + CONF.URL_LIST_ADMIN,
                    timeout: CONF.TIMEOUT,
                    params: {
                        access_token : accessToken,
                        page : page,
                        limit : limit

                    }
                }).success(function (response) {
                    deferred.resolve(response);
                }).error(deferred.reject);
                return deferred.promise;
            },
            getListOfUser: function (page,limit,role) {
                var deferred = $q.defer();
                var accessToken = localStorageService.cookie.get(CONF.accessToken);
                $http({
                    method: 'GET',
                    url: CONF.MASTER_PATH + CONF.URL_LIST_USER,
                    timeout: CONF.TIMEOUT,
                    params: {
                        access_token : accessToken,
                        page : page,
                        limit : limit,
                        role : role,
                        clientId : role

                    }
                }).success(function (response) {
                    deferred.resolve(response);
                }).error(deferred.reject);
                return deferred.promise;
            },
            deactivateUser: function (id) {
                var deferred = $q.defer();
                var accessToken = localStorageService.cookie.get(CONF.accessToken);
                $http({
                    method: 'GET',
                    url: CONF.MASTER_PATH + CONF.URL_DEACTIVATE_USER+id,
                    timeout: CONF.TIMEOUT,
                    params: {
                        access_token : accessToken
                    }
                }).success(function (response) {
                    deferred.resolve(response);
                }).error(deferred.reject);
                return deferred.promise;
            },
            activateUserById: function (id) {
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: CONF.MASTER_PATH + CONF.URL_ACTIVATE_ACCOUNT+"/"+id,
                    timeout: CONF.TIMEOUT,
                    params: {
                        access_token : localStorageService.cookie.get(CONF.accessToken)
                    }

                }).success(function (response) {

                    deferred.resolve(response);

                }).error(deferred.reject);

                return deferred.promise;
            },
            deactivateAdmin: function (id) {
                var deferred = $q.defer();
                var accessToken = localStorageService.cookie.get(CONF.accessToken);
                $http({
                    method: 'GET',
                    url: CONF.MASTER_PATH + CONF.URL_DEACTIVATE_ADMIN+id,
                    timeout: CONF.TIMEOUT,
                    params: {
                        access_token : accessToken
                    }
                }).success(function (response) {
                    deferred.resolve(response);
                }).error(deferred.reject);
                return deferred.promise;
            },
            activateAdminById: function (id) {
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: CONF.MASTER_PATH + CONF.URL_ACTIVATE_ACCOUNT_ADMIN+"/"+id,
                    timeout: CONF.TIMEOUT,
                    params: {
                        access_token : localStorageService.cookie.get(CONF.accessToken)
                    }

                }).success(function (response) {

                    deferred.resolve(response);

                }).error(deferred.reject);

                return deferred.promise;
            },
            getVerifyOwner: function (page,limit) {
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: CONF.MASTER_PATH + CONF.URL_GET_VERIFY_OWNER,
                    timeout: CONF.TIMEOUT,
                    params: {
                        page : page,
                        limit : limit,
                        access_token : localStorageService.cookie.get(CONF.accessToken)
                    }
                }).success(function (response) {
                    deferred.resolve(response);
                }).error(deferred.reject);
                return deferred.promise;
            },
            verifyOwner: function (id,data) {
                var deferred = $q.defer();
                $http({
                    method: 'PUT',
                    url: CONF.MASTER_PATH + CONF.URL_VERIFY_OWNER + "/" + id,
                    timeout: CONF.TIMEOUT,
                    data : data,
                    params: {
                        access_token : localStorageService.cookie.get(CONF.accessToken)
                    }
                }).success(function (response) {
                    deferred.resolve(response);
                }).error(deferred.reject);
                return deferred.promise;
            },

        };

        }]);
