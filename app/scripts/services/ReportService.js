'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('marketplaceApp')
    .service('ReportService', [
        '$http',
        '$q',
        'CONF',
        '$sessionStorage',
        function ($http, $q, CONF, $sessionStorage) {
            return {

                getListReportSales: function(page,limit) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_TRANSACTION,
                        timeout: CONF.TIMEOUT,
                        params:{
                            page:page,
                            limit:limit,
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                /*deleteTax: function (request) {
                    var deferred = $q.defer();

                    $http({
                        method: 'DELETE',
                        headers: {
                            'Content-Type':'application/json'
                        }
                        url: CONF.MASTER_PATH + CONF.URL_DELETE_TAX,
                        timeout: CONF.TIMEOUT,
                        data: request,
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },*/
                
            };
        }]);
