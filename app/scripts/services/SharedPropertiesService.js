'use strict';

/**
 * Created by nostra on 2/8/16.
 */
    angular.module('marketplaceApp')
        .service('sharedPropertiesService',['localStorageService','CONF', function (localStorageService,CONF) {
            //only 2 type toast : normal and error

            var property = {};
            property.searchValue = "";
            property.latitude = {};
            property.longitude = {};
            property.listBilboard = [];
            property.mediaList = [];

            return {
                setSearchValue:function(value){
                    property.searchValue=value;
                },
                getSearchValue:function(){
                  return property.searchValue;
                },

                setLatitudeMapsValue:function(value){
                    property.latitude=value;
                },

                getLatitudeMapsValue:function(){
                	return property.latitude;
                },

                setLongitudeMapsValue:function(value){
                	property.longitude=value;
                },

                getLongitudeMapsValue:function(){
                	return property.longitude;
                },
                setListBillboard:function(value){
                    property.listBilboard=value;
                },
                getListBillboard:function() {
                    return property.listBilboard;
                },
                setMediaList:function(value){
                    property.mediaList=value;
                },

                getMediaList:function(){
                    return property.mediaList;
                }

            };
     }]);