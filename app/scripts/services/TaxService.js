'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('nostraApp')
    .service('TaxService', [
        '$http',
        '$q',
        'CONF',
        'localStorageService',
        function ($http, $q, CONF, localStorageService) {

            return {
                getlistOfTax: function (page,limit) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_OF_TAX,
                        timeout: CONF.TIMEOUT,
                        params: {
                            page : page,
                            limit : limit
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }, 
                createTax: function (contentData) {
                    var deferred = $q.defer();

                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CREATE_TAX,
                        timeout: CONF.TIMEOUT,
                        data: contentData,

                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                deleteTax: function (request) {
                    var deferred = $q.defer();

                    $http({
                        method: 'DELETE',
                        headers: {
                            'Content-Type':'application/json'
                        },
                        url: CONF.MASTER_PATH + CONF.URL_DELETE_TAX,
                        timeout: CONF.TIMEOUT,
                        data: request,
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }
                
            };
        }]);
