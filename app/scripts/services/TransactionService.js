'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('marketplaceApp')
    .service('TransactionService', [
        '$http',
        '$q',
        '$sessionStorage',
        'localStorageService',
        'CONF',
        function($http, $q, $sessionStorage, localStorageService, CONF) {
            return {
                getListTranscaction: function(page,limit) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_TRANSACTION,
                        timeout: CONF.TIMEOUT,
                        params:{
                            page:page,
                            limit:limit,
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getDetailTransaction: function (id) {
                    var deferred = $q.defer();
                    var accessToken = localStorageService.cookie.get(CONF.accessToken);

                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_DETAIL_TRANSACTION,
                        timeout: CONF.TIMEOUT,
                        params: {
                            order_code : id,
                            token: accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                approveTransaction: function (request) {
                    var deferred = $q.defer();

                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_APPROVE_TRANSACTION,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params : {
                            token :access_token
                        }


                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }
            };
        }
    ]);