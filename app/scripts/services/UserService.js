'use strict';

/**
 * @ngdoc service
 * @name nostraApp.auth
 * @description
 * # auth
 * Service in the nostraApp.
 */
angular.module('marketplaceApp')
    .service('UserService', [
        '$http',
        '$q',
        '$sessionStorage',
        'localStorageService',
        'CONF',
        function($http, $q, $sessionStorage, localStorageService, CONF) {
            return {
                getListBank: function() {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_LIST_BANK,
                        timeout: CONF.TIMEOUT,
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },
                getListUserProdusen: function(page,limit) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_PRODUSEN,
                        timeout: CONF.TIMEOUT,
                        params:{
                            page:page,
                            limit:limit,
                            cat:'produsen',
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },
                getDetailUserProfile: function(id) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_DETAIL_PROFILE + '/' + id,
                        timeout: CONF.TIMEOUT,
                        params:{
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },
                getListUserReseller: function(page,limit) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_LIST_RESELLER,
                        timeout: CONF.TIMEOUT,
                        params:{
                            page:page,
                            limit:limit,
                            cat:'reseller',
                            token: access_token
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                activation: function(id) {
                    var deferred = $q.defer();
                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_ACTIVATION_ACCOUNT,
                        timeout: CONF.TIMEOUT,
                        params:{
                            id:id
                        },
                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                createUser: function(request) {
                    var deferred = $q.defer();
                    var accessToken = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_CREATE_USER,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params:{
                            token:accessToken
                        }

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                registerUser: function(request) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_REGISTER_USER_NEW,
                        timeout: CONF.TIMEOUTFILE,
                        data: request

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                forgotPassword: function(request) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_FORGOT_PASSWORD,
                        timeout: CONF.TIMEOUT,
                        data: request

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                getDetailProfile: function (id) {
                    var deferred = $q.defer();
                    var accessToken = localStorageService.cookie.get(CONF.accessToken);

                    $http({
                        method: 'GET',
                        url: CONF.MASTER_PATH + CONF.URL_GET_PROFILE,
                        timeout: CONF.TIMEOUTFILE,
                        params: {
                            id : id,
                            token: accessToken
                        }
                    }).success(function (response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                updateSite: function(fd) {
                    var deferred = $q.defer();
                    var accessToken = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_UPDATE_SITE,
                        timeout: CONF.TIMEOUTFILE,
                        data: fd,
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined },
                        params: {
                            token: accessToken
                        }

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                updateProfile: function(fd) {
                    var deferred = $q.defer();
                    var accessToken = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'POST',
                        url: CONF.MASTER_PATH + CONF.URL_UPDATE_PROFILE,
                        timeout: CONF.TIMEOUTFILE,
                        data: fd,
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined },
                        params: {
                            token: accessToken
                        }

                    }).success(function(response) {

                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                },

                deleteUser: function (request) {
                    var deferred = $q.defer();
                    var access_token = localStorageService.cookie.get(CONF.accessToken);
                    $http({
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        url: CONF.MASTER_PATH + CONF.URL_DELETE_USER,
                        timeout: CONF.TIMEOUT,
                        data: request,
                        params:{
                            token: access_token
                        }
                    }).success(function (response) {
                        deferred.resolve(response);

                    }).error(deferred.reject);

                    return deferred.promise;
                }
            };
        }
    ]);